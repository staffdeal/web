<?php

error_reporting(E_ALL);
ini_set('display_errors', true);

date_default_timezone_set('Europe/Berlin');

$rootDir = dirname(dirname(__FILE__));

set_include_path(
	'C:/wamp/bin/ZendFramework-1.12.5/library' . PATH_SEPARATOR .
    $rootDir . '/application/library/' . PATH_SEPARATOR .
    $rootDir . '/application/modules/admin/library' . PATH_SEPARATOR .
    $rootDir . '/application/modules/default/library' . PATH_SEPARATOR .
    get_include_path());


define('ROOT_PATH', $rootDir);
define('APPLICATION_PATH', $rootDir . '/application/');

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));


include(APPLICATION_PATH . '/library/Geocoder/src/autoload.php');

require_once 'Zend/Loader/Autoloader.php';
$loader = Zend_Loader_Autoloader::getInstance();
$loader->setFallbackAutoloader(true);


require_once 'Epr/System.php';
require_once 'Poundation/Poundation.php';

function __($text) {
    return \Poundation\__($text);
}

Zend_Registry::set('rootDir', $rootDir);

$runtimeTypeEnv = getenv('APPLICATION_ENV');
if ($runtimeTypeEnv === false) {
    $runtimeTypeEnv = Epr_Application::RUNTIMETYPE_PRODUCTION;
}

// overriding env

if (file_exists($rootDir . '/testing.txt')) {
	$runtimeTypeEnv = Epr_Application::RUNTIMETYPE_TESTING;
}
if (file_exists($rootDir . '/development.txt')) {
	$runtimeTypeEnv = Epr_Application::RUNTIMETYPE_DEVELOPMENT;
}

if (!defined('APPLICATION_ENV')) {
    define('APPLICATION_ENV', $runtimeTypeEnv);
}

define('APPLICATION_MODE', $runtimeTypeEnv);

// Create application, bootstrap
$application = new Zend_Application(
	APPLICATION_MODE,
    APPLICATION_PATH . 'config/application.ini'
);

$application->bootstrap();

//$sesSystem = new Zend_Session_Namespace('sesSystem');

/*
 * Set Layout here
 */


$viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();

$layoutPath = APPLICATION_PATH . 'views/layouts';
$scriptPath = APPLICATION_PATH . 'views/scripts';

define('DEFAULT_SCRIPT_PATH', $scriptPath);
define('DEFAULT_LAYOUT_PATH', $layoutPath);


$layout = new Zend_Layout(array('layoutPath' => $layoutPath));

$view = $layout->getView();

$view->addHelperPath('./library/Zend/View/Helper');
$view->addHelperPath('Epr/View/Helper', 'Epr_View_Helper');
$view->addBasePath(APPLICATION_PATH . 'modules/admin/views/placeholder/');
$view->addBasePath(APPLICATION_PATH . 'modules/admin/views/partials/');

$viewRenderer->setView($view);

Zend_Registry::getInstance()->set('view', $view);

Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
Zend_Layout::startMvc(
    array('layoutPath' => $layoutPath,
        'layout' => 'layout'
    )
);

$application->run();