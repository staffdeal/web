<?php

date_default_timezone_set('Europe/Berlin');

$rootDir = dirname(dirname(__FILE__));

set_include_path(
	$rootDir . '/application/library/' . PATH_SEPARATOR .
		$rootDir . '/application/modules/admin/library' . PATH_SEPARATOR .
		$rootDir . '/application/modules/default/library' . PATH_SEPARATOR .
		get_include_path());


define('ROOT_PATH', $rootDir);
define('APPLICATION_PATH', $rootDir . '/application/');

// Define application environment
defined('APPLICATION_ENV')
	|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));


require_once 'Zend/Loader/Autoloader.php';
$loader = Zend_Loader_Autoloader::getInstance();
$loader->setFallbackAutoloader(true);


require_once 'Epr/System.php';
require_once 'Poundation/Poundation.php';


$feed = Epr_Feed::createFeedWithURL(\Poundation\PURL::URLWithString('http://heise.de.feedsportal.com/c/35207/f/653902/index.rss'));
if ($feed->read(1)) {

	foreach ($feed->getEntries() as $entry) {
		if ($entry instanceof Epr_Feed_Item) {
			$images = $entry->getImages();
		}
	}

}

//$html = new \Poundation\PString($raw);
//
//$fragment = new \Poundation\XML_PFragment($html);
//
//echo $html;