$(document).ready(function () {
    window.setTimeout(function () {
        adjustErrorMessage();
    },1000);
});

$(window).resize(function () {
    adjustErrorMessage();
});

var adjustErrorMessage = function () {
    $('div.error').each(function () {
        var errorDiv = $(this);
        var errorField = errorDiv.attr('error-data-field');
        if (errorField) {
            errorDiv.show();

            var wrapperId = 'wrapper_' + errorField;
            var outlineElement = $('#' + wrapperId + ' label');
            if (outlineElement.length > 0) {

                var position = outlineElement.offset();
                var width = outlineElement.width();
                if (position.top) {
                    var yOffset = position.top - 2;
                    errorDiv.css('top', yOffset + 'px');
                }
                if (position.left && width) {
                    var xOffset = position.left + width;
                    errorDiv.css('left', xOffset + 'px');
                }
            }

        }
    });
};

var initEditors = function (selector) {

    var doSelector = (typeof selector == 'undefined') ? 'textarea.wysiwyg' : selector;
    $(doSelector).each(function (key, val) {
        initEditor($(val));
    });

}

var initEditor = function (textarea, enableImageBrowser, enableImageList) {

    var args = arguments;

    if (textarea.tinymce) {

        var imageContext = 1;

        if (typeof wysiwygContext != 'undefined') {
            var id = textarea.attr('id');
            if (typeof wysiwygContext[id] != 'undefined') {
               var imageContext = wysiwygContext[id];
            }
        }

        var doShowImageBrowser = (typeof enableImageBrowser == 'undefined') ? true : (enableImageBrowser !== false);
        var doShowImageList = (typeof enableImageList == 'undefined') ? true : (enableImageList !== false);

        var options = {
            // Location of TinyMCE script
            script_url: 'tiny_mce.js',
            document_base_url: 'http://www.ensureAbsolutePathes.forTheEditors.imageBrowser.com',


            // General options
            plugins: "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

            // Theme options
            theme_advanced_buttons1: "undo,redo,|,cut,copy,paste,pastetext,pasteword,|,print,fullscreen,|,visualaid,|,code,help",
            theme_advanced_buttons2: "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,|,forecolor,backcolor,|,bullist,numlist,|,outdent,indent,blockquote,|,sub,sup",
            theme_advanced_buttons3: ",imageBrowser,image,|,link,unlink,anchor,|,tablecontrols,|,hr,removeformat,|,charmap,emotions,iespell,media,advhr",
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            theme_advanced_resizing: true

        };

        if (doShowImageBrowser) {
            options['setup'] = function (ed) {
                // Register example button
                ed.addButton('imageBrowser', {
                    title: 'Starts the image Browser',
                    image: '/images/icons/browser.gif',
                    class: 'mce_image',
                    onclick: function () {
                        showImageBrowser({
                            tinymce: textarea,
                            multiple: true,
                            context: imageContext
                        });
                    }
                });
            };
        }

        if (doShowImageList) {
            options['external_image_list_url'] = '/admin/ajax/image-list-editor';
        }

        textarea.tinymce(options);
    }
}

var bindDateRangeFields = function (startId, endId, fullId) {
    $(document).ready(function () {

        var range = {
            start: null,
            end: null
        }

        var startDateValue = $('#' + startId).val();
        var endDateValue = $('#' + endId).val();

        if (startDateValue) {
            var startDate = new Date(startDateValue);
            if (startDate) {
                range.start = startDate;
            }
        }

        if (endDateValue) {
            var endDate = new Date(endDateValue);
            if (endDate) {
                range.end = endDate;
            }
        }

        if (!range.start) {
            $('#' + startId).val('');
        }

        if (!range.end) {
            $('#' + startId).val('');
        }


        var updateFullField = function () {
            $('#' + fullId).val(JSON.stringify(range));
        }

		$('#' + startId).datepicker("option","onSelect", function (dateText) {
			var startDate = new Date(dateText);
			range.start = startDate;
			updateFullField();
		});
		$('#' + endId).datepicker("option","onSelect", function (dateText) {
			var endDate = new Date(dateText);
			range.end = endDate;
			updateFullField();
		});

        updateFullField();

    });
}
