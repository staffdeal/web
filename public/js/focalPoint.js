"use strict";


var Focal = function (imageElement, xElement, yElement) {

    var that = this;

    that.imageElement = imageElement;
    that.xElement = xElement;
    that.yElement = yElement;

    that.x = -1.0;
    that.y = -1.0;

    that.indicator = null;

    this.init = function() {
        if (that.imageElement && that.xElement && that.yElement) {
            that.indicator = jQuery('<div class="focal indicator"></div>');
            that.imageElement.parent().append(that.indicator);

            that.x = parseFloat(that.xElement.val());
            that.y = parseFloat(that.yElement.val());

            that.updateIndicator();

            that.imageElement.on('click', that.handleMouseDown);
        }
    };

    this.updateIndicator = function() {
        if (that.x >= 0.0 && that.y >= 0.0) {

            var imageWidth = that.imageElement.width();
            var imageHeight = that.imageElement.height();
            var imageOffsetX = that.imageElement.position().left;
            var imageOffsetY = that.imageElement.position().top;
            var indicatorWidth = that.indicator.width();
            var indicatorHeight = that.indicator.height();

            var x = (imageWidth * that.x) - (indicatorWidth / 2.0) - 3.0 + imageOffsetX; // 3.0 is for the border
            var y = (imageHeight * that.y) - (indicatorHeight / 2.0) - 3.0 + imageOffsetY;

            that.indicator.css('left', x + 'px');
            that.indicator.css('top', y + 'px');
        }
    };

    this.updateFields = function() {
        if (that.xElement) {
            xElement.val(that.x);
        }
        if (that.yElement) {
            yElement.val(that.y);
        }
    }

    this.handleMouseDown = function (event) {
        if (event) {
            var mouseX = (event.offsetX || event.clientX - that.imageElement.offset().left);
            var mouseY = (event.offsetY || event.clientY - that.imageElement.offset().top);

            if (0 <= mouseX && 0 <= mouseY) {
                var imageWidth = that.imageElement.width();
                var imageHeight = that.imageElement.height();

                that.x = (mouseX / imageWidth).toFixed(3);
                that.y = (mouseY / imageHeight).toFixed(3);

                that.updateIndicator();
                that.updateFields();
            }
        }
    };

    this.init();
};

