var linkControlButtonsToDataField = function (removeButtonId, dataFieldId) {
	if (removeButtonId && dataFieldId) {
		$(document).ready(function () {
			var removeButton = $('#' + removeButtonId);
			var dataField = $('input#' + dataFieldId);

			if (removeButton && dataField) {
				setRemoteButtonStateFromDataField(removeButton, dataField);
				dataField.change(function () {
					setRemoteButtonStateFromDataField(removeButton, dataField);
				});
				removeButton.bind('click', function (event) {
					event.preventDefault();
					dataField.val('');
					dataField.trigger('change');
				});
			}

		});
		return true;
	}

	return false;
}

var linkImageThumbnailToDataField = function (imageId, dataFieldId, autoResize) {
	if (imageId && dataFieldId) {

		var doAutoResize = (autoResize == true);

		$(document).ready(function () {

			var image = $('#' + imageId);
			var dataField = $('input#' + dataFieldId);

			if (image && dataField) {
				setThumbnailImageFromDataField(imageId, dataField, doAutoResize);
				dataField.change(function () {
					setThumbnailImageFromDataField(imageId, dataField, doAutoResize);
				});
			}
		});
	}

	return false;
}

var linkDevicePreviewImagesToDataField = function (previewImages, dataFieldId, containerId) {

	$(document).ready(function () {

		var previewContainer = $('#' + containerId);
		var dataField = $('input#' + dataFieldId);
		if (previewContainer && dataField) {
			adjustDevicePreviewVisibility(previewContainer, dataField);
			dataField.change(function () {
				adjustDevicePreviewVisibility(previewContainer, dataField);

			});
		}

	});

	previewImages.forEach(function (devicePreviewImageId) {
		linkImageThumbnailToDataField(devicePreviewImageId, dataFieldId);
	});

	return false;

}

var adjustDevicePreviewVisibility = function (devicePreview, dataField) {
	if (devicePreview && dataField) {
		var imageIds = imageIdsFromDatafield(dataField);
		if ($.isArray(imageIds) && imageIds.length > 0) {
			devicePreview.show();
		} else {
			devicePreview.hide();
		}
	}
}

var setRemoteButtonStateFromDataField = function (button, datafield) {
	if (button && datafield) {
		var elementHasValue = false;
		var elementValue = datafield.val();
		if (elementValue) {
			var parsedValue = $.parseJSON(elementValue);
			if ($.isArray(parsedValue)) {
				if (parsedValue.length > 0) {
					elementHasValue = true;
				}
			}
		}

		if (elementHasValue) {
			button.show();
		} else {
			button.hide();
		}
	}
}

var setThumbnailImageFromDataField = function (imageId, datafield, autoResize) {

	var doAutoResize = (autoResize == true);

	var image = $('#' + imageId);
	if (image && datafield) {
		var firstImageId = null;
		var imageIds = imageIdsFromDatafield(datafield);
		if ($.isArray(imageIds) && imageIds.length > 0) {
			firstImageId = imageIds[0];
		}

		var width = image.width();
		var height = image.height();

		var useDummy = (!firstImageId);

		var imageName = '';

		if (useDummy) {
			imageName+= 'dummy';
		} else {
			imageName+= firstImageId;
		}

		var imageURL = '/files/images/' + imageName;
		if (!doAutoResize) {
			imageURL = imageURL + '-' + width + 'x' + height;
		}

		if (useDummy) {
			imageURL+= '.jpg';
		} else if (forceImageRefresh) {
            imageURL += '?' + (new Date()).getTime();
        }

        image.attr('src', imageURL);
	}
}

var imageIdsFromDatafield = function (datafield) {

	var elementValue = datafield.val();
	if (elementValue) {
		var parsedValue = $.parseJSON(elementValue);
		if ($.isArray(parsedValue)) {
			return parsedValue;
		}
	}
	return null;
}