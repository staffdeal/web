"use strict";

var SORT_DESC = 'desc';
var SORT_ASC = 'asc';

var imageEditorVisible = false;
var imageUploadEnabled = false;
var imageSizeBig = true;
var maxSize = 1024 * 1024 * 2;
var imageMultipleSelect = false;
var imageDataField = null;

var originalImageData = null;

var imageContext = null;

var imageBrowserTinyMCE = null;

var selectedImages = new Array();
var selectedImagesURLs = new Array();

var sortField = 'name';
var sortDirection = SORT_ASC;
var currentOffset = 0;
var countPerPage = 10;

var wasUpload = false;

var imageQuery = null;

var forceImageRefresh = false;

Array.prototype.remove = function (from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
};

$(document).ready(function () {
    $('#imageBrowserCancelButton').bind('click', function (event) {
        if (imageEditorVisible) {
            if (imageDataField) {
                var newRawData = JSON.stringify(selectedImages);
                if (newRawData != originalImageData) {
                    imageDataField.val(originalImageData);
                    imageDataField.trigger('change');
                }
            }
            $('#imageBrowserOverlay').hide();
            $('#imageBrowserContainer').hide();
            $('#imageBrowserContent').empty();
            $('body').css('overflow', 'auto');
            imageBrowserTinyMCE = null;
            imageEditorVisible = false;
        }
    });
    $('#imageBrowserOverlay').bind('click', function (event) {
        if (imageEditorVisible) {
            writeSelectedImagesToTarget();
            $('#imageBrowserOverlay').hide();
            $('#imageBrowserContainer').hide();
            $('#imageBrowserContent').empty();
            $('body').css('overflow', 'auto');
            imageBrowserTinyMCE = null;
            imageEditorVisible = false;
            forceImageRefresh = false;
        }
    });
});

var showImageBrowserAjaxLoader = function () {
    $('#imageBrowserAjaxLoader').show();
};

var hideImageBrowserAjaxLoader = function () {
    $('#imageBrowserAjaxLoader').hide();
};

var showImageBrowser = function (options) {
    if (imageEditorVisible == false) {
        imageEditorVisible = true;

        selectedImages = new Array();
        selectedImagesURLs = new Array();

        imageUploadEnabled = (options['enableUploads'] != false);
        imageSizeBig = false;
        if (typeof options['maxSize'] == 'number') {
            maxSize = options['maxSize'];
        }
        imageMultipleSelect = (options['multiple'] == true);

        imageDataField = null;
        var imageDataFieldName = options['dataField'];
        if (imageDataFieldName) {
            imageDataField = $('input[name="' + imageDataFieldName + '"]').first();
            if (imageDataField) {
                var selectedImageRawData = imageDataField.val();
                originalImageData = selectedImageRawData;
                var selectedImageData = [];
                if (selectedImageRawData && selectedImageRawData.length > 1) {
                     selectedImageData = $.parseJSON(selectedImageRawData);
                }
                if ($.isArray(selectedImageData)) {
                    selectedImages = selectedImageData;
                }
            }
        }

        imageContext = (options['context']) ? options['context'] : null;

        if (!imageContext) {
            alert('Cannot load any images since the context is not setup properly.');
            return;
        }
        $('#ajaxImageUploadForm').attr('action','/admin/ajax/image-upload?context=' + imageContext);

        imageBrowserTinyMCE = null;

        if (imageDataFieldName == null) {
            var tinymce = options['tinymce'];
            if (tinymce.tinymce) {
                imageBrowserTinyMCE = tinymce;
            }
        }

        $('#imageBrowserOverlay').show();

        var offsetY = $(window).scrollTop() + 100;
        $('#imageBrowserContainer').css('top', offsetY + 'px').show();

        $('#imageBrowserUpload').hide();
        $('#ajaxImageCancelUploadButton').hide();
        $('#imageBrowserShowUpload').show();
        if (imageUploadEnabled) {
            $('#imageBrowserShowUpload').click(showUpload);
            $('#ajaxImageCancelUploadButton').click(cancelUpload);
        }

        disableImageUploadButton();

        $('body').css('overflow', 'hidden');

        loadImagesData();

        $('#ajaxImageUploadFile').bind('change', function (event) {
            var hadError = false;
            $.each(this.files, function (key, file) {
                if (file) {
                    if (file.size > maxSize) {
                        alert('The size of the selected file (' + file.name + ') exceeds the allowed maximum of ' + maxSize / 1024 + ' kb.');
                        hadError = true;
                    }
                }
            });
            if (hadError) {
                resetSelectedUploadFiles();
                disableImageUploadButton();
            } else {
                enableImageUploadButton();
            }
        });

        $('#ajaxImageUploadForm').ajaxForm({
            beforeSend: function () {
                $('ajaxUploadProgress').css('width', '0%');
                $('#ajaxImageUploadForm').hide();
                $('#ajaxUploadProgressContainer').show();
            },
            uploadProgress: function (event, position, total, percentComplete) {
                var width = percentComplete + '%'
                $('#ajaxUploadProgress').css('width', width);
            },
            complete: function (xhr) {
                $('#ajaxUploadProgressContainer').hide();
                $('#ajaxImageUploadForm').show();

				var rawResponse = (xhr.responseText) ? xhr.responseText : xhr.response;

				if (xhr.status == 200 && rawResponse) {
                    var response = $.parseJSON(rawResponse);
                    if (response.status == 'OK') {

                        resetSelectedUploadFiles();

                        if (!wasUpload) {
                            wasUpload = true;
                            $('#imageBrowserContent').empty();
                        }
                        var newImages = response['files'];
                        if (newImages) {
                            $.each(newImages, function (key, val) {
                                if (key && val.url) {
                                    addImageItem(key, val.url, val.name, val.width, val.height, val.creation);
                                }
                            });
                        }

                    } else if (response.status == 'error') {
                        var message = response.message;
                        if (message) {
                            alert(message);
                            resetSelectedUploadFiles();
                        } else {
                            alert('Something went wrong while uploading the images. Please try again.');
                        }
                    } else {
                        alert('Something went wrong while uploading the images. Please try again.');
                    }
                } else {
                    alert('Something went wrong while uploading the images. Please try again.');
                }
            }
        });

    }
};

var showUpload = function () {
    $('#imageBrowserUpload').show();
    $('#ajaxImageCancelUploadButton').show();
    $('#imageBrowserShowUpload').hide();
};

var cancelUpload = function () {
    $('#imageBrowserUpload').hide();
    $('#ajaxImageCancelUploadButton').hide();
    $('#imageBrowserShowUpload').show();
};

var resetSelectedUploadFiles = function () {
    $('input[type=file]').wrap('<form></form>').parent().trigger('reset').children().unwrap('<form></form>');
    cancelUpload();
};

var disableImageUploadButton = function () {
    $('#ajaxImageUploadButton').attr('disabled', 'disabled');
};

var enableImageUploadButton = function () {
    $('#ajaxImageUploadButton').removeAttr('disabled');
};


var loadImagesData = function (offsetFactor) {

    $('#imageBrowserContent').empty();

    showImageBrowserAjaxLoader();

    countPerPage = numberOfRowsFiting();

    if (typeof offsetFactor == 'undefined') {
        offsetFactor = 0;
    }

    offsetFactor = Math.min(Math.max(-1, Math.round(offsetFactor, 0)), 1);

    var offset = currentOffset + (countPerPage * offsetFactor);

    var parameters = {
        sortKey: sortField,
        sortDirection: sortDirection,
        count: countPerPage,
        offset: offset,
        context: imageContext,
        q: imageQuery
    };

    var url = '/admin/ajax/image-list?' + jQuery.param(parameters);

    $.ajax({
        url: url,
        dataType: 'json',
        success: function (response) {
            wasUpload = false;
            hideImageBrowserAjaxLoader();
            showImageFromResponse(response);
        },
        error: function (request, status, error) {
            hideImageBrowserAjaxLoader();
            alert(error);
        }
    });
};

var numberOfRowsFiting = function () {
    "use strict";

    var containerHeight = jQuery('#imageBrowserContent').height();
    if (containerHeight) {
        var numberOfElements = containerHeight / 70;
        return Math.floor(numberOfElements) - 1;
    }

    return 10;
};

var showImageFromResponse = function (response) {

    var imageEditorContentDiv = $('#imageBrowserContent');

    if (response.images) {

        if (imageEditorContentDiv) {
            $.each(response.images, function (key, val) {
                if (key && val.url) {
                    addImageItem(key, val.url, val.name, val.width, val.height, val.creation);
                }
            });
        }


        if (response.navigation) {
            var navigation = response.navigation;
            jQuery('#imageBrowserNavigationButtonPrevious').prop("disabled", !navigation.hasPrevious);
            jQuery('#imageBrowserNavigationButtonNext').prop("disabled", !navigation.hasNext);
            currentOffset = navigation.offset;
            jQuery('td.paginationInfo').text('#' + (navigation.offset + 1) + ' - #' + (navigation.offset + navigation.count) + ' of ' + navigation.total);
        }
    }

    cancelUpload();
};

var addImageItem = function (id, url, name, width, height, creation) {

    if (id && url && name) {

        var imageEditorContentDiv = $('#imageBrowserContent');

        var selectedIndex = $.inArray(id, selectedImages);
        var selected = (selectedIndex > -1);

        if (selected) {
            selectedImagesURLs.push(url);
        }

        var selectedClass = (selected) ? 'active' : '';

        var sizeClass = (imageSizeBig) ? 'size150' : 'size100';

        var sizeInfo = width + ' x ' + height + ' Px';

        var elementHTML = '<div class="imageBrowserItem ' + selectedClass + ' ' + sizeClass + '" id="' + id + '">';
        elementHTML += '<img src="' + url + '?height=150&width=150" />';
        elementHTML += '<a href="/admin/assets/edit/id/' + id + '" class="editImage" target="_blank" onclick="forceImageRefresh = true;"></a>';
        elementHTML += '<div class="fileinfo">';
        elementHTML += '<span class="sizeinfo pull-left">' + sizeInfo + '</span>';
        if (creation) {
            elementHTML += '<span class="sizeinfo pull-right">' + creation + '</span>';
        }

        elementHTML += '<span class="filename">' + name + '</span>';
        elementHTML += '</div></div>';

        imageEditorContentDiv.append(elementHTML);

        var index = 0;
        var urlIndex = 0;
        $('#' + id + ' div.fileinfo').bind('click', function (event) {
            event.preventDefault();

            var row = $('#' + id);

            var active = row.hasClass('active');
            if (active) {
                row.removeClass('active');
                index = $.inArray(id, selectedImages);
                if (index >= 0) {
                    selectedImages.remove(index, index);
                }

                urlIndex = $.inArray(url, selectedImagesURLs);
                if (urlIndex >= 0) {
                    selectedImagesURLs.remove(urlIndex, urlIndex);
                }

            } else {
                if (imageMultipleSelect) {
                    index = $.inArray(id, selectedImages);
                    if (index == -1) {
                        selectedImages.push(id);
                    }

                    urlIndex = $.inArray(url, selectedImagesURLs);
                    if (urlIndex == -1) {
                        selectedImagesURLs.push(url);
                    }

                } else {
                    selectedImages = new Array();
                    selectedImages.push(id);
                    selectedImagesURLs = new Array();
                    selectedImagesURLs.push(url);
                    $('.imageBrowserItem').removeClass('active');
                }
                row.addClass('active');
                if (imageDataField) {
                    writeSelectedImagesToTarget();
                }
            }
        });

        $('#' + id + ' span').ellipsis();

    }
};


var writeSelectedImagesToTarget = function () {
    if (imageDataField) {
        imageDataField.val(JSON.stringify(selectedImages));
        imageDataField.trigger('change');
    } else if (imageBrowserTinyMCE.tinymce) {

        $.each(selectedImagesURLs, function (key, url) {
            var imgHTML = '<img src="' + url + '" />';
            imageBrowserTinyMCE.tinymce().execCommand('mceInsertContent', false, imgHTML);
        });

    }
};


var changeSortingToField = function(fieldname) {
    if (fieldname == sortField) {
        toggleSortingDirection();
    } else {
        sortField = fieldname;
        sortDirection = SORT_ASC;
    }
    loadImagesData();
};

var toggleSortingDirection = function () {
    if (sortDirection == SORT_ASC) {
        sortDirection = SORT_DESC;
    } else {
        sortDirection = SORT_ASC;
    }
};

var searchForImage = function (event) {
    "use strict";
    if (!event) {
        event = window.event;
    }
    var keyCode = event.which || event.keyCode;
    if (keyCode == 13) {
        currentOffset = 0;
        countPerPage = 10;
        loadImagesData();
    }
    imageQuery = document.getElementById('image-q').value;

    return false;
};

var loadNext = function () {
    "use strict";
    loadImagesData(1);
};

var loadPrevious = function() {
    "use strict";
    loadImagesData(-1);
};