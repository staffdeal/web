# meinEPR Web Portal

## Requirements

- PHP >= 5.3
- CouchDB >= 1.6.0
- Zend Framework 1
- Support for .htaccess files
- mod_rewrite

## Installation

- Create a directory and check out the repository.
- Create a virtual host with a domain name containing a subdomain, e.g. meinepr.mydomain.local. The document root is the folder "public".
- Add the environment setting to the vhost configuration:
    
    SetEnv APPLICATION_ENV development

- Add the ZF path to the include_path of your PHP installation.
- Copy and edit the config file (application/config/client.ini.dist) (see next paragraph).

## Configuration

You need to copy and edit the configuration file to one of the following paths:

- application/config/client.ini (do not stage and commit it)
- a path of your choice. You need to add an environment directive to your vhost definition, e.g. 

    SetEnv CLIENT_CONFIG_PATH /etc/meinEPR/clients/

The name of the file must match the subdomain you're using. If no file matching your subdomain is found, the name "client" is used.

In the config file, you need to adjust the values to your needs. Please notice the three paths, that need to exist and be writeable.