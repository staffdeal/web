<?php

use Poundation\PMailAddress;
use Poundation\Server\PRequest;

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    private $_databaseProxyPath = null;

    protected function _bootstrap($resource = null)
    {
        try {
            parent::_bootstrap($resource);
        } catch (Exception $e) {
            echo $e->getMessage();

            parent::_bootstrap('frontController');
            $front = $this->getResource('frontController');
            $front->registerPlugin(new Plugins_BootstrapError($e));
        }
    }

    protected function _initErrorHandling()
    {
        $front = Zend_Controller_Front::getInstance();
        if ($front) {
            $errorPlugin = new Zend_Controller_Plugin_ErrorHandler();
            $errorPlugin->setErrorHandlerModule('default');
            $errorPlugin->setErrorHandlerController('error');
            $errorPlugin->setErrorHandlerAction('error');
            $front->registerPlugin($errorPlugin, 100);
        } else {
            die("Cannot load front controller…dying");
        }
    }

    protected function _initTimezone()
    {
        $timezone = date_default_timezone_get();
        if (!$timezone) {
            date_default_timezone_set('Europe/Berlin');
        }
    }

    protected function _initPlugins()
    {
        $front = Zend_Controller_Front::getInstance();

        $front->registerPlugin(new Epr_Controller_Plugin_ErrorControllerSwitcher());
        $front->registerPlugin(new Plugins_Translate());
        $front->registerPlugin(new Plugins_Frontend_Page());

    }

    /**
     * Initializing the Config
     *
     * @return type
     */
    protected function _initConfig()
    {
        $config = $this->getOptions();
        Zend_Registry::set('config', $config);

        return $config;
    }

    /**
     * Initializing the Error Handling
     */
    protected function _initErrorOutput()
    {
        $config = $this->getOptions();
        error_reporting(E_ALL | E_STRICT);
        ini_set('display_errors', $config['phpSettings']['display_errors']);
    }


    protected function _initClientConfig()
    {

        $configFilename = null;
        $checkedFiles   = array();

        $configPath = APPLICATION_PATH . '../config/';

        if ($configFilename == null) {
            $generalConfig = $configPath . '/client.ini';
            if (file_exists($generalConfig)) {
                $configFilename = $generalConfig;
            } else {
                $checkedFiles[] = $generalConfig;
            }
        }

        if ($configFilename == null) {
            $request   = PRequest::request();
            $subdomain = $request->getURL()->subdomains()->string('.');
            if ($subdomain->length() == 0) {
                $subdomain = 'www';
            }
            $subdomainConfig = $configPath . '/' . $subdomain . '.ini';
            if (file_exists($subdomainConfig)) {
                $configFilename = $subdomainConfig;
            } else {
                $checkedFiles[] = $subdomainConfig;
            }
        }


        if ($configFilename == null && getenv('CLIENT_CONFIG_PATH')) {

            $envPath = __(getenv('CLIENT_CONFIG_PATH'));
            if (!$envPath->hasPrefix('/')) {
                $envPath = ROOT_PATH . '/' . $envPath;
            }

            $clientsConfigDirectory = realpath($envPath);

            if ($clientsConfigDirectory === false) {
                throw new Exception('Config path (env) "' . $envPath . '" does not exist.');
            }

            $subdomainConfig = $clientsConfigDirectory . '/' . $subdomain . '.ini';
            if (file_exists($subdomainConfig)) {
                $configFilename = $subdomainConfig;
            } else {
                $checkedFiles[] = $subdomainConfig;
            }

            if ($configFilename == null) {
                $generalConfig = $clientsConfigDirectory . '/client.ini';
                if (file_exists($generalConfig)) {
                    $configFilename = $generalConfig;
                } else {
                    $checkedFiles[] = $generalConfig;
                }
            }
        }

        if ($configFilename == null) {
            $checkedFiles = array_unique($checkedFiles);
            throw new Exception("Could not load client's config file in these paths: " . implode(', ', $checkedFiles));
        } else {
            $clientsConfig = new Zend_Config_Ini($configFilename);
            Zend_Registry::set('client', $clientsConfig);

            if (isset($clientsConfig->info->mail)) {
                $mail = PMailAddress::createFromString($clientsConfig->info->mail);
                if ($mail->verify()) {
                    Zend_Registry::set('adminMail', $mail);
                } else {
                    throw new Exception("Client's general mail address (" . $clientsConfig->info->mail . ") invalid.");
                }
            } else {
                throw new Exception("No general mail address in client's config.");
            }

            if (!isset($clientsConfig->database->proxiesPath)) {
                throw new Exception('No database proxy path in client config.');
            }
        }
    }

    /**
     * Setting up Logging
     */
    protected function _initLogging()
    {

        $logger = new Epr_Log();

        /** get the config */
        $config = $this->getOptions();

        $loglevel       = (int)$config['logging']['level'];
        $filelogging    = (bool)$config['logging']['writer']['file'];
        $firebuglogging = (bool)$config['logging']['writer']['firebug'];

        $filter = new Zend_Log_Filter_Priority($loglevel);

        $format = '%timestamp% ' . uniqid() . ' %priorityName%: %message% ' . PHP_EOL;

        $formatter = new Zend_Log_Formatter_Simple($format);

        if ($filelogging) {
            //writes to path

            $logPath      = 'logs';
            $clientConfig = Zend_Registry::get('client');
            if (isset($clientConfig->log->path)) {
                $logPath = $clientConfig->log->path;
            }

            $logPath = Epr_System::getEffectivePath($logPath);
            if (!file_exists($logPath)) {
                mkdir($logPath, 0770, true);
            }

            $logFile = $logPath . '/' . $config['project']['name'] . '.log';

            $writer = new Zend_Log_Writer_Stream($logFile);
            $writer->addFilter($filter);
            $writer->setFormatter($formatter);
            $logger->addWriter($writer);
        }

        if ($firebuglogging) {
            $writer_fb = new Zend_Log_Writer_Firebug();
            $writer_fb->addFilter($filter);
            $logger->addWriter($writer_fb);

        }

        if ($filelogging || $firebuglogging) {
            Zend_Registry::set('logger', $logger);

        } else {
            $writer_null = new Zend_Log_Writer_Null();
            $logger->addWriter($writer_null);
            Zend_Registry::set('logger', $logger);
        }

        _logger()->info("Bootstrap: Logger startet");
        _logger()->info('Request: ' . $_SERVER['REQUEST_URI']);
    }

    protected function _initSecurity()
    {

        $config = Zend_Registry::get('client');
        if (isset($config->security->salt)) {

            $salt = __($config->security->salt);
            if ($salt->length() < 10) {
                throw new Exception('Salt too short, must be at least 10 charachters long.');
            } else {
                Zend_Registry::set('salt', (string)$salt);
            }

        } else {
            throw new Exception("No salt found in client's config. Add salt to section 'security'.");
        }
    }

    /**
     * Setting up Mail
     */
    protected function _initMail()
    {

        $config = Zend_Registry::get('client');

        $transport = null;
        $from      = null;

        if (isset($config->mail->type)) {

            $type = strtolower($config->mail->type);
            if ($type == 'smtp') {

                $mailConfig = array(
                    'auth'     => $config->mail->authType,
                    'username' => $config->mail->username,
                    'password' => $config->mail->password
                );

                if (isset($config->mail->ssl)) {
                    $mailConfig['ssl'] = $config->mail->ssl;
                }

                if (isset($config->mail->port)) {
                    $mailConfig['port'] = $config->mail->port;
                }

                $from = $config->mail->from;

                $transport = new Zend_Mail_Transport_Smtp($config->mail->smtpServer, $mailConfig);

            } else if ($type == 'sendmail') {

                $transport = new Zend_Mail_Transport_Sendmail();

            }

        }

        if ($transport instanceof Zend_Mail_Transport_Abstract) {
            Zend_Mail::setDefaultTransport($transport);
            Zend_Registry::set('mailTransport', $transport);

            if (!is_null($from)) {
                Zend_Mail::setDefaultFrom($from);
            }

            _logger()->info("Mail Setup finished");
        } else {
            _logger()->critical('Mail setup failed');
        }

    }

    protected function _initAcl()
    {
        $acl = Plugins_Auth_Acl::getInstance();
        Zend_View_Helper_Navigation_HelperAbstract::setDefaultAcl($acl);
        Zend_View_Helper_Navigation_HelperAbstract::setDefaultRole('guest');
        Zend_Registry::set('Zend_Acl', $acl);
        _logger()->info("Bootstrap: ACL initialized");

        return $acl;
    }

    /**
     * Routes Configuration
     */
    protected function _initRouting()
    {

        $frontController = Zend_Controller_Front::getInstance();

        /** @var Zend_Controller_Router_Rewrite $router */
        $router = $frontController->getRouter();

        $defaultLang = new Zend_Controller_Router_Route(
            ':module/:controller/:action/*',
            array(
                'language'   => 'en',
                'module'     => 'default',
                'controller' => 'index',
                'action'     => 'index'
            )
        );
        $router->addRoute('lang_default', $defaultLang);

        $redeemRoute = new Zend_Controller_Router_Route(
            'redeem/:dealId',
            array(
                'module'     => 'api',
                'controller' => 'deals',
                'action'     => 'redeem'
            )
        );
        $router->addRoute('redeemRoute', $redeemRoute);
        $redeemRoute = new Zend_Controller_Router_Route(
            'redeem/:dealId/:type',
            array(
                'module'     => 'api',
                'controller' => 'deals',
                'action'     => 'redeem'
            )
        );
        $router->addRoute('redeemRoute', $redeemRoute);

        $eventBookingRoute = new Zend_Controller_Router_Route(
          'api/events/:id/booking',
          array(
              'module' => 'api',
              'controller' => 'events',
              'action' => 'booking'
          )
        );
        $router->addRoute('eventBooking', $eventBookingRoute);

        $eventsStart = new Zend_Controller_Router_Route_Static(
            'events',
            array(
                'module'     => 'default',
                'controller' => 'events',
                'action'     => 'index'
            )
        );
        $router->addRoute('eventsStart', $eventsStart);

        $eventsShow = new Zend_Controller_Router_Route(
            'events/:slug',
            array(
                'module' => 'default',
                'controller' => 'events',
                'action' => 'show'
            )
        );
        $router->addRoute('eventsShow', $eventsShow);

        $eventsBooking = new Zend_Controller_Router_Route(
            'events/:slug/booking',
            array(
                'module' => 'default',
                'controller' => 'events',
                'action' => 'booking'
            )
        );
        $router->addRoute('eventsBooking', $eventsBooking);

        $images = new Zend_Controller_Router_Route(
            'files/images/:filename',
            array(
                'module'     => 'default',
                'controller' => 'static',
                'action'     => 'image',
                'filename'   => '0'
            )
        );
        $router->addRoute('images', $images);

        $css = new Zend_Controller_Router_Route(
            'files/css/:filename',
            array(
                'module'     => 'default',
                'controller' => 'static',
                'action'     => 'css',
                'filename'   => '0'
            )
        );
        $router->addRoute('css', $css);

        $favicon = new Zend_Controller_Router_Route_Static(
            'favicon.ico', array(
                'module'     => 'default',
                'controller' => 'static',
                'action'     => 'favicon',
            )
        );
        $router->addRoute('favicon', $favicon);

        $appTemplates = new Zend_Controller_Router_Route(
            'files/templates/:module/:platform/:id',
            array(
                'module' => 'default',
                'controller' => 'static',
                'action' => 'template'
            )
        );
        $router->addRoute('appTemplates', $appTemplates);

        $apps = new Zend_Controller_Router_Route_Static(
            'apps',
            array(
                'module'     => 'default',
                'controller' => 'index',
                'action'     => 'apps'
            )
        );
        $router->addRoute('apps', $apps);


        /**
         * News Routes
         * - single item
         * - category
         * - start route
         */
        $newsItemRoute = new Zend_Controller_Router_Route(
            'news/:newsCategorySlug/:newsSlug',
            array(
                'module'           => 'default',
                'controller'       => 'news',
                'action'           => 'show',
                'newsCategorySlug' => 'uncategorized',
                'newsSlug'         => ''
            )
        );
        $router->addRoute('newsItem', $newsItemRoute);

        $newsCategoryRoute = new Zend_Controller_Router_Route(
            'news/:newsCategorySlug',
            array(
                'module'           => 'default',
                'controller'       => 'news',
                'action'           => 'category',
                'newsCategorySlug' => 'uncategorized'
            )
        );
        $router->addRoute('newsCategory', $newsCategoryRoute);

        $newsStart = new Zend_Controller_Router_Route_Static(
            'news',
            array(
                'module'     => 'default',
                'controller' => 'news',
                'action'     => 'index'
            )
        );
        $router->addRoute('newsStart', $newsStart);

        $newsMore = new Zend_Controller_Router_Route(
            'news/more/:id',
            array(
                'module'     => 'default',
                'controller' => 'news',
                'action'     => 'more'
            )
        );
        $router->addRoute('newsMore', $newsMore);


		$newsPagesApi = new Zend_Controller_Router_Route(
			'api/news/page/:page',
			array(
				'module'     => 'api',
				'controller' => 'news',
				'action'     => 'index'
			)
		);
		$router->addRoute('newsPagesApi', $newsPagesApi);

        /**
         * Ideas Routes
         */
        $ideasItemRoute = new Zend_Controller_Router_Route(
            'ideas/:ideaCategorySlug/:competitionSlug',
            array(
                'module'           => 'default',
                'controller'       => 'ideas',
                'action'           => 'show',
                'ideaCategorySlug' => 'uncategorized',
                'competitionSlug'  => ''
            )
        );
        $router->addRoute('ideaCompetitionItem', $ideasItemRoute);

        /**
         * Ideas Routes
         */
        $ideasCompetitionIdeasListRoute = new Zend_Controller_Router_Route(
            'ideas/:ideaCategorySlug/:competitionSlug/ideas',
            array(
                'module'           => 'default',
                'controller'       => 'ideas',
                'action'           => 'list-ideas',
                'ideaCategorySlug' => 'uncategorized',
                'competitionSlug'  => ''
            )
        );
        $router->addRoute(
            'ideaCompetitionIdeasList',
            $ideasCompetitionIdeasListRoute
        );

        $ideasCategoryRoute = new Zend_Controller_Router_Route(
            'ideas/:ideaCategorySlug',
            array(
                'module'           => 'default',
                'controller'       => 'ideas',
                'action'           => 'category',
                'ideaCategorySlug' => 'uncategorized'
            )
        );
        $router->addRoute('ideaCategory', $ideasCategoryRoute);

        $ideasStart = new Zend_Controller_Router_Route_Static(
            'ideas',
            array(
                'module'     => 'default',
                'controller' => 'ideas',
                'action'     => 'index'
            )
        );
        $router->addRoute('ideasStart', $ideasStart);

        $ideaAdd = new Zend_Controller_Router_Route(
            'ideas/:slug/submit',
            array(
                'module'     => 'default',
                'controller' => 'ideas',
                'action'     => 'submit'
            )
        );
        $router->addRoute('ideaAdd', $ideaAdd);

        $ideaDetail = new Zend_Controller_Router_Route(
            'idea/detail/:id',
            array(
                'module'     => 'default',
                'controller' => 'ideas',
                'action'     => 'details',
                'id'         => ''
            )
        );
        $router->addRoute('ideaDetail', $ideaDetail);

        /**
         * Deals Routes
         */
        $dealsStart = new Zend_Controller_Router_Route_Static(
            'deals',
            array(
                'module'     => 'default',
                'controller' => 'deals',
                'action'     => 'index'
            )
        );
        $router->addRoute('dealsStart', $dealsStart);

        $dealsDetails = new Zend_Controller_Router_Route(
            'deals/show/:id',
            array(
                'module'     => 'default',
                'controller' => 'deals',
                'action'     => 'details',
                'id'         => null
            )
        );
        $router->addRoute('dealsDetails', $dealsDetails);

        $dealsBarcode = new Zend_Controller_Router_Route('api/barcode/ean/:code', array(
            'module' => 'api',
            'controller' => 'index',
            'type' => 'ean',
            'action' => 'barcode'
        ));
        $router->addRoute('barcodeRoute', $dealsBarcode);

        $dealsSave = new Zend_Controller_Router_Route(
            'deals/save/:id/:callbackRoute',
            array(
                'module'        => 'default',
                'controller'    => 'deals',
                'action'        => 'save',
                'id'            => null,
                'callbackRoute' => null
            )
        );
        $router->addRoute('dealsSave', $dealsSave);

        $dealsPrint = new Zend_Controller_Router_Route(
            'deals/print/:id/',
            array(
                'module'     => 'default',
                'controller' => 'deals',
                'action'     => 'print'
            )
        );
        $router->addRoute('dealsPrint', $dealsPrint);

        $dealsDealers = new Zend_Controller_Router_Route(
            'deals/dealers/:id/',
            array(
                'module'     => 'default',
                'controller' => 'deals',
                'action'     => 'dealers'
            )
        );
        $router->addRoute('dealsDealers', $dealsDealers);

        /*
         * API Auth Route
         */
        $authRoute = new Zend_Controller_Router_Route(
            'user/verify/:verificationId',
            array(
                'module'         => 'default',
                'controller'     => 'user',
                'action'         => 'verify',
                'verificationId' => false
            )
        );
        $router->addRoute('authRoute', $authRoute);

        $frontendLogin = new Zend_Controller_Router_Route_Static(
            'user/login',
            array(
                'module'     => 'default',
                'controller' => 'user',
                'action'     => 'logout'
            )
        );
        $router->addRoute('frontendLogin', $frontendLogin);

        $frontendLogout = new Zend_Controller_Router_Route_Static(
            'user/logout',
            array(
                'module'     => 'default',
                'controller' => 'user',
                'action'     => 'logout'
            )
        );
        $router->addRoute('frontendLogout', $frontendLogout);

        $frontendRegistered = new Zend_Controller_Router_Route_Static(
            'user/registered',
            array(
                'module'     => 'default',
                'controller' => 'user',
                'action'     => 'registered'
            )
        );
        $router->addRoute('frontendRegistered', $frontendRegistered);

        $myDeals = new Zend_Controller_Router_Route_Static(
            '/user/deals',
            array(
                'module'     => 'default',
                'controller' => 'user',
                'action'     => 'deals'
            )
        );
        $router->addRoute('myDeals', $myDeals);

        $myIdeas = new Zend_Controller_Router_Route_Static(
            '/user/ideas',
            array(
                'module'     => 'default',
                'controller' => 'user',
                'action'     => 'ideas'
            )
        );
        $router->addRoute('myIdeas', $myIdeas);

        $myAccount = new Zend_Controller_Router_Route_Static(
            '/user/account',
            array(
                'module'     => 'default',
                'controller' => 'user',
                'action'     => 'account'
            )
        );
        $router->addRoute('myAccount', $myAccount);

        $outlineRoute = new Zend_Controller_Router_Route_Static(
            '/user/outline',
            array(
                'module'     => 'default',
                'controller' => 'user',
                'action'     => 'outline'
            )
        );
        $router->addRoute('outlineRoute', $outlineRoute);

        $resetPasswordRoute = new Zend_Controller_Router_Route_Static(
            '/user/reset-password',
            array(
                'module'     => 'default',
                'controller' => 'user',
                'action'     => 'reset-password'
            )
        );
        $router->addRoute('resetPasswordRoute', $resetPasswordRoute);

        $resetTwoPasswordRoute = new Zend_Controller_Router_Route(
            '/user/new-password/:id',
            array(
                'module'     => 'default',
                'controller' => 'user',
                'action'     => 'new-password'
            )
        );
        $router->addRoute('reset2PasswordRoute', $resetTwoPasswordRoute);

        $pagesRoute = new Zend_Controller_Router_Route(
            '/pages/:path',
            array(
                'module'     => 'default',
                'controller' => 'static',
                'action'     => 'page'
            )
        );
        $router->addRoute('pagesRoute', $pagesRoute);

        $auth = Zend_Auth::getInstance();
        $frontController->registerPlugin(new Plugins_Auth_AccessControl($auth));

        $errorHandling = new Zend_Controller_Plugin_ErrorHandler();
        $errorHandling->setErrorHandler(
            array(
                'module'     => 'default',
                'controller' => 'error',
                'action'     => 'error'
            )
        );

        $frontController->registerPlugin($errorHandling);

        $frontController->setParam('useDefaultControllerAlways', false);
        _logger()->info("Bootstrap: Routes initialized");

    }


    protected function _initDoctrine()
    {

        $config = Zend_Registry::get('config');

        $commonLibPath = APPLICATION_PATH . '/library';
        if (isset($config['paths'])) {
            $paths = $config['paths'];
            if (isset($paths['common'])) {
                $commonLibPath = $paths['common'];
            }
        }
        if (!file_exists($commonLibPath . '/Doctrine')) {
            throw new Exception('Cannot locate Doctrine, specify paths.common in application.ini');
        } else {
            Zend_Registry::set('commonLibPath', $commonLibPath);
        }

        $loader = new \Doctrine\Common\ClassLoader($commonLibPath);
        $loader->register();
        $annotationNs = 'Doctrine\\ODM\\CouchDB\\Mapping\\Annotations';

        $path = $commonLibPath;
        Doctrine\Common\Annotations\AnnotationRegistry::registerAutoloadNamespace($annotationNs, $path);

        // set the date time converter to our implementation which uses a different format
        \Doctrine\ODM\CouchDB\Types\Type::overrideType(\Doctrine\ODM\CouchDB\Types\Type::DATETIME, 'Epr_DB_DateTimeType');
        // the color converter
        \Doctrine\ODM\CouchDB\Types\Type::addType('color', 'Epr_DB_ColorType');
        // the address converter
        \Doctrine\ODM\CouchDB\Types\Type::addType('address', 'Epr_DB_AddressType');
        // the URL converter
        \Doctrine\ODM\CouchDB\Types\Type::addType('url', 'Epr_DB_URLType');

        \Doctrine\ODM\CouchDB\Types\Type::addType('mail', 'Epr_DB_EmailType');
        \Doctrine\ODM\CouchDB\Types\Type::addType('coordinate', 'Epr_DB_CoordinateType');

        _logger()->info("Bootstrap: Doctrine initialized");
    }

    protected function _initDatabaseProxy()
    {

        $clientConfig = Zend_Registry::get('client');
        $proxyPath    = ROOT_PATH . '/application/library/Proxy';

        if (strlen($proxyPath) >= 2) {

            //$proxyPath = Epr_System::getEffectivePath($proxyPath);

            if (!file_exists($proxyPath)) {
                _logger()->info('Creating proxy dir at ' . $proxyPath);
                mkdir($proxyPath, 0770, true);
            }

            if (file_exists($proxyPath)) {
                $this->_databaseProxyPath = $proxyPath;
            }
        }

        _logger()->info("Bootstrap: DB Proxy initialized");

    }

    protected function _initDatabase()
    {

        $config = Zend_Registry::get('client');

        $databaseHost = __($config->database->host);
        if ($databaseHost->length() == 0) {
            _logger()->warn("No database host specified in config, using 'localhost'.");
            $databaseHost = __('localhost');
        }

        $databaseUser     = __($config->database->username);
        $databasePassword = __($config->database->password);

        $databasePort = __($config->database->port);
        if ($databasePort->length() == 0) {
            _logger()->warn("No database port specified in config, using '5984'.");
            $databasePort = __('5984');
        }

        $databaseName = __($config->database->dbname)->lowercase();
        if ($databaseName->length() == 0) {
            throw new Exception("No database name found in client's config.");
        }

        $httpClient = new \Doctrine\CouchDB\HTTP\SocketClient($databaseHost, $databasePort->integerValue(), (string)$databaseUser, (string)$databasePassword);

        $dbClient = new Doctrine\CouchDB\CouchDBClient($httpClient, $databaseName);

        $doctrineConfig         = new \Doctrine\ODM\CouchDB\Configuration();
        $metadataDriver = $doctrineConfig->newDefaultAnnotationDriver();
        $doctrineConfig->setMetadataDriverImpl($metadataDriver);

        $luceneHandler = '_fti';
        if (isset($config->database->luceneHandler)) {
            $luceneHandler = $config->database->luceneHandler;
        }
        if (strlen($luceneHandler) > 0) {
            $doctrineConfig->setLuceneHandlerName($luceneHandler);
        }

        $luceneAnalyzer = 'standard';
        if (isset($config->database->luceneAnalyzer) && strlen($config->database->luceneAnalyzer) > 0) {
            $luceneAnalyzer = $config->database->luceneAnalyzer;
        }
        Zend_Registry::set('luceneAnalyzerName', $luceneAnalyzer);

        if (!is_null($this->_databaseProxyPath) && file_exists($this->_databaseProxyPath)) {
            $doctrineConfig->setProxyDir($this->_databaseProxyPath);
            if (APPLICATION_ENV == Epr_Application::RUNTIMETYPE_DEVELOPMENT) {
                $doctrineConfig->setAutoGenerateProxyClasses(true);
            }
        }

        $dm = new \Doctrine\ODM\CouchDB\DocumentManager($dbClient, $doctrineConfig);

        Zend_Registry::set('dm', $dm);

        try {
            // create database if not existing
            $httpClient->request('PUT', '/' . $databaseName);
        } catch (Exception $e) {
            throw new Exception('Connection to Database failed');
        }

        _logger()->info("Bootstrap: DB initialized");



    }

    protected function _initApplication()
    {

        $application = Epr_Application::application();
        Zend_Registry::set('app', $application);
        _logger()->info('Application running using document version ' . $application->getDocumentsVersion());
    }


    protected function _initApplicationUpdates()
    {
        Epr_Application::application()->performUpdateActionsIfNeeded();
    }

    protected function _initFirstAdministrator()
    {

        if (!Epr_User_Collection::hasActiveRootUser()) {

            $clientConfig = Zend_Registry::get('client');

            $firstAdministrator = new Epr_User($clientConfig->info->mail, Epr_User::REGISTRATION_CONTEXT_ADMIN);
            $firstAdministrator->setPassword('admin');
            $firstAdministrator->setRole(Epr_Roles::ROLE_ROOT);
            $firstAdministrator->setFirstname('Administrator');
            $firstAdministrator->activate();
            _dm()->persist($firstAdministrator);
            _dm()->flush();
        }
    }

    protected function _initCaching()
    {

        $clientConfig = Zend_Registry::get('client');
        $cacheEnabled = false;

        if (isset($clientConfig->cache->enabled)) {
            $cacheEnabled = $clientConfig->cache->enabled;
        }

        if ($cacheEnabled) {

            $cacheDir = 'tmp/cache';

            if (isset($clientConfig->cache->path)) {
                $cacheDir = $clientConfig->cache->path;
            }

            $cacheDir = Epr_System::getEffectivePath($cacheDir);

            if (!file_exists($cacheDir)) {
                _logger()->info('Creating cache dir at ' . $cacheDir);
                mkdir($cacheDir, 0770, true);
            }

            Epr_Cache_Provider::setupCache(
                array(
                    'lifetime' => isset($clientConfig->cache->lifetime) ? (int)$clientConfig->cache->lifetime : 7200,
                    'cacheDir' => $cacheDir
                )
            );

        } else {
            _logger()->info("Bootstrap: Caching disabled");
        }
    }

    protected function _initTranslation()
    {

    }
}

