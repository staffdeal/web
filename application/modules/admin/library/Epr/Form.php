<?php
/**
 * Builds the setup for an Erp_Backend_Form
 * User: janfanslau
 * Date: 07.09.12
 * Time: 18:34
 */

class Epr_Form extends Zend_Form
{
    const FIELD_PUBLISHABLECONTROL = 'form_self_publishableControl';

    private $preserveDecorators = false;

    private $entity = null;
    private $publishableControl = null;

    public function __construct($entity = null, $options = null) {
        $this->entity = $entity;
        parent::__construct($options);
    }

    public function setPreserveDecorators($value) {
        $this->preserveDecorators = ($value == true);
    }

    public $elementDecorators = array(
        'ViewHelper',
        'Description',
        'Errors',
        array(array('td' => 'HtmlTag'), array('tag' => 'td')),
        array(array('td' => 'HtmlTag'), array('tag' => 'td')),
        array('Label', array('tag' => 'th', 'class'=>'test')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
        );

    public function init($options = null)
    {
        $this->addElementPrefixPath('Epr_Form_Element_Decorator',
            'Epr/Form/Element/Decorator',
            'decorator');
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'div', 'border'=>'0', 'cellspacing'=>'0', 'id'=>'id-form')),
            'Form',
        ));

        $this->setMethod('post');

        if ($this->getPublishableControlElement()) {
            $this->addElement($this->getPublishableControlElement());
        }
    }

    public function addElement($element, $name = null, $options = null) {
        if ($this->preserveDecorators === false) {
            $element->setDecorators(array('Composite'));
        }
        parent::addElement($element, $name, $options);
    }

    public function getEntity() {
        return $this->entity;
    }

    /**
     * @return Epr_Form_Element_PublishableControl|null
     */
    public function getPublishableControlElement()
    {
        if (is_null($this->publishableControl)) {
            if ($this->entity instanceof Epr_Document_Publishable) {
                $this->publishableControl = new Epr_Form_Element_PublishableControl(self::FIELD_PUBLISHABLECONTROL);
                $this->publishableControl->setState($this->entity->getState());
                $this->publishableControl->setPublishDate($this->entity->getPublishedDate());
                $this->publishableControl->setLastUnpublishDate($this->entity->getUnpublishedDate());
                $this->publishableControl->setLastStateChangeReason($this->entity->getStateChangeReason());
                $this->publishableControl->setAllowPublishDateChange($this->entity->allowAutomaticPublishing());
            }
        }
        return $this->publishableControl;
    }
}