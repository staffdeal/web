<?php
/**
 * User: janfanslau
 * Date: 07.09.12
 * Time: 18:38
 */
class Epr_Form_Element_Decorator_Submit extends Zend_Form_Decorator_Abstract
{

    public function buildLabel()
    {
        $element = $this->getElement();
        $label = $element->getLabel();
        if ($translator = $element->getTranslator()) {
            $label = $translator->translate($label);
        }
        if ($element->isRequired()) {
            $label .= '*';
        }
        if($label != '')
            $label .= ':';

        return $element->getView()
            ->formLabel($element->getName(), $label);
    }

    public function buildInput()
    {

        $element = $this->getElement();
        $helper  = $element->helper;
        return $element->getView()->$helper(
            $element->getName(),
            $element->getValue(),
            $element->getAttribs(),
            $element->options
        );
    }

    public function buildErrors()
    {
        $element  = $this->getElement();
        $messages = $element->getMessages();
        if (empty($messages)) {
            return '';
        }
        return $element->getView()->formErrors($messages);
    }

    public function buildDescription()
    {
        $element = $this->getElement();
        $desc    = $element->getDescription();
        if (empty($desc)) {
            return '';
        }
        return $desc;
    }

    public function render($content)
    {
        $element = $this->getElement();
        if (!$element instanceof Zend_Form_Element) {
            return $content;
        }
        if (null === $element->getView()) {
            return $content;
        }

        $separator = $this->getSeparator();
        $placement = $this->getPlacement();
        $label     = $this->buildLabel();
        $input     = $this->buildInput();
        $errors    = $this->buildErrors();
        $desc      = $this->buildDescription();

        $elementClass = 'element';
        if ($this->getElement()->isRequired()) {
            $elementClass.= ' required';
        }

        if ($element instanceof Zend_Form_Element_Submit) {
            $elementClass .= ' submit';
        }

        $dataContext = $element->getAttrib('data-context');

        $color = $element->getAttrib('color');


        $output = '<div class="' . ($color ? $color . ' ' : '') . $elementClass . ($dataContext ? ' has-context' : '') . '" id="wrapper_' . $element->getName() . '"' . ($dataContext ? ' data-context="' . $dataContext . '"' : '') . '>';

        if (_user() && _user()->isAdministrator() && _user()->getShowBackendHints() && strlen($desc) > 0) {
            if (isset($element->inlineDescription) && $element->inlineDescription == true) {
                $output.= '<div class="description">' . $desc .'</div>';
            } else {
                $output.= '<img src="/images/forms/help.png" class="description" title="' . $desc . '"/>';
            }
        }

        if (isset($element->inlineDescription)) {
            unset($element->inlineDescription);
        }

        $output.= '<div class="label">'. $label;
        $output.= '</div>';
        $output.= '<div class="input submit">';
        $output.= $input;
        $output.= '</div>';

        if (strlen($errors) > 0) {
            $output.= '<div class="error" error-data-field="' . $this->getElement()->getName() . '">';
            $output.= '<div class="error-inner">';
            $output.= $errors;
            $output.= '</div>';
            $output.= '</div>';
        }

        $output.= '</div><div class="clear"></div>';

        switch ($placement) {
            case (self::PREPEND):
                return $output . $separator . $content;
            case (self::APPEND):
            default:
                return $content . $separator . $output;
        }
    }
}
