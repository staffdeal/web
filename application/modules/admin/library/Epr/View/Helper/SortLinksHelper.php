<?php
/**
 * @package Backend
 * @author daniel 
 */

class Epr_View_Helper_SortLinksHelper extends Zend_View_Helper_Abstract {

    private $_sortKey;
    private $_sortDirection = SORT_ASC;

    private function getSortKey()
    {
        if (Zend_Registry::isRegistered(Epr_Backend_Controller_Action::SORT_KEY_KEY)) {
            return Zend_Registry::get(Epr_Backend_Controller_Action::SORT_KEY_KEY);
        } else {
            return null;
        }
    }

    private function getSortDirection()
    {
        if (Zend_Registry::isRegistered(Epr_Backend_Controller_Action::SORT_DIRECTION_KEY)) {
            return Zend_Registry::get(Epr_Backend_Controller_Action::SORT_DIRECTION_KEY);
        } else {
            return SORT_ASC;
        }
    }

    public function getSearchQuery() {
        $query = null;
        if (Zend_Registry::isRegistered(Epr_Backend_Controller_Action::SEARCH_QUERY)) {
            $query = Zend_Registry::get(Epr_Backend_Controller_Action::SEARCH_QUERY);
        }
        return $query;
    }

    public function sortLinksHelper($key, $title) {
        $output = '';
        $currentKey = $this->getSortKey();
        $currentDir = $this->getSortDirection();

        $ascClass = 'sorting';
        $descClass = 'sorting';

        $output .= '<a href="?';

        $searchQuery = $this->getSearchQuery();
        if (is_string($searchQuery) && strlen($searchQuery) > 0) {
            $output.= 'q=' . urlencode($searchQuery) . '&';
        }

        if ($key == $currentKey) {
            $alternateDir = ($currentDir == SORT_ASC) ? 'desc' : 'asc';
            $output .= 'sortKey=' . $key . '&sortDirection=' .$alternateDir . '">';
            $output .= $title . '&nbsp;';

            if ($this->getSortDirection() === SORT_ASC) {
                $ascClass .= ' active';
            } else {
                $descClass .= ' active';
            }

            $output .= '<span class="' . $ascClass . '">▲</span>';
            $output .= '<span class="' . $descClass . '">▼</span>';
        } else {
            $output .= 'sortKey=' . $key . '&sortDirection=asc">';
            $output .= $title;
            $output .= '<span class="' . $ascClass . '">▲</span>';
            $output .= '<span class="' . $descClass . '">▼</span>';
        }
        $output .= '</a>';

        return $output;
    }

}