<?php

class Epr_View_Helper_PublishableControlHelper extends Zend_View_Helper_FormElement {

    const PUBLISH_LATER_VALUE = 'later';

    public function publishableControlHelper($name, $value = null, $attributes = null, $options = null) {

        $info = $this->_getInfo($name, $value, $attributes);
        $state = $value;
        $id = $info['id'];
        $publishedDate = (isset($options[Epr_Form_Element_PublishableControl::OPTION_LAST_PUBLISH_DATE])) ? $options[Epr_Form_Element_PublishableControl::OPTION_LAST_PUBLISH_DATE] : null;
        $unpublishedDate = (isset($options[Epr_Form_Element_PublishableControl::OPTION_LAST_UNPUBLISH_DATE])) ? $options[Epr_Form_Element_PublishableControl::OPTION_LAST_UNPUBLISH_DATE] : null;
        $reason = (isset($options[Epr_Form_Element_PublishableControl::OPTION_LAST_STATE_CHANGE_REASON])) ? $options[Epr_Form_Element_PublishableControl::OPTION_LAST_STATE_CHANGE_REASON] : null;
        $selectDate = ($state == Epr_Document_Publishable::STATE_DRAFT) && (isset($options[Epr_Form_Element_PublishableControl::OPTION_ALLOW_PUBLISHDATE_CHANGE])) ? ($options[Epr_Form_Element_PublishableControl::OPTION_ALLOW_PUBLISHDATE_CHANGE] == true) : false;

        $output = '';
        $output.= '<div class="publishable controls regular">';

        $publishLaterActiveClass = '';
        $publishNowActiveClass = '';
        $saveAsDraftActiveClass = '';
        if ($state == Epr_Document_Publishable::STATE_DRAFT) {
            $saveAsDraftActiveClass = 'active';
        } else {
            if ($publishedDate instanceof \Poundation\PDate) {
                if ($publishedDate->isPast()) {
                    $publishNowActiveClass = 'active';
                } else {
                    $publishLaterActiveClass = 'active';
                }
            }
        }

        $publishText = _t('Publish now');
        $draftText = _t('Save as draft');
        if ($state == Epr_Document_Publishable::STATE_PUBLISHED) {
            $publishText = _t('Save published');
            $draftText = _t('Save as draft');
        }
        $output.= '<div class="buttons">';
        $output.= '<button class="form-submit ' . $publishNowActiveClass . '" onclick="document.getElementById(\'' . $id . '\').value = \'' . Epr_Document_Publishable::STATE_PUBLISHED . '\'; this.form.submit(); return false;">' . $publishText . '</button>';
        $output.= '<button class="form-submit ' . $saveAsDraftActiveClass . '" onclick="document.getElementById(\'' . $id . '\').value = \'' . Epr_Document_Publishable::STATE_DRAFT . '\'; this.form.submit(); return false;">' . $draftText . '</button>';
        $output.= '</div>';
        $output.= '</div>';

        $output.= '<div class="publishable state late">';
        $output.= __(_t($state))->uppercaseAtBeginning();
        $output.= '</div>';


        $output.= '<div class="publishable controls">';
        if ($selectDate) {
            $dateId = $id . '_date';
            $dateString = ($publishedDate instanceof \Poundation\PDate) ? $publishedDate->getFormatedString('Y-m-d') : '';
            $timeId = $id . '_time';
            $timeString = ($publishedDate instanceof \Poundation\PDate) ? $publishedDate->getFormatedString('H:i') : '';
            $publishLaterText = _t('Publish at');
            $output.= '<div class="date">';
            $output.= '<input class="inp-form date-picker" type="text" name="' . $dateId . '" id="' . $dateId . '" value="' . $dateString . '" readonly="readonly" />';
            $output.= '<input class="inp-form time-picker" type="text" name="' . $timeId . '" id="' . $timeId . '" value="' . $timeString . '" autocomplete="off" />';
            $output.= '<button class="form-submit ' . $publishLaterText . '" onclick="document.getElementById(\'' . $id . '\').value = \'' . self::PUBLISH_LATER_VALUE . '\'; this.form.submit(); return false;">' . $publishLaterText . '</button>';
            $output.= '</div>';
        }
        $output.= '</div>';

        $output.= '<div class="clear"></div>';

        $output.= '<input type="hidden" name="' . $name . '" id="' . $id . '" value="' . $state . '">';

        return $output;
    }

}