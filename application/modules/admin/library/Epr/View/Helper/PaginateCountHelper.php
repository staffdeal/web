<?php

/**
 * @package Backend
 * @author daniel
 */
class Epr_View_Helper_PaginateCountHelper extends Zend_View_Helper_Abstract
{

    const CONTEXT_TOP = 'top';
    const CONTEXT_BOTTOM = 'bottom';

    public function paginateCountHelper($context = self::CONTEXT_BOTTOM, $result = null, $includeClearfix = false)
    {

        $values = array(5, 10, 25, 50, 75, 100);

        $currentValue = 0;
        if (Zend_Registry::isRegistered(Epr_Backend_Controller_Action::PAGINATE_COUNT)) {
            $currentValue = (int)Zend_Registry::get(Epr_Backend_Controller_Action::PAGINATE_COUNT);
        }

        $currentDisplayCount = 0;
        if ($result instanceof \Doctrine\CouchDB\View\Result) {
            $currentDisplayCount = $result->getTotalRows();
        }

        $minValueToDisplay = ($context == self::CONTEXT_TOP) ? 10 : min($values);
        if ($currentDisplayCount > $minValueToDisplay) {

            $parts = array();
            for ($i = 0; $i < count($values); $i++) {
                $count = $values[$i];

                if ($count == $currentValue) {
                    $parts[] = '<span class="paginateCount active">' . $count . '</span>';
                } else {

                    $url = '?';
                    if (Zend_Registry::isRegistered(Epr_Backend_Controller_Action::SEARCH_QUERY)) {
                        $query = Zend_Registry::get(Epr_Backend_Controller_Action::SEARCH_QUERY);
                        if (strlen($query) > 0) {
                            $url .= 'q=' . urlencode($query) . '&';
                        }
                    }

                    $parts[] = '<a class="paginateCount" href="' . $url .'count=' . $count . '">' . $count . '</a>';
                }

            }

            $className = ($context == self::CONTEXT_TOP) ? 'top' : 'bottom';

            return '<div class="paginateCountLinks ' . $className . '">' . implode('', $parts) . '</div>' . (($includeClearfix) ? '<div class="clear"></div>' : '');

        }
    }

}