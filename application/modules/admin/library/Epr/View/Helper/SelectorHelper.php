<?php

class Epr_View_Helper_SelectorHelper extends Zend_View_Helper_FormElement
{

    public function selectorHelper($name, $value = null, $attributes = null, $options = null)
    {

        $info = $this->_getInfo($name, $value, $attributes);

        $id = $info['id'];

        if (!is_array($options)) {
            $options = array();
        }
        $options['dataField'] = $name;
        $options['maxSize'] = (int)(ini_get('upload_max_filesize')) * 1024 * 1024;

        $size = (isset($options['previewSize'])) ? $options['previewSize'] : array('width' => 200, 'height' => 150);
		$devicePreviews = (isset($options['devicePreview']) ? $options['devicePreview'] : array());
		$previewContainerId = $id . '_devicePreviews';

		$autoResizingEnabled = (isset($options['autoResizing'])) ? ($options['autoResizing']) : false;

        $multipleSelection = (isset($options['multiple']) && $options['multiple'] == true);
        $includeThumbnail = ($multipleSelection == false && isset($options['showThumbnail']) && $options['showThumbnail'] == true);

        // preparing the JS code to start the browser
        $jsOptions = json_encode($options);
        $jsFunction = '<script type="text/javascript">' . "\n";
        $jsFunction .= "\t" . 'var startImageBrowser_' . $name . ' = function() {' . "\n";
        $jsFunction .= "\t\t" . 'var ' . $name . '_options = ' . $jsOptions . ';' . "\n";
        $jsFunction .= "\t\t" . 'showImageBrowser(' . $name . '_options);' . "\n";
        $jsFunction .= "\t\t" . 'return false;' . "\n";
        $jsFunction .= "\t" . '};' . "\n";
        $jsFunction .= '</script>' . "\n\n";
        $output = $jsFunction;

        // collecting the information for the data field.
        $imageIds = array();
        if ($value instanceof Epr_Content_Image) {
            $imageIds[] = $value->getId();
        } else if (is_array($value)) {
            foreach ($value as $image) {
                if ($image instanceof Epr_Content_Image) {
                    $imageIds[] = $image->getId();
                }
            }
        }
        $selectedImages = htmlspecialchars(json_encode($imageIds));


        // the data field
        $dataField = '<input type="hidden" ';
        $dataField.= 'name="' . $name . '" ';
        $dataField.= 'id="' . $id . '" ';
        $dataField.= 'value="' . $selectedImages . '" ';
        $dataField.= '>';

        $output.= $dataField;

        $class = (isset($attributes['class'])) ? $attributes['class'] : '';
        if ($includeThumbnail) {
            $class.= ' ' . 'thumbnail';
        }
        $output .= '<div class="imageBrowserSelector' . $class . '">';

        // the remove button id
        $removeButtonId = 'remove_' . $id;

        if ($includeThumbnail) {

            // with thumbnail

            $imageId = 'image_' . $id;

            // the thumbnail image
			$class = 'imageThumbnail';
			if ($autoResizingEnabled) {
				$class.= ' autoResizing';
			}

            $thumbnail = '<img class="' . $class . '" ';
			if (!$autoResizingEnabled) {
				$thumbnail.= 'style="width:' . $size['width'] . 'px; height:' . $size['height'] . 'px"';
			}

            $thumbnail.= 'id="' . $imageId . '" ';
            $thumbnail.= 'onclick="return startImageBrowser_' . $name . '();" ';

            $thumbnail .= '/>';

            $output .= $thumbnail;

			// device previews
			$linkToDevicePreviesArrayJS = '';
			if (is_array($devicePreviews) && count($devicePreviews) > 0) {

				$linkToDevicePreviesArrayJS = '[';
				$devicePreviewHTML = '<div class="devicePreviews" id="' . $previewContainerId . '">';

				$i = 0;
				$devicePreviewIds = array();
				foreach($devicePreviews as $devicePreviewData) {

					$width = max((int)$devicePreviewData['width'], 1);
					$height = max((int)$devicePreviewData['height'], 1);
					$radius = max((int)$devicePreviewData['radius'], 1);
					$caption = $devicePreviewData['caption'];
					$devicePreviewId = $imageId . '_devicePreview_' . $i;

					$devicePreviewHTML.= '<div class="devicePreview">';
					$devicePreviewHTML.= '<h4>' . $caption . '</h4>';

					$devicePreviewHTML.= '<img ';
					$devicePreviewHTML.= 'style="width:' . $width . 'px; height:' . $height . 'px; border-radius: ' . $radius . 'px;" ';
					$devicePreviewHTML.= 'id="' . $devicePreviewId . '"';
					$devicePreviewHTML.= 'title="Preview of the selected image on ' . $caption . ' devices."';
					$devicePreviewHTML.= '>';

					$devicePreviewHTML.= '</div>';

					$i++;
					$devicePreviewIds[] = '"' . $devicePreviewId . '"';
				}

				$output.= $devicePreviewHTML . '</div>';

				$linkToDevicePreviesArrayJS.= implode(',', $devicePreviewIds);
				$linkToDevicePreviesArrayJS.= ']';

			}

            // the remove button
            $removeButton = '<img src="/images/icons/redCross.png" class="remove"';
            $removeButton.= 'id="' . $removeButtonId . '" ';
            $removeButton.= '/>';
            $output.= $removeButton;

            // the js to link the thumbnail to the data field
            $output.=  '<script type="text/javascript">' . "\n";
            $output.= "\t" . 'linkImageThumbnailToDataField("' . $imageId . '","' . $id . '",' . (($autoResizingEnabled) ? 'true' : 'false') . ');' . "\n";
			if (strlen($linkToDevicePreviesArrayJS) > 0) {
				$output.= "\t" . 'linkDevicePreviewImagesToDataField(' . $linkToDevicePreviesArrayJS . ',"' . $id . '", "' . $previewContainerId .'");' . "\n";
			}
            $output.=  '</script>' . "\n";

        }   else {

            // no thumbnail
            $removeButton = '<button ';
            $removeButton.= 'id="' . $removeButtonId . '" ';
            $removeButton.= 'onclick="return false;" ';
            $removeButton.= '>Remove</button>';

            $selectButton = '<button ';
            $selectButton .= 'onclick="return startImageBrowser_' . $name . '();" ';
            $selectButton .= '>Select Image</button>';

            $output .= $selectButton . ' ' . $removeButton;

        }


        // the js to to link the remove button the the data field.
        $output.=  '<script type="text/javascript">' . "\n";
        $output.= "\t" . 'linkControlButtonsToDataField("' . $removeButtonId . '","' . $id . '");' . "\n";
        $output.=  '</script>' . "\n";

        $output .= '</div>';

        return $output;
    }

}