<?php
/**
 * @package Backend
 * @author daniel 
 */

class Epr_View_Helper_PaginateLinksHelper extends Zend_View_Helper_Abstract {

    public function paginateLinksHelper($result) {

        $output = '';

        if ($result instanceof \Doctrine\CouchDB\View\Result) {

            $startIndex = $result->getOffset();
            $lastIndex = $startIndex + $result->count() - 1;
            $total = $result->getTotalRows();

            $count = 10;
            if (Zend_Registry::isRegistered(Epr_Backend_Controller_Action::PAGINATE_COUNT)) {
                $count = (int)Zend_Registry::get(Epr_Backend_Controller_Action::PAGINATE_COUNT);
            }

            $url = '?';
            if (Zend_Registry::isRegistered(Epr_Backend_Controller_Action::SEARCH_QUERY)) {
                $query = Zend_Registry::get(Epr_Backend_Controller_Action::SEARCH_QUERY);
                if (strlen($query) > 0) {
                    $url.='q=' . urlencode($query) . '&';
                }
            }

            if ($count <= $total) {

                $output .= '<div id="globalPagination"><div class="control">';

                // first link
                if ($startIndex > 0) {
                    $output .= '<a href="' . $url . 'offset=0" class="paginateLink">|◄</a>';
                } else {
                    $output .= '<span class="paginateLink">|◄</span>';
                }

                // previous link
                if ($startIndex > 0) {
                    $output .= '<a href="' . $url . 'offset=' . max(0, ($startIndex - $count)) . '" class="paginateLink">◄</a>';
                } else {
                    $output .= '<span class="paginateLink">◄</span>';
                }

                $rangeText = ($startIndex !== $lastIndex) ? ('#' . ($startIndex + 1) . ' - #' . ($lastIndex + 1)) : '#' . ($startIndex + 1);
                $output .= '<span class="paginateInfo">Showing ' . $rangeText . ' of ' . $result->getTotalRows() . '</span>';

                // next link
                if ($lastIndex < $total - 1) {
                    $output .= '<a href="' . $url . 'offset=' . ($lastIndex + 1) . '" class="paginateLink">►</a>';
                } else {
                    $output .= '<span class="paginateLink">►</span>';
                }

                // last link
                if ($lastIndex < $total - 1) {
                    $output .= '<a href="' . $url . 'offset=' . ($total - $count) . '" class="paginateLink">►|</a>';
                } else {
                    $output .= '<span class="paginateLink">►|</span>';
                }

                $output .= '</div></div>';
            }

        }

        return $output;
    }

}