<?php

class Epr_View_Helper_SearchBoxHelper extends Zend_View_Helper_Abstract {

    public function getSearchQuery() {
        $query = null;
        if (Zend_Registry::isRegistered(Epr_Backend_Controller_Action::SEARCH_QUERY)) {
            $query = Zend_Registry::get(Epr_Backend_Controller_Action::SEARCH_QUERY);
        }
        return $query;
    }

    public function searchBoxHelper($smartLinks = null, $formId = 'mainform') {

        $output = '<div class="search">';

        $currentQuery = $this->getSearchQuery();
        $queryText = (is_string($currentQuery) && strlen($currentQuery) > 0) ? htmlentities($currentQuery) : '';
        $output.= '<input type="text" name="q" id="q" placeholder="' . _t('Search query') . '" value="' . $queryText . '" autocomplete="off" />';
        $output.= '<button class="form-submit go">' . _t('Go') . '</button>';
        if (strlen($currentQuery) > 0) {
            $output.= '<button class="form-submit reset" onclick="document.getElementById(\'q\').value=\'\';">' . _t('Reset') . '</button>';
        }

        if (is_array($smartLinks) && count($smartLinks) > 0) {
            $output .= '<div class="smartlinks">';
            foreach ($smartLinks as $name => $query) {
                $output .= '<a href="#" onclick="document.getElementById(\'q\').value=\'' . $query . '\';document.getElementById(\'' . $formId . '\').submit();">' . $name . '</a>';
            }
            $output .= '</div>';
        }

        $output.= '</div>';

        return $output;
    }

}