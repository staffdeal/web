<?php
/**
 * @package Backend
 * @author daniel 
 */

class Epr_View_Helper_PaginateInfoHelper extends Zend_View_Helper_Abstract {

    public function paginateInfoHelper ($result) {
        $output = '';
        if ($result instanceof \Doctrine\CouchDB\View\Result) {
            $start = $result->getOffset();
            $end = $start + $result->count();
            $output .= 'Showing ' . ($start + 1) . ' - ' . $end . ' of ' . $result->getTotalRows();
        }
        return $output;
    }

}