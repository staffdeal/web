<?php

class Epr_View_Helper_ColorPickerHelper extends Zend_View_Helper_FormElement
{

	public function colorPickerHelper($name, $value = null, $attributes = null, $options = null)
	{
		$output = '';

		$color = ($value instanceof \Poundation\PColor) ? $value : \Poundation\PColor::colorFromString('#123456');

		$style = (isset($options[Epr_Form_Element_ColorPicker::OPTION_STYLE])) ? $options[Epr_Form_Element_ColorPicker::OPTION_STYLE] : Epr_Form_Element_ColorPicker::PICKER_STYLE_HUE;

		$class = 'colorPicker';
		if (isset($attributes['class'])) {
			$class.= ' ' . $attributes['class'];
		}

		$output .= '<input  type="text" class="' . $class . '" id="' . $name . '" name="' . $name . '" value="' . $color->getHexString() . '" />';

		$output .= '<script>
    $(document).ready(function() {
       $(\'#' . $name . '\').minicolors({
           theme: \'none\',
		   control: \'' . $style . '\',
		   letterCase: \'uppercase\'
           });
    });
</script>';


		return $output;

	}
}