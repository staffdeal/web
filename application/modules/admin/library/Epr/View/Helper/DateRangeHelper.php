<?php

class Epr_View_Helper_DateRangeHelper extends Zend_View_Helper_FormElement {

    public function dateRangeHelper($name, $value = null, $attributes = null, $options = null)
    {
        $startName = $name . '_start';
        $endName = $name . '_end';
        $class = 'date-picker';
        if (isset($attributes['class'])) {
            $class.= ' ' . $attributes['class'];
        }

        $startDate = null;
        $endDate = null;
        if ($value instanceof \Poundation\PDateRange) {
            $startDate = $value->getStartDate()->format('Y-m-d');
            $endDate = $value->getEndDate()->format('Y-m-d');
        }

        $front = Zend_Controller_Front::getInstance();
        $request = $front->getRequest();
        $oldStartValue = $request->getParam($startName,false);
        $oldEndValue = $request->getParam($endName,false);

        //if ($value == null) {
            if ($oldStartValue) {
                $startDate = $oldStartValue;
            }
            if ($oldEndValue) {
                $endDate = $oldEndValue;
            }
        //}

        $output = '<input type="text" ';
        $output.= 'id="' . $startName . '" ';
        $output.= 'name="' . $startName . '" ';
        $output.= 'class="' . $class . '" ';
        if ($startDate) {
            $output.= 'value="' . $startDate . '" ';
        }
        $output.= '/>';

        $output.= '<span class="date-picker-range-separator">–</span>';

        $output.= '<input type="text" ';
        $output.= 'id="' . $endName . '" ';
        $output.= 'name="' . $endName . '" ';
        $output.= 'class="' . $class . '" ';
        if ($endDate) {
            $output.= 'value="' . $endDate . '" ';
        }
        $output.= '/>';

        $output.= '<input type="hidden" style="width:400px"';
        $output.= 'id="' . $name . '" ';
        $output.= 'name="' . $name . '" ';
        $output.= '/>';

        $output.= '<script type="text/javascript">';

        $output.= "\t" . 'bindDateRangeFields("' . $startName . '","' . $endName . '","' . $name . '");';

        $output.= '</script>';

        return $output;
    }

}