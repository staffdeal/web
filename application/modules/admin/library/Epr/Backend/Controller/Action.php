<?php

/**
 * Index Controller for admin module
 *
 */
class Epr_Backend_Controller_Action extends Epr_Controller_Action
{

    const SORT_KEY_KEY = 'viewSortKey';
    const SORT_DIRECTION_KEY = 'viewSortDirection';
    const PAGINATE_COUNT = 'viewPaginateCount';
    const PAGINATION_OFFSET = 'viewPaginationOffset';
    const SEARCH_QUERY = 'viewSearchQuery';

    protected $_flashMessenger = null;


    public function preDispatch()
    {
        parent::preDispatch();
        $this->_setLanguage();

        $this->_getTopSearch();
        $this->_getNavigation();
        $this->_getNavRight();

        $this->_helper->layout()->setLayout('admin');

        $this->view->assign('title', Zend_Registry::get('client')->info->name);

        $controllerName = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
        $actionName = Zend_Controller_Front::getInstance()->getRequest()->getActionName();
        $settingsSortKey = 'admin_' . $controllerName . '_' . $actionName . '_sort_key';
        $settingsSortDir = 'admin_' . $controllerName . '_' . $actionName . '_sort_dir';
        $settingsPaginateCount = 'admin_' . $controllerName . '_' . $actionName . '_paginate_count';
        $settingsOffset = 'admin_' . $controllerName . '_' . $actionName . '_offset';
        $settingsSearch = 'admin_' . $controllerName . '_' . $actionName . '_query';

        $sortKey = $this->getParam('sortKey', null);
        if ($sortKey) {
            $this->setSortKey($sortKey);
            $sortDir = $this->getParam('sortDirection', 'asc');
            if ($sortDir == 'asc') {
                $this->setSortDirection(SORT_ASC);
            } else if ($sortDir == 'desc') {
                $this->setSortDirection(SORT_DESC);
            }

            _user()->setAdminSetting($settingsSortKey, $this->getSortKey());
            _user()->setAdminSetting($settingsSortDir, $this->getSortDirection());
        } else {
            $this->setSorting(_user()->getAdminSetting($settingsSortKey), _user()->getAdminSetting($settingsSortDir));
        }

        $paginateCount = $this->getParam('count', null);
        if ($paginateCount) {
            $paginateCount = max(5, (int)$paginateCount);
            $this->setPaginateCount($paginateCount);
            _user()->setAdminSetting($settingsPaginateCount, $paginateCount);
            _user()->setAdminSetting($settingsOffset, 0);
        } else {
            $this->setPaginateCount(_user()->getAdminSetting($settingsPaginateCount, 10));
        }

        $offset = $this->getParam('offset', null);
        if (!is_null($offset)) {
            $offset = abs((int)$offset);
            $this->setPaginationOffset($offset);
            //_user()->setAdminSetting($settingsOffset, $offset);
        } else {
            //$this->setPaginationOffset(_user()->getAdminSetting($settingsOffset, 0));
            $this->setPaginationOffset(0);
        }

        $searchQuery = $this->getParam('q', null);
        $allParams = $this->getRequest()->getParams();
        if (!isset($allParams['q'])) {
            $this->setSearchQuery(_user()->getAdminSetting($settingsSearch));
        } else {
            if (is_string($searchQuery) && strlen($searchQuery) > 0) {
                $this->setSearchQuery($searchQuery);
                _user()->setAdminSetting($settingsSearch, $searchQuery);
            } else {
                _user()->setAdminSetting($settingsSearch, null);
            }
        }
    }


    private function _setLanguage()
    {
        $language = $this->getRequest()->getParam('language', 'de');

        switch ($language) {
            case "de":
                $locale = 'de_DE';
                break;
            case "en":
                $locale = 'en_US';
                break;
            default:
                $locale = "de_DE";
                break;
        }

        $zl = new Zend_Locale();
        $zl->setLocale($locale);
        Zend_Registry::set("Zend_Locale", $zl);
        _logger()->info('Current Language = "' . $language . '"');
    }

    private function _getTopSearch()
    {
        $view = new Zend_View();
        $view->setBasePath(APPLICATION_PATH . 'modules/admin/views/placeholder/');

        $this->view->placeholder('top-search')->append($view->render('top-search.phtml'));

    }

    private function _getNavigation()
    {
        $view = new Zend_View();
        $view->setBasePath(APPLICATION_PATH . 'modules/admin/views/placeholder/');
        $view->assign('controller', $this->getRequest()->getControllerName());
        $view->assign('action', $this->getRequest()->getActionName());

        $this->view->placeholder('navigation')->append($view->render('navigation.phtml'));
    }

    private function _getNavRight()
    {
        $view = new Zend_View();
        $view->setBasePath(APPLICATION_PATH . 'modules/admin/views/placeholder/');

        $this->view->placeholder('navigation-right')->append($view->render('nav-right.phtml'));
    }

    public function setSortKey($key)
    {
        Zend_Registry::set(self::SORT_KEY_KEY, $key);

        return $this;
    }

    public function getSortKey()
    {
        if (Zend_Registry::isRegistered(self::SORT_KEY_KEY)) {
            return Zend_Registry::get(self::SORT_KEY_KEY);
        } else {
            return null;
        }
    }

    public function setSortDirection($direction)
    {
        Zend_Registry::set(self::SORT_DIRECTION_KEY, $direction);

        return $this;
    }

    public function getSortDirection()
    {
        if (Zend_Registry::isRegistered(self::SORT_DIRECTION_KEY)) {
            return Zend_Registry::get(self::SORT_DIRECTION_KEY);
        } else {
            return SORT_ASC;
        }
    }

    public function setSorting($key, $direction = SORT_ASC)
    {
        return $this->setSortKey($key)->setSortDirection($direction);
    }

    public function setDefaultSorting($key, $direction = SORT_ASC)
    {
        if (!$this->getSortKey()) {
            $this->setSorting($key, $direction);
        }

        return $this;
    }

    public function setPaginateCount($count)
    {
        Zend_Registry::set(self::PAGINATE_COUNT, $count);

        return $this;
    }

    public function getPaginateCount()
    {
        $count = 0;
        if (Zend_Registry::isRegistered(self::PAGINATE_COUNT)) {
            $count = (int)Zend_Registry::get(self::PAGINATE_COUNT);
        }

        return max(5, $count);
    }

    public function setPaginationOffset($offset)
    {
        Zend_Registry::set(self::PAGINATION_OFFSET, $offset);

        return $this;
    }

    public function getPaginationOffset()
    {
        $offset = 0;
        if (Zend_Registry::isRegistered(self::PAGINATION_OFFSET)) {
            $offset = Zend_Registry::get(self::PAGINATION_OFFSET, $offset);
        }

        return $offset;
    }

    public function getSearchQuery()
    {
        $query = null;
        if (Zend_Registry::isRegistered(self::SEARCH_QUERY)) {
            $query = Zend_Registry::get(self::SEARCH_QUERY);
        }
        return $query;
    }

    public function setSearchQuery($query) {
        if (is_string($query) && strlen($query) > 8) {
            $query = str_replace('default:', '', $query);
        }
        Zend_Registry::set(self::SEARCH_QUERY, (string)$query);
        return $this;
    }

}