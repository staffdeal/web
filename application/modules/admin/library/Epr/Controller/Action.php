<?php

/**
 * EPR Controller Action
 *
 */
class Epr_Controller_Action extends Zend_Controller_Action
{

	/**
	 * @return Epr_Controller_Action_Helper_SysFlashMessenger|null
	 */
	protected function getSysFlashMessenger()
	{
		$messenger = $this->getHelper('sysFlashMessenger');

		return ($messenger instanceof Epr_Controller_Action_Helper_SysFlashMessenger) ? $messenger : null;
	}

	/**
	 * Builds an custom URL, including language parameter.
	 *
	 * @param string $action
	 * @param string $controller
	 * @param string $module
	 * @param string $options
	 *
	 * @return Zend_Controller_Action_Helper_Url
	 */
	protected function getCustomURL($action, $controller = null, $module = null, $options = array())
	{

		$helper = $this->getHelper('CustomUrl');
		if ($helper instanceof Epr_Controller_Action_Helper_CustomUrl) {
			return $helper->custom($action, $controller, $module, $options);
		} else {
			return null;
		}
	}

	public function postDispatch()
	{
		if ($this->getRequest()->getControllerName() != 'static') {

			$ignoredRequest = array(
				'default' => array(
					'user' => array(
						'login',
						'logout',
						'verify',
						'registered',
						'reset-password',
						'new-password'
					)
				),
				'admin'   => array(
					'auth' => array(
						'login',
						'logout'
					)
				)
			);

			$module     = (isset($ignoredRequest[$this->getRequest()->getModuleName()])) ? $ignoredRequest[$this->getRequest()->getModuleName()] : array();
			$controller = (isset($module[$this->getRequest()->getControllerName()])) ? $module[$this->getRequest()->getControllerName()] : array();
			if (array_search($this->getRequest()->getActionName(), $controller) === false) {
				$navigationSession          = new Zend_Session_Namespace('navigation');
				$navigationSession->lastURL = $_SERVER['REQUEST_URI'];
			}
		}
	}

	protected function redirectToLastURL()
	{
		$navigationSession = new Zend_Session_Namespace('navigation');
		if (isset($navigationSession->lastURL)) {
			$this->redirect($navigationSession->lastURL);

			return true;
		}

		return false;
	}
}
