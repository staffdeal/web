<?php

/**
 * Index Controller for admin module
 *
 */
//use DateTime;
class Admin_DealsController extends Epr_Backend_Controller_Action
{

    public function indexAction()
    {
        $this->redirect($this->getCustomURL('list', 'deals', 'admin'));
    }

    public function listAction()
    {

        $this->setDefaultSorting('startDate', SORT_DESC);

        $this->view->assign('pageHeading', $this->view->translate('Deals List'));

        $searchQuery = $this->getSearchQuery();
        if ($searchQuery) {
            $deals = Epr_Deal_Collection::getDealsMatching($searchQuery, $this->getSortKey(), $this->getSortDirection(), $this->getPaginateCount(), $this->getPaginationOffset());
            if ($deals instanceof \Doctrine\CouchDB\View\LuceneResult) {
                $this->setSearchQuery($deals->getExecutedQuery());
            }
        } else {
            $deals = Epr_Deal_Collection::getSorted($this->getSortKey(), $this->getSortDirection(), $this->getPaginateCount(), $this->getPaginationOffset());
        }

        $this->view->assign('editUrl', $this->getCustomURL('update', 'deals', 'admin'));
        //$this->view->assign('previewUrl', $this->getCustomURL('preview', 'deals', 'default'));
        $this->view->assign('deleteUrl', $this->getCustomURL('delete-deal', 'deals', 'admin'));
        $this->view->assign('deactivateUrl', $this->getCustomURL('activate-deal', 'deals', 'admin'));
        $this->view->assign('featureURL', $this->getCustomURL('feature-deal', 'deals', 'admin'));

        $this->view->assign('deals', $deals);
    }

    public function updateAction()
    {
        $this->view->assign('pageHeading', $this->view->translate('Add Deal'));

        $id = $this->_request->getParam('id', false);

        $deal      = null;
        $isNewDeal = false;

        if ($id) {
            $deal = _dm()->getRepository('Epr_Deal')->find($id);
        }

        $form = Epr_Form_Deal::dealForm($deal);

        if ($this->_request->isPost() && $form->isValid($this->_request->getParams())) {
            if (is_null($deal)) {
                $deal      = new Epr_Deal();
                $isNewDeal = true;
            }

            $deal->setTitle($form->getValue('title'));
            $deal->setText($form->getValue('text'));
            $deal->setTeaser($form->getValue('teaser'));

            $period = $form->getValue('period');
            if ($period instanceof \Poundation\PDateRange) {
                $deal->setStartDate($period->getStartDate());
                $deal->setEndDate($period->getEndDate());
            }

            $titleImage = $form->getValue('titleImage');
            if ($titleImage instanceof Epr_Content_Image) {
                $deal->setTitleImage($titleImage);
            } else {
                $deal->removeTitleImage();
            }

            $deal->setAsFeatured($form->getValue('isFeatured'));
            $deal->setActive($form->getValue('isActive'));
            $deal->setIsManuallySoldOut($form->getValue('isSoldOut'));

            $deal->setPrice($form->getValue('price'));
            $deal->setOldPrice($form->getValue('oldPrice'));
            $deal->setReminderText($form->getValue('reminder'));

            $codeData           = array();
            $onsiteCodeModule   = $deal->getOnsiteCodeModule();
            $onsiteModuleFields = $onsiteCodeModule->getSettingsFormFields();
            foreach ($onsiteModuleFields as $moduleField) {
                $savedField = $form->getElement($moduleField->getName());
                $onsiteCodeModule->saveSettingsField($savedField);
            }
            $codeData[$onsiteCodeModule->getDataKey()] = $onsiteCodeModule->getData();

            $onlineCodeModule       = $deal->getOnlineCodeModule();
            $onlineCodeModuleFields = $onlineCodeModule->getSettingsFormFields();
            foreach ($onlineCodeModuleFields as $moduleField) {
                $savedField = $form->getElement($moduleField->getName());
                $onlineCodeModule->saveSettingsField($savedField);
            }
            $codeData[$onlineCodeModule->getDataKey()] = $onlineCodeModule->getData();
            $deal->setCodeModulesData($codeData);


            $dealersModule = _app()->getModuleWithIdentifier(Epr_Modules_Dealers::identifier());
            if ($dealersModule instanceof Epr_Modules_Dealers) {

                $deal->resetDealerCategories();

                $i = 0;
                foreach ($dealersModule->getCategories() as $category => $fields) {

                    $name = 'addCat' . $i;

                    $categoryValue = $form->getValue($name);
                    if (is_array($categoryValue)) {
                        foreach ($categoryValue as $selectedFieldIndex) {
                            $selectedFieldIndex = (int)$selectedFieldIndex;
                            $deal->selectDealerCategoryField($i, $selectedFieldIndex);
                        }
                    }

                    $i++;
                }

            }

            try {
                if ($isNewDeal) {
                    _dm()->persist($deal);
                }

                Epr_Deal_Collection::invalidateCacheForProvidedDeals();
                Epr_Deal_Collection::invalidateCacheForDealersForDeal($deal->getId());

                $this->redirect($this->getCustomURL('list', 'deals', 'admin'));
            } catch (Exception $e) {
                $this->getSysFlashMessenger()->error($this->view->translate('Item could not be saved'));
                $this->getSysFlashMessenger()->info($e->getMessage());
            }

            Epr_Deal_Collection::invalidateCacheForProvidedDeals();

        }

        $this->view->assign('form', $form);

    }

    public function activateDealAction()
    {
        $id = $this->_request->getParam('id', false);

        $deal = _dm()->getRepository('Epr_Deal')->find($id);
        if ($deal instanceof Epr_Deal) {
            $active = $deal->isActive() ? false : true;
            $deal->setActive($active);

            $this->getSysFlashMessenger()->setStatic()->success(!$active ? 'Item deactivated' : 'Item activated.');

            Epr_Deal_Collection::invalidateCacheForProvidedDeals();
            Epr_Deal_Collection::invalidateCacheForDealersForDeal($deal->getId());

            $this->redirect($this->getCustomURL('list', 'deals', 'admin'));
        }

        $this->getSysFlashMessenger()->setStatic()->error('Updating Item failed');
        $this->redirect($this->getCustomURL('list', 'deals', 'admin'));
    }

    public function deleteDealAction()
    {
        $id   = $this->_request->getParam('id', false);
        $deal = _dm()->getRepository('Epr_Deal')->find($id);
        if ($deal instanceof Epr_Deal) {
            _dm()->remove($deal);
            $this->getSysFlashMessenger()->setStatic()->success('Item removed.');

            Epr_Deal_Collection::invalidateCacheForProvidedDeals();
            Epr_Deal_Collection::invalidateCacheForDealersForDeal($deal->getId());

            $this->redirect($this->getCustomURL('list', 'deals', 'admin'));
        }

        $this->getSysFlashMessenger()->setStatic()->error('Given ID is no Deal.');
        $this->redirect($this->getCustomURL('list', 'deals', 'admin'));
    }

    public function featureDealAction() {
        $id   = $this->_request->getParam('id', false);
        $deal = _dm()->getRepository('Epr_Deal')->find($id);
        if ($deal instanceof Epr_Deal) {
            $deal->setAsFeatured(!$deal->isFeatured());
            Epr_Deal_Collection::invalidateCacheForProvidedDeals();
            Epr_Deal_Collection::invalidateCacheForDealersForDeal($deal->getId());
        }
        $this->redirect($this->getCustomURL('list', 'deals', 'admin'));
    }

    public function codeGeneratorAction()
    {

        $form = Epr_Form_CodeGenerator::form();

        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getParams())) {

            $prefix   = $form->getValue(Epr_Form_CodeGenerator::FIELD_PREFIX);
            $appendix = $form->getValue(Epr_Form_CodeGenerator::FIELD_APPENDIX);

            $start = (int)$form->getValue(Epr_Form_CodeGenerator::FIELD_START_NUMBER);
            $end   = (int)$form->getValue(Epr_Form_CodeGenerator::FIELD_END_NUMBER);

            $doStart = min($start, $end);
            $doEnd   = max($start, $end);

            if ($doEnd - $doStart <= 10000) {

                $format = '%0' . strlen((string)$doEnd) . 'd';

                $codes = array();
                for ($i = $doStart; $i <= $doEnd; $i++) {
                    $currentNumber = sprintf($format, $i);
                    $codes[]       = $prefix . $currentNumber . $appendix;
                }

                $codesField = new Zend_Form_Element_Textarea('generatedCodes');
                $codesField->setValue(implode("\n", $codes));
                $codesField->setLabel('Generated Codes');
                $codesField->setAttrib('class', 'inp-form wide');
                $codesField->setDescription('Copy and paste these codes.');
                $form->addElement($codesField);
            } else {
                $form->getElement(Epr_Form_CodeGenerator::FIELD_END_NUMBER)->addError('You cannot generate more than 10.000 codes at once.');
            }


        }

        $this->view->assign('form', $form);

    }

}
