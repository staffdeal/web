<?php

class Admin_DealersController extends Epr_Backend_Controller_Action
{

	public function indexAction()
	{
		$this->redirect($this->getCustomURL('list', 'dealers', 'admin'));
	}

	public function listAction()
	{
        $this->view->assign('pageHeading', $this->view->translate('Dealers List'));

        $this->setDefaultSorting('title', SORT_ASC);
        $searchQuery = $this->getSearchQuery();

		if ($searchQuery) {
			$dealers = Epr_Dealer_Collection::getDealersMatching($searchQuery, $this->getSortKey(), $this->getSortDirection(), $this->getPaginateCount(), $this->getPaginationOffset());
			if ($dealers instanceof \Doctrine\CouchDB\View\LuceneResult) {
				$this->setSearchQuery($dealers->getExecutedQuery());
			}
		} else {
			$dealers = Epr_Dealer_Collection::getSorted($this->getSortKey(), $this->getSortDirection(), $this->getPaginateCount(), $this->getPaginationOffset());
		}


        $this->view->assign('dealers', $dealers);

		$this->view->assign('editUrl', $this->getCustomURL('update', 'dealers', 'admin'));
		$this->view->assign('deleteUrl', $this->getCustomURL('delete', 'dealers', 'admin'));
		$this->view->assign('toggleActivationUrl', $this->getCustomURL('toggle-activation', 'dealers', 'admin'));
		$this->view->assign('geocodeUrl', $this->getCustomURL('geocode', 'dealers', 'admin'));
	}

	public function deleteAllWithoutLocationAction()
	{

		if (_user()->isRoot()) {
			$dealers = Epr_Dealer_Collection::getAll();

			$dealersToRemove = array();
			foreach ($dealers as $dealer) {
				if ($dealer instanceof Epr_Dealer) {
					if (is_null($dealer->getAddress()) || ($dealer->getAddress() && is_null($dealer->getAddress()->getCoordinate()))) {
						$dealersToRemove[] = $dealer;
					}
				}
			}

			unset($dealers);

			foreach ($dealersToRemove as $dealerToRemove) {
				_dm()->remove($dealerToRemove);
			}
		}

		$this->redirect($this->getCustomURL('list', 'dealers', 'admin'));
	}

	public function updateAction()
	{

		$id = $this->_request->getParam('id', false);

		$dealer      = null;
		$isNewDealer = true;

		$titleText = $this->view->translate('Create Dealer');

		if ($id) {
			$dealer    = _dm()->find(Epr_Dealer::DOCUMENTNAME, $id);
			$titleText = $this->view->translate('Edit Dealer');
		}

		$this->view->assign('pageHeading', $titleText);

		$module     = Epr_Modules_Dealers::sharedModule();
		$categories = array();
		if ($module instanceof Epr_Modules_Dealers) {
			$categories = $module->getCategories();
		}

		$form = Epr_Form_Dealer::dealerForm($categories, $dealer);

		if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getParams())) {

			if (is_null($dealer)) {
				$isNewDealer = true;
				$dealer      = new Epr_Dealer();
			}

			$dealer->setTitle($form->getValue(Epr_Form_Dealer::DEALER_FORM_NAME));

			$address = new \Poundation\PAddress();
			$dealer->setAddress($address);

			$address->setStreet($form->getValue(Epr_Form_Dealer::DEALER_FORM_STREET));
			$address->setZip($form->getValue(Epr_Form_Dealer::DEALER_FORM_ZIP));
			$address->setCity($form->getValue(Epr_Form_Dealer::DEALER_FORM_CITY));
			$address->setCountry($form->getValue(Epr_Form_Dealer::DEALER_FORM_COUNTRY));

			$rawLat     = $form->getValue(Epr_Form_Dealer::DEALER_FORM_LAT);
			$rawLon     = $form->getValue(Epr_Form_Dealer::DEALER_FORM_LON);
			$coordinate = null;
			if (strlen($rawLat) > 0 && strlen($rawLon) > 0) {
				$coordinate = \Poundation\PCoordinate::createCoordinate((float)$rawLat, (float)$rawLon);
			}
			if ($coordinate instanceof \Poundation\PCoordinate) {
				$address->setCoordinate($coordinate);
			} else {
				$address->removeCoordinate();
			}

			$logo = $form->getValue(Epr_Form_Dealer::DEALER_FORM_LOGO);
			if ($logo instanceof Epr_Content_Image) {
				$dealer->setTitleImage($logo);
			} else {
				$dealer->removeTitleImage();
			}

			$dealer->setTelephone($form->getValue(Epr_Form_Dealer::DEALER_FORM_TEL));
			$dealer->setWeb($form->getValue(Epr_Form_Dealer::DEALER_FORM_WEB));
			$dealer->setEmail($form->getValue(Epr_Form_Dealer::DEALER_FORM_MAIL));

			$dealer->setOpeningHours($form->getValue(Epr_Form_Dealer::DEALER_FORM_OPENINGHOURS));
			$dealer->setText($form->getValue(Epr_Form_Dealer::DEALER_FORM_DESCRIPTION));

			$dealer->setActive($form->getValue(Epr_Form_Dealer::DEALER_FORM_ACTIVE));

			$i = 0;
			foreach ($categories as $categoryName => $categoryFields) {
				$value = $form->getValue(Epr_Form_Dealer::DEALER_FORM_CATEGORIES_PREFIX . $i);
				$dealer->setAdditionCategoryValue((int)$i, (int)$value);
				$i++;
			}

			if ($isNewDealer) {
				try {
					_dm()->persist($dealer);
					$this->redirect($this->getCustomURL('list', 'dealers', 'admin') . '#' . $dealer->getId());
				} catch (Exception $e) {
					$this->getSysFlashMessenger()->setStatic()->error('Creating the dealer failed.');
				}
			}

		} else {

			$geocode = $this->getParam('geocode', null);
			if ($geocode === 'true') {

				if ($dealer instanceof Epr_Dealer && $dealer->getAddress() instanceof \Poundation\PAddress) {

					$geocoder = Epr_System::getGeocoder();
					if ($geocoder) {
						$address = (string)$dealer->getAddress();
						$result  = $geocoder->geocode($address);

						if ($result) {
							$streetElement = $form->getElement(Epr_Form_Dealer::DEALER_FORM_STREET);
							$streetElement->setValue($result->getStreetName() . ' ' . $result->getStreetNumber());
							$streetElement->setDescription('Original street: ' . $dealer->getAddress()->getStreet());
							$streetElement->setAttrib('inlineDescription',true);

							$zipElement = $form->getElement(Epr_Form_Dealer::DEALER_FORM_ZIP);
							$zipElement->setValue($result->getZipcode());
							$zipElement->setDescription('Original zip code: ' . $dealer->getAddress()->getZip());
							$zipElement->setAttrib('inlineDescription',true);

							$cityElement = $form->getElement(Epr_Form_Dealer::DEALER_FORM_CITY);
							$cityElement->setValue($result->getCity());
							$cityElement->setDescription('Original city: ' . $dealer->getAddress()->getCity());
							$cityElement->setAttrib('inlineDescription',true);

							$countryElement = $form->getElement(Epr_Form_Dealer::DEALER_FORM_COUNTRY);
							$countryElement->setValue($result->getCountry());
							$countryElement->setDescription('Original country: ' . $dealer->getAddress()->getCountry());
							$countryElement->setAttrib('inlineDescription',true);

							$latElement = $form->getElement(Epr_Form_Dealer::DEALER_FORM_LAT);
							$latElement->setValue($result->getLatitude());
							if ($dealer->getAddress()->getCoordinate()) {
								$latElement->setDescription('Original lat: ' . $dealer->getAddress()->getCoordinate()->getLatitude());
								$latElement->setAttrib('inlineDescription',true);
							}

							$lonElement = $form->getElement(Epr_Form_Dealer::DEALER_FORM_LON);
							$lonElement->setValue($result->getLongitude());
							if ($dealer->getAddress()->getCoordinate()) {
								$lonElement->setDescription('Original lon: ' . $dealer->getAddress()->getCoordinate()->getLongitude());
								$lonElement->setAttrib('inlineDescription',true);
							}

						}

					}

				}

			}

		}

		$this->view->assign('form', $form);
	}

	public function deleteAction()
	{

		$dealer = null;
		$id     = $this->_request->getParam('id', false);
		if ($id) {

			$dealer = _dm()->find(Epr_Dealer::DOCUMENTNAME, $id);
		}

		if ($dealer instanceof Epr_Dealer) {
			_dm()->remove($dealer);
		}

		$this->redirect($this->getCustomURL('list', 'dealers', 'admin'));
	}

	public function toggleActivationAction()
	{
		$dealer = null;
		$id     = $this->_request->getParam('id', false);
		if ($id) {

			$dealer = _dm()->find(Epr_Dealer::DOCUMENTNAME, $id);
		}

		if ($dealer instanceof Epr_Dealer) {
			$dealer->setActive(!$dealer->isActive());
		}

		$this->redirect($this->getCustomURL('list', 'dealers', 'admin'));
	}

	public function importAction()
	{

		$this->view->assign('pageHeading', $this->view->translate('Import CSV data'));

		$csvData = $this->getParam(Epr_Form_Dealer::IMPORT_FORM_VALUE, null);
		$form    = Epr_Form_Dealer::importForm($csvData);

		if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getParams())) {

			$csvData = str_replace(chr(10), '', $csvData);
			$rows    = explode(chr(13), $csvData);

			$data = array();

			$startLine = (int)$form->getValue(Epr_Form_Dealer::IMPORT_FORM_START_LINE);
			if ($startLine > 0) {
				$startLine--;
				for ($i = $startLine; $i < count($rows); $i++) {
					$row    = $rows[$i];
					$fields = str_getcsv($row, $form->getValue(Epr_Form_Dealer::IMPORT_FORM_DELIMITER));
					if ($fields) {
						$data[] = $fields;
					}
					unset($row);
					unset($fields);
				}
			}

			$dealers = array();

			foreach ($data as $dealerData) {

				$name = $dealerData[1];
				if (strlen($dealerData[2]) > 0) {
					$name .= ' ' . $dealerData[2];
				}

				$name = trim($name);

				if (strlen($name) > 0) {
					$dealer = new Epr_Dealer();

					$dealer->setTitle($name);

					$street  = $dealerData[3];
					$zip     = $dealerData[4];
					$city    = $dealerData[5];
					$country = strtolower($dealerData[6]);

					if (strlen($street) > 2 && strlen($zip) > 3 && strlen($city) > 2 && strlen($country) > 0) {
						$address = new \Poundation\PAddress();
						$address->setStreet($street)->setZip($zip)->setCity($city);

						if ($country == 'd') {
							$country = 'Deutschland';
						} else if ($country == 'a') {
							$country = 'Österreich';
						} else if ($country == 'ch') {
							$country = 'Schweiz';
						} else if ($country == 'b') {
							$country = 'Begien';
						} else if ($country == 'i') {
							$country = 'Italien';
						} else if ($country == 'nl') {
							$country = 'Niederlande';
						} else if ($country == 'l') {
							$country = 'Luxemburg';
						}

						$address->setCountry($country);

						$dealer->setAddress($address);
					}

					$dealer->setTelephone($dealerData[11]);

					$url = __($dealerData[15]);
					if ($url->first(4) !== 'http') {
						$url = $url->ensureFirstCharacter('http://');
					}
					if (\Poundation\PURL::isValidURLString($url)) {
						$dealer->setWeb(\Poundation\PURL::URLWithString($url));
					}

					$mail = $dealerData[16];
					if (\Poundation\PMailAddress::verifyAddress($mail)) {
						$dealer->setEmail($mail);
					}

					$dealer->activate();

					_dm()->persist($dealer);

				}
			}

		}

		$this->view->assign('form', $form);

	}

	public function geocodeAction()
	{

		$dealer = null;

		$id = $this->getParam('id', null);
		if (!is_null($id)) {
			$dealer = _dm()->find(Epr_Dealer::DOCUMENTNAME, $id);
		}

		if ($dealer instanceof Epr_Dealer) {
			$this->redirect($this->getCustomURL('update', 'dealers', 'admin') . '/id/' . $id . '/geocode/true');
		} else {
			$this->redirect($this->getCustomURL('list', 'dealers', 'admin'));
		}

	}
}