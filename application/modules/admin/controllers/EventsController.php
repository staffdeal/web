<?php

class Admin_EventsController extends Epr_Backend_Controller_Action {

    public function indexAction() {
        $this->redirect($this->getCustomURL('list', 'events', 'admin'));
    }

    public function listAction() {

        $this->setDefaultSorting('date', SORT_DESC);
        $offset = $this->getPaginationOffset();

        $searchQuery = $this->getSearchQuery();
        if ($searchQuery) {
            $eventsResult = Epr_Events_Collection::getEventsMatching($searchQuery, $this->getSortKey(), $this->getSortDirection(), $this->getPaginateCount(), $offset);
            if ($eventsResult instanceof \Doctrine\CouchDB\View\LuceneResult) {
                $this->setSearchQuery($eventsResult->getExecutedQuery());
            }
        } else {
            $eventsResult = Epr_Events_Collection::getSorted($this->getSortKey(), $this->getSortDirection(), $this->getPaginateCount(), $offset);
        }


        $this->view->assign('events', $eventsResult);

        $this->view->assign('editUrl', $this->getCustomURL('update', 'events', 'admin'));
        $this->view->assign('deleteUrl', $this->getCustomURL('delete', 'events', 'admin'));
        $this->view->assign('bookingsUrl', $this->getCustomURL('bookings', 'events', 'admin'));
    }

    public function addAction() {
        $this->updateAction();
        $this->render('update');
    }

    public function updateAction() {

        $event = null;

        $eventId = $this->getRequest()->getParam('id', null);
        if (is_string($eventId)) {
            $event = Epr_Events_Collection::eventWithId($eventId);
        }
        $this->view->assign('event', $event);

        $isNew = is_null($event); // keep in mind that there was no event
        if ($isNew) {
            $event = new Epr_Event(); // create one because the form needs it
        }

        $form = new Epr_Form_Event($event);
        $this->view->assign('form', $form);

        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getParams())) {

            $event->setTitle($form->getValue(Epr_Form_Event::FIELD_TITLE));
            $event->setSlug($form->getValue(Epr_Form_Event::FIELD_SLUG));
            $event->setDate(\Poundation\PDate::createDate($form->getValue(Epr_Form_Event::FIELD_DATE)));
            $event->setTitleImage($form->getValue(Epr_Form_Event::FIELD_IMAGE));
            $event->setText($form->getValue(Epr_Form_Event::FIELD_TEXT));

            if ($form->getPublishableControlElement()->isDraft()) {
                $event->unpublish();
            } else if ($form->getPublishableControlElement()->isPublished()) {
                $event->publish();
            }

            try {
                if ($isNew) {
                    _dm()->persist($event);
                }
            } catch (Exception $e) {
                $this->getSysFlashMessenger()->error('Error saving event. Sorry');
                _logger()->error($e);
            }
            $this->redirect($this->getCustomURL('list', 'events', 'admin'));

        }
    }

    public function deleteAction() {
        $event = null;

        $eventId = $this->getRequest()->getParam('id', null);
        if (is_string($eventId)) {
            $event = Epr_Events_Collection::eventWithId($eventId);
        }

        if ($event) {
            _dm()->remove($event);
        }

        $this->redirect($this->getCustomURL('list', 'events', 'admin'));
    }

    public function bookingsAction() {
        $event = null;

        $eventId = $this->getRequest()->getParam('id', null);
        if (is_string($eventId)) {
            $event = Epr_Events_Collection::eventWithId($eventId);
        }

        if ($event) {
            $this->view->assign('event', $event);
            $this->view->assign('downloadUrlCsv', $this->getCustomURL('download-list', 'events', 'admin', array('id' => $event->getId(), 'format' => 'csv')));
        } else {
            $this->redirect($this->getCustomURL('list', 'events', 'admin'));
        }
    }

    public function downloadListAction() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $event = null;

        $eventId = $this->getRequest()->getParam('id', null);
        if (is_string($eventId)) {
            $event = Epr_Events_Collection::eventWithId($eventId);
        }

        if ($event) {
            $supportedFormats = array('csv');
            $format = ($this->getRequest()->getParam('format', NULL));
            if (is_null($format) || array_search($format, $supportedFormats) === false) {
                $format = $supportedFormats[0];
            }

            if ($format == 'csv') {

                $separator = $this->getRequest()->getParam('seperator',';');
                if (!is_string($separator) || strlen($separator) < 1) {
                    $separator = ';';
                }

                $this->getResponse()->setHeader('Content-Description', 'File Transfer');
                $this->getResponse()->setHeader('Content-Type', 'text/csv; charset=utf-8');
                $this->getResponse()->setHeader('Content-Disposition', 'attachment; filename=' . __($event->getTitle())->slug() . '-export.csv');
                $this->getResponse()->setHeader('Content-Transfer-Encoding', 'binary');
                $this->getResponse()->setHeader('Expires', '0');
                $this->getResponse()->setHeader('Cache-control', 'private, must-revalidate');
                $this->getResponse()->setHeader('Pragma', 'public');

                $rows = array(array('Name', 'Email', 'Regisration Info', 'Date'));

                foreach ($event->getBookings() as $booking) {
                    if ($booking instanceof Epr_Event_Booking) {
                       if($booking->getUser()!=null && $booking->getUser()->getFullname() != ''){
                        $rows[] = array($booking->getUser()->getFullname(), $booking->getUser()->getEmail(), $booking->getUser()->getRegistrationInformation(), $booking->getDate()->getFormatedString('Y/m/d h:i'));
                       }
                    }
                }

                $stream = fopen('php://temp/maxmemory:'. (5*1024*1024), 'r+');
                foreach ($rows as $row) {
                    fputcsv($stream, $row, $separator);
                }
                rewind($stream);
                $prefix = '';
                if ($this->getRequest()->getParam('includeSeperator', false)) {
                    $prefix = 'sep=' . $separator . "\n";
                }
                $csv = $prefix . stream_get_contents($stream);
                fclose($stream);
                $this->getResponse()->setBody($csv);
            }

        }
    }

}