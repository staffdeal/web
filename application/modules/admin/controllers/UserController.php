<?php

/**
 * Index Controller for admin module
 *
 */
class Admin_UserController extends Epr_Backend_Controller_Action
{

    const TEST_MESSAGE_CONTEXT = 'test';

    public function indexAction()
    {
        $this->redirect($this->getCustomURL('list', 'user', 'admin'));
    }

    public function listAction()
    {
        $this->view->assign('pageHeading', $this->view->translate('User List'));
        $this->setDefaultSorting('email', SORT_ASC);

        $searchQuery = $this->getSearchQuery();
        if ($searchQuery) {
            $users = Epr_User_Collection::getUsersMatching($searchQuery, $this->getSortKey(), $this->getSortDirection(), $this->getPaginateCount(), $this->getPaginationOffset());
            if ($users instanceof \Doctrine\CouchDB\View\LuceneResult) {
                $this->setSearchQuery($users->getExecutedQuery());
            }
        } else {
            $users = Epr_User_Collection::getSorted($this->getSortKey(), $this->getSortDirection(), $this->getPaginateCount(), $this->getPaginationOffset());
        }

        $this->view->assign('editUrl', $this->getCustomURL('update', 'user', 'admin'));
        $this->view->assign('tokensUrl', $this->getCustomURL('tokens', 'user', 'admin'));
        $this->view->assign('deleteUrl', $this->getCustomURL('delete', 'user', 'admin'));
        $this->view->assign('deactivateUrl', $this->getCustomURL('deactivate', 'user', 'admin'));

        $this->view->assign('users', $users);

    }

    public function tokensAction()
    {

        $id = $this->_request->getParam('id', false);

        $tokens = null;

        if ($id) {
            $user = _dm()->getRepository('Epr_User')->find($id);
            $this->view->assign('pageHeading', $this->view->translate("User's tokens (" . $user->getEmail() . ')'));
            $tokens = new \Poundation\PArray(Epr_Token_Collection::getTokensForUser($user));
            $this->view->assign('items', $tokens);
        } else {
            $this->view->assign('pageHeading', $this->view->translate('Tokens'));
            $tokens = new \Poundation\PArray(Epr_Token_Collection::getAllTokens());
        }

        $tokens->sortUsingSortDescriptor(new \Poundation\PSortDescriptor('lastUsageDate', SORT_DESC));

        $this->view->assign('items', $tokens);

        $this->view->assign('deleteUrl', $this->getCustomURL('delete-token', 'user', 'admin'));
        $this->view->assign('sendMessageUrl', $this->getCustomURL('send-test-message', 'user', 'admin'));

    }

    public function deleteTokenAction()
    {

        $success = false;
        $error   = null;

        $id = $this->_request->getParam('id', false);
        if ($id) {
            $token = _dm()->find('Epr_Token', $id);
            if ($token instanceof Epr_Token) {
                try {
                    _dm()->remove($token);
                    $success = true;
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
            }
        }

        if ($success == false) {
            $this->getSysFlashMessenger()->error('Unable to delete token.');
            if (!is_null($error)) {
                $this->getSysFlashMessenger()->info($error);
            }
        }

        $this->redirect($this->getCustomURL('tokens', 'user', 'admin'));
    }

    public function deleteAction()
    {
        $id   = $this->_request->getParam('id', false);
        $user = _dm()->getRepository('Epr_User')->find($id);
        if ($user instanceof Epr_User) {
            _dm()->remove($user);
            $this->getSysFlashMessenger()->success('User removed.');
            $this->redirect($this->getCustomURL('list', 'user', 'admin'));
        }

        $this->getSysFlashMessenger()->error('Given ID is no User.');
        $this->redirect($this->getCustomURL('list', 'user', 'admin'));

    }

    public function deleteNotificationTokensAction()
    {

        $tokens = Epr_Token_Collection::getAllTokens();
        foreach ($tokens as $token) {
            if ($token instanceof Epr_Token) {
                if ($token->getType() == Epr_Token::TYPE_IOS_APN || $token->getType() == Epr_Token::TYPE_ANDROID_GCM) {
                    _dm()->remove($token);
                }
            }
        }

        $this->redirect($this->getCustomURL('tokens', 'user', 'admin'));

    }

    public function deleteApiTokensAction()
    {

        $tokens = Epr_Token_Collection::getAllTokens();
        foreach ($tokens as $token) {
            if ($token instanceof Epr_Token) {
                if ($token->getType() == Epr_Token::TYPE_API_ACCESS) {
                    _dm()->remove($token);
                }
            }
        }

        $this->redirect($this->getCustomURL('tokens', 'user', 'admin'));

    }

    public function deactivateAction()
    {
        $id   = $this->_request->getParam('id', false);
        $user = _dm()->getRepository('Epr_User')->find($id);
        if ($user instanceof Epr_User) {
            if ($user->isActive()) {
                $user->deactivate();
                $this->getSysFlashMessenger()->success('User deactivated.', true);
            } else {
                $user->activate();
                $this->getSysFlashMessenger()->success('User activated.', true);
            }

            $this->redirect($this->getCustomURL('list', 'user', 'admin'));
        }

        $this->getSysFlashMessenger()->error('Given ID is no User.', true);
        $this->redirect($this->getCustomURL('list', 'user', 'admin'));
    }

    public function updateAction()
    {
        $this->view->assign('pageHeading', $this->view->translate('Add User'));

        $request = $this->getRequest();

        $id = $request->getParam('id', false);

        $user = null;

        if ($id) {
            $user = _dm()->getRepository('Epr_User')->find($id);
        }
        $form = Epr_Form_User::userForm($user);

        if ($request->isPost() && $form->isValid($request->getParams())) {

            $isNewUser = false;
            $user      = ($id) ? _dm()->getRepository('Epr_User')->find($id) : null;
            if (is_null($user)) {
                $user      = new Epr_User($form->getValue('email'), Epr_User::REGISTRATION_CONTEXT_ADMIN);
                $isNewUser = true;
            }
            $user->setEmail($form->getValue('email'));
            $user->setLastname($form->getValue('lastname'));
            $user->setFirstname($form->getValue('firstname'));

            $selectedRole = $form->getValue('role');
            if (_user()->getRole() != Epr_Roles::ROLE_ROOT) {
                if ($selectedRole == Epr_Roles::ROLE_ROOT) {
                    $selectedRole = Epr_Roles::ROLE_ADMIN;
                }
            }
            $user->setRole($selectedRole);
            $user->setActive($form->getValue('isActive'));

            $password       = $form->getValue('password', false);
            $passwordRepeat = $form->getValue('password_repeat', false);

            if ($password && $passwordRepeat && $password == $passwordRepeat) {
                $user->setPassword($password);
            }

            try {
                _dm()->persist($user);
                $this->redirect($this->getCustomURL('list', 'user', 'admin'));
            } catch (Exception $e) {
                $this->getSysFlashMessenger()->error($this->view->translate('Item could not be saved'));
                $this->getSysFlashMessenger()->info($e->getMessage());
            }
        }

        $this->view->assign('form', $form);

    }

    public function sendTestMessageAction()
    {

        $notification = null;
        $tokenId      = $this->_getParam('token', null);
        if (!is_null($tokenId)) {

            $token = Epr_Token_Collection::getTokenWithId($tokenId);

            if ($token instanceof Epr_Token) {

                $notification = $token->createNotification('This is a test notification.', null, self::TEST_MESSAGE_CONTEXT);

                $reason = $this->_getParam('reason', null);
                if (is_string($reason) && strlen($reason) > 0) {
                    $notification->setReason($reason);
                }

                if ($notification instanceof Epr_Notification) {
                    Epr_Notification_Queue::getQueue()->addNotification($notification);
                }

            }
        }

        if ($notification instanceof Epr_Notification) {
            $this->redirect($this->getCustomURL('message-queue', 'user', 'admin') . '#' . $notification->getId());
        } else {
            $this->redirect($this->getCustomURL('tokens', 'user', 'admin'));
        }

    }

    public function messageQueueAction()
    {
        $this->view->assign('pageHeading', $this->view->translate('Message Queue'));

        $messages = new \Poundation\PArray(Epr_Notification_Collection::getAllNotifications());

        $messages->sortUsingSortDescriptor(new \Poundation\PSortDescriptor('creationDate', SORT_DESC));

        $this->view->assign('messages', $messages);
    }

    public function deleteSentMessagesAction()
    {

        $messages = Epr_Notification_Collection::getNotificationsByStatus(Epr_Notification::STATUS_SENT);
        foreach ($messages as $message) {
            _dm()->remove($message);
        }

        $this->redirect($this->getCustomURL('message-queue', 'user', 'admin'));
    }

    public function deleteFailedMessagesAction()
    {

        $messages = Epr_Notification_Collection::getNotificationsByStatus(Epr_Notification::STATUS_FAILED);
        foreach ($messages as $message) {
            _dm()->remove($message);
        }

        $this->redirect($this->getCustomURL('message-queue', 'user', 'admin'));
    }

    public function cancelMessageAction()
    {
        $message = null;

        $id = $this->getRequest()->getParam('id', null);
        if (is_string($id)) {
            $message = Epr_Notification_Collection::getNotificationWithId($id);
        }

        if ($message instanceof Epr_Notification_Abstract) {
            $message->cancel();
        }

        $this->redirect($this->getCustomURL('message-queue', 'user', 'admin'));
    }

}
