<?php

/**
 * Index Controller for admin module
 *
 */
class Admin_AuthController extends Epr_Controller_Action
{

	public function loginAction()
	{
        // forwarding to the frontend login
        $this->redirect($this->getCustomURL('login', 'user', 'default'));
        return;
	}

	public function logoutAction()
	{
		_logger()->info('logout');
		$this->getHelper('viewRenderer')->setNoRender();

		$request = $this->getRequest();
		$auth    = Zend_Auth::getInstance();
		$auth->clearIdentity();
		//Epr_System::setUser(new Epr_User());
		$this->redirect($this->getCustomURL('login', 'user', 'default'));
	}

}
