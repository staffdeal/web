<?php

use Poundation\PDictionary;

/**
 * Index Controller for admin module
 *
 */
class Admin_NewsController extends Epr_Backend_Controller_Action
{

	public function preDispatch()
	{
		parent::preDispatch();
		$this->buildRelative();
	}

	public function indexAction()
	{
		$this->redirect($this->getCustomURL('list', 'news', 'admin'));
	}

	public function listAction()
	{
        $this->setDefaultSorting('publishDate', SORT_DESC);

		$searchQuery = $this->getSearchQuery();
		if ($searchQuery) {
			$newsResult = Epr_News_Collection::getMatchingNews($searchQuery, $this->getSortKey(), $this->getSortDirection(), $this->getPaginateCount(), $this->getPaginationOffset());
			if ($newsResult instanceof \Doctrine\CouchDB\View\LuceneResult) {
				$this->setSearchQuery($newsResult->getExecutedQuery());
			}
		} else {
			$newsResult = Epr_News_Collection::getSorted($this->getSortKey(), $this->getSortDirection(), $this->getPaginateCount(), $this->getPaginationOffset());
		}

        $this->view->assign('pageHeading', $this->view->translate('News List'));

        $this->view->assign('firstUrl', $this->getCustomURL('list', 'news', 'admin') . '/start/0');
		$this->view->assign('previousUrl', $this->getCustomURL('list', 'news', 'admin') . '/start/3');
		$this->view->assign('nextUrl', $this->getCustomURL('list', 'news', 'admin', array('start' => 5)));
		$this->view->assign('lastUrl', $this->getCustomURL('list', 'news', 'admin') . '/start/7');

		$this->view->assign('editUrl', $this->getCustomURL('update', 'news', 'admin'));
		$this->view->assign('previewUrl', $this->getCustomURL('preview', 'news', 'default'));
		$this->view->assign('deleteUrl', $this->getCustomURL('delete-news', 'news', 'admin'));
		$this->view->assign('deactivateUrl', $this->getCustomURL('activate-news', 'news', 'admin'));

		$notificationsModule = _app()->getModuleWithIdentifier(Epr_Modules_Notifications::identifier());
		if ($notificationsModule instanceof Epr_Modules_Notifications && $notificationsModule->hasUsableNotificationModules()) {
			$this->view->assign('notifyUrl', $this->getCustomURL('send-notifications', 'news', 'admin'));
		}

		$this->view->assign('news', $newsResult);
	}

	public function deleteNewsAction()
	{
		$id   = $this->getRequest()->getParam('id', false);
		$news = _dm()->getRepository('Epr_News')->find($id);
		if ($news instanceof Epr_News) {
			_dm()->remove($news);
			$this->getSysFlashMessenger()->setStatic()->success('News removed.');
			$this->redirect($this->getCustomURL('list', 'news', 'admin'));
		}
		$this->getSysFlashMessenger()->setStatic()->error('Given ID is no News.');
		$this->redirect($this->getCustomURL('list', 'news', 'admin'));
	}

	public function deleteCategoryAction()
	{

		$this->view->assign('pageHeading', $this->view->translate('Delete Category'));

		$categoryToRemove = null;

		$id = $this->_getParam('id', null);
		if (is_string($id)) {
			$categoryToRemove = Epr_News_Category::getCategoryWithId($id);
		}

		$affectedCategories = array();
		$allCategories      = Epr_News_Category_Collection::getAll();

		// find out which categories are about to be deleted
		if ($categoryToRemove instanceof Epr_News_Category) {

			$affectedCategories[] = $categoryToRemove;
			foreach ($allCategories as $candidate) {
				if ($candidate instanceof Epr_News_Category) {

					if ($candidate->hasCategory($categoryToRemove)) {
						$affectedCategories[] = $candidate;
					}
				}

				unset($candidate);
			}
		}

		// check if the categories have news
		$needToAsk = false;
		foreach ($affectedCategories as $singleCategory) {
			if ($singleCategory instanceof Epr_News_Category) {

				if ($singleCategory->getNewsCount() > 0) {
					$needToAsk = true;
					break;
				}

			}
		}

		// collect all feeds that use the categories
		$affectedFeeds = array();
		foreach (Epr_Feed_Collection::getAll() as $feed) {

			if ($feed instanceof Epr_Feed) {
				if ($feed->getCategory()) {
					if (array_search($feed->getCategory(), $affectedCategories) !== false) {
						$affectedFeeds[] = $feed;
					}
				}
			}
		}

		// there is not a single news in the categories
		if ($needToAsk == false) {

			foreach ($affectedFeeds as $feed) {
				if ($feed instanceof Epr_Feed) {
					$feed->setCategory(null);
				}
			}

			foreach ($affectedCategories as $singleCategory) {

				if ($singleCategory->getNewsCount() == 0) {
					_dm()->remove($singleCategory);
				}
			}

			$this->redirect($this->getCustomURL('list-categories', 'news', 'admin'));

			return;
		}

		// find out which categories remain
		$targetCategories = array();
		$targetSelection  = array();
		foreach ($allCategories as $category) {

			if ($category instanceof Epr_News_Category) {
				if (array_search($category, $affectedCategories) === false) {
					$targetCategories[]                  = $category;
					$targetSelection[$category->getId()] = $category->getTitle();
				}
			}

			unset($category);
		}

		$form = new Epr_Form();

		foreach ($affectedCategories as $category) {
			if ($category instanceof Epr_News_Category) {

				if ($category->getNewsCount() == 0) {

					$categoryTarget = new Zend_Form_Element_Text($category->getId());
					$categoryTarget->setValue('The category "' . $category->getTitle() . '" has no news entries.');
					$categoryTarget->setAttrib('class', 'label');
					$categoryTarget->setAttrib('disabled', 'disabled');

				} else {

					$categoryTarget = new Zend_Form_Element_Multiselect($category->getId());
					$categoryTarget->setMultiOptions($targetSelection);
					$categoryTarget->setAttrib('class', 'styledselect_form_1');
					$categoryTarget->setRequired(true);
					$categoryTarget->setDescription('Select the category to which all news entries are moved when \'' . $category->getTitle() . '\' is deleted.');
				}

				$categoryTarget->setLabel($category->getTitle());

				$form->addElement($categoryTarget);

			}

			unset($category);

		}

		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setValue('Move news entries and delete categories');
		$submit->setLabel('');
		$submit->setAttrib('class', 'form-submit');
		$form->addElement($submit);

		if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getParams())) {
			//$this->redirect($this->getCustomURL('list-categories', 'news', 'admin'));

			foreach ($affectedFeeds as $feed) {
				if ($feed instanceof Epr_Feed) {
					$feed->setCategory(null);
				}
			}

			foreach ($affectedCategories as $category) {

				if ($category instanceof Epr_News_Category) {

					$targetId = $form->getValue($category->getId());
					if (is_array($targetId) && count($targetId) > 0) {
						$targetId = $targetId[0];
					}

					if (is_string($targetId)) {
						$targetCategory = Epr_News_Category::getCategoryWithId($targetId);

						if ($targetCategory instanceof Epr_News_Category) {

							$categoryNews = $category->getNews();
							foreach ($categoryNews as $news) {

								if ($news instanceof Epr_News) {

									$news->setCategory($targetCategory);

								}

							}

							_dm()->remove($category);

						}
					}

				}

				unset($category);
			}

			$this->redirect($this->getCustomURL('list-categories', 'news', 'admin'));
		} else {
			$this->view->assign('form', $form);
		}

	}

	public function activateCategoryAction()
	{
		$id = $this->getRequest()->getParam('id', false);

		$category = _dm()->getRepository('Epr_News_Category')->find($id);
		if ($category instanceof Epr_News_Category) {
			$active = $category->isActive() ? false : true;
			$category->setActive($active);
			$category->save();

			$this->getSysFlashMessenger()->setStatic()->success(!$active ? 'Item deactivated' : 'Item activated.');

			$this->redirect($this->getCustomURL('list-categories', 'news', 'admin'));
		}
		$this->getSysFlashMessenger()->setStatic()->error('Updating Item failed');
		$this->redirect($this->getCustomURL('list-categories', 'news', 'admin'));
	}


	public function smartEditAction()
	{
		$id   = $this->getRequest()->getParam('id', false);
		$task = $this->getRequest()->getParam('task', false);
		$type = $this->getRequest()->getParam('type', false);

		if (!$id || !$type || !$task) {
			throw new Zend_Controller_Action_Exception('Nothing to edit', 0);
		}

		$object = null;

		switch ($type) {
			case 'news':
				$object = _dm()->getRepository('Epr_News')->find($id);
				break;
			case 'category':
				$object = _dm()->getRepository('Epr_News_Category')->find($id);
				break;
		}

		if (is_null($object)) {
			throw new Exception('No Object given', 0);
		}

		switch ($task) {
			case 'delete':

				break;
			case 'deactivate':
				break;
		}

	}

	public function updateAction()
	{
		$this->view->assign('pageHeading', $this->view->translate('Add/Edit News'));

		$id = $this->getRequest()->getParam('id', false);

		$news      = null;
		$isNewNews = true;

		if ($id) {
			$news      = _dm()->getRepository('Epr_News')->find($id);
			$isNewNews = false;
			$this->view->assign('subjectId', $id);
		}

		if (is_null($news)) {
			$news = new Epr_News();
		}

		$categories = _dm()->getRepository('Epr_News_Category')->findAll();
		$form       = Epr_Form_News::newsForm($news, $categories);

		if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getParams())) {

			$news->setTitle($form->getValue(Epr_Form_News::NEWS_TITLE));
			$news->setText($form->getValue(Epr_Form_News::NEWS_TEXT));
			$news->setSlug($form->getValue(Epr_Form_News::NEWS_SLUG));
			$news->setSource($form->getValue(Epr_Form_News::NEWS_SOURCE));
			$news->setTitleImage($form->getValue(Epr_Form_News::NEWS_TITLE_IMAGE));

			$category = null;
			if ($form->getValue(Epr_Form_News::NEWS_CATEGORY)) {
				$category = Epr_News_Category::getCategoryWithId($form->getValue(Epr_Form_News::NEWS_CATEGORY));
			}
			if ($category instanceof Epr_News_Category) {
				$news->setCategory($category);
			} else {
				$news->removeCategory();
			}

			$news->setPublicUrl($form->getValue(Epr_Form_News::NEWS_PUBLIC_URL));

			$publishDate = $form->getPublishableControlElement()->getPublishDate();
			if ($form->getPublishableControlElement()->getState() == Epr_Document_Publishable::STATE_DRAFT) {
				$news->unpublish();
			} else {
				$news->publishAt($publishDate);
			}

			try {
				if ($isNewNews) {
					_dm()->persist($news);
				}
				$this->redirect($this->getCustomURL('list', 'news', 'admin'));
			} catch (Exception $e) {
				$this->getSysFlashMessenger()->error($this->view->translate('Item could not be saved'));
				$this->getSysFlashMessenger()->info($e->getMessage());
			}
		}

		$this->view->assign('news', $news);
		$this->view->assign('form', $form);
	}


	public function removeNewsImageAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$id     = $this->getRequest()->getParam('imageId', false);
		$newsId = $this->getRequest()->getParam('itemId', false);
		if ($newsId !== false && $id !== false) {
			$news = _dm()->getRepository('Epr_News')->find($newsId);
			/** @var Epr_News $news */
			if ($news->removeContentImage($id)) {
				echo 'Done';
			} else {
				echo 'Failed';
			}
		} else {
			echo 'Wrong parameter';
		}

	}

	public function updateCategoryAction()
	{
		$this->view->assign('pageHeading', $this->view->translate('Add/Edit News Category'));

		$id = $this->getRequest()->getParam('id', false);

		$newsCategory  = null;
		$isNewCategory = true;

		if ($id) {
			$newsCategory  = _dm()->getRepository('Epr_News_Category')->find($id);
			$isNewCategory = false;
		}

		$allCategories = new \Poundation\PArray(_dm()->getRepository('Epr_News_Category')->findAll());
		$categories    = array();
		foreach ($allCategories as $category) {
			if ($category instanceof Epr_News_Category) {
				if ($category->getId() != $id) {
					$categories[] = $category;
				}
			}
		}

		$form = Epr_Form_News::getCategoryForm($newsCategory, $categories);

		if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getParams())) {
			if (is_null($newsCategory)) {
				$newsCategory = new Epr_News_Category();
			}

			$newsCategory->setTitle($form->getValue('name'));
			$color = $form->getValue('color');
			if ($color instanceof \Poundation\PColor) {
				$newsCategory->setColor($color);
			} else {
				$newsCategory->removeColor();
			}

			$newsCategory->setSlug($form->getValue('slug'));
			$newsCategory->setCategory(_dm()->getRepository('Epr_News_Category')->find($form->getValue('parentCategory')));
			$newsCategory->setActive($form->getValue('isActive'));
			$newsCategory->setSticky($form->getValue('isSticky'));
			try {
				if ($isNewCategory) {
					_dm()->persist($newsCategory);
				}

				$this->_helper->viewRenderer->setNoRender(true);
				$this->_helper->layout->disableLayout();
				$this->redirect($this->getCustomURL('list-categories', 'news', 'admin'));
			} catch (Exception $e) {
				$this->getSysFlashMessenger()->error($this->view->translate('Item could not be saved'));
				$this->getSysFlashMessenger()->info($e->getMessage());
			}
		}

		$this->view->assign('form', $form);

	}

	public function listCategoriesAction()
	{

        $this->setDefaultSorting('title', SORT_ASC);
        $offset = $this->getPaginationOffset();

		$this->view->assign('pageHeading', $this->view->translate('News Category List'));

        $categories = Epr_News_Category_Collection::getSorted($this->getSortKey(), $this->getSortDirection(), $this->getPaginateCount(), $offset);

		//         foreach ($categories as $category) {
		//             $items = Epr_News_Collection::getByCategoryId($category->getId());
		//         }

		$this->view->assign('editUrl', $this->getCustomURL('update-category', 'news', 'admin'));
		$this->view->assign('deleteUrl', $this->getCustomURL('delete-category', 'news', 'admin'));
		$this->view->assign('deactivateUrl', $this->getCustomURL('activate-category', 'news', 'admin'));

		$this->view->assign('categories', $categories);
	}

	private function buildRelative()
	{
		/**
		 * Create Related-Activities Placeholder
		 */
		$view = new Zend_View();
		$view->setBasePath(APPLICATION_PATH . 'modules/admin/views/placeholder/');

		$related = array();

		//        $this->view->placeholder('related-activities')->append($view->render('related-activities.phtml'));
	}

	public function listFeedsAction()
	{

        $this->setDefaultSorting('title', SORT_ASC);
        $offset = $this->getPaginationOffset();

		$feeds = Epr_Feed_Collection::getSorted($this->getSortKey(), $this->getSortDirection(), $this->getPaginateCount(), $offset);
		$this->view->assign('feeds', $feeds);

		$this->view->assign('itemsUrl', $this->getCustomURL('list-feed-items', 'news', 'admin'));
		$this->view->assign('editUrl', $this->getCustomURL('update-feed', 'news', 'admin'));
		$this->view->assign('deleteUrl', $this->getCustomURL('delete-feed', 'news', 'admin'));
		$this->view->assign('toggleUrl', $this->getCustomURL('toggle-feed', 'news', 'admin'));
	}

	public function updateFeedAction()
	{

		$feed = null;
		$id   = $this->getParam('id', null);

		if (is_string($id)) {
			$feed = _dm()->find(Epr_Feed::DOCUMENTNAME, $id);
			$this->view->assign('pageHeading', $this->view->translate('Edit News Feed'));
		} else {
			$this->view->assign('pageHeading', $this->view->translate('Add News Feed'));
		}

		$categories = _dm()->getRepository('Epr_News_Category')->findAll();
		$form       = Epr_Form_News::feedForm($feed, $categories);

		if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getParams())) {

			$isNewFeed = false;
			if (is_null($feed)) {
				$feed      = new Epr_Feed();
				$isNewFeed = true;
			}

			$feed->setTitle($form->getValue(Epr_Form_News::FEED_TITLE));
			$feed->setUrl($form->getValue(Epr_Form_News::FEED_URL));
			$feed->setActive($form->getValue(Epr_Form_News::FEED_ACTIVE));

			$category   = null;
			$categoryId = $form->getValue(Epr_Form_News::FEED_CATEGORY);
			if ($categoryId) {
				$category = _dm()->find(Epr_News_Category::DOCUMENTNAME, $categoryId);
			}

			if ($category instanceof Epr_News_Category) {
				$feed->setCategory($category);
			} else {
				$feed->removeCategory();
			}

			$existingFeed = Epr_Feed_Collection::getFeedWithURL($feed->getUrl());
			if ($existingFeed instanceof Epr_Feed && $existingFeed->getId() != $feed->getId()) {
				$this->getSysFlashMessenger()->error($this->view->translate("This URL is already in use."));
			} else {

				$error = null;
				$valid = $feed->isValid($error);
				if (!$valid) {

					$this->getSysFlashMessenger()->error($this->view->translate("The feed's XML data could not be read, you probably provided the wrong URL."));
					$this->getSysFlashMessenger()->info($error);

				} else {

					try {
						if ($isNewFeed) {
							_dm()->persist($feed);
						}
						$this->_helper->viewRenderer->setNoRender(true);
						$this->_helper->layout->disableLayout();
						$this->redirect($this->getCustomURL('list-feeds', 'news', 'admin'));
					} catch (Exception $e) {
						$this->getSysFlashMessenger()->error($this->view->translate('Feed could not be saved'));
						$this->getSysFlashMessenger()->info($e->getMessage());
					}
				}
			}
		}

		$this->view->assign('form', $form);

	}

	public function deleteFeedAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$feed = null;
		$id   = $this->getParam('id', null);

		if (is_string($id)) {
			$feed = _dm()->find(Epr_Feed::DOCUMENTNAME, $id);
		}

		if ($feed instanceof Epr_Feed) {
			try {
				_dm()->remove($feed);
			} catch (Exception $e) {
				$this->getSysFlashMessenger()->error($this->view->translate("Could not delete feed."));
				$this->getSysFlashMessenger()->info($e->getMessage());
			}
		}

		$this->redirect($this->getCustomURL('list-feeds', 'news', 'admin'));
	}

	public function toggleFeedAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$feed = null;
		$id   = $this->getParam('id', null);

		if (is_string($id)) {
			$feed = _dm()->find(Epr_Feed::DOCUMENTNAME, $id);
		}

		if ($feed instanceof Epr_Feed) {
			try {
				$feed->setActive(!$feed->isActive());
			} catch (Exception $e) {
				$this->getSysFlashMessenger()->error($this->view->translate("Could not set feed state."));
				$this->getSysFlashMessenger()->info($e->getMessage());
			}
		}

		$this->redirect($this->getCustomURL('list-feeds', 'news', 'admin'));
	}

	public function listFeedItemsAction()
	{

        $this->setDefaultSorting('date', SORT_DESC);
        $offset = $this->getPaginationOffset();

		$feed = null;
		$id   = $this->getParam('id', null);

		if (is_string($id)) {
			$feed = _dm()->find(Epr_Feed::DOCUMENTNAME, $id);
		}

		if ($feed instanceof Epr_Feed) {

			$this->view->assign('pageHeading', $this->view->translate('Feed Items "' . $feed->getTitle() . '"'));
			if ($feed->read()) {

                $items = \Poundation\PArray::create($feed->getEntries());
                $sortDescriptor = null;
                if ($this->getSortKey() === 'date') {
                    $sortDescriptor = new \Poundation\PSortDescriptor('releaseDate', $this->getSortDirection());
                } else if ($this->getSortKey() == 'title') {
                    $sortDescriptor = new \Poundation\PSortDescriptor('title', $this->getSortDirection());
                }

                if ($sortDescriptor) {
                    $items->sortUsingSortDescriptor($sortDescriptor);
                }


				$this->view->assign('items', $items);
                $this->view->assign('feed', $feed);
				$this->view->assign('convertToNewsItemUrl', $this->getCustomURL('convert-to-news-item', 'news', 'admin'));
			} else {
				$this->getSysFlashMessenger()->error($this->view->translate("Could not read feed."));
				$this->getSysFlashMessenger()->info($feed->getLastError()->getMessage());
			}

		} else {
			$this->getSysFlashMessenger()->error($this->view->translate("Could not load feed."));
			$this->redirect($this->getCustomURL('list-feeds', 'news', 'admin'));
		}
	}

	public function convertToNewsItemAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$feedId = $this->getParam('feed', null);
		$itemId = $this->getParam('item', null);

		if (strlen($feedId) > 0 && strlen($itemId) > 0) {

			$feed = _dm()->find(Epr_Feed::DOCUMENTNAME, $feedId);
			if ($feed instanceof Epr_Feed) {

				$feedCategory = $feed->getCategory();

				$item = $feed->getItemWithId($itemId);

				if ($item instanceof Epr_Feed_Item) {

					$news = new Epr_News();

					$news->setTitle($item->getTitle());
					$news->setText($item->getText());
					if (!is_null($feedCategory)) {
						$news->setCategory($feed->getCategory());
					}

					$news->setSource($feed->getTitle());
					$news->setPublicUrl($item->getUrl());

					$imageURLs = $item->getImages();

					$imageImportException = null;

					if (count($imageURLs) > 0) {
						$imageToUse = null;
						$maxSize    = 0;

						foreach ($imageURLs as $imageURL) {

							$imagePURL = \Poundation\PURL::URLWithString($imageURL);
							$image = \Poundation\PImage::createImageFromURL($imagePURL, $imagePURL->filename());
							if ($image) {
								$size = $image->getWidth() * $image->getHeight();
								if ($size > $maxSize) {
									$maxSize    = $size;
									$imageToUse = $image;
								}
							}

							unset($size);
							unset($image);

						}

						if (!is_null($imageToUse)) {
							if ($maxSize > 50 * 50) {

								$contentImage = Epr_Content_Image_Collection::imageWithHash($imageToUse->getHash());
								if (is_null($contentImage)) {

									$contentImage = Epr_Content_Image::imageFromPImage($imageToUse);
									$contentImage->setAccess(Epr_Content_Image::ACCESS_GENERAL);
									try {
										_dm()->persist($contentImage);
									} catch (Exception $e) {
										$imageImportException = $e;
									}

								}

								if ($contentImage) {
									$news->setTitleImage($contentImage);
								}

							}
						}
					}

					if (is_null($imageImportException)) {

						try {
							_dm()->persist($news);
							_dm()->flush(); // flushing since we need the id right now
							$item->markAsConvertedToNews();
						} catch (Exception $e) {
							throw $e;
						}
					} else {
						$this->getSysFlashMessenger()->error($this->view->translate("Could not import the image. News Item has not been saved."));
					}

					$this->redirect($this->getCustomURL('update', 'news', 'admin', array('id' => $news->getId())));
				}
			}
		}
	}

	public function sendNotificationsAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$news = null;

		$id = $this->getRequest()->getParam('id', null);
		if (!is_null($id)) {
			$news = Epr_News::getNewsWithId($id);
		}

		if ($news instanceof Epr_News) {
			$count = $news->sendNotifications();
			$this->getSysFlashMessenger()->info('Added ' . $count . ' ' . ($count == 1 ? 'notification' : 'notifications') . ' to the queue.', true);
		}

		$this->redirect($this->getCustomURL('list', 'news', 'admin'));
	}
}
