<?php

/**
 * Index Controller for admin module
 *
 */
class Admin_IndexController extends Epr_Backend_Controller_Action
{

	public function indexAction()
	{
		$this->view->assign('pageHeading', $this->view->translate('Dashboard'));

		$dispatcherModule = _app()->getModuleWithIdentifier(Epr_Modules_Dispatcher::identifier());
		if ($dispatcherModule instanceof Epr_Modules_Dispatcher) {

			if ($dispatcherModule->isPaired()) {

				$feedURL = $dispatcherModule->getFeedURL();
				if ($feedURL) {

					$feed = Epr_Feed::createFeedWithURL($feedURL);
					if ($feed->read(365)) {
						$this->view->assign('feed', $feed);
					}
				}
			}
		}

		$codeBaseFeedURL = \Poundation\PURL::URLWithString('https://staffdeal-gmbh.codebasehq.com/projects/staffsale/overview.rss?access_token=UDYxswxQc5VVTH8A8KJF');
		$codeBaseFeed = Epr_Feed::createFeedWithURL($codeBaseFeedURL);
		if ($codeBaseFeed->read(10)) {
			$this->view->assign('codeBaseFeed', $codeBaseFeed);
		}

		$dbInfo = _dm()->getCouchDBClient()->getDatabaseInfo(_dm()->getCouchDBClient()->getDatabase());
		$dbInfo['version'] = _dm()->getCouchDBClient()->getVersion();
		$this->view->assign('dbInfo', $dbInfo);
	}

}
