<?php

class Admin_AjaxController extends Epr_Backend_Controller_Action
{

    public function preDispatch() {
        parent::preDispatch();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        error_reporting(0);
        ini_set('display_errors', 'off');
    }

    public function imageListAction() {
        $context = $this->getRequest()->getParam('context',null);
        if (is_null($context)) {
            $context = Epr_Content_Image::ACCESS_GENERAL;
        }

        $count = $this->getPaginateCount();
        $offset = $this->getPaginationOffset();

        $sortKey = $this->getSortKey();
        if ($sortKey != 'name' && $sortKey != 'creationDate') {
            $sortKey = 'name';
        };

        $query = $this->getSearchQuery();

        $images = null;

        if ($query) {
            $query = $query . ' access:' . $context;
            $images = Epr_Content_Image_Collection::getImagesMatching($query,$sortKey, $this->getSortDirection(), $count, $offset);
        } else {
            if ($sortKey == 'name') {
                $images = Epr_Content_Image_Collection::getImagesByAccessSortedByName($context, $count, $offset, $this->getSortDirection());
            } else if ($sortKey == 'creationDate') {
                $images = Epr_Content_Image_Collection::getImagesByAccessSortedByCreationDate($context, $count, $offset, $this->getSortDirection());
            }
        }

        $total = Epr_Content_Image_Collection::countImagesByAccess($context);

        if ($images) {

            $responseArray = array();
            foreach ($images as $image) {

                if ($image instanceof Epr_Content_Image) {
                    if ($image->hasAccess($context)) {
                        $imageData                      = array(
                            'name'     => $image->getName(),
                            'url'      => $image->getPublicIDBasedURL(),
                            'width'    => $image->getWidth(),
                            'height'   => $image->getHeight(),
                            'creation' => null
                        );
                        if ($image->getCreationDate()) {
                            $imageData['creation'] = $image->getCreationDate()->format('Y-m-d H:i:s');
                        }
                        $responseArray[$image->getId()] = $imageData;
                    }
                }
            }
        }

        $hasNext = ($offset + count($images) < $total);
        $navigation = array('total' => $total, 'offset' => $offset, 'count' => count($images), 'hasNext' => $hasNext, 'hasPrevious' => ($offset > 0));

		$this->getResponse()->setHeader('Pragma: no-cache',true);
        $response = json_encode(array('images' => $responseArray, 'navigation' => $navigation));
        $this->getResponse()->setBody($response);
    }

    public function imageListEditorAction() {
        $images = Epr_Content_Image_Collection::allImages();

        $output = 'var tinyMCEImageList = new Array(';


        $outputArray = array();
        foreach ($images as $image) {
            if ($image instanceof Epr_Content_Image) {
                $outputArray[] = '["' . $image->getName() . '","' . $image->getPublicIDBasedURL() . '"]';
            }
        }

        $output .= implode(",\n", $outputArray);

        $output .= ');';

        $this->getResponse()->setBody($output);

    }

    public function imageUploadAction()
    {

        $context = $this->getRequest()->getParam('context',null);
        if (is_null($context)) {
            $context = Epr_Content_Image::ACCESS_GENERAL;
        }

        $upload = new Zend_File_Transfer_Adapter_Http(array(
            'ignoreNoFile' => true
        ));

        $response = array();

        if (!$upload->receive()) {
            $response['status'] = 'error';
            $messages = $upload->getMessages();
            foreach ($messages as $message) {
                if (strpos($message, 'exceeds the defined ini size') !== false) {
                    $response['message'] = 'The uploaded file is too big.';
                } else {
                    $response['message'] = $message;
                }
            }
        } else {
            $files = $upload->getFileInfo();
            foreach ($files as $file => $info) {

                if ($upload->isValid($file)) {

                    $filename = $upload->getFileName($file);

                    $image = \Poundation\PImage::createImageFromFilename($filename);
                    if ($image) {

                        $imageName = $image->getName();
                        $existingImage = Epr_Content_Image_Collection::imageWithFilename($imageName);
                        if ($existingImage instanceof Epr_Content_Image && $existingImage->hasAccess($context)) {
                            // there is already an image with that name
                            $name = __(pathinfo($imageName, PATHINFO_FILENAME));
                            $extension = pathinfo($imageName, PATHINFO_EXTENSION);

                            $index = 1;
                            $newName = $name;

                            while ($existingImage != null) {
                                $newName = $name . ' (' . $index++ . ').' . $extension;
                                $existingImage = Epr_Content_Image_Collection::imageWithFilename($newName);
                            }

                            $image->setName($newName);
                        }

                        $contentImage = Epr_Content_Image::imageFromPImage($image);
                        if ($contentImage) {
                            $contentImage->setAccess($context);
                            _dm()->persist($contentImage);
                            $response['files'][$contentImage->getId()] = array(
                                'name' => $contentImage->getName(),
                                'url' => $contentImage->getPublicIDBasedURL(),
                                'width'    => $contentImage->getWidth(),
                                'height'   => $contentImage->getHeight(),
                                'creation' => $contentImage->getCreationDate()->format('Y-m-d H:i:s')
                            );
                        }
                        $response['status'] = 'OK';
                    } else {
						$response['status'] = 'error';
						$response['message'] = 'The file you uploaded could not be understood as an image.';
					}
                }
            }
        }

        $this->getResponse()->setBody(json_encode($response));

    }

    public function toggleWebpageImageAction() {

        $subjectId = $this->getParam('subjectID', false);
        $itemId = $this->getParam('itemId', false);

        $this->getResponse()->setBody('ajax check ' . $subjectId);

    }

}