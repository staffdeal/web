<?php

/**
 * Index Controller for admin module
 *
 */
class Admin_SettingsController extends Epr_Backend_Controller_Action
{

    public function indexAction()
    {
        $this->view->assign('pageHeading', $this->view->translate('Settings'));
    }

    public function generalAction()
    {
        $this->view->assign('pageHeading', $this->view->translate('General Settings'));

    }

    public function modulesAction()
    {
        $this->view->assign('pageHeading', $this->view->translate('Module Settings'));

        $modules = new \Poundation\PArray();
        foreach (_app()->getModules() as $module) {
            if ($module instanceof Epr_Module) {
                $modules[] = $module;
            }
        }

        $modules->sortUsingSortDescriptor(new \Poundation\PSortDescriptor('title'));


        $this->view->assign('modules', $modules);

        $this->view->assign('editUrl', $this->getCustomURL('updateModule', 'settings', 'admin'));

    }

    public function updatemoduleAction()
    {
        $this->view->assign('pageHeading', $this->view->translate('Edit Module Settings'));

        $allModules = _app()->getModules();

        $id = $this->_request->getParam('id', false);
        if ($id !== false) {

            $module = _app()->getModuleWithIdentifier($id);
            if ($module) {
                $form = Epr_Form_Settings::moduleForm($module);

                if ($form !== false) {

                    $moduleFields = $module->getSettingsFormFields(Epr_Module::CONTEXT_GLOBAL);
                    if (is_array($moduleFields) && count($moduleFields) > 0) {
                        $form->addElements($moduleFields);
                    }

                    if ($this->_request->isPost() && $form->isValid($this->_request->getParams())) {

                        $noError = true;
                        foreach ($moduleFields as $field) {
                            if ($field instanceof Zend_Form_Element) {
                                $noError &= $module->saveSettingsField($field);
                            }
                        }

                        if ($noError) {
                            try {
                                _dm()->flush($module);

                                if (!_isDebugging()) {
                                    $this->redirect($this->getCustomURL('modules', 'settings', 'admin'));
                                }
                            } catch (Exception $e) {
                                $this->getSysFlashMessenger()
                                    ->error($this->view->translate('Module could not be saved'));
                                $this->getSysFlashMessenger()->info($e->getMessage());
                            }
                        } else {
                            $this->getSysFlashMessenger()->error($this->view->translate('Invalid input'));
                        }
                    }

                    if (count($form->getElements()) > 0) {

                        $submitCaption = $module->getSettingsSaveButtonCaption();
                        if ($submitCaption !== false) {
                            $submit = new Zend_Form_Element_Submit('submit');
                            $submit->setLabel('');
                            $submit->setValue($submitCaption);
                            $submit->setAttrib('class', 'form-submit');
                            $form->addElement($submit);
                        }

                    } else {
                        $form = $this->view->translate('There are no settings for this module.');
                    }

                }

                $this->view->assign('form', $form);
            }

        }
    }

    public function databaseCleanupAction()
    {
        $this->view->assign('pageHeading', $this->view->translate('Database Cleanup'));

        $connection = _dm()->getCouchDBClient();

        $output     = "Compaction:\n";
        $compaction = $connection->compactDatabase();
        foreach ($compaction as $key => $value) {
            $output .= $key . ': ' . $value . "\n";
        }

        $output .= "View Cleanup:\n";
        $cleanup = $connection->viewCleanup();
        foreach ($cleanup as $key => $value) {
            $output .= $key . ': ' . $value . "\n";
        }

        $designDocs = array('binary', 'auth', 'notifications');
        foreach ($designDocs as $designDoc) {
            $output .= "View Compaction of $designDoc:\n";
            $binary = $connection->compactView($designDoc);
            foreach ($binary as $key => $value) {
                $output .= $key . ': ' . $value . "\n";
            }
        }

        $allTranslations     = _dm()->getRepository(Epr_Translate_Locale::DOCUMENT_NAME)->findAll();
        $removedTranslations = 0;
        foreach ($allTranslations as $translation) {
            if ($translation instanceof Epr_Translate_Locale) {
                if (count($translation->getData()) == 0) {
                    _dm()->remove($translation);
                    $removedTranslations++;
                }
            }
        }

        $output .= "<hr>\nEmpty translations: $removedTranslations\n";


        $numberOfImages = 0;
        $tmpImages = Epr_Content_Image_Collection::imageWithAccess(Epr_Content_Image::ACCESS_TMP);
        if ($tmpImages) {

            foreach($tmpImages as $image) {
                if ($image instanceof Epr_Content_Image && $image->hasAccess(Epr_Content_Image::ACCESS_TMP)) {
                    _dm()->remove($image);
                    $numberOfImages++;
                }
            }
        }
        $output .= "<hr>\nRemoved temporary images: $numberOfImages";

        $this->view->assign('output', $output);
    }

    public function databaseCleanupFeedItemsAction()
    {

        $col      = _dm()->getRepository('Epr_Feed_Item');
        $allItems = $col->findAll();

        foreach ($allItems as $item) {
            _dm()->remove($item);
        }

        $this->redirect($this->getCustomURL('database-cleanup', 'settings', 'admin'));
    }

    public function runCronAction()
    {

        $this->view->assign('pageHeading', $this->view->translate('Run Cron'));
        $outputs = _app()->runCronJobs();
        $this->view->assign('outputs', $outputs);

    }
}
