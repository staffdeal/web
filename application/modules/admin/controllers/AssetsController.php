<?php

class Admin_AssetsController extends Epr_Backend_Controller_Action {

    public function indexAction() {
        $this->redirect($this->getCustomURL('list', 'assets', 'admin'));
    }

    public function listAction() {

        $this->view->assign('pageHeading', $this->view->translate('Assets'));

        $this->setDefaultSorting('name', SORT_ASC);

        $searchQuery = $this->getSearchQuery();
        if ($searchQuery) {
            $images = Epr_Content_Image_Collection::getImagesMatching($searchQuery, $this->getSortKey(), $this->getSortDirection(), $this->getPaginateCount(), $this->getPaginationOffset());
            if ($images instanceof \Doctrine\CouchDB\View\LuceneResult) {
                $this->setSearchQuery($images->getExecutedQuery());
            }
        } else {
            $images = Epr_Content_Image_Collection::getSorted($this->getSortKey(), $this->getSortDirection(), $this->getPaginateCount(), $this->getPaginationOffset());
        }

        $this->view->assign('images', $images);


        $this->view->assign('editUrl', $this->getCustomURL('edit', 'assets', 'admin'));
        $this->view->assign('trashUrl', $this->getCustomURL('trash', 'assets', 'admin'));
    }

    public function editAction() {

        $image = null;

        $id = $this->getRequest()->getParam('id', null);
        if ($id) {
            $image = _dm()->find(Epr_Content_Image::DOCUMENTNAME, $id);
        }

        if ($image instanceof Epr_Content_Image) {
            $form = new Epr_Form_ContentImage($image);

            $this->view->assign('pageHeading', $this->view->translate('Edit Asset'));
            $this->view->assign('image', $image);
            $this->view->assign('form', $form);

            if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getParams())) {
                $image->setName($form->getFilename());
                $image->setFocalPoint((float)$form->getValue('focalX'), (float)$form->getValue('focalY'));
                $this->redirect($this->getCustomURL('list', 'assets', 'admin'));
            }


        } else {
            $this->redirect($this->getCustomURL('list', 'assets', 'admin'));
        }
    }

    public function trashAction() {

        $image = null;

        $id = $this->getRequest()->getParam('id', null);
        if ($id) {
            $image = _dm()->find(Epr_Content_Image::DOCUMENTNAME, $id);
        }

        if ($image instanceof Epr_Content_Image) {

            $this->view->assign('pageHeading', $this->view->translate('Move Asset "' . $image->getName() . '" to trash'));
            $this->view->assign('image', $image);

            $usages = $image->usages();

            if ($usages && count($usages) > 0) {

                $viewUsages = [];
                foreach($usages as $usage) {
                    if ($usage instanceof Epr_Document_WebPage) {
                        $viewUsages[] = get_class($usage) . ': ' . $usage->getTitle();
                    } else if ($usage instanceof Epr_Theme) {
                        $viewUsages[] = 'Theme: ' . $usage->getTitle();
                    } else if ($usage instanceof Epr_Cms_Page) {
                        $viewUsages[] = 'CMS Page: ' . $usage->getTitle();
                    }
                }

                $this->view->assign('usages', $viewUsages);

            } else {

                $action = $this->getRequest()->getParam('trashit', null);
                if ($action || strtolower($action) == 'true') {
                    $image->moveToTrash();
                    $this->redirect($this->getCustomURL('list', 'assets', 'admin'));
                }
            }

        } else {
            $this->redirect($this->getCustomURL('list', 'assets', 'admin'));
        }
    }

}