<?php

class Admin_CmsController extends Epr_Backend_Controller_Action
{

    public function indexAction()
    {
        $this->redirect($this->getCustomURL('list-pages', 'cms', 'admin'));
    }

    public function listThemesAction()
    {

        $this->view->assign('pageHeading', $this->view->translate('Themes'));

        $themes = Epr_Theme_Collection::allThemes();
        if (count($themes) == 0) {
            $theme = Epr_Theme_Collection::getActiveTheme(); // pretty bad since we need to know some internals of the collection class
            _dm()->persist($theme);
            _dm()->flush();
            $themes = Epr_Theme_Collection::allThemes();
        }

        $this->view->assign('themes', $themes);
        $this->view->assign('editUrl', $this->getCustomURL('edit-theme', 'cms', 'admin'));

    }

    public function editThemeAction()
    {

        $this->view->assign('pageHeading', $this->view->translate('Edit Theme'));

        $theme = null;

        $id = $this->getRequest()->getParam('id', null);
        if (is_string($id)) {

            $theme = _dm()->find(Epr_Theme::DOCUMENTNAME, $id);

        }

        if ($theme instanceof Epr_Theme) {
            $form = Epr_Form_Cms::themeForm($theme);

            if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getParams())) {

                $theme->setName($form->getValue(Epr_Form_Cms::THEME_NAME));
                $theme->setTitle($form->getValue(Epr_Form_Cms::THEME_TITLE));
                $theme->setTintColor($form->getValue(Epr_Form_Cms::THEME_TINT_COLOR));
                $theme->setDarkAreaColor($form->getValue(Epr_Form_Cms::THEME_DARK_AREA_COLOR));
                $theme->setLightAreaColor($form->getValue(Epr_Form_Cms::THEME_LIGHT_AREA_COLOR));
                $theme->setBackgroundColor($form->getValue(Epr_Form_Cms::THEME_BACKGROUND_COLOR));
                $theme->setLogoImage($form->getValue(Epr_Form_Cms::THEME_LOGO));
                $theme->setLowContrastLogoImage($form->getValue(Epr_Form_Cms::THEME_LOW_CONTRAST_LOGO));
                $theme->setFaviconImage($form->getValue(Epr_Form_Cms::THEME_FAVICON));

                $theme->setAppleTouchIcon($form->getValue(Epr_Form_Cms::THEME_APPLE_TOUCH_ICON));
                $theme->setAppleTouchIcon72($form->getValue(Epr_Form_Cms::THEME_APPLE_TOUCH_ICON_72));
                $theme->setAppleTouchIcon114($form->getValue(Epr_Form_Cms::THEME_APPLE_TOUCH_ICON_114));
                $theme->setAppleTouchIcon144($form->getValue(Epr_Form_Cms::THEME_APPLE_TOUCH_ICON_144));

                $theme->setSoldOutImage($form->getValue(Epr_Form_Cms::THEME_SOULD_OUT_IMAGE));

                $theme->setCustomHTMLHeader($form->getValue(Epr_Form_Cms::THEME_HTML_HEADER));
                $theme->setCustomCSS($form->getValue(Epr_Form_Cms::THEME_CSS));
                $theme->setFooterText($form->getValue(Epr_Form_Cms::THEME_FOOTER));

                $this->redirect($this->getCustomURL('list-themes', 'cms', 'admin'));
            }

            $this->view->assign('form', $form);

        } else {

            $this->redirect($this->getCustomURL('list-themes', 'cms', 'admin'));

        }
    }

    public function listPagesAction()
    {
        $this->view->assign('pageHeading', $this->view->translate('Pages List'));
        $this->view->assign('editUrl', $this->getCustomURL('edit-element', 'cms', 'admin'));

        $cmsModule = _app()->getModuleWithIdentifier(Epr_Modules_Cms::identifier());
        if ($cmsModule instanceof Epr_Modules_Cms) {

            $pages = \Poundation\PArray::create($cmsModule->getPages());
            $pages->sortByPropertyName('path');
            $this->view->assign('elements', $pages);
        }

    }

    public function editElementAction()
    {

        $element = null;
        $path = $this->getParam('path', null);

        if ($path) {
            $element = Epr_Cms_Element_Collection::getElementWithAbsolutePath($path);
        }

        if ($element instanceof Epr_Cms_Element) {

            $this->view->assign('pageHeading', $this->view->translate('Edit CMS element "' . $element->getAbsolutePath() . '"'));
            $this->view->assign('editUrl', $this->getCustomURL('edit-element', 'cms', 'admin'));

            $form = new Epr_Form();

            $fields = $element->getEditorFields();
            $form->addElements($fields);

            $submit = new Zend_Form_Element_Submit('submit');
            $submit->setLabel('');
            $submit->setValue('Submit');
            $submit->setAttrib('class', 'form-submit');
            $form->addElement($submit);

            if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getParams())) {

                $success = true;

                foreach ($fields as $field) {
                    $success = $success && $element->saveEditorField($field);
                }

                if ($success == false) {
                    $this->getSysFlashMessenger()->error('Something went wrong while saving', true);
                }

                $pathComponents = __($path)->components('.');
                if ($pathComponents->count() > 1) {

                    $backPath = $pathComponents->shrinkBy(1)->string('.');
                    $this->redirect($this->getCustomURL('edit-element', 'cms', 'admin', array('path' => (string)$backPath)));
                } else {
                    $this->redirect($this->getCustomURL('list-pages', 'cms', 'admin'));
                }

            }

            $this->view->assign('form', $form);

            $subElements = $element->getElements();
            $this->view->assign('subelements', $subElements);

        } else {
            $this->redirect($this->getCustomURL('list-pages', 'cms', 'admin'));
        }

    }

    public function listTranslationsAction()
    {
        $this->view->assign('pageHeading', $this->view->translate('Edit Translations'));

        $repo = _dm()->getRepository(Epr_Translate_Locale::DOCUMENT_NAME);
        $translations = $repo->findAll();

        $this->view->assign('elements', $translations);
        $this->view->assign('editUrl', $this->getCustomURL('edit-translation', 'cms', 'admin'));
        $this->view->assign('deleteUrl', $this->getCustomURL('remove-translation', 'cms', 'admin'));

    }

    public function editTranslationAction()
    {

        $translation = null;

        $id = $this->getParam('id', null);
        if (is_string($id)) {
            $translation = _dm()->find(Epr_Translate_Locale::DOCUMENT_NAME, $id);
        }

        if ($translation instanceof Epr_Translate_Locale) {
            $this->view->assign('pageHeading', $this->view->translate('Edit Translations'));

            $editMissingOnly = $this->getRequest()->getParam('onlyMissing', false);

            $form = new Epr_Form();

            foreach ($translation->getData() as $name => $value) {

                if ($editMissingOnly == false || strlen($value) == 0) {

                    $class = 'inp-form wide';
                    if (strlen($value) == 0) {
                        $class .= ' warning';
                    }

                    $fieldId = md5($name);
                    $field = new Zend_Form_Element_Text($fieldId);
                    $field->setLabel($name);
                    $field->setValue($value);
                    $field->getDecorator('Label')->setOption('escape', false);
                    $field->setAttrib('class', $class);
                    $form->addElement($field);

                    unset($field);
                }
            }

            $submit = new Zend_Form_Element_Submit('submit');
            $submit->setLabel('');
            $submit->setValue('Submit');
            $submit->setAttrib('class', 'form-submit');
            $form->addElement($submit);

            if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getParams())) {
                foreach ($translation->getData() as $name => $value) {
                    $fieldId = md5($name);

                    $formElement = $form->getElement($fieldId);
                    if ($formElement instanceof Zend_Form_Element_Text) {

                        $formValue = __($form->getValue($fieldId))->stripTags(array(
                            '<b>',
                            'a',
                            'i',
                            'u',
                            'strong',
                            '<br />'
                        ))->trim();
                        if ($formValue == 'REMOVE ME!!!') {
                            $translation->removeString($name);
                        } else {
                            $translation->setString($name, ($formValue->length() == 0) ? null : (string)$formValue);
                        }

                    }
                }

                $this->redirect($this->getCustomURL('list-translations', 'cms', 'admin'));
            }

            $this->view->assign('form', $form);

        } else {
            $this->redirect($this->getCustomURL('list-translations', 'cms', 'admin'));
        }
    }

    public function removeTranslationAction()
    {
        $translation = null;

        $id = $this->getParam('id', null);
        if (is_string($id)) {
            $translation = _dm()->find(Epr_Translate_Locale::DOCUMENT_NAME, $id);
        }

        if ($translation instanceof Epr_Translate_Locale) {
            _dm()->remove($translation);
        }

        $this->redirect($this->getCustomURL('list-translations', 'cms', 'admin'));
    }

}
