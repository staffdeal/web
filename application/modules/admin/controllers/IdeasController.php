<?php

/**
 * Index Controller for admin module
 *
 */
class Admin_IdeasController extends Epr_Backend_Controller_Action
{

    public function preDispatch()
    {
        parent::preDispatch();

    }

    public function indexAction()
    {
        $this->redirect($this->getCustomURL('list-competitions', 'ideas', 'admin'));
    }

    /**
     * Only a listing of ideas
     */
    public function listIdeasAction()
    {
        $this->view->assign('pageHeading', $this->view->translate('Ideas List'));

        $competitionId = $this->_request->getParam('id', false);
        if ($competitionId) {

            $this->setDefaultSorting('submissionDate', SORT_DESC);
            $ideas = \Poundation\PArray::create(Epr_Idea_Collection::findByCompetitionId($competitionId));

            if ($this->getSortKey() === 'submissionDate') {
                $ideas->sortByPropertyName('submissionDate', $this->getSortDirection());
            }

        }

        $this->view->assign('detailUrl', $this->getCustomURL('show', 'ideas', 'admin'));
        $this->view->assign('deactivateUrl', $this->getCustomURL('toggle-activation', 'ideas', 'admin'));
        $this->view->assign('ideas', $ideas);
    }

    public function showAction()
    {
        $this->view->assign('pageHeading', $this->view->translate('Idea Detail'));
        $id = $this->_request->getParam('id', false);

        if ($id) {
            $idea = _dm()->getRepository('Epr_Idea')->find($id);
            if ($idea instanceof Epr_Idea) {
                $this->view->assign('toogleStateURL', $this->getCustomURL('toggle-activation', 'ideas', 'admin', array('id' => $idea->getId())));
            }
        }

        $this->view->assign('idea', $idea);
    }

    public function toggleActivationAction()
    {
        $id = $this->_request->getParam('id', false);

        if ($id) {
            $idea = _dm()->getRepository('Epr_Idea')->find($id);

            if ($idea instanceof Epr_Idea) {
                $idea->setActive(!$idea->isActive());
            }
        }

        $this->redirect(
            $this->getCustomURL(
                'list-ideas', 'ideas', 'admin', array(
                    'id' => $idea->getCompetition()->getId()
                )
            )
        );
    }

    /**
     * List of Competitions
     */
    public function listCompetitionsAction()
    {
        $this->setDefaultSorting('startDate', SORT_DESC);

        $searchQuery = $this->getSearchQuery();
        if ($searchQuery) {
            $competitions = Epr_Idea_Competition_Collection::getCompetitionsMatching($searchQuery, $this->getSortKey(), $this->getSortDirection(), $this->getPaginateCount(), $this->getPaginationOffset());
            if ($competitions instanceof \Doctrine\CouchDB\View\LuceneResult) {
                $this->setSearchQuery($competitions->getExecutedQuery());
            }
        } else {
            $competitions = Epr_Idea_Competition_Collection::getSorted($this->getSortKey(), $this->getSortDirection(), $this->getPaginateCount(), $this->getPaginationOffset());
        }

        $this->view->assign('editUrl', $this->getCustomURL('update-competition', 'ideas', 'admin'));
        $this->view->assign('previewUrl', $this->getCustomURL('preview-competition', 'ideas', 'default'));
        $this->view->assign('deleteUrl', $this->getCustomURL('delete-competition', 'ideas', 'admin'));
        $this->view->assign('deactivateUrl', $this->getCustomURL('activate-competition', 'ideas', 'admin'));
        $this->view->assign('listUrl', $this->getCustomURL('list-ideas', 'ideas', 'admin'));

        $this->view->assign('pageHeading', $this->view->translate('Competition List'));
        $this->view->assign('competitions', $competitions);
    }


    /**
     * List of Competition Categories
     */
    public function listCompetitionCategoriesAction()
    {

        $this->setDefaultSorting('title', SORT_ASC);
        $offset     = max(0, (int)$this->getParam('offset', 0));
        $categories = Epr_Idea_Category_Collection::getSorted($this->getSortKey(), $this->getSortDirection(), $this->getPaginateCount(), $offset);

        $this->view->assign('editUrl', $this->getCustomURL('update-competition-category', 'ideas', 'admin'));
        $this->view->assign('deleteUrl', $this->getCustomURL('delete-competition-category', 'ideas', 'admin'));
        $this->view->assign('deactivateUrl', $this->getCustomURL('activate-competition-category', 'ideas', 'admin'));

        $this->view->assign('pageHeading', $this->view->translate('Competition Category List'));
        $this->view->assign('categories', $categories);
    }

    /**
     * Add/Edit a new Competition
     */
    public function updateCompetitionAction()
    {
        $topic       = 'Add Competition';
        $id          = $this->_request->getParam('id', false);
        $competition = null;

        if ($id) {
            $topic       = 'Update Competition';
            $competition = _dm()->getRepository('Epr_Idea_Competition')->find($id);
        }

        $categories = _dm()->getRepository('Epr_Idea_Category')->findAll();

        $form = Epr_Form_Idea::competitionForm($competition, $categories);

        if ($this->_request->isPost() && $form->isValid($this->_request->getParams())) {

            $isNewCompetition = false;

            if (is_null($competition)) {
                $competition      = new Epr_Idea_Competition();
                $isNewCompetition = true;
            }

            $competition->setTitle($form->getValue('title'));
            $competition->setSlug($form->getValue('slug'));
            $competition->setCompany($form->getValue('company'));
            $competition->setTeaser($form->getValue('teaser'));
            $competition->setEndText($form->getValue('endText'));
            $competition->setText($form->getValue('text'));
            $competition->setRules($form->getValue('rules'));

            $period = $form->getValue('period');
            if ($period instanceof \Poundation\PDateRange) {
                $competition->setStartDate($period->getStartDate());
                $competition->setEndDate($period->getEndDate());
            }

            $competition->setCategory(_dm()->getRepository('Epr_Idea_Category')->find($form->getValue('category')));
            $competition->setActive($form->getValue('isActive'));

            $image = $form->getValue('image');
            if ($image instanceof Epr_Content_Image) {
                $competition->setTitleImage($image);
            } else {
                $competition->removeTitleImage();
            }

            try {

                if ($isNewCompetition) {
                    _dm()->persist($competition);
                }
                $this->redirect($this->getCustomURL('list-competitions', 'ideas', 'admin'));
            } catch (Exception $e) {
                $this->getSysFlashMessenger()->error($this->view->translate('Item could not be saved'));
                $this->getSysFlashMessenger()->info($e->getMessage());
            }

        }

        $this->view->assign('pageHeading', $this->view->translate($topic));
        $this->view->assign('form', $form);
    }

    /**
     * Add/Update a new Competition Category
     */
    public function updateCompetitionCategoryAction()
    {
        $topic    = 'Add Competition Category';
        $id       = $this->_request->getParam('id', false);
        $category = null;

        $category = null;

        if ($id) {
            $topic    = 'Update Competition Category';
            $category = _dm()->getRepository('Epr_Idea_Category')->find($id);
        }

        $form = Epr_Form_Idea::getCategoryForm($category);

        if ($this->_request->isPost() && $form->isValid($this->_request->getParams())) {
            $isNewCategory = false;
            if (is_null($category)) {
                $category      = new Epr_Idea_Category();
                $isNewCategory = true;
            }

            $image = $form->getValue('image');
            if ($image instanceof Epr_Content_Image) {
                $category->setTitleImage($image);
            } else {
                $category->removeTitleImage();
            }

            $category->setTitle($form->getValue('name'));
            $category->setSlug($form->getValue('slug'));
            $category->setActive($form->getValue('isActive'));

            try {
                if ($isNewCategory) {
                    _dm()->persist($category);
                }
                $this->redirect($this->getCustomURL('list-competition-categories', 'ideas', 'admin'));

            } catch (Exception $e) {
                $this->getSysFlashMessenger()->error($this->view->translate('Item could not be saved'));
                $this->getSysFlashMessenger()->info($e->getMessage());
            }

        }

        $this->view->assign('pageHeading', $this->view->translate($topic));
        $this->view->assign('form', $form);
    }


    public function addIdeaAction()
    {
        $competitions = _dm()->getRepository('Epr_Idea_Competition')->findAll();

        $form = Epr_Form_Idea::getIdeaForm($competitions);
        $form->setAction($this->getCustomURL('submit', 'ideas', 'api'));
        $this->view->assign('form', $form);
    }


    /**
     * Delete / Deactivate Competition
     */

    public function activateCompetitionAction()
    {
        $id = $this->_request->getParam('id', false);

        $competition = _dm()->getRepository('Epr_Idea_Competition')->find($id);
        if ($competition instanceof Epr_Idea_Competition) {
            $active = $competition->isActive() ? false : true;
            $competition->setActive($active);
            $competition->save();

            $this->getSysFlashMessenger()->setStatic()->success(!$active ? 'Item deactivated' : 'Item activated.');
            $this->redirect($this->getCustomURL('list-competitions', 'ideas', 'admin'));
        }

        $this->getSysFlashMessenger()->setStatic()->error('Updating Item failed');
        $this->redirect($this->getCustomURL('list-competitions', 'ideas', 'admin'));
    }

    public function deleteCompetitionAction()
    {
        $id          = $this->_request->getParam('id', false);
        $competition = _dm()->getRepository('Epr_Idea_Competition')->find($id);
        if ($competition instanceof Epr_Idea_Competition) {
            _dm()->remove($competition);
            $this->getSysFlashMessenger()->setStatic()->success('Item removed.');
            $this->redirect($this->getCustomURL('list-competitions', 'ideas', 'admin'));
        }

        $this->getSysFlashMessenger()->setStatic()->error('Something went wrong with this ID.');
        $this->redirect($this->getCustomURL('list-competitions', 'ideas', 'admin'));
    }


    /**
     * Delete / Deactivate Competition Category
     */

    public function activateCompetitionCategoryAction()
    {
        $id = $this->_request->getParam('id', false);

        $category = _dm()->getRepository('Epr_Idea_Category')->find($id);
        if ($category instanceof Epr_Idea_Category) {
            $active = $category->isActive() ? false : true;
            $category->setActive($active);

            $this->getSysFlashMessenger()->setStatic()->success(!$active ? 'Item deactivated' : 'Item activated.');
            $this->redirect($this->getCustomURL('list-competition-categories', 'ideas', 'admin'));
        }

        $this->getSysFlashMessenger()->setStatic()->error('Updating Item failed');
        $this->redirect($this->getCustomURL('list-competition-categories', 'ideas', 'admin'));
    }

    public function deleteCompetitionCategoryAction()
    {
        $id       = $this->_request->getParam('id', false);
        $category = _dm()->getRepository('Epr_Idea_Category')->find($id);
        if ($category instanceof Epr_Idea_Category) {
            _dm()->remove($category);
            $this->getSysFlashMessenger()->setStatic()->success('Item removed.');
            $this->redirect($this->getCustomURL('list-competition-categories', 'ideas', 'admin'));
        }

        $this->getSysFlashMessenger()->setStatic()->error('Something went wrong with this ID.');
        $this->redirect($this->getCustomURL('list-competition-categories', 'ideas', 'admin'));
    }

}
