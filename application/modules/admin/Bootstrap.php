<?php

class Admin_Bootstrap extends Zend_Application_Module_Bootstrap
{

	protected function _initFormElements()
	{
		Epr_Form_Element_Image_Selector::addDefaultPreview('Square', 55,55,0);
		Epr_Form_Element_Image_Selector::addDefaultPreview('Landscape', 80, 55, 0);
        Epr_Form_Element_Image_Selector::addDefaultPreview('Portrait', 55, 80, 0);
		Epr_Form_Element_Image_Selector::addDefaultPreview('Circle', 54, 54, 27);
	}
}

