<?php

class Api_NotificationsController extends Epr_Controller_ApiController
{

	public function init()
	{
		parent::init();
		$this->registerContextsForAction(array(
											  'index',
											  'subscribe',
											  'unsubscribe'
										 ));

	}

	public function indexAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
	}

	public function subscribeAction()
	{
		$this->_helper->viewRenderer('subscriptions');

		$topic = $this->getParam('topic', null);
		if (is_string($topic)) {

			$deviceToken = $this->getParam('deviceToken');
			if (strlen($deviceToken) > 5) {

				$token = Epr_Token_Collection::getTokenWithDeviceToken($deviceToken);
				if (!$token instanceof Epr_Token) {

					$deviceType = null;
					$userAgent  = __($_SERVER['HTTP_USER_AGENT']);
					if ($userAgent->contains('iPhone OS')) {
						$deviceType = Epr_Token::TYPE_IOS_APN;
					} else if ($userAgent->contains('Android')) {
						$deviceType = Epr_Token::TYPE_ANDROID_GCM;
					}

					if (!is_null($deviceType)) {

						$token = null;
						if ($deviceType == Epr_Token::TYPE_IOS_APN) {
							$token = Epr_Token::createIOSAPNToken($deviceToken);
						} else if ($deviceType == Epr_Token::TYPE_ANDROID_GCM) {
							$token = Epr_Token::createAndroidGCMToken($deviceToken);
						}

						if (!is_null($token)) {
							_dm()->persist($token);
						}
					}
				}

				if ($token instanceof Epr_Token) {

					$token->addTopic($topic);

					$apiToken = _token();
					if ($apiToken instanceof Epr_Token && $apiToken->getType() == Epr_Token::TYPE_API_ACCESS) {
						$user = $apiToken->getUser();
						if ($user instanceof Epr_User && $user->isActive()) {
							$token->assignUser($user);
						}
					}
				}
			}
		}

		$this->view->assign('token', $token);

	}

	public function unsubscribeAction()
	{
		$this->_helper->viewRenderer('subscriptions');

		$topic = $this->getParam('topic', null);
		$id    = $this->getParam('id', null);

		if (is_string($topic) && is_string($id)) {

			$token = _dm()->find('Epr_Token', $id);
			if ($token instanceof Epr_Token && ($token->isDeviceToken())) {

				$token->removeTopic($topic);

				if (count($token->getTopics()) == 0) {
					_dm()->remove($token);
				}
			}
		}
		$this->view->assign('token', $token);
	}
}