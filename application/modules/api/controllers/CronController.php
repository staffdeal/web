<?php

class Api_CronController extends Epr_Controller_ComController {

    public function dailyAction() {
        $connection = Epr_Com_Connection::inboundConnection();
        $message = $connection->getMessage();

        $reply = $connection->successReplyMessage();
        $reply->setValue('type','daily');

        $connection->reply();
    }

}