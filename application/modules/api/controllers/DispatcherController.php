<?php

class Api_DispatcherController extends Epr_Controller_ComController
{

    public function preDispatch()
    {

        $rawBody = $this->getRequest()->getRawBody();

        return parent::preDispatch();
    }

    public function pingAction()
    {

        $connection = Epr_Com_Connection::inboundConnection();
        $message    = $connection->getMessage();

        $url         = $message->getValue('url');
        $referenceId = $message->getValue('id');
        $feedUrl     = $message->getValue('feed');

        if ($url && \Poundation\PURL::isValidURLString($url) && $referenceId) {

            $module = _app()->getModuleWithIdentifier('Dispatcher');
            if ($module instanceof Epr_Modules_Dispatcher) {

                if ($module->pair($url, null, $referenceId)) {

                    if (\Poundation\PURL::isValidURLString($feedUrl)) {
                        $feedUrl = \Poundation\PURL::URLWithString($feedUrl);
                        $module->setFeedURL($feedUrl);
                    }

                    $reply = $connection->successReplyMessage();
                    $reply->setValue(_app()->getName(), 'caption');
                    $reply->setValue(false, 'syncNeeded');

                    $connection->reply($reply);
                }
            }
        } else {
            $connection->errorReplyMessage('Invalid URL provided');
        }
    }

    public function cronAction()
    {

        $connection = Epr_Com_Connection::inboundConnection();
        $reply      = $connection->successReplyMessage();
        $reply->setValue('outputs', _app()->runCronJobs());
        $connection->reply($reply);

    }

    public function dealsAction()
    {

        $dealersModule = _app()->getModuleWithIdentifier(Epr_Modules_Dealers::identifier());

        $deals         = Epr_Deal_Collection::getActive();
        $responseDeals = array();
        foreach ($deals as $deal) {
            if ($deal instanceof Epr_Deal) {

                $onsiteCodeModule = $deal->getOnsiteCodeModule();
                $onlineCodeModule = $deal->getOnlineCodeModule();
                if (($onsiteCodeModule && $onsiteCodeModule->isValid()) || ($onlineCodeModule && $onlineCodeModule->isValid())) {

                    $dealStartDate = $deal->getStartDate();
                    $dealEndDate   = $deal->getEndDate();
                    if ($dealStartDate && $dealEndDate) {
                        $startDate = new \Poundation\PDate($deal->getStartDate());
                        $endDate   = new \Poundation\PDate($deal->getEndDate());

                        $dealData = array(
                            'id'        => $deal->getId(),
                            'provider'  => _app()->getName(),
                            'title'     => $deal->getTitle(),
                            'text'      => $deal->getText(),
                            'startDate' => $startDate->getInISO8601Format(),
                            'endDate'   => $endDate->getInISO8601Format(),
                            'price'     => $deal->getPrice(),
                            'oldPrice'  => $deal->getOldPrice()
                        );

                        if ($deal->getTeaser()) {
                            $dealData['teaser'] = $deal->getTeaser();
                        }

                        if ($deal->getTitleImage() instanceof Epr_Content_Image) {
                            $dealData['titleImage'] = $deal->getTitleImage()->getPublicIDBasedURL();
                        }

                        if ($deal->isFeatured()) {
                            $dealData['featured'] = true;
                        }

                        if ($deal->getReminderText()) {
                            $dealData['reminder'] = $deal->getReminderText();
                        }

                        if (!$deal->isManuallySoldOut()) {
                            if ($onsiteCodeModule->isValid()) {
                                $dealData['pass'] = array(
                                    'onsite' => (string)$deal->getOnsiteRedeemURL()
                                );
                            }
                        }


                        if ($onlineCodeModule && $onlineCodeModule->isValid()) {
                            $dealData['pass']['online'] = array(
                                'description' => $onlineCodeModule->getDescription(),
                                'shopURL' => $onlineCodeModule->getURL()
                            );

                            if ($deal->isManuallySoldOut()) {
                                $dealData['pass']['online']['available'] = false;
                            } else {
                                if (!$onlineCodeModule->usesCodes()) {
                                    $dealData['pass']['online']['available'] = true;
                                } else {
                                    $dealData['pass']['online']['available'] = $onlineCodeModule->isAvailable();
                                    if ($dealData['pass']['online']['available']) {
                                        $dealData['pass']['online']['redeemURL'] = $deal->getOnlineRedeemURL();
                                    }
                                }
                            }
                        }


                        $selectedCategories = array();

                        if ($dealersModule instanceof Epr_Modules_Dealers) {
                            for ($i = 0; $i < count($dealersModule->getCategories()); $i++) {
                                $selectedCategories[$i] = $deal->selectedFieldIndexesInCategory($i);
                            }
                        }

                        $dealData['dealerCategories'] = $selectedCategories;


                    }

                    $responseDeals[] = $dealData;
                }
            }
        }

        $connection      = Epr_Com_Connection::inboundConnection();
        $responseMessage = $connection->successReplyMessage();
        $responseMessage->setValue($responseDeals, 'deals');
        $connection->reply($responseMessage);
    }

    public function dealersAction()
    {

        $dealers         = Epr_Dealer_Collection::getAllActive();
        $responseDealers = array();

        $dealersModule = _app()->getModuleWithIdentifier(Epr_Modules_Dealers::identifier());

        foreach ($dealers as $dealer) {
            if ($dealer instanceof Epr_Dealer) {

                $dealerData = array(
                    'id'   => $dealer->getId(),
                    'name' => $dealer->getTitle()
                );

                if ($dealer->getAddress() instanceof \Poundation\PAddress) {
                    $dealerData['address'] = $dealer->getAddress();
                }
                if ($dealer->getTelephone()) {
                    $dealerData['telephone'] = $dealer->getTelephone();
                }
                if ($dealer->getWeb()) {
                    $dealerData['url'] = (string)$dealer->getWeb();
                }
                if ($dealer->getEmail()) {
                    $dealerData['email'] = (string)$dealer->getEmail();
                }

                if ($dealersModule instanceof Epr_Modules_Dealers) {

                    $additionalCategories = array();

                    $i = 0;
                    foreach ($dealersModule->getCategories() as $category => $fields) {

                        $additionalCategories[] = array(
                            'index' => $i,
                            'value' => $dealer->getAdditionCategoryValue($i)
                        );

                        $i++;
                    }

                    $dealerData['additionalCategories'] = $additionalCategories;
                }

                $responseDealers[] = $dealerData;

            }
        }

        $connection      = Epr_Com_Connection::inboundConnection();
        $responseMessage = $connection->successReplyMessage();
        $responseMessage->setValue($responseDealers, 'dealers');
        $connection->reply($responseMessage);
    }

}