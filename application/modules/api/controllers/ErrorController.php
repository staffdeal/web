<?php

/**
 * {0}
 * 
 * @author
 * @version 
 */
	

class Api_ErrorController extends Epr_Controller_ApiController
{
    public function init()
    {
        parent::init();

        $this->registerContextForAction('error');
        $this->registerContextForAction('token');
        $this->registerContextForAction('noauth');
        $this->registerContextForAction('registrationinsufficent');
    	$this->registerContextForAction('registrationexists');
        $this->registerContextForAction('registrationinternalerror');
        $this->registerContextForAction('missingparamter');
    }
   
	/**
	 * The default action - show the home page
	 */
    public function errorAction() 
    {

        _logger()->warn('API ErrorController');

        $defaultError = new stdClass();
        $defaultError->type = 'EXCEPTION_OTHER';
        $defaultError->exception = new Exception('Error Handler - Unknown Error', -1);

		$error = $this->_getParam('error_handler', $defaultError);

        switch ($error->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 Fehler -- Controller oder Aktion nicht gefunden
                $this->getResponse()
                     ->setRawHeader('HTTP/1.1 404 Not Found');

                 $this->view->assign('message', $this->view->translate('404 - Page not found'));

                break;

            default:
                // Anwendungsfehler; Fehler Seite anzeigen, aber den
                // Status Code nicht �nndern

                if(isset($error->noAuth) && $error->noAuth){
                    $this->_redirect('/api/error/noauth');
                }else{
                    $this->view->assign('message', $error->exception->getMessage());
                }

                break;
       
    	}


        $this->view->assign('error', $error);
    }
    
    /** Used when a request has an invalid token. */
    public function tokenAction() {
    	$this->getResponse()->setHttpResponseCode(401);
    }
    
    /** Used when a request tries to access private resources without providing a valid token. */
    public function noauthAction() {
    	$this->getResponse()->setHttpResponseCode(403);
    }
    
    public function registrationexistsAction() {
    	$this->getResponse()->setHttpResponseCode(404);
    }
    
    public function registrationinsufficentAction() {
    	$this->getResponse()->setHttpResponseCode(405);
    }
    
    public function registrationinternalerrorAction() {
    	$this->getResponse()->setHttpResponseCode(406);
    }

    public function missingparamterAction() {
        $this->getResponse()->setHttpResponseCode(412);
    }
    
    
      
}

