<?php

class Api_UserController extends Epr_Controller_ApiController
{

    public function init()
    {
        parent::init();
        $this->registerContextForAction('saved-deals');
    }

    public function savedDealsAction()
    {

        $errorCode     = 0;
        $errorMMessage = null;
        if ($this->getRequest()->isPost()) {
            $user = _user();
            if ($user) {

                $body  = $this->getRequest()->getRawBody();
                $xml   = null;
                $deals = null;
                try {
                    $xml = @new SimpleXMLElement($body);
                } catch (Exception $e) {

                }

                if ($xml) {
                    if (isset($xml->deals)) {
                        $deals = $xml->deals;
                    }
                }
                if ($deals) {
                    foreach ($deals->children() as $dealElement) {
                        if ($dealElement) {
                            $dealId = (string)$dealElement['id'];
                            if (strlen($dealId) < 8) {
                                $errorCode     = 400;
                                $errorMMessage = 'You deal data structure is incorrect';
                                break;
                            }

                            $deal = Epr_Deal_Collection::getProvidedDealWithId($dealId);
                            if ($deal) {

                                if (isset($dealElement->onsite)) {
                                    $onsiteCode = (string)$dealElement->onsite;
                                    if (strlen($onsiteCode) >= 4) {
                                        $validator = new Zend_Validate_Barcode_Ean13();
                                        if ($validator->checkLength($onsiteCode) && $validator->checksum($onsiteCode)) {
                                            _user()->setDealCode($deal, Epr_Deal::TYPE_ONSITE, $onsiteCode);
                                        }
                                    }
                                }
                                if (isset($dealElement->online)) {
                                    $onlineCode = (string)$dealElement->online;
                                    if (strlen($onlineCode) >= 4) {
                                        _user()->setDealCode($deal, Epr_Deal::TYPE_ONLINE, $onlineCode);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $errorCode     = 400;
                    $errorMMessage = 'You data could not be read. Is it proper XML?';
                }

                if ($errorCode == 0) {
                    $deals = $user->getDeals();
                    if ($deals) {
                        $this->view->assign('deals', $deals);
                    }
                }
            }
        } else {
            $errorCode     = 405;
            $errorMMessage = 'Only POST requests are allowed to access this ressource.';
        }

        if ($errorCode != 0) {
            $this->view->assign('errorCode', $errorCode);
            $this->view->assign('errorMessage', $errorMMessage);
            $this->getResponse()->setHttpResponseCode($errorCode);
        }
    }

}