<?php

class Api_IndexController extends \Epr_Controller_ApiController
{

    public function indexAction()
    {
        $this->getHelper('viewRenderer')->setNoRender();
        $this->redirect($this->getCustomURL('index', 'info', 'api'));
    }

    public function barcodeAction()
    {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $type = $this->getRequest()->getParam('type', 'ean');
        $code = $this->getRequest()->getParam('code', null);

        if ($code) {
            if (strtolower($type) == 'ean') {

                $code = (string)__($code)->first(12);

                $barcodeOptions  = array('text' => $code);
                $rendererOptions = array('imageType' => 'jpg');

                Zend_Barcode::render('ean13', 'image', $barcodeOptions, $rendererOptions);
            }
        }

    }

}