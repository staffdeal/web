<?php

class Api_ComController extends Epr_Controller_ComController {


	public function errorAction() {
		$connection = Epr_Com_Connection::inboundConnection();
		$reply      = $connection->errorReplyMessage('Message name missing');
		$connection->reply();
	}

	public function indexAction() {
		$connection = Epr_Com_Connection::inboundConnection();
		if ($connection->getMessage() == null) {

			$replyMessage = Epr_Com_Message::createMessageWithName('reply');
			$replyMessage->setValue('Error', 'status');
			$replyMessage->setValue('The message you sent was malformed and could not be parsed.', 'Description');

			$connection->reply($replyMessage);
		}
	}

	//	public function pairAction() {
	//
	//		$connection = Epr_Com_Connection::inboundConnection();
	//		$message = $connection->getMessage();
	//
	//        $errorMessage = false;
	//
	//        $module = _app()->getModuleWithIdentifier('Dispatcher');
	//        if ($module && $module instanceof Epr_Modules_Dispatcher) {
	//
	//            if (!$module->isPaired()) {
	//
	//                $url = $message->getValue('url');
	//                $dispatcherKey = $message->getValue('key');
	//
	//                $backendKey = $module->pair($url,$dispatcherKey);
	//                if ($backendKey) {
	//
	//                    $reply = $connection->successReplyMessage();
	//                    $reply->setValue(Zend_Registry::get('client')->info->name,'name');
	//                    $reply->setValue($backendKey, 'key');
	//
	//                    $connection->reply();
	//
	//                } else {
	//                    $errorMessage = 'Could not pair, insufficent data';
	//                }
	//
	//            } else {
	//
	//                $errorMessage = 'Backend is already paired';
	//            }
	//
	//        } else {
	//            $errorMessage = 'Internal error loading the dispatcher module';
	//        }
	//
	//        if ($errorMessage !== false) {
	//            $connection->replyWithErrorMessage($errorMessage);
	//        }
	//	}
	//
	//    public function pingAction() {
	//
	//        $connection = Epr_Com_Connection::inboundConnection();
	//        $message = $connection->getMessage();
	//
	//        $replyMessage = $connection->successReplyMessage();
	//        foreach($message->getValues() as $key=>$value) {
	//            $replyMessage->setValue($value,$key);
	//        }
	//
	//        $connection->reply($replyMessage);
	//    }
	//

}