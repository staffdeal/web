<?php

class Api_NewsController extends \Epr_Controller_ApiController
{
    public function init()
    {
        parent::init();
        $this->registerContextForAction('index');
        $this->registerContextForAction('read');
    }

    public function indexAction()
    {

        $newsModule = _app()->getModuleWithIdentifier(Epr_Modules_News::identifier());
        if ($newsModule instanceof Epr_Modules_News) {

            $this->view->assign('maxAge', $newsModule->getMaximumNewsAge());
            $range = $newsModule->getNewsDateRange();

            $appVersion = _clientVersion();

            $allNews = null;
            $hasMoreNews = false;

            if ($appVersion == 1) {
                $page = 1;
                $allNews = Epr_News_Collection::getActiveInDateRange($range);
                $num2display = 0;
            } else  {
                $page = max(0, (int)$this->getRequest()->getParam("page", 1));

                $num2display = max(5, (int)$this->getRequest()->getParam('count', 10));
                $skip        = $num2display * ($page - 1);
                $allNews     = Epr_News_Collection::getActiveInDateRange($range, $num2display, true, $skip);
                $hasMoreNews = ($allNews->getTotalRows() > ($allNews->getOffset() + $num2display));
            }

            if ($allNews instanceof \Doctrine\CouchDB\View\Result) {

                $this->view->assign('hasMoreNews', $hasMoreNews);

                $this->view->assign('page', $page);
                $this->view->assign('count', count($allNews));
                $this->view->assign('requestedCount', $num2display);
                $this->view->assign('total', $allNews->getTotalRows());

                if ($page == 1) {
                    $allCategories = Epr_News_Category_Collection::getActiveCategoriesInRange($range);
                    usort(
                        $allCategories,
                        function ($a, $b) {

                            if ($a instanceof Epr_News_Category && $b instanceof Epr_News_Category) {
                                return strcasecmp($a->getTitle(), $b->getTitle());
                            }

                            return 0;
                        }
                    );
                    $this->view->assign('categories', $allCategories);
                }

                $newsToDisplay = array();

                foreach ($allNews as $news) {
                    if ($news instanceof Epr_News) {

                        if ($news->getCategory()->isEffectiveActive()) {
                            $newsToDisplay[] = $news;
                        }

                    }
                }
                $this->view->assign('news', $newsToDisplay);
            }
        }
    }

    public function readAction() {

        $news = null;

        $id = $this->getRequest()->getParam('id', false);
        if ($id) {
            try {
                $news = _dm()->find(Epr_News::DOCUMENTNAME, $id);
            } catch (Exception $e) {}
        }

        if ($news instanceof Epr_News) {
            $count = max(0, (int)$this->getRequest()->getParam('count', 1));
            if ($count > 0) {
                $news->registerView($count);
            }
            $this->view->assign('news', $news);
        }

    }

    public function saveAction()
    {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $connection = Epr_Com_Connection_Inbound::connection();
        $message    = $connection->getMessage();

        $errorText = false;
        $newsItem  = false;

        $apiID        = $message->getValue('id');
        $title        = $message->getValue('title');
        $date         = $message->getValue('publishDate');
        $categoryName = $message->getValue('category');
        $publicURL    = $message->getValue('publicURL');
        $text         = $message->getValue('text');

        if (!is_string($apiID) || strlen($apiID) == 0) {
            $apiID = Epr_Com_Connection::connectionKey();
        }


        if (is_string($title) && strlen($title) > 0) {

            $isNewNewsItem = false;
            $newsItem      = Epr_News_Collection::getByApiID($apiID);
            if ($newsItem === null) {
                $newsItem = new Epr_News();
                $newsItem->setApiID($apiID);
                $newsItem->setActive(true);
                $isNewNewsItem = true;

            }
            $newsItem->setHeadline($title);

            if (!$date instanceof DateTime) {
                $date = new DateTime('');
            }
            $newsItem->setPublishDate($date);

            if (is_string($categoryName) && strlen($categoryName)) {
                $category = Epr_News_Category_Collection::getCategoryByName($categoryName);
                if ($category) {
                    $newsItem->setCategory($category);
                }
            }

            if (\Poundation\PURL::isValidURLString($publicURL)) {
                $newsItem->setPublicUrl($publicURL);
            }

            if (is_string($text)) {
                $newsItem->setText($text);
            }

            if ($isNewNewsItem) {
                _dm()->persist($newsItem);
            }

            foreach ($message->getAttachments() as $key => $attachment) {
                if ($attachment instanceof Epr_Com_Message_Attachment) {
                    $image = \Poundation\PImage::createImageFromString($attachment->getData(), $attachment->getName());
                    if ($image) {

                    }
                }
            }

        } else {
            $errorText = 'No title provided.';
        }


        if ($newsItem instanceof Epr_News) {
            $reply = $connection->successReplyMessage();
            $reply->setValue($newsItem->getApiID(), 'id');
//            if ($newsItem->getCategory()) {
//                $reply->setValue($newsItem->getCategory()->getId(), 'categoryId');
//            }
            $connection->reply();
        } else {
            if ($errorText === false) {
                $errorText = 'Error processing news request.';
            }
            $connection->replyWithErrorMessage($errorText);
        }

    }
}
