<?php

class Api_InfoController extends Epr_Controller_ApiController {

	public function init() {
		parent::init();

		$this->registerContextForAction('index');
        $this->registerContextForAction('dealers');
	}

	public function indexAction() {
		$clientConfig = Zend_Registry::get('client');

		$this->view->assign('version', Epr_Application::application()->getDocumentVersion());
		$this->view->assign('clientName', $clientConfig->info->name);
		$this->view->assign('clientMail', $clientConfig->info->mail);

		$infoModule = _app()->getModuleWithIdentifier(Epr_Modules_Info::identifier());
		if ($infoModule instanceof Epr_Modules_Info) {
			$this->view->assign('about', $infoModule->getAboutText());
			$this->view->assign('impress', $infoModule->getImpressText());
			$this->view->assign('mediaURL', $infoModule->getMediaURL());
            $this->view->assign('facebookProfileName', $infoModule->getFacebookName());
            $this->view->assign('facebookProfileID', $infoModule->getFacebookProfileID());
            $this->view->assign('twitterProfileName', $infoModule->getTwitterName());

			if($infoModule->getFacebookName()) {
                $this->view->assign('FacebookPageURL', $infoModule->getFacebookPageUrl());
            }
			if($infoModule->getTwitterName()) {
                $this->view->assign('TwitterPageURL', $infoModule->getTwitterPageUrl());
            }
            $this->view->assign('reasonsMobile', $infoModule->getReasonsToRegisterMobile());
		}

		$appStoresModule = _app()->getModuleWithIdentifier(Epr_Modules_Appstores::identifier());
		if ($appStoresModule instanceof Epr_Modules_Appstores) {
			$this->view->assign('iOSVersion', $appStoresModule->getAppleVersionNumber());
			$this->view->assign('AndroidVersion', $appStoresModule->getGoogleVersionNumber());
			$this->view->assign('updateText', $appStoresModule->getUpdateText());
		}

		$token = _token();
		if ($token) {
			$this->view->assign('token', $token);
		}

		$modules = _app()->getAPIModules();
		if (is_array($modules)) {
			$this->view->assign('modules', $modules);
		}

		$userModule = _app()->getModuleWithIdentifier(Epr_Modules_Users::identifier());
		if ($userModule instanceof Epr_Modules_Users) {
			$registrationModule = $userModule->getUserRegistrationModule();
			if ($registrationModule) {
				$this->view->assign('registrationModule', $registrationModule);
			}
		}

		$analyticsModule = _app()->getModuleWithIdentifier(Epr_Modules_Analytics::identifier());
		$this->view->assign('analyticsModule', $analyticsModule);

	}
    public function dealersAction() {

        $dealers = Epr_Dealer_Collection::getAllActive();
        $this->view->assign('dealers', $dealers);

    }

}
