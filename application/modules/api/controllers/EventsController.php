<?php

class Api_EventsController extends Epr_Controller_ApiController {

    public function init() {
        parent::init();
        $this->registerContextForAction('index');
        $this->registerContextForAction('booking');
    }

    public function indexAction() {

        $events = Epr_Events_Collection::getAPIEvents();
        if ($events) {
            $this->view->assign('events', $events);
        }
    }

    public function bookingAction() {

        $event = null;
        $user = _user();

        $eventId = $this->getParam('id', null);
        if ($eventId) {
            $event = Epr_Events_Collection::eventWithId($eventId);
        }

        if ($event && $user) {
            $booking = $event->bookUser($user);
            if ($booking) {
                $this->view->assign('event', $event);
            }
        }

    }



}