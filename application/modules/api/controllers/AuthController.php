<?php

use Poundation\PMailAddress;

class Api_AuthController extends \Epr_Controller_ApiController
{

    public function init()
    {
        parent::init();
        $this->registerContextForAction('index');
        $this->registerContextForAction('token');
        $this->registerContextForAction('reset');
        $this->registerContextForAction('register');
        $this->registerContextForAction('change-password');
        $this->registerContextForAction('reset-password');
    }

    public function indexAction()
    {
        $this->redirect($this->getCustomURL('index', 'info', 'api'));
    }

    public function tokenAction()
    {

        $status = 'Authentication Error';

        $token = _token();
        if ($token) {
            $this->view->assign('token', $token);
            $status = 'Redundant login, OK';

        } else {

            $request  = $this->getRequest();
            $email    = PMailAddress::createFromString($request->getParam('email', ''));
            $password = $request->getParam('password', false);

            if (isset($email) && $password !== false) {

                $authAdapter = new Epr_Auth_Adapter(array('noSession' => true));
                $authAdapter->setIdentity((string)$email);
                $authAdapter->setCredential($password);

                $auth = Zend_Auth::getInstance();
                if ($auth->authenticate($authAdapter)->isValid()) {
                    _user()->markAsMobileLoggedIn();
                    $newToken = Epr_Token::createAPIToken(_user());
                    $newToken->setAdditionalInformation($_SERVER['HTTP_USER_AGENT']);
                    _dm()->persist($newToken);
                    _dm()->flush(); // we flush here since we need the id right now

                    $this->view->assign('token', $newToken);
                    $status = 'OK';
                }
            }
        }

        $this->view->assign('status', $status);
    }

    public function cleanupAction()
    {

        $this->getHelper('viewRenderer')->setNoRender();

        $repo      = Epr_Token_Collection::getRepository();
        $allTokens = $repo->findAll();

        $now               = new DateTime();
        $now               = $now->getTimestamp();
        $invalidDifference = 120;

        $count = 0;
        foreach ($allTokens as $token) {
            if ($token instanceof Epr_Token) {

                $lastUsage = $token->getLastUsageDate()->getTimestamp();
                $age       = $now - $lastUsage;
                if ($age > $invalidDifference) {
                    _dm()->remove($token);
                    $count++;
                }
            }
        }

        echo "Removed " . $count . " " . (($count == 1) ? "token" : "tokens") . '.';

    }

    public function resetAction()
    {
        $token = _token();
        if ($token) {
            _dm()->remove($token);
        }
    }

    public function registerAction()
    {

        $status  = false;
        $error   = null;
        $code    = 0;
        $subCode = 0;

        $dry = false;
        if (!_app()->isProduction()) {
            $dry = ($this->getRequest()->getParam('dryRun', false) == 'true');
        }

        // first, we get the module that handles the registration
        $usersModule = _app()->getModuleWithIdentifier(Epr_Modules_Users::identifier());
        if ($usersModule instanceof Epr_Modules_Users) {
            $module = $usersModule->getUserRegistrationModule();
            if ($module instanceof Epr_Module_UserRegistration_Abstract) {

                $params = $this->_request->getParams();
                // we check of the module is happy with the params
                if ($module->isRegistrationDataSufficient($params, $error, $subCode)) {

                    // we check of the mail address is already known
                    $email           = (string)__($params['email'])->lowercase();
                    $usersCollection = Epr_User_Collection::getRepository();
                    $existingUsers   = $usersCollection->findBy(array('email' => $email));
                    if (count($existingUsers) == 0) {

                        try {
                            // creating a new user object
                            $newUser = $module->createNewUser($params, Epr_User::REGISTRATION_CONTEXT_API);
                            if ($newUser) {
                                $newUser->setRole(Epr_Roles::ROLE_USER);
                                // we pass the user and the params to the module so it can process it
                                // IMPORTANT: $params must be the same object it was earlier since the module might
                                // have change it
                                if ($module->processNewUser($newUser, $params)) {
                                    // if the module confirmed the creation we persist the new user
                                    $newUser->activate();
                                    if (!$dry) {
                                        _dm()->persist($newUser);
                                    }
                                    $status = 'OK';
                                    $code   = 200;
                                } else {
                                    throw new Exception('Internal error, passed params are not valid though they have been checked successfully.');
                                }
                            }

                        } catch (Exception $e) {
                            $status = 'Internal Error: ' . $e->getMessage();
                            $code   = 500;
                        }

                    } else {
                        $status  = 'Email already exists';
                        $code    = 404;
                        $subCode = 1;
                    }

                } else {
                    $status = 'Parameters insufficient';
                    if (!is_null($error)) {
                        $status .= ': ' . $error;
                    }
                    $code = 405;
                }

            } else {
                $status = 'Internal Error';
                $code   = 500;
            }
        } else {
            $status = 'Internal Error';
            $code   = 500;
        }

        if ($code == 200) {
            $this->view->assign('status', $status);
        } else {
            $this->_helper->viewRenderer('error-general');
            $this->view->assign('code', $code);
            $this->view->assign('message', $status);
            $this->getResponse()->setHttpResponseCode($code);
            if ($subCode != 0) {
                $this->view->assign('subCode', $subCode);
            }
        }

    }

    public function changePasswordAction()
    {

        $code    = 0;
        $subCode = 0;
        $message = null;

        $token = _token();
        if ($token && $token->getType() == Epr_Token::TYPE_API_ACCESS) {

            $oldPassword = $this->getParam('oldPassword', null);
            $newPassword = $this->_getParam('newPassword', null);

            if (is_string($oldPassword) && strlen($oldPassword) > 0) {

                if (is_string($newPassword) && strlen($newPassword) > 0) {

                    if ($newPassword !== $oldPassword) {

                        $user = $token->getUser();
                        if ($user instanceof Epr_User && $user->checkPassword($oldPassword)) {

                            $usersModule = _app()->getModuleWithIdentifier(Epr_Modules_Users::identifier());
                            if ($usersModule instanceof Epr_Modules_Users) {

                                $registrationModule = $usersModule->getUserRegistrationModule();

                                $success = false;

                                if ($registrationModule instanceof Epr_Module_UserRegistration_Abstract) {
                                    $success = $registrationModule->isPasswordConsideredSafe($newPassword);
                                } else {
                                    // if the current module does not support password complexity we skip this
                                    $success = true;
                                }

                                if ($success) {
                                    $user->setPassword($newPassword);
                                    $code    = 200;
                                    $message = '"Password changed';
                                } else {
                                    $code    = 405;
                                    $message = 'Password does not fulfill complexity rules';
                                }
                            } else {
                                $code    = 500;
                                $message = 'Internal server error, no user registration module found';
                            }
                        } else {
                            $code    = 403;
                            $message = 'Old password is incorrect';
                        }

                    } else {
                        $code    = 400;
                        $message = 'The new password matches the old one';
                    }

                } else {
                    $code    = 400;
                    $message = 'You need to provide the new password';
                }

            } else {
                $code    = 400;
                $message = 'You need to provide the old password';
            }

        } else {
            $code    = 401;
            $message = 'You need to provide a token to change the password';
        }

        if ($code != 200) {
            $this->_helper->viewRenderer('error-general');
            $this->view->assign('code', $code);
            $this->view->assign('message', $message);
            $this->getResponse()->setHttpResponseCode($code);
            if ($subCode != 0) {
                $this->view->assign('subCode', $subCode);
            }
        } else {
            $this->view->assign('code', $code);
            $this->view->assign('message', $message);
        }

    }

    public function resetPasswordAction()
    {

        $code    = 0;
        $subCode = 0;
        $message = null;

        $emailString = $this->getRequest()->getParam('email', null);
        if (!is_null($emailString)) {

            $email = PMailAddress::createFromString($emailString);

            if (!is_null($email)) {
                $user = Epr_User::getUserWithEmail($email);

                if ($user) {

                    $userModule = _app()->getModuleWithIdentifier(Epr_Modules_Users::identifier());
                    if ($userModule instanceof Epr_Modules_Users) {

                        if ($userModule->resetUser($user)) {
                            $code = 200;
                        } else {
                            // error resetting the password
                            _app()->sendAdminMail('Could not reset a user password', 'The user password could not be reset. Check the user module.');
                            $code    = 500;
                            $subCode = 1;
                            $message = 'Internal error resetting the password';
                        }

                    } else {
                        // internal error
                        $code    = 500;
                        $subCode = 2;
                        $message = 'Internal error resetting the password';
                    }

                } else {
                    // unknown user
                    $code    = 403;
                    $subCode = 1;
                    $message = 'The email address provided is not known.';

                }
            } else {
                $code    = 400;
                $subCode = 2;
                $message = 'Malformed parameter "email"';

            }
        } else {
            $code    = 400;
            $subCode = 1;
            $message = 'Missing parameter "email"';
        }

        if ($code != 200) {
            $this->_helper->viewRenderer('error-general');
            $this->view->assign('code', $code);
            $this->view->assign('message', $message);
            $this->getResponse()->setHttpResponseCode($code);
            if ($subCode != 0) {
                $this->view->assign('subCode', $subCode);
            }
        } else {
            $this->view->assign('code', $code);
            $this->view->assign('message', $message);
        }


    }

}
