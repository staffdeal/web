<?php

class Api_IdeasController extends \Epr_Controller_ApiController
{

    public function init()
    {
        parent::init();
        $this->registerContextForAction('index');
        $this->registerContextForAction('list');
        $this->registerContextForAction('competitions');
        $this->registerContextForAction('submit');
    }

    /**
     * @return \Poundation\PDate
     */
    private function getEndDate() {
        $endDate = \Poundation\PDate::now()->addDays(0 - $this->getCompetitionsMaximumAge());
        return $endDate;
    }

    private function getCompetitionsMaximumAge() {
        $maxAge      = 0;
        $ideasModule = _app()->getModuleWithIdentifier('Ideas');
        if ($ideasModule instanceof Epr_Modules_Ideas) {
            $maxAge = max(0, $ideasModule->getCompetitionsMaximumNewsAge());
        }
        return $maxAge;
    }

	public function indexAction() {
		$this->redirect($this->getCustomURL('list','ideas','api'));
	}

    /**
     * Returns Ideas for a competition
     * @throws Epr_Exception_Api
     */
    public function listAction()
    {
        $competitionId = $this->_request->getParam('competition', false);
        if($competitionId === false) {
            $this->redirect($this->getCustomURL('missingparamter','error','api'));
        }

        $ideas = Epr_Idea_Collection::findByCompetitionId($competitionId);
        $this->view->assign('ideas', $ideas);
    }

    /**
     * Returns current competitions
     */
    public function competitionsAction() {

        $this->view->assign('competitionsMaxAge', $this->getCompetitionsMaximumAge());
        $endDate = $this->getEndDate();

    	$categories = Epr_Idea_Competition_Collection::getActiveCategoriesUntilDate($endDate);
    	$this->view->assign('categories',$categories);
    	
        $loadedCompetitions = Epr_Idea_Competition_Collection::getActiveUntilDate($endDate);
        $competitions = array();
        foreach ($loadedCompetitions as $competition) {
            /** @var Epr_Idea_Competition $competition */
            if ($competition->getCategory()->isActive()) {
                $competitions[] = $competition;
            }
        }

        $this->view->assign('competitions', $competitions);
    }

    public function submitAction()
    {
        if ($this->_request->isPost()) {

            $token = _token();
            if (is_null($token) || is_null($token->getUser())) {
                $this->redirect($this->getCustomURL('noauth','error','api'));
                return;
            }

            $id = $this->_request->getParam('id', false);
            $competitionId = $this->_request->getParam('competition', false);

            if (!$competitionId) {
                $this->view->assign('success', false);
                $this->view->assign('message', 'No Competition given');
                return;
            }
			
            $competition = _dm()->getRepository('Epr_Idea_Competition')->find($competitionId);


            if ($id) {
                $idea = _dm()->getRepository('Epr_Idea')->find($id);
            } else {
                $idea = new Epr_Idea();
            }

            try {

            	$submittedText = $this->_request->getParam('text',false);
            	$submittedURL = $this->_request->getParam('url',false);
            	
            	if ($submittedText === false) {
            		$this->view->assign('success', false);
            		$this->view->assign('message', "No field 'text' in request");
            	} else {
            	
					$idea->setSubmissionDate(new DateTime());
					$idea->setText($submittedText);

                    $url = \Poundation\PURL::URLWithString($submittedURL);
                    if ($url) {
                        $idea->setUrl($url);
                    }

                	$idea->setUser($token->getUser());
                	$idea->setCompetition($competition);
                    foreach($this->getImages() as $image) {
                        if ($image instanceof Epr_Content_Image) {
                            $image->setAccess(Epr_Content_Image::ACCESS_IDEA_IMAGE);
                            _dm()->persist($image);
                            $idea->addImage($image);
                        }
                    }


                	_dm()->persist($idea);
                	$this->view->assign('success', true);
                	$this->view->assign('message',$idea->getId());
            	}
                return;
            } catch (Exception $e) {
                $this->view->assign('success', false);
                $this->view->assign('message', $e->getMessage());
            }
        }

        $this->view->assign('success', false);
        $this->view->assign('message', 'GET method not supported');
    }

    private function getImages($fieldName = null)
    {
        $uploadAdapter = new Zend_File_Transfer_Adapter_Http();

        $files  = $uploadAdapter->getFileInfo();

        $images = array();

        foreach($files as $file => $fileInfo) {
            if ($uploadAdapter->isUploaded($file)) {
                if ($uploadAdapter->isValid($file)) {
                    if ($uploadAdapter->receive($file)) {
                        $info = $uploadAdapter->getFileInfo($file);
                        $tmp  = $info[$file]['tmp_name'];

                        $image = \Poundation\PImage::createImageFromFilename($tmp);
                        if ($image) {
                            $contentImage = Epr_Content_Image::imageFromPImage($image);
                            if ($contentImage) {
                                $images[] = $contentImage;
                            }
                        }
                    }
                }
            }
        }


        return $images;
    }

}