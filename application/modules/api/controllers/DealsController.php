<?php

class Api_DealsController extends \Epr_Controller_ApiController
{

    public function init()
    {
        parent::init();
        $this->registerContextForAction('index');
        $this->registerContextForAction('status');
        $this->registerContextForAction('dealers');
        $version = _clientVersion();
        if ($version > 1) {
            $this->registerContextForAction('redeem');
        }
    }

    public function indexAction()
    {
        $deals = Epr_Deal_Collection::getProvidedDeals();
        if (is_array($deals)) {
            $reminderDate = \Poundation\PDate::today()->addDays(4);
            foreach ($deals as $deal) {
                if ($deal instanceof Epr_Dispatcher_Deal) {
                    if ($deal->getReminderText()) {
                        $endDate = \Poundation\PDate::createDate($deal->getEndDate());
                        if (!$endDate->isBefore($reminderDate)) {
                            $deal->setReminderText(null); // removing the reminder text if the deal is not about to expire
                        }
                    }
                }
            }
        }
        $this->view->assign('deals', $deals);
    }

    public
    function statusAction()
    {
        $deal = null;

        $dealId = $this->getRequest()->getParam('id', false);
        if ($dealId) {
            $deal = Epr_Deal_Collection::getProvidedDealWithId($dealId);
        }

        if ($deal) {
            $this->view->assign('deal', $deal);
        }
    }

    public
    function dealersAction()
    {

        $dealId = $this->getRequest()->getParam('id', false);
        if ($dealId) {

            $list = Epr_Deal_Collection::getDealersForDeal($dealId);
            if ($list) {
                $this->view->assign('dealId', $dealId);
                $this->view->assign('dealers', $list);
            }
        }
    }

    /**
     * Action to redeem given deals by ID
     * - action will be called through external provider clients
     */
    public
    function redeemAction()
    {
        $success = false;

        $dealId = $this->getRequest()->getParam('dealId', false);
        if ($dealId) {

            $deal = _dm()->find(Epr_Deal::DOCUMENTNAME, $dealId);
            if ($deal instanceof Epr_Deal) {

                $version = _clientVersion();
                if ($version == 1) {

                    $this->_helper->layout()->disableLayout();
                    $this->_helper->viewRenderer->setNoRender(true);

                    $codeModule = $deal->getOnsiteCodeModule();
                    if ($codeModule->isValid()) {

                        $code = $codeModule->redeem();
                        if ($code) {

                            // since Zend_Barcode adds the checksum automatically we need to remove it first
                            $code = (string)__($code)->first(12);

                            $barcodeOptions  = array('text' => $code);
                            $rendererOptions = array('imageType' => 'jpg');

                            Zend_Barcode::render('ean13', 'image', $barcodeOptions, $rendererOptions);

                            $success = true;
                        }
                    }

                    if (!$success) {
                        $this->getResponse()->setHttpResponseCode(400);
                        $this->getResponse()->setHeader('Content-type', 'text/text');
                        $this->getResponse()->setBody('No barcode');
                    }
                } else {

                    $data = array();

                    $type = $this->getRequest()->getParam('type', 'onsite');
                    if ($type == 'onsite') {
                        $codeModule = $deal->getOnsiteCodeModule();
                        if ($codeModule->isValid()) {
                            $code = $codeModule->redeem();
                            if ($code) {
                                //$code            = (string)__($code)->first(12);
                                $success         = true;
                                $data['ean']     = $code;
                                $data['barcode'] = $url = _app()->getPublicURL()->addPathComponent('/api/barcode/ean/')
                                    ->addPathComponent($code);
                            }
                        }
                    } else if ($type == 'online') {
                        $codeModule = $deal->getOnlineCodeModule();
                        if ($codeModule->isValid() && $codeModule->isAvailable()) {

                            $code = $codeModule->redeem();
                            if ($code) {
                                $success = true;
                                if (is_string($code)) {
                                    $data['code'] = $code;
                                }
                                $url = $codeModule->getURL();
                                if ($url) {
                                    $data['url'] = $codeModule->getURL();
                                } else {
                                    $description = $codeModule->getDescription();
                                    if ($description) {
                                        $data['description'] = $description;
                                    }
                                }

                                if ($codeModule instanceof Epr_Modules_CodeList) {
                                    if ($codeModule->getMode() == Epr_Modules_CodeList::MODE_UNIQUE) {
                                        Epr_Deal_Collection::invalidateCacheForProvidedDeals();
                                    }
                                }

                            }
                        }

                        $codeData                            = $deal->getCodeModulesData();
                        $codeData[$codeModule->getDataKey()] = $codeModule->getData();
                        $deal->setCodeModulesData($codeData);

                    }

                    $this->view->assign('deal', $deal);
                    $this->view->assign('data', $data);


                }
            }
        }


    }

}