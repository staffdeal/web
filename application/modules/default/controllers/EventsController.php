<?php

class EventsController extends Epr_Frontend_Controller_Action {

    public function indexAction() {
        $page = Epr_Cms_Page::getPageWithPath(Epr_Modules_Events::PAGE_INDEX_PATH);
        $this->view->assign('page', $page);
    }

    public function showAction() {
        $page = Epr_Cms_Page::getPageWithPath(Epr_Modules_Events::PAGE_SHOW_PATH);
        $this->view->assign('page', $page);

        $event = null;
        $slug = $this->getRequest()->getParam('slug', null);
        if ($slug) {
            $event = Epr_Events_Collection::eventWithSlug($slug);
        }

        if ($event instanceof Epr_Event) {
            $detailsElement = $page->getElementWithPath(Epr_Modules_Events::SHOW_DETAILS_PATH);
            if ($detailsElement instanceof Epr_Modules_Events_DetailsElement) {
                $detailsElement->setEvent($event);
                $detailsElement->setAllowBooking($event->getCanBeBooked());
            }
        }

    }

    public function bookingAction() {

        $event = null;
        $slug = $this->getRequest()->getParam('slug', null);
        if ($slug) {
            $event = Epr_Events_Collection::eventWithSlug($slug);
        }

        if ($event instanceof Epr_Event && $event->getCanBeBooked() && _user()) {
            $event->bookUser(_user());
        }

        $this->showAction();
        $this->render('show');

    }

}