<?php

class ErrorController extends Epr_Frontend_Controller_Action
{

	/**
	 * The default action - show the home page
	 */
	public function errorAction()
	{

		$is404 = false;

		$logger = _logger();
		if ($logger) {
			// we need to check the existance of the logger since we may be invoked because the logger failed
			$logger->warn('Default ErrorController');
		}

		$defaultError            = new stdClass();
		$defaultError->type      = 'EXCEPTION_OTHER';
		$defaultError->exception = new Exception('Error Handler - Unknown Error', -1);

		$error = $this->_getParam('error_handler', $defaultError);

		switch ($error->type) {
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
				// 404 Fehler -- Controller oder Aktion nicht gefunden
				$is404 = true;
				break;

			default:
				// Anwendungsfehler; Fehler Seite anzeigen, aber den
				// Status Code nicht ändern

				if (isset($error->noAuth) && $error->noAuth) {
					//$this->_redirect('/admin/auth/login');
					$this->view->assign('message', $error->exception->getMessage());
				} else {
					$this->view->assign('message', $error->exception->getMessage());
				}

				break;

		}

		if (!_app()->isProduction()) {
			$this->view->assign('error', $error);
		}

		if ($is404) {
			$this->getResponse()->setRawHeader('HTTP/1.1 404 Not Found');
			$this->render('error404');
		}
	}

	public function error404Action() {

	}

}

