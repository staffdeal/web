<?php

class StaticController extends Epr_Frontend_Controller_Action
{

    const IMAGES_MAXAGE = 3600;
    const CSS_MAXAGE = 60;

    public function preDispatch()
    {

        $this->addDirectOutputAction('css');
        $this->addDirectOutputAction('image');
        $this->addDirectOutputAction('favicon');

        parent::preDispatch();

    }

    public function cssAction()
    {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $content = '';

        $filename = $this->getRequest()->getParam('filename', null);

        if ($filename == 'theme.css') {

            $theme = Epr_Theme_Collection::getActiveTheme();
            $content = $theme->getCSS();
            $this->getResponse()->setHeader('Content-type', 'text/css');
            $this->getResponse()->setHeader('Pragma', 'public', true);
            $this->getResponse()->setHeader('Cache-Control', ' max-age=' . self::CSS_MAXAGE, true);
            $this->getResponse()
                ->setHeader('Expires', gmdate('D, d M Y H:i:s \G\M\T', time() + self::CSS_MAXAGE), true);
        } else {
            $this->fileNotFound($filename);
        }

        $this->getResponse()->setBody($content);

    }

    public function imageAction()
    {

        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $height = 0;
        $width = 0;

        $filename = $this->getRequest()->getParam('filename', false);
        $originalFilename = $filename;
        $imageDocument = Epr_Content_Image_Collection::imageWithFilename($originalFilename);


        $extension = null;

        if (is_string($filename) && strlen($filename) > 0) {

            $extensionComponents = explode('.', $filename);
            if (count($extensionComponents) > 1) {
                $extension = $extensionComponents[count($extensionComponents) - 1];
                unset($extensionComponents[count($extensionComponents) - 1]);
                $filename = implode('.', $extensionComponents);
            }


            $filenameComponents = explode('-', $filename);
            if (count($filenameComponents) > 1) {
                $dimensions = strtolower($filenameComponents[count($filenameComponents) - 1]);

                $dimensionComponents = explode('x', $dimensions);
                if (count($dimensionComponents) == 2) {
                    $width = (int)$dimensionComponents[0];
                    $height = (int)$dimensionComponents[1];

                    if (!$imageDocument) {
                        unset($filenameComponents[count($filenameComponents) - 1]);
                        $filenameCandidate = implode('-', $filenameComponents);
                        $imageDocument = Epr_Content_Image_Collection::imageWithFilename($filenameCandidate);
                        if ($imageDocument instanceof Epr_Content_Image) {
                            $filename = $filenameCandidate;
                        } else {
                            $imageDocument = Epr_Content_Image_Collection::imageWithId($filenameCandidate);
                            if ($imageDocument instanceof Epr_Content_Image) {
                                $filename = $filenameCandidate;
                            }
                        }

                        if (!$imageDocument instanceof Epr_Content_Image) {
                            $filenameCandidate .= '.' . $extension;
                            $imageDocument = Epr_Content_Image_Collection::imageWithFilename($filenameCandidate);
                            if ($imageDocument instanceof Epr_Content_Image) {
                                $filename = $filenameCandidate;
                            }
                        }
                    }
                }
            }

            if (!$imageDocument) {
                $imageDocument = Epr_Content_Image_Collection::imageWithFilename($filename);
                if (!$imageDocument) {
                    $imageDocument = Epr_Content_Image_Collection::imageWithId($filename);
                }
            }
        }


        if ($imageDocument) {


            $browserEtag = $this->getFrontController()->getRequest()->getHeader('If-None-Match', null);
            if (is_string($browserEtag) && $browserEtag == $imageDocument->getVersion()) {
                $this->getResponse()->setHttpResponseCode(304);
            } else {

                $image = null;

                if ($height == 0 && $this->getParam('height', false) !== false) {
                    $height = (int)$this->getParam('height', 0);
                }
                if ($width == 0 && $this->getParam('width', false) !== false) {
                    $width = (int)$this->getParam('width', 0);
                }

                if ($width > 0 && $height == 0) {

                    $sizedImage = $imageDocument->getImageByWidth($width);
                    if ($sizedImage) {
                        $image = $sizedImage;
                    }

                } else if ($width == 0 && $height > 0) {

                    $sizedImage = $imageDocument->getImageByHeight($height);
                    if ($sizedImage) {
                        $image = $sizedImage;
                    }

                } else if ($width > 0 && $height > 0) {
                    $sizedImage = $imageDocument->getCropedImageBySize($width, $height);
                    if ($sizedImage) {
                        $image = $sizedImage;
                    }
                }

                if ($image == null) {
                    $image = $imageDocument->getImage();
                }

                if (!is_null($image)) {

                    $mime = null;
                    if (!is_null($extension)) {
                        $mime = \Poundation\PMIME::createMIMEWithFileExtension($extension);
                    }
                    if (!$mime instanceof \Poundation\PMIME || !$mime->isImage()) {
                        $mime = \Poundation\PMIME::createMIMEWithType($image->getMIME());
                    }

                    $this->getResponse()->setHeader('Content-type', $mime->getMIMEType());
                    $this->getResponse()->setHeader('Content-Transfer-Encoding', 'binary', true);
                    $this->getResponse()->setHeader('Etag', $imageDocument->getVersion());
                    $this->getResponse()->setHeader('Pragma', 'public, no-cache', true);
//                $this->getResponse()->setHeader('Cache-Control', ' max-age=' . self::IMAGES_MAXAGE, ', no-cache', true);
                    $this->getResponse()->setHeader('Cache-Control', 'no-cache', true);
//                $this->getResponse()
//                    ->setHeader('Expires', gmdate('D, d M Y H:i:s \G\M\T', time() + self::IMAGES_MAXAGE), true);
                    $this->getResponse()->setBody($image->getData($mime));
                } else {
                    $this->fileNotFound($filename);
                }
            }
        } else {
            $this->fileNotFound($filename);
        }

    }

    public function faviconAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->getResponse()->setHttpResponseCode(404);
    }

    private function fileNotFound($filename)
    {
        $this->getResponse()->setHttpResponseCode(404);
    }

    public function pageAction()
    {

        $page = null;

        $pagePath = $this->getRequest()->getParam('path', null);
        if (is_string($pagePath) && strlen($pagePath) > 0) {
            $page = Epr_Cms_Page::getPageWithPath($pagePath);
        }

        if ($page instanceof Epr_Cms_Page) {
            $this->view->assign('page', $page);
        }

    }

    public function templateAction()
    {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $content = null;

        $modulePath = $this->getRequest()->getParam('module', null);
        $platform = $this->getRequest()->getParam('platform', null);
        $id = $this->getRequest()->getParam('id', null);

        if (is_string($modulePath) && is_string($id)) {

            $module = _app()->getAPIModuleWithPath($modulePath);
            if ($module) {
                $id = __($id)->removeTrailingCharactersWhenMatching('.html');
                if ($id->length() > 0 && strlen($platform) > 0) {
                    $value = $module->getTemplateValueForIdentifier($id, $platform);
                    if ($value) {
                        $content = $value;
                    }
                }
            }
        }

        if (strlen($content) > 0) {
            $this->getResponse()->setBody($content);
        } else {
            $this->fileNotFound($id . '.html');
        }


    }

}