<?php

/**
 * Ideas Frontend Controller
 *
 */
class IdeasController extends Epr_Frontend_Controller_Action
{

	public function indexAction()
	{

		$page = Epr_Cms_Page::getPageWithPath(Epr_Modules_Ideas::PAGE_PATH);
		$this->view->assign('page', $page);

	}

	public function categoryAction()
	{
		$page        = Epr_Cms_Page::getPageWithPath(Epr_Modules_Ideas::PAGE_PATH);
		$menuElement = $page->getElementWithPath(Epr_Modules_Ideas::MENU_PATH);
		$listElement = $page->getElementWithPath(Epr_Modules_Ideas::LIST_PATH);

		$slug     = $this->getRequest()->getParam('ideaCategorySlug');
		$category = _dm()->getRepository('Epr_Idea_Category')->findOneBy(array('slug' => $slug));
		if ($category instanceof Epr_Idea_Category) {

			if ($menuElement instanceof Epr_Modules_Ideas_CategoryMenuElement) {
				$menuElement->setCurrentCategory($category);
			}

			if ($listElement instanceof Epr_Modules_Ideas_Competitions_ListElement) {
				$listElement->setCurrentCategory($category);
			}

		}

		$this->view->assign('page', $page);
		$this->render('index');

	}

	public function showAction()
	{

		$slug        = $this->getRequest()->getParam('competitionSlug');
		$competition = _dm()->getRepository('Epr_Idea_Competition')->findOneBy(array('slug' => $slug));

		$page           = Epr_Cms_Page::getPageWithPath(Epr_Modules_Ideas::PAGE_COMPETITION_DETAILS);
		$menuElement    = $page->getElementWithPath(Epr_Modules_Ideas::MENU_PATH);
		$detailsElement = $page->getElementWithPath(Epr_Modules_Ideas::COMPETITION_DETAILS_PATH);

		if ($competition instanceof Epr_Idea_Competition) {
			if ($menuElement instanceof Epr_Modules_Ideas_CategoryMenuElement) {
				$menuElement->setCurrentCategory($competition->getCategory());
			}
			if ($detailsElement instanceof Epr_Modules_Ideas_Competitions_DetailsElement) {
				$detailsElement->setCompetition($competition);
			}
		}

		$this->view->assign('page', $page);
		$this->render('index');

	}

	public function previewCompetitionAction()
	{

		$competition = null;

		$id = $this->getRequest()->getParam('id', null);
		if (strlen($id) > 0) {
			$competition = _dm()->find(Epr_Idea_Competition::DOCUMENTNAME, $id);
		}

		if ($competition instanceof Epr_Idea_Competition && _user() && _user()->isAdministrator()) {

			$page           = Epr_Cms_Page::getPageWithPath(Epr_Modules_Ideas::PAGE_COMPETITION_DETAILS);
			$menuElement    = $page->getElementWithPath(Epr_Modules_Ideas::MENU_PATH);
			$detailsElement = $page->getElementWithPath(Epr_Modules_Ideas::COMPETITION_DETAILS_PATH);

			if ($competition instanceof Epr_Idea_Competition) {
				if ($menuElement instanceof Epr_Modules_Ideas_CategoryMenuElement) {
					$menuElement->setCurrentCategory($competition->getCategory());
				}
				if ($detailsElement instanceof Epr_Modules_Ideas_Competitions_DetailsElement) {
					$detailsElement->setCompetition($competition);
					$detailsElement->setShowEditLink(true);
				}
			}

			$this->view->assign('page', $page);
			$this->render('index');
		} else {
			$this->redirect($this->getCustomURL('index'));
		}
	}

	/**
	 * User action to add an idea to given competition
	 */
	public function submitAction()
	{

		$slug = $this->getRequest()->getParam('slug', null);

		$competition = _dm()->getRepository('Epr_Idea_Competition')->findOneBy(array('slug' => $slug));

		$this->view->assign('competition', $competition);

		$status = '';

		if ($competition instanceof Epr_Idea_Competition) {

			$form = new Epr_Form_Frontend_Idea();

			if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getParams())) {

				$text = (string)__($form->getValue(Epr_Form_Frontend_Idea::FIELD_TEXT))->stripTags();
				$url  = \Poundation\PURL::URLWithString($form->getValue(Epr_Form_Frontend_Idea::FIELD_URL));

				// Save Idea
				$idea = new Epr_Idea();
				$idea->setUser(_user());
				$idea->setCompetition($competition);

				$idea->setText($text);
				$idea->setUrl($url);

				foreach ($this->getImages() as $image) {
					if ($image instanceof Epr_Content_Image) {
						$image->setAccess(Epr_Content_Image::ACCESS_IDEA_IMAGE);
						_dm()->persist($image);
						$idea->addImage($image);
					}
				}
				_dm()->persist($idea);

				$this->view->assign('success', true);

				$backendURL = _app()->getPublicURL()->addPathComponent($this->getCustomURL('show', 'ideas', 'admin', array('id' => $idea->getId())));
				_app()->sendAdminMail('A new idea has been submitted.', sprintf('Somebody posted a new idea. It is visible to other users. You may to deactivate the idea here: %s.', $backendURL));
			}
		} else {
			$this->redirect($this->getCustomURL('index'));
		}

		$this->view->assign('form', $form);
	}


	/**
	 * Shows an idea detail
	 */
	public function detailsAction()
	{
		_logger()->info("Default IdeasController Details-Action");
		$request = $this->getRequest();
		$id      = $request->getParam('id', false);
		if (!$id) {
			throw new Zend_Controller_Action_Exception('Idea not available', 404);
		}

		$idea = _dm()->getRepository('Epr_Idea')->find($id);
		if (!$idea instanceof Epr_Idea) {
			throw new Zend_Controller_Action_Exception('Idea not available', 404);
		}

		$this->view->assign('idea', $idea);
	}

	/**
	 * List ideas for a single competition;
	 */
	public function listIdeasAction()
	{
		$request         = $this->getRequest();
		$categorySlug    = $request->getParam('ideaCategorySlug');
		$competitionSlug = $this->getRequest()->getParam('competitionSlug');
		$competition     = _dm()->getRepository('Epr_Idea_Competition')->findOneBy(array('slug' => $competitionSlug));

		$ideas = $competition->getIdeas();
		$this->view->assign('ideas', $ideas);
	}

	/**
	 * @param null $fieldName
	 *
	 * @return array
	 */
	private function getImages($fieldName = null)
	{
		$uploadAdapter = new Zend_File_Transfer_Adapter_Http();

		$files = $uploadAdapter->getFileInfo();

		$images = array();

		foreach ($files as $file => $fileInfo) {
			if ($uploadAdapter->isUploaded($file)) {
				if ($uploadAdapter->isValid($file)) {
					if ($uploadAdapter->receive($file)) {
						$info = $uploadAdapter->getFileInfo($file);
						$tmp  = $info[$file]['tmp_name'];

						$image = \Poundation\PImage::createImageFromFilename($tmp);
						if ($image) {
							$contentImage = Epr_Content_Image::imageFromPImage($image);
							if ($contentImage) {
								$images[] = $contentImage;
							}
						}
					}
				}
			}
		}

		return $images;
	}
}
