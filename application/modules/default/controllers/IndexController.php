<?php

/**
 * Index Controller
 *
 */
class IndexController extends Epr_Frontend_Controller_Action
{


    public function indexAction()
    {
        $indexPage = Epr_Cms_Page::getPageWithPath('index');
        $this->view->assign('page', $indexPage);
    }

    public function appsAction()
    {
        $url = null;

        $userAgent = \Poundation\PString::createFromString($_SERVER['HTTP_USER_AGENT'])->lowercase();

        $module = _app()->getModuleWithIdentifier(Epr_Modules_Appstores::identifier());
        if ($module instanceof Epr_Modules_Appstores) {
            if ($userAgent->contains('iphone') || $userAgent->contains('ipod') || $userAgent->contains('ipad')) {
                $url = $module->getAppStoreURL();
            } else if ($userAgent->contains('android')) {
                $url = $module->getPlayStoreURL();
            }
        }

        if ($url) {
            $this->redirect($url);
        } else {
            $this->redirect('/');
        }

    }

    public function fourOhFourAction()
    {

    }

}