<?php

/**
 * User Controller
 * Handles all user relevant Stuff
 * - login
 * - logout
 * - register
 */

use Poundation\PMailAddress;

class UserController extends Epr_Frontend_Controller_Action
{

    public function indexAction()
    {
        $this->redirect($this->getCustomURL('index', 'index'));
    }

    //Handles User Login
    public function loginAction()
    {
        if (_user() instanceof Epr_User) {
            $this->redirect($this->getCustomURL('index', 'user', 'default'));

            return;
        }

        $request = $this->getRequest();
        $params  = $request->getParams();

        // getForm
        $action    = $this->getCustomURL('login', 'user', 'default');
        $loginForm = new Epr_Form_Frontend_Login();


        // Handle Post
        if (isset($params[Epr_Form_Frontend_Login::FIELD_SUBMIT]) && $request->isPost() && $loginForm->isValid($params)) {

            $email    = PMailAddress::createFromString($request->getParam(Epr_Form_Frontend_Login::FIELD_EMAIL, null));
            $password = $request->getParam(Epr_Form_Frontend_Login::FIELD_PASSWORD, null);

            if (!is_null($email) && !is_null($password)) {

                $authAdapter = new Epr_Auth_Adapter(array('noSession' => true));
                $authAdapter->setIdentity((string)$email);
                $authAdapter->setCredential($password);

                $auth = Zend_Auth::getInstance();

                if ($auth->authenticate($authAdapter)->isValid()) {

                    if (_user() instanceof Epr_User) {
                        _user()->markAsWebLoggedIn();
                        _logger()->info('User Logged in');

                        if (!$this->redirectToLastURL()) {
                            $this->redirect($this->getCustomURL('index'));
                        }

                        return;
                    } else {
                        $this->view->assign('loginError', (_t('Something went wrong while trying to load your data. Please try again.')));
                    }
                } else {
                    $this->view->assign('loginError', (_t('Your credentials do not match any known user. Maybe a typo? Try again!')));
                }
            }
        }
        // redirect to home as logged In user


        // Display Form
        $this->view->assign('loginForm', $loginForm);

        $usersModule = _app()->getModuleWithIdentifier(Epr_Modules_Users::identifier());
        if ($usersModule instanceof Epr_Modules_Users) {
            $module = $usersModule->getUserRegistrationModule();
            if ($module instanceof Epr_Module_UserRegistration_Abstract) {

                $dealers          = Epr_Dealer_Collection::getAllActive();
                $registrationForm = new Epr_Form_Frontend_Registration(
                    array(
                        'dealers' => \Poundation\PArray::create($dealers)->nativeArray(),
                        'module'  => $module
                    )
                );

                if (isset($params[Epr_Form_Frontend_Registration::FIELD_SUBMIT]) && $request->isPost() && $registrationForm->isValid($params)) {

                    $dry = false;
                    if (!_app()->isProduction()) {
                        $dry = ($this->getRequest()->getParam('dryRun', false) == 'true');
                    }

                    $dealer   = null;
                    $dealerId = $registrationForm->getValue(Epr_Form_Frontend_Registration::FIELD_DEALER, null);
                    if (is_string($dealerId)) {
                        $dealer = _dm()->find(Epr_Dealer::DOCUMENTNAME, $dealerId);
                    }

                    // convert the post params to the params of the module
                    $moduleParams = array(
                        'email'    => $params[Epr_Form_Frontend_Registration::FIELD_EMAIL],
                        'password' => $params[Epr_Form_Frontend_Registration::FIELD_PASSWORD],
                        'firstname' => $params[Epr_Form_Frontend_Registration::FIELD_FIRSTNAME],
                        'lastname' => $params[Epr_Form_Frontend_Registration::FIELD_LASTNAME],
                        'location' => 'dummy',  // we do this to satisfy the initial check
                        'dealer'   => $dealer
                    );

                    // we check of the module is happy with the params
                    if ($module->isRegistrationDataSufficient($moduleParams, $error)) {
                        try {
                            // creating a new user object
                            $newUser = $module->createNewUser($moduleParams, Epr_User::REGISTRATION_CONTEXT_WEB);
                            if ($newUser) {
                                $newUser->setRole(Epr_Roles::ROLE_USER);

                                $newUser->setFirstname($registrationForm->getValue(Epr_Form_Frontend_Registration::FIELD_FIRSTNAME));
                                $newUser->setLastname($registrationForm->getValue(Epr_Form_Frontend_Registration::FIELD_LASTNAME));

                                // we pass the user and the params to the module so it can process it
                                // IMPORTANT: $params must be the same object it was earlier since the module might
                                // have change it
                                if ($module->processNewUser($newUser, $moduleParams)) {
                                    // if the module confirmed the creation we persist the new user
                                    if (!$dry) {
                                        _dm()->persist($newUser);
                                        $this->redirect($this->getCustomURL('registered', 'user'));

                                        return;
                                    }
                                } else {
                                    throw new Exception('Internal error, passed params are not valid though they have been checked successfully.');
                                }
                            }

                        } catch (Exception $e) {
                            _logger()->critical($e->getMessage());
                            $this->view->assign('registrationError', _t('Something went wrong while saving your data. Sorry.'));
                        }
                    } else {
                        if (!is_null($error)) {
                            $this->view->assign('registrationError', $error);

                        }
                    }

                }

                $this->view->assign('registrationForm', $registrationForm);
                $this->view->assign('termsOfUse', $module->getTermsOfUse());
                $this->view->assign('privacyPolicy', $module->getPrivacyPolicy());
            }
        }
    }


    public function logoutAction()
    {
        _logger()->info("User Logout-Action");

        _logger()->info('logout');
        $this->getHelper('viewRenderer')->setNoRender();

        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();

        if (!$this->redirectToLastURL()) {
            $this->redirect($this->getCustomURL('index'));
        }
    }

    public function registeredAction()
    {
        // nothing to do since the view is static
        // DO NOT REMOVE THIS METHOD.
    }

    public function verifyAction()
    {

        // we logout every user that is currently logged in
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();

        $id = $this->getRequest()->getParam('verificationId', null);

        $user = null;

        if (is_string($id) && strlen($id) > 10) {

            $query  = _dm()->createQuery('auth', 'userByVerificationId')->onlyDocs(true);
            $result = $query->setKey($id)->execute();
            if ($result && count($result) == 1) {

                $firstResult = $result[0];
                if ($firstResult instanceof Epr_User) {
                    if ($firstResult->isVerificationIdValid($id)) {
                        $firstResult->markAsVerified();

                        $usersModule = _app()->getModuleWithIdentifier(Epr_Modules_Users::identifier());
                        if ($usersModule instanceof Epr_Modules_Users) {
                            $module = $usersModule->getUserRegistrationModule();
                            if ($module instanceof Epr_Module_UserRegistration) {
                                if ($module->processVerification($firstResult)) {
                                    $user = $firstResult;
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($user instanceof Epr_User) {
            $this->view->assign('mail', $user->getEmail());
            $this->view->assign('text', 'Die E-Mail-Adresse wurde erfolgreich bestätigt. Sie können sich jetzt jederzeit anmelden und auch die iOS- und Android-Apps verwenden.');
        } else {
            $this->view->assign('text', 'Leider konnten wir Ihren Account nicht aktivieren. Bitte versuchen Sie es erneut oder nehmen Sie Kontakt mit uns auf.');
        }

    }

    public function dealsAction()
    {
        $page = Epr_Cms_Page::getPageWithPath(Epr_Modules_Users::PAGE_DEALS);

        $list = $page->getElementWithPath(Epr_Modules_Users::DEALS_INDEX);
        if ($list instanceof Epr_Modules_Deals_ListElement) {
            if (_user()) {
                $list->setUser(_user());
            }
        }

        $this->view->assign('page', $page);
        $this->render('index');
    }

    public function resetPasswordAction()
    {
        if (_user()) {
            $this->redirect($this->getCustomURL('account', 'user'));
        } else {

            $form = new Epr_Form_Frontend_Account_ResetPassword();

            if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getParams())) {

                $message = null;

                $email = PMailAddress::createFromString($form->getValue(Epr_Form_Frontend_Account_ResetPassword::FIELD_EMAIL));
                if ($email) {
                    $user = Epr_User::getUserWithEmail($email);
                    if ($user) {

                        $usersModule = _app()->getModuleWithIdentifier(Epr_Modules_Users::identifier());
                        if ($usersModule instanceof Epr_Modules_Users) {

                            if (!$usersModule->resetUser($user)) {
                                $message = _t('We had a problem reseting your password.') . ' ' . _t('Please try again later.');
                                _app()->sendAdminMail('Could not reset a user password', 'The user password could not be reset. Check the user module.');
                            }
                        } else {
                            $message = _t('We had a problem reseting your password.') . ' ' . _t('Please try again later.');
                            _app()->sendAdminMail('Could not reset a user password', 'While reseting a password I could not load the user module.');
                        }
                    } else {
                        $message = _t('This email address is not registered.');
                    }
                } else {
                    $message = _t('This is not a valid mail address.');
                }

                if (!is_null($message)) {
                    $this->view->assign('errorMessage', $message);
                } else {
                    $this->view->assign('successMessage', _t('Please check your inbox, we sent a mail with further instructions.'));
                }
            }

            $this->view->assign('form', $form);

        }
    }

    public function newPasswordAction()
    {
        if (_user()) {
            _user()->abortResetPassword();
            $this->redirect($this->getCustomURL('account', 'user'));
        } else {

            $user = null;

            $id = $this->getRequest()->getParam('id', null);
            if (!is_null($id)) {

                $query  = _dm()->createQuery('auth', 'userByResetPasswordId')->onlyDocs(true);
                $result = $query->setKey($id)->execute();
                if ($result && count($result) == 1) {
                    $user = $result[0];
                }
            }

            if ($user instanceof Epr_User) {

                $module = _app()->getActiveUserRegistrationModule();
                if ($module) {
                    $form = new Epr_Form_Frontend_Account_ResetPassword_NewPassword($module);
                } else {
                    throw new Exception('Could not load user registration module.');
                }

                if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getParams())) {

                    $password = $form->getValue(Epr_Form_Frontend_Account_ResetPassword_NewPassword::FIELD_PASSWORD_NEW);
                    $user->setPassword($password);
                    $this->redirect($this->getCustomURL('login', 'user'));

                } else {
                    $this->view->assign('form', $form);
                }

            } else {
                $this->view->assign('errorMessage', _t('Your link does not work. You probably changed the password already.'));
            }

        }
    }

    public function accountAction()
    {
        $dataForm     = new Epr_Form_Frontend_Account_PersonalData(_user());
        $passwordForm = null;

        $module = _app()->getActiveUserRegistrationModule();
        if ($module) {
            $passwordForm = new Epr_Form_Frontend_Account_Password(_user(), $module);
        }

        if ($this->getRequest()->isPost()) {

            if (is_string($this->getRequest()->getParam(Epr_Form_Frontend_Account_PersonalData::FIELD_SUBMIT, null))) {
                // the data form
                if ($dataForm->isValid($this->getRequest()->getParams())) {

                    _user()->setFirstname($dataForm->getValue(Epr_Form_Frontend_Account_PersonalData::FIELD_FIRSTNAME));
                    _user()->setLastname($dataForm->getValue(Epr_Form_Frontend_Account_PersonalData::FIELD_LASTNAME));
                    $dataForm->getElement(Epr_Form_Frontend_Account_PersonalData::FIELD_EMAIL)
                        ->setValue(_user()->getEmail());

                }
            } else if (is_string(
                $this->getRequest()->getParam(Epr_Form_Frontend_Account_Password::FIELD_SUBMIT, null)
            )
            ) {
                // the password form
                if ($passwordForm->isValid($this->getRequest()->getParams())) {
                    _user()->setPassword($passwordForm->getValue(Epr_Form_Frontend_Account_Password::FIELD_PASSWORD_NEW));
                    $this->view->assign('passwordMessage', _t('Your password has been changed.'));
                }
            }
        }

        $this->view->assign('dataForm', $dataForm);
        $this->view->assign('passwordForm', $passwordForm);

    }

    public function outlineAction()
    {

        if (_user() && _user()->isAdministrator()) {
            toggleOutlineTranslation();
        }

        $this->redirectToLastURL();

    }

}
