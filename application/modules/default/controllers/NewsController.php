<?php

/**
 * News Controller
 *
 */
class NewsController extends Epr_Frontend_Controller_Action
{

    public function indexAction()
    {
        $page = Epr_Cms_Page::getPageWithPath(Epr_Modules_News::PAGE_PATH);
        $this->view->assign('page', $page);
    }

    public function categoryAction()
    {

        $page = Epr_Cms_Page::getPageWithPath(Epr_Modules_News::PAGE_PATH);
        $listElement = $page->getElementWithPath(Epr_Modules_News::NEWS_LIST_PATH);
        $menuElement = $page->getElementWithPath(Epr_Modules_News::NEWS_CATEGORY_MENU_PATH);

        $category = null;
        $categorySlug = $this->getRequest()->getParam('newsCategorySlug', null);
        if ($categorySlug) {
            $category = _dm()->getRepository('Epr_News_Category')->findOneBy(array('slug' => $categorySlug));
        }

        if ($category instanceof Epr_News_Category) {
            if ($listElement instanceof Epr_Modules_News_ListElement) {
                $listElement->setCurrentCategory($category);
            }
            if ($menuElement instanceof Epr_Modules_News_CategoryMenuElement) {
                $menuElement->setCurrentCategory($category);
            }
        }

        $this->view->assign('page', $page);
        $this->render('index');

    }


    // Displays a single News
    public function showAction()
    {

        //$preview = $this->getRequest()->getParam('preview', false);
        $newsSlug = $this->getRequest()->getParam('newsSlug');

        $newsItem = _dm()->getRepository('Epr_News')->findOneBy(array('slug' => $newsSlug));
        if (!$newsItem instanceof Epr_News) {
            throw new Zend_Controller_Action_Exception('News Entry not found', 404);
        }

        $moreUrl = $newsItem->getPublicUrl();

        $page = Epr_Cms_Page::getPageWithPath(Epr_Modules_News::PAGE_DETAILS);
        $page->setSharedDataForKey($moreUrl, 'moreUrl');
        $menuElement = $page->getElementWithPath(Epr_Modules_News::DETAILS_MENU);
        if ($menuElement instanceof Epr_Modules_News_CategoryMenuElement) {
            $menuElement->setCurrentCategory($newsItem->getCategory());
        }

        $detailsElement = $page->getElementWithPath(Epr_Modules_News::DETAILS_NEWS);
        if ($detailsElement instanceof Epr_Modules_News_DetailsElement) {
            $detailsElement->setNewsItem($newsItem);
            //$detailsElement->setShowEditLink((_user() && _user()->isAdministrator()));
        }

		// News viewed => increase viewCounter
		$newsItem->registerView();

        $this->view->assign('page', $page);
        $this->render('index');

    }

    public function previewAction()
    {

        if (_user() && _user()->isAdministrator()) {

            $news = null;

            $newsId = $this->getRequest()->getParam('id', null);
            if (strlen($newsId) > 0) {
                $news = Epr_News::getNewsWithId($newsId);
            }

            if (!$news instanceof Epr_News) {
                throw new Zend_Controller_Action_Exception('News Entry not found', 404);
            }

            $page = Epr_Cms_Page::getPageWithPath(Epr_Modules_News::PAGE_DETAILS);
            $menuElement = $page->getElementWithPath(Epr_Modules_News::DETAILS_MENU);
            if ($menuElement instanceof Epr_Modules_News_CategoryMenuElement) {
                $menuElement->setCurrentCategory($news->getCategory());
            }

            $detailsElement = $page->getElementWithPath(Epr_Modules_News::DETAILS_NEWS);
            if ($detailsElement instanceof Epr_Modules_News_DetailsElement) {
                $detailsElement->setNewsItem($news);
                $detailsElement->setShowEditLink(true);
            }

            $this->view->assign('page', $page);
            $this->render('index');
        } else {
            $this->redirect($this->getCustomURL('index'));
        }
    }

    public function moreAction()
    {

        $news = null;
        $redirectTo = null;

        $id = $this->getParam('id', null);
        if (!is_null($id)) {
            $news = Epr_News::getNewsWithId($id);
        }

        if ($news instanceof Epr_News) {
            $redirectTo = $news->getPublicUrl();
            if (!is_null($redirectTo)) {
                $news->registerHit();
            }
        }

        if (is_null($redirectTo)) {
            $redirectTo = _app()->getPublicURL();
        }

        $this->redirect($redirectTo);

    }

}
