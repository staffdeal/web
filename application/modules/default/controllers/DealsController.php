<?php

/**
 * Deals Frontend Controller
 *
 */
class DealsController extends Epr_Frontend_Controller_Action
{

    public function indexAction()
    {
        $page = Epr_Cms_Page::getPageWithPath(Epr_Modules_Deals::PAGE_INDEX_PATH);
        $this->view->assign('page', $page);

    }

    public function detailsAction()
    {

        $page    = Epr_Cms_Page::getPageWithPath(Epr_Modules_Deals::PAGE_DETAILS_PATH);
        $details = $page->getElementWithPath(Epr_Modules_Deals::DETAILS_PATH);
        $actions = $page->getElementWithPath(Epr_Modules_Deals::DETAILS_ACTIONS_PATH);

        $deal     = null;
        $userDeal = null;
        $request  = $this->getRequest();
        $params   = $request->getParams();
        $id       = $request->getParam('id', null);
        $form     = null;

        if ($id) {
            $deal = Epr_Dispatcher_Deal::getDealById($id);

            if ($deal) {
                $passes = $deal->getPass();

                if ($details instanceof Epr_Modules_Deals_DetailsElement) {
                    $details->setDeal($deal);
                }


                $numberOfPossibleSaves = (isset($passes->onsite)) + (isset($passes->online));
                $numberOfSaves = 0;

                if (_user()) {
                    $numberOfSaves = (_user()->hasDealCode($deal, Epr_Deal::TYPE_ONSITE)) + (_user()->hasDealCode($deal, Epr_Deal::TYPE_ONLINE));
                }

                if ($numberOfSaves < $numberOfPossibleSaves) {
                    $form = new Epr_Form_Frontend_Deal_Save($deal);

                    if (_user() && $this->getRequest()->isPost() && $form->isValid($this->getRequest()->getParams())) {

                        $type = $form->getValue(Epr_Form_Frontend_Deal_Save::FIELD_TYPE);
                        if ($type == Epr_Deal::TYPE_ONSITE || $type == Epr_Deal::TYPE_ONLINE) {
                            if (!_user()->hasDealCode($deal, $type)) {
                                $code = $deal->getRedeemCode($type);
                                if ($code) {
                                    _user()->setDealCode($deal, $type, $code);
                                } else {
                                    $this->view->assign('savingError', _t('The deal could not be saved. We are very sorry. Please try again later.'));
                                }
                            }
                        }
                    }

                    if (_user() && _user()->hasDealCode($deal, Epr_Deal::TYPE_ONLINE)) {
                        if (isset($passes->online) && isset($passes->online->description)) {
                            $this->view->assign('onlineDescription', $passes->online->description);
                        }
                    }

                    $usersModule = _app()->getModuleWithIdentifier(Epr_Modules_Users::identifier());
                    if ($usersModule instanceof Epr_Modules_Users) {
                        $module = $usersModule->getUserRegistrationModule();
                        if ($module instanceof Epr_Module_UserRegistration_Abstract) {
                            $this->view->assign('termsOfUse', $module->getTermsOfUse());
                        }
                    }
                }

                if ($actions instanceof Epr_Modules_Deals_Details_ActionElement) {
                    $actions->setDeal($deal);
                    $this->view->assign('form', $form);
                }
            }
        }

        if (is_null($deal)) {
            $this->redirect($this->getCustomURL('index'));
        }

        $this->view->assign('page', $page);

    }

    public function previewAction()
    {

        $page    = Epr_Cms_Page::getPageWithPath(Epr_Modules_Deals::PAGE_DETAILS_PATH);
        $details = $page->getElementWithPath(Epr_Modules_Deals::DETAILS_PATH);

        $deal     = null;
        $userDeal = null;
        $request  = $this->getRequest();
        $params   = $request->getParams();
        $id       = $request->getParam('id', null);
        $form     = null;

        if ($id) {
            $deal = Epr_Dispatcher_Deal::getDealById($id);
        }

        if ($deal instanceof Epr_Dispatcher_Deal && _user() && _user()->isAdministrator()) {


            if ($details instanceof Epr_Modules_Deals_DetailsElement) {
                $details->setDeal($deal);
            }

            $this->view->assign('page', $page);
            $this->render('index');

        } else {
            $this->redirect($this->getCustomURL('index'));
        }
    }

    public function printAction()
    {

        if (_user()) {

            $id   = $this->getRequest()->getParam('id', null);
            $deal = null;

            if ($id) {
                $deal = Epr_Deal_Collection::getProvidedDealWithId($id);
                if ($deal instanceof Epr_Dispatcher_Deal) {
                    $this->view->assign('deal', $deal);
                } else {
                    //$this->redirect($this->getCustomURL('index'));
                }
            }
        } else {
            $this->redirect($this->getCustomURL('index'));
        }

    }

    public function dealersAction()
    {
        $id   = $this->getRequest()->getParam('id', null);
        $deal = null;

        if ($id) {
            $deal = Epr_Deal_Collection::getProvidedDealWithId($id);
            if ($deal) {
                $this->view->assign('deal', $deal);

                $dealers       = Epr_Deal_Collection::getDealersForDeal($deal->getReferenceDealId());
                $parsedDealers = array();
                foreach ($dealers as $loadedDealer) {

                    $dealer = Epr_Dealer::createFromDispatchDealer($loadedDealer);
                    if ($dealer && $dealer->hasCoordinate()) {
                        $parsedDealers[] = $dealer;
                    }
                }

                $this->view->assign('dealers', $parsedDealers);

            } else {
                $this->redirect($this->getCustomURL('index'));
            }
        }
    }

}
