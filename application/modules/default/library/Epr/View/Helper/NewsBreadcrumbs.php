<?php

class Epr_View_Helper_NewsBreadcrumbs extends Zend_View_Helper_Abstract
{

	private $allCategories;
	private $currentCategory;
	private $currentNewsItem;

	public function newsBreadcrumbs($all, $current = null, $newsItem = null)
	{

		$this->allCategories   = $all;
		$this->currentCategory = $current;
		$this->currentNewsItem = $newsItem;
		echo $this->renderedContent();

	}

	private function renderedContent()
	{
		$output = '';

		if ($this->currentCategory instanceof Epr_News_Category) {

			$newsModule = _app()->getModuleWithIdentifier(Epr_Modules_News::identifier());

			$output.= '<ul class="breadcrumb">';
			$output.= '<li><a href="/news/">' . $newsModule->getDisplayname() . '</a></li>';


			$parent = $this->currentCategory->getCategory();
			while ($parent instanceof Epr_News_Category) {
				$output .= '<li><a href="/news/' . $parent->getSlug() . '">' . $parent->getTitle() . '</a></li>';
				$parent = $parent->getCategory();
			}

			if ($this->currentNewsItem instanceof Epr_News) {
				$output .= '<li><a href="/news/' . $this->currentCategory->getSlug() . '">' . $this->currentCategory->getTitle() . '</a></li>';
				$output.= '<li>' . __($this->currentNewsItem->getTitle())->shortenAfterChar(30, ' ') . '</li>';
			} else {
				$output.= '<li>' . $this->currentCategory->getTitle() . '</li>';
			}


			$output.= '</ul>';
		}

		return $output;
	}

}