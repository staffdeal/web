<?php

use Poundation\PColor;

class Epr_View_Helper_NewsCategoryNavigation extends Zend_View_Helper_Abstract
{

	private $current;

	private $rootCategories;
	private $allCategories;

	public function newsCategoryNavigation($all, $current = null)
	{
		$this->setAll($all);
		$this->setCurrent($current);
		echo $this->renderedContent();
	}

	protected function setCurrent($current)
	{
		$this->current = $current;
	}

	protected function getCurrent()
	{
		return $this->current;
	}

	protected function setAll($all)
	{
		$this->allCategories = $all;

		$this->rootCategories = array();
		foreach ($all as $category) {
			if ($category instanceof Epr_News_Category) {
				if (!$category->getCategory() instanceof Epr_News_Category) {
					$this->rootCategories[] = $category;
				}
			}
		}
	}

	protected function getAll()
	{
		return $this->all;
	}

	private function getSubcategoriesForCategory(Epr_News_Category $category)
	{

		$subcategories = array();
		foreach ($this->allCategories as $candidate) {
			if ($candidate instanceof Epr_News_Category) {

				if ($candidate->getCategory() === $category) {
					$subcategories[] = $candidate;
				}

			}
		}

		return $subcategories;
	}

	private function renderedContent()
	{


		$output = '<div class="list-group">';



		$categoriesToDisplay = $this->rootCategories;

		if ($this->current instanceof Epr_News_Category) {

			$categoriesToDisplay = $this->getSubcategoriesForCategory($this->current);

		}

		foreach ($categoriesToDisplay as $category) {
			if ($category instanceof Epr_News_Category) {
				$output .= $this->renderedCategoryEntry($category);
			}
		}


		$output .= '</div>';

		return $output;
	}

	private function renderedCategoryEntry(Epr_News_Category $category)
	{

		return $this->renderedSingleCategoryEntry($category);
	}

	private function renderedSingleCategoryEntry(Epr_News_Category $category, $intend = 0)
	{

		$active = ($category == $this->current) ? ' active' : '';

		$output = '<a href="/news/' . $category->getSlug() . '" class="list-group-item' . $active . '"><span class="not-breaking"><img class="colorwell" style="background-color: ' . $category->getColor() . ';" />&nbsp;' . $category->getTitle() . '</span></a>';



		return $output;
	}

}