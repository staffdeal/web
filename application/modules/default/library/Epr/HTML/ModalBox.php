<?php
/**
 * @package Backend
 * @author daniel 
 */

class Epr_HTML_ModalBox {

    protected $id;

    protected $showCloseButton = true;

    protected $title = '';

    protected $headerHTML = '';
    protected $contentHTML = '';
    protected $footerHTML = '';

    protected $linkedElements = array();

    public function __construct($id = null) {
        if (!is_string($id) || strlen($id) == 0) {
            $id = \Poundation\PString::createUUID()->stringValue();
        }
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId() {
        return $this->id;
    }

    /**
     * If set to true, the modal box will show a close button (besides the title). This only applies if no header HTML
     * is set (or is set to null).
     *
     * @param $value
     * @return $this
     */
    public function setShowCloseButton($value) {
        $this->showCloseButton = ($value == true);
        return $this;
    }

    /**
     * Returns true if a close button is shown.
     * @return bool
     */
    public function getShowCloseButton() {
        return $this->showCloseButton;
    }

    /**
     * Sets the title of the modal box. The title is only shown if no header HTML is set (or is set to null).
     * @param $html
     * @return $this
     */
    public function setTitle($html) {
        $this->title = $html;
        return $this;
    }

    /**
     * Returns the title.
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Sets the header HTML of the modal box. If set to something else than null the title and the close button are not
     * rendered anymore.
     *
     * @param $html
     * @return $this
     */
    public function setHeader($html) {
        $this->headerHTML = $html;
        return $this;
    }

    /**
     * Returns the header HTML.
     * @return string
     */
    public function getHeader() {
        return $this->headerHTML;
    }

    /**
     * Sets the content HTML of the modal box.
     *
     * @param $html
     * @return $this
     */
    public function setContent($html) {
        $this->contentHTML = $html;
        return $this;
    }

    /**
     * Returns the content HTML.
     * @return string
     */
    public function getContent() {
        return $this->contentHTML;
    }

    /**
     * Sets the footer HTML of the modal box.
     *
     * @param $html
     * @return $this
     */
    public function setFooter($html) {
        $this->footerHTML = $html;
        return $this;
    }

    /**
     * Returns the footer HTML.
     * @return string
     */
    public function getFooter() {
        return $this->footerHTML;
    }

    /**
     * Adds an selector to the list of elements that activate the box on click.
     * @param $selector
     * @return $this
     */
    public function addLinkedElementSelector($selector) {
        if (strlen($selector) > 0) {
            $this->linkedElements[] = $selector;
        }
        return $this;
    }


    public function renderedHTML() {
        $output = '';

        $header = $this->getHeader();
        if (strlen($header) == 0) {
            $header = '';
            if ($this->getShowCloseButton()) {
                $header .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            }
            $header .= '<h4 class="modal-title">' . $this->getTitle() . '</h4>';

        }

        $output = '<div class="modal fade" id="' . $this->getId() .'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    ' . $header . '
                </div>
                <div class="modal-body" style="height: 60%; overflow-y: scroll">
                    ' . $this->getContent() . '
                </div>
                <div class="modal-footer">
                    ' . $this->getFooter() . '
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function () {';

        foreach($this->linkedElements as $link) {
            $output .= "\njQuery('$link').attr('data-toggle', 'modal').attr('data-target', '#" . $this->getId() . "');";
        }

        $output .= '
        });
    </script>
    ';

        return $output;
    }

    public function render() {
        echo $this->renderedHTML();
    }

    public function __toString(){
        return $this->renderedHTML();
    }
}