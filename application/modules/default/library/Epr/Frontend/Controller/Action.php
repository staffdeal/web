<?php

/**
 * Controller for frontend(default) module
 *
 */
class Epr_Frontend_Controller_Action extends Epr_Controller_Action
{

	protected $_flashMessenger = null;

	private $directOutputActions = array();

	protected function addDirectOutputAction($name)
	{
		$this->directOutputActions[] = $name;
	}

	public function preDispatch()
	{
		parent::preDispatch();
		$this->_setLanguage();

		$action = $this->getRequest()->getActionName();
		if (array_search($action, $this->directOutputActions) === false) {

			$this->_getTopSearch();
			$this->_getNavigation();
			$this->_getNavRight();
			$this->_getFooter();

			$this->_helper->layout()->setLayout('layout');

			if ($this->getRequest()->getParam('noAuth', false)) {
				//$this->view->assign('message', 'Not Authentificated for that Page');
				$this->view->assign('redirected', true);
			}
			$this->view->assign('title', Zend_Registry::get('client')->info->name);
		}
	}




	private function _setLanguage()
	{
		$language = $this->getRequest()->getParam('language', 'de');

		switch ($language) {
			case "de":
				$locale = 'de_DE';
				break;
			case "en":
				$locale = 'en_US';
				break;
			default:
				$locale = "de_DE";
				break;
		}

		$zl = new Zend_Locale();
		$zl->setLocale($locale);
		Zend_Registry::set("Zend_Locale", $zl);
		_logger()->info('Current Language = "' . $language . '"');
	}

	private function _getTopSearch()
	{
		$view = new Zend_View();
		$view->setBasePath(APPLICATION_PATH . 'modules/admin/views/placeholder/');

		$this->view->placeholder('top-search')->append($view->render('top-search.phtml'));

	}

	private function _getNavigation()
	{
		$view = new Zend_View();
		$view->setBasePath(APPLICATION_PATH . 'modules/default/views/placeholder/');
		$view->assign('controller', $this->getRequest()->getControllerName());
		$view->assign('action', $this->getRequest()->getActionName());

		/**
		 * Create Login Form
		 */
		$action = $this->getCustomURL('login', 'user', 'default');
		$form   = new Epr_Form_Frontend_Navbar_Login();
		$form->setAction($action);
		$view->assign('loginForm', $form);

		$this->view->placeholder('navigation')->append($view->render('navigation.phtml'));
	}

	private function _getNavRight()
	{
		$view = new Zend_View();
		$view->setBasePath(APPLICATION_PATH . 'modules/admin/views/placeholder/');

		$this->view->placeholder('navigation-right')->append($view->render('nav-right.phtml'));
	}

	private function _getFooter()
	{
		$view = new Zend_View();
		$view->setBasePath(APPLICATION_PATH . 'modules/default/views/placeholder/');
		$this->view->placeholder('footer')->append($view->render('footer.phtml'));
	}

}
