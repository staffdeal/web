<?php

namespace MyCouchDBProxyNS\__CG__;

/**
 * THIS CLASS WAS GENERATED BY THE DOCTRINE ORM. DO NOT EDIT THIS FILE.
 */
class Epr_User extends \Epr_User implements \Doctrine\ODM\CouchDB\Proxy\Proxy
{
    private $__doctrineDocumentManager__;
    private $__doctrineIdentifier__;
    public $__isInitialized__ = false;
    public function __construct($documentManager, $identifier)
    {
        $this->__doctrineDocumentManager__ = $documentManager;
        $this->__doctrineIdentifier__ = $identifier;
    }
    public function __load()
    {
        if (!$this->__isInitialized__ && $this->__doctrineDocumentManager__) {
            $this->__isInitialized__ = true;
            $this->__doctrineDocumentManager__->refresh($this);
            unset($this->__doctrineDocumentManager__, $this->__doctrineIdentifier__);
        }
    }

    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    
    public function setLastname($lastname)
    {
        $this->__load();
        return parent::setLastname($lastname);
    }

    public function getLastname()
    {
        $this->__load();
        return parent::getLastname();
    }

    public function setFirstname($firstname)
    {
        $this->__load();
        return parent::setFirstname($firstname);
    }

    public function getFirstname()
    {
        $this->__load();
        return parent::getFirstname();
    }

    public function setEmail($email)
    {
        $this->__load();
        return parent::setEmail($email);
    }

    public function getEmail()
    {
        $this->__load();
        return parent::getEmail();
    }

    public function getRegistrationContext()
    {
        $this->__load();
        return parent::getRegistrationContext();
    }

    public function setPassword($password)
    {
        $this->__load();
        return parent::setPassword($password);
    }

    public function checkPassword($input)
    {
        $this->__load();
        return parent::checkPassword($input);
    }

    public function getSalt()
    {
        $this->__load();
        return parent::getSalt();
    }

    public function generateSalt()
    {
        $this->__load();
        return parent::generateSalt();
    }

    public function resetPassword()
    {
        $this->__load();
        return parent::resetPassword();
    }

    public function abortResetPassword()
    {
        $this->__load();
        return parent::abortResetPassword();
    }

    public function getRole()
    {
        $this->__load();
        return parent::getRole();
    }

    public function setRole($role)
    {
        $this->__load();
        return parent::setRole($role);
    }

    public function isAdministrator()
    {
        $this->__load();
        return parent::isAdministrator();
    }

    public function isRoot()
    {
        $this->__load();
        return parent::isRoot();
    }

    public function setAsAuthenticated($hasAuth)
    {
        $this->__load();
        return parent::setAsAuthenticated($hasAuth);
    }

    public function hasAuth()
    {
        $this->__load();
        return parent::hasAuth();
    }

    public function markAsWebLoggedIn()
    {
        $this->__load();
        return parent::markAsWebLoggedIn();
    }

    public function getLastLogin()
    {
        $this->__load();
        return parent::getLastLogin();
    }

    public function markAsLoggedIn()
    {
        $this->__load();
        return parent::markAsLoggedIn();
    }

    public function getLastMobileLogin()
    {
        $this->__load();
        return parent::getLastMobileLogin();
    }

    public function markAsMobileLoggedIn()
    {
        $this->__load();
        return parent::markAsMobileLoggedIn();
    }

    public function associatedDealer()
    {
        $this->__load();
        return parent::associatedDealer();
    }

    public function associateWithDealer(\Epr_Dealer $dealer, $selectedDealer = NULL)
    {
        $this->__load();
        return parent::associateWithDealer($dealer, $selectedDealer);
    }

    public function getAssociatedDealer()
    {
        $this->__load();
        return parent::getAssociatedDealer();
    }

    public function isVerified()
    {
        $this->__load();
        return parent::isVerified();
    }

    public function getVerificationId()
    {
        $this->__load();
        return parent::getVerificationId();
    }

    public function isVerificationIdValid($id)
    {
        $this->__load();
        return parent::isVerificationIdValid($id);
    }

    public function markAsVerified()
    {
        $this->__load();
        return parent::markAsVerified();
    }

    public function getRegistrationInformation()
    {
        $this->__load();
        return parent::getRegistrationInformation();
    }

    public function setRegistrationInformation($value)
    {
        $this->__load();
        return parent::setRegistrationInformation($value);
    }

    public function getAvatarUrl($s = 80, $d = 'mm', $r = 'g')
    {
        $this->__load();
        return parent::getAvatarUrl($s, $d, $r);
    }

    public function setDeals($deals)
    {
        $this->__load();
        return parent::setDeals($deals);
    }

    public function getDeals()
    {
        $this->__load();
        return parent::getDeals();
    }

    public function getDealWithId($id)
    {
        $this->__load();
        return parent::getDealWithId($id);
    }

    public function sendMail($subject, $body, $isHTML = false)
    {
        $this->__load();
        return parent::sendMail($subject, $body, $isHTML);
    }

    public function saveDeal(\Epr_Dispatcher_Deal $deal)
    {
        $this->__load();
        return parent::saveDeal($deal);
    }

    public function getShowBackendHints()
    {
        $this->__load();
        return parent::getShowBackendHints();
    }

    public function getAdminSetting($key, $default = NULL)
    {
        $this->__load();
        return parent::getAdminSetting($key, $default);
    }

    public function setAdminSetting($key, $value)
    {
        $this->__load();
        return parent::setAdminSetting($key, $value);
    }

    public function setDealCode(\Epr_Dispatcher_Deal $deal, $type, $code)
    {
        $this->__load();
        return parent::setDealCode($deal, $type, $code);
    }

    public function getDealCode(\Epr_Dispatcher_Deal $deal, $type)
    {
        $this->__load();
        return parent::getDealCode($deal, $type);
    }

    public function hasDealCode(\Epr_Dispatcher_Deal $deal, $type)
    {
        $this->__load();
        return parent::hasDealCode($deal, $type);
    }

    public function isDealSaved(\Epr_Dispatcher_Deal $deal)
    {
        $this->__load();
        return parent::isDealSaved($deal);
    }

    public function setActive($isActive)
    {
        $this->__load();
        return parent::setActive($isActive);
    }

    public function getId()
    {
        $this->__load();
        return parent::getId();
    }

    public function save()
    {
        $this->__load();
        return parent::save();
    }

    public function activate()
    {
        $this->__load();
        return parent::activate();
    }

    public function deactivate()
    {
        $this->__load();
        return parent::deactivate();
    }

    public function isActive()
    {
        $this->__load();
        return parent::isActive();
    }

    public function getCreationDate()
    {
        $this->__load();
        return parent::getCreationDate();
    }

    public function setCreationDate($date)
    {
        $this->__load();
        return parent::setCreationDate($date);
    }

    public function setAssociateForKey($associate, $key)
    {
        $this->__load();
        return parent::setAssociateForKey($associate, $key);
    }

    public function removeAssociateForKey($key)
    {
        $this->__load();
        return parent::removeAssociateForKey($key);
    }

    public function getAssociateForKey($key)
    {
        $this->__load();
        return parent::getAssociateForKey($key);
    }

    public function isEqual($otherObject)
    {
        $this->__load();
        return parent::isEqual($otherObject);
    }

    public function __toString()
    {
        $this->__load();
        return parent::__toString();
    }

    public function classObject()
    {
        $this->__load();
        return parent::classObject();
    }

    public function isKindOfClass($class)
    {
        $this->__load();
        return parent::isKindOfClass($class);
    }

    public function implementsInterface($interface)
    {
        $this->__load();
        return parent::implementsInterface($interface);
    }


    public function __sleep()
    {
        return array('__isInitialized__', 'id','lastname','firstname','email','password','role','salt','lastLogin','lastMobileLogin','registrationContext','registrationInformation','verificationId','resetPasswordId','adminSettings','dealCodes','sendActivationMail','isActive','associates');
    }
}