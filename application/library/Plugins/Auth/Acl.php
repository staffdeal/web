<?php

class
Plugins_Auth_Acl extends Zend_Acl
{

    protected static $_instance;

    protected function __construct()
    {
        // ROLES
        $this->addRole(new Zend_Acl_Role(Epr_Roles::ROLE_GUEST));
        $this->addRole(new Zend_Acl_Role(Epr_Roles::ROLE_USER), Epr_Roles::ROLE_GUEST);
        $this->addRole(new Zend_Acl_Role(Epr_Roles::ROLE_ASSISTANT), Epr_Roles::ROLE_USER);
        $this->addRole(new Zend_Acl_Role(Epr_Roles::ROLE_ADMIN), Epr_Roles::ROLE_ASSISTANT);
        $this->addRole(new Zend_Acl_Role(Epr_Roles::ROLE_ROOT), Epr_Roles::ROLE_ADMIN);

        // Admin
        $this->addResource(new Zend_Acl_Resource('mvc:admin'));
        $this->addResource(new Zend_Acl_Resource('mvc:admin.index', 'mvc:admin'));
        $this->addResource(new Zend_Acl_Resource('mvc:admin.auth', 'mvc:admin'));
        $this->addResource(new Zend_Acl_Resource('mvc:admin.news', 'mvc:admin'));
        $this->addResource(new Zend_Acl_Resource('mvc:admin.user', 'mvc:admin'));
        $this->addResource(new Zend_Acl_Resource('mvc:admin.ideas', 'mvc:admin'));
        $this->addResource(new Zend_Acl_Resource('mvc:admin.deals', 'mvc:admin'));
        $this->addResource(new Zend_Acl_Resource('mvc:admin.dealers', 'mvc:admin'));
        $this->addResource(new Zend_Acl_Resource('mvc:admin.settings', 'mvc:admin'));
        $this->addResource(new Zend_Acl_Resource('mvc:admin.ajax', 'mvc:admin'));
        $this->addResource(new Zend_Acl_Resource('mvc:admin.cms', 'mvc:admin'));
        $this->addResource(new Zend_Acl_Resource('mvc:admin.assets', 'mvc:admin'));
        $this->addResource(new Zend_Acl_Resource('mvc:admin.events', 'mvc:admin'));

        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:admin.auth', array('login'));
        $this->allow(Epr_Roles::ROLE_ADMIN, 'mvc:admin.auth', array('logout'));
        $this->allow(Epr_Roles::ROLE_ADMIN, 'mvc:admin.index');
        $this->allow(Epr_Roles::ROLE_ADMIN, 'mvc:admin.news');
        $this->allow(Epr_Roles::ROLE_ADMIN, 'mvc:admin.user');
        $this->allow(Epr_Roles::ROLE_ADMIN, 'mvc:admin.ideas');
        $this->allow(Epr_Roles::ROLE_ADMIN, 'mvc:admin.deals');
        $this->allow(Epr_Roles::ROLE_ADMIN, 'mvc:admin.dealers');
        $this->allow(Epr_Roles::ROLE_ADMIN, 'mvc:admin.settings');
        $this->allow(Epr_Roles::ROLE_ADMIN, 'mvc:admin.ajax');
        $this->allow(Epr_Roles::ROLE_ADMIN, 'mvc:admin.cms');
        $this->allow(Epr_Roles::ROLE_ADMIN, 'mvc:admin.assets');
        $this->allow(Epr_Roles::ROLE_ADMIN, 'mvc:admin.events');

        // Api
        $this->addResource(new Zend_Acl_Resource('mvc:api'));
        $this->addResource(new Zend_Acl_Resource('mvc:api.error'), 'mvc:api');
        $this->addResource(new Zend_Acl_Resource('mvc:api.index'), 'mvc:api');
        $this->addResource(new Zend_Acl_Resource('mvc:api.auth'), 'mvc:api');
        $this->addResource(new Zend_Acl_Resource('mvc:api.info'), 'mvc:api');
        $this->addResource(new Zend_Acl_Resource('mvc:api.notifications'), 'mvc:api');
        $this->addResource(new Zend_Acl_Resource('mvc:api.com'), 'mvc:api');
        $this->addResource(new Zend_Acl_Resource('mvc:api.dispatcher'), 'mvc:api');
        $this->addResource(new Zend_Acl_Resource('mvc:api.deals'), 'mvc:api');
        $this->addResource(new Zend_Acl_Resource('mvc:api.user'), 'mvc:api');
        $this->addResource(new Zend_Acl_Resource('mvc:api.events', 'mvc:api'));

        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:api.error');
        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:api.index');
        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:api.auth');
        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:api.info');
        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:api.notifications');
        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:api.com');
        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:api.dispatcher');
        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:api.deals', array('redeem'));
        $this->allow(Epr_Roles::ROLE_USER, 'mvc:api.user');
        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:api.events');
        $this->allow(Epr_Roles::ROLE_USER, 'mvc:api.events', array('booking'));

        // Default
        $this->addResource(new Zend_Acl_Resource('mvc:default'));
        $this->addResource(new Zend_Acl_Resource('mvc:default.index'));
        $this->addResource(new Zend_Acl_Resource('mvc:default.static'));
        $this->addResource(new Zend_Acl_Resource('mvc:default.user'));

        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:default.index');
        $this->allow(Epr_Roles::ROLE_GUEST, 'mvc:default.static');
        $this->allow(
            Epr_Roles::ROLE_GUEST,
            'mvc:default.user',
            array(
                'login',
                'registered',
                'verify',
                'outline',
                'reset-password',
                'new-password'
            )
        );

        $this->allow(
            Epr_Roles::ROLE_USER,
            'mvc:default.user',
            array(
                'index',
                'logout',
                'account',
                'deals'
            )
        );

        return $this;

    }

    /**
     *
     * @return Plugins_Auth_Acl
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public static function resetInstance()
    {
        self::$_instance = null;
        self::getInstance();
    }

    public function loadModulesAcl()
    {
        $plugins = _app()->getAPIModules();
        foreach ($plugins as $plugin) {
            if ($plugin instanceof Epr_Module) {
                $authLevel = $plugin->getAPIAuthLevel();

                $pluginApiResource = 'mvc:api' . '.' . __($plugin->getId())->substringFromPositionOfString(
                        '_'
                    )->removeLeadingCharactersWhenMatching('_')->lowercase();

                if (!$this->has($pluginApiResource, 'mvc:api')) {
                    $this->addResource($pluginApiResource, 'mvc:api');
                }
                $this->allow($authLevel, $pluginApiResource);

                $pluginDefaultResource = 'mvc:default' . '.' . __($plugin->getId())->substringFromPositionOfString(
                        '_'
                    )->removeLeadingCharactersWhenMatching('_')->lowercase();
                $this->addResource($pluginDefaultResource, 'mvc:default');
                $this->allow($authLevel, $pluginDefaultResource);

            }
        }
    }

}