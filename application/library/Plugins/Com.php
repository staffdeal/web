<?php

class Plugins_Com extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        if ($this->getRequest()->getModuleName() != 'api' || $this->getRequest()->getControllerName() != 'com') {
            return;
        }
        $connection = Epr_Com_Connection::inboundConnection();

        if ($connection && $connection->getMessage() && $connection->getMessage()->getName()) {

            $message = $connection->getMessage();
            if ($message) {

                $authorized = false;
                foreach (_app()->getModules() as $module) {
                    if ($module instanceof Epr_Com_Message_Authority && $module instanceof Epr_Module) {
                        _logger()->info('Checking if module ' . $module->identifier() . ' is signing authority');
                        if ($message->checkSignatureFromAuthority($module)) {
                            _logger()->info('Module ' . $module->identifier() . ' is signing authority');
                            $authorized = true;
                            break;
                        }
                    }
                }

                if ($authorized) {

                    $messageName = __($message->getName());
                    $messageComponents = $messageName->components('.');

                    $module = 'api';
                    $controller = 'com';
                    $action = false;

                    if ($messageComponents->count() == 0) {
                        $action = 'error';
                        _logger()->alert('Message does not contain name.');
                    } else if ($messageComponents->count() == 1) {
                        $action = (string)$messageComponents[0];
                    } else {
                        $namespace = __($messageComponents[0])->lowercase();
                        $controller = (string)$namespace;
                        $action = (string)$messageComponents[1];
                    }

                    $canBeDispatched = false;

                    if (is_string($action) && strlen($action) > 0) {

                        $front = Zend_Controller_Front::getInstance();
                        $testRequest = new Zend_Controller_Request_Http();
                        $testRequest->setModuleName($module)->setControllerName($controller)->setActionName($action);

                        $dispatcher = $front->getDispatcher();
                        $canBeDispatched = ($dispatcher->isDispatchable($testRequest));
                    }

                    if ($canBeDispatched) {

                        $request->setModuleName($module)->setControllerName($controller)->setActionName($action);

                    } else {
                        _logger()->alert('Message ' . $messageName . ' could not be dispatched');
                        $connection->replyWithErrorMessage('Internal error.');
                        $request->setDispatched(true);
                    }

                } else {
                    $connection->replyWithErrorMessage('I do not accept messages without a key.');
                    $request->setDispatched(true);
                }
            }
        }
    }
}