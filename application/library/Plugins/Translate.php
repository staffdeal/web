<?php

class Plugins_Translate extends Zend_Controller_Plugin_Abstract
{

    const REGISTRY_KEY = 'Zend_Translate';


    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {

        $moduleName = $request->getModuleName();
        if ($moduleName == 'default' || $moduleName == 'admin') {

            $languages = array('de');

            $translate = (Zend_Registry::isRegistered(self::REGISTRY_KEY)) ? Zend_Registry::get(self::REGISTRY_KEY) : null;
            if (!$translate instanceof Zend_Translate) {

                $options = array(
                    'adapter' => 'Epr_Translate_Adapter_DB',
                    'locale'  => 'de',
                    'content' => $moduleName
                );

                if (Epr_Application::application()->isDevelopment()) {
                    $translationLogger          = new Zend_Log(new Zend_Log_Writer_Stream(ROOT_PATH . '/missing-translations.log'));
                    $options['log']             = $translationLogger;
                    $options['logUntranslated'] = true;
                }

                $translate = new Zend_Translate($options);
                Zend_Registry::set(self::REGISTRY_KEY, $translate);
            }

            foreach ($languages as $language) {
                $translate->addTranslation(
                    array(
                        'content' => $moduleName,
                        'locale'  => $language
                    )
                );
            }

        }

        if ($moduleName == 'default') {
            _app()->registerLocalizationStringsIfNeeded();
        }
    }


}