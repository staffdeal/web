function(doc) {
    if (doc.type == 'Epr_Deal') {
        emit(doc.startDate.split(' ')[0], doc);
    }
}