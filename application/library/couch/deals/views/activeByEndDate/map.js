function(doc) {
    if (doc.type == 'Epr_Deal' && doc.isActive) {
        emit(doc.endDate.split('T')[0], doc);
    }
}