function (doc) {
    if (doc.type == 'Epr_Idea_Competition' && doc.isActive) {
        emit(doc.endDate, doc);
    }
}