function (doc) {
    if (doc.type == 'Epr_Idea_Competition') {
        emit(doc.startDate, doc);
    }
}