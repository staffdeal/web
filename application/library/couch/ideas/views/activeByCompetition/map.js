function (doc) {
    if (doc.type == 'Epr_Idea' && doc.isActive) {
        emit(doc.competition, doc);
    }
}