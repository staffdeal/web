function(doc) {
    if (doc.type == 'Epr_Content_Image') {
        if (doc.access) {
            var binary = doc.access.toString(2);

            for(var i = 0, u = binary.length; i < u; i++) {
                var factor = parseInt(binary[u - i - 1]);
                if (factor == 1) {
                    var containedValue = Math.pow(2, i);
                    emit([containedValue, doc.creationDate], doc);
                }
            }

        }
    }
}