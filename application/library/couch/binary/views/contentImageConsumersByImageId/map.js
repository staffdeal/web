function (doc) {

    if (doc.titleImage) {
        emit(doc.titleImage, doc);
    }

    if (doc.type == 'Epr_Theme') {
        if (doc.logoImage) {
            emit(doc.logoImage, doc);
        }
        if (doc.lowContrastLogoImage) {
            emit(doc.lowContrastLogoImage, doc);
        }
        if (doc.faviconImage) {
            emit(doc.faviconImage, doc);
        }
        if (doc.appleTouchIcon) {
            emit(doc.appleTouchIcon, doc);
        }
        if (doc.appleTouchIcon_72) {
            emit(doc.appleTouchIcon_72, doc);
        }
        if (doc.appleTouchIcon_114) {
            emit(doc.appleTouchIcon_114, doc);
        }
        if (doc.appleTouchIcon_144) {
            emit(doc.appleTouchIcon_144, doc);
        }
    }

    if (doc.type == 'Epr_Cms_Page') {
        if (doc.image) {
            emit(doc.image, doc);
        }
    }

}