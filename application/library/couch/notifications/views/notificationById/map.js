function (doc) {
    if (doc.parent_type == 'Epr_Notification') {
        emit(doc._id, doc);
    }
}