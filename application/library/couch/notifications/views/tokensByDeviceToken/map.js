function(doc) {
	if (doc.type == 'Epr_Token' && doc.deviceToken) {
		emit(doc.deviceToken, doc);
	}
}