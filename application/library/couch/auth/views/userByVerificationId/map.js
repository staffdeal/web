function (doc) {
	if (doc.type == 'Epr_User' && doc.verificationId) {
		emit(doc.verificationId, doc);
	}
}