function (doc) {
	if (doc.type == 'Epr_User' && doc.resetPasswordId) {
		emit(doc.resetPasswordId, doc);
	}
}