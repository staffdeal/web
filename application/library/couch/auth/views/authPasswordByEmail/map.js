function (doc) {
    if (doc.type == 'Epr_User' && doc.isActive) {
        emit(doc.email, doc.password);
    }
}