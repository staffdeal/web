function (doc) {
    if (doc.type == 'Epr_Dealer' && doc.isActive) {
        emit(doc.title, doc);
    }
}