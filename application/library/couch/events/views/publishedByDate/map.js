function (doc) {
    "use strict";
    if (doc.type == 'Epr_Event') {
        if (doc.state == 'published') {
            emit(doc.date, doc);
        }
    }
}