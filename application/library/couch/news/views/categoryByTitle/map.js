function(doc) {
    if (doc.type === 'Epr_News_Category' && doc.title) {
        emit(doc.title, doc);
    }
}