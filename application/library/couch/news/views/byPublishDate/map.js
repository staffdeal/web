function (doc) {
    if (doc.type == 'Epr_News') {
        emit(doc.automaticPublishDate || doc.manualPublishDate, doc);
    }
}