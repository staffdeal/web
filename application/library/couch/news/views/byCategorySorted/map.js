function (doc) {
	if (doc.type == 'Epr_News' && doc.state == 'published') {
		emit([doc.category, doc.automaticPublishDate || doc.manualPublishDate], doc);
	}
}