function (doc) {
    if (doc.type == 'Epr_News' && doc.state == 'draft') {
        if (doc.automaticPublishDate) {
            emit(doc.automaticPublishDate, doc);
        }
    }
}