function (doc) {
    if (doc.slug) {
        emit(doc.slug, doc._id)
    }
}