<?php

interface Epr_Searchable {

    static function getSearchNames();
    static function getSearchFieldsForName($name); // return an array of fields to search
    static function getSearchFiltersForName($name);
    static function registerSearchDesignDocument($documentName, $client);
}