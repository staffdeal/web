<?php

/** @Document (repositoryClass="Epr_Deal_Collection") */
class Epr_Deal extends Epr_Document_WebPage
{
    const TYPE_ONSITE = 'onsite';
    const TYPE_ONLINE = 'online';

    const ONLINE_NOCODE = 'nocode';

    const DOCUMENTNAME = 'Epr_Deal';
    /** @Id */
    public $id;

    /** @Field(type="string") */
    private $teaser;

    /**
     * @Field(type="datetime")
     * @Index
     * */
    private $startDate;

    /**
     * @Field(type="datetime")
     * @Index
     */
    private $endDate;

    /**
     * @Field(type="boolean")
     * @Index
     */
    private $featured = false;

    /** @Field(type="mixed") */
    private $codeData = array();

    /** @Field(type="string") */
    private $activeOnsiteModuleIdentifier = 'Module_StaticCode';

    /** @var Epr_Module_DealCode_Abstract */
    private $onsiteCodeModule = null;

    /** @var Epr_Module_DealCode_Abstract */
    private $onlineCodeModule = null;

    /** @Field(type="mixed") */
    private $dealerCategories = array();

    /** @Field(type="string") */
    private $price;

    /** @Field(type="string") */
    private $oldPrice;

    /** @Field(type="string") */
    private $reminderText;

    /** @Field(type="boolean") */
    private $manuallySoldOut = false;


    /**
     * Sets the end date of the deal.
     *
     * @param DateTime $endDate
     *
     * @return Epr_Deal
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Returns the end date of the deal.
     *
     * @return DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Sets the teaser of the deal.
     *
     * @param string $teaser
     *
     * @return Epr_Deal
     */
    public function setTeaser($teaser)
    {
        $this->teaser = $teaser;

        return $this;
    }

    /**
     * Returns the teaser of the deal.
     *
     * @return string|null
     */
    public function getTeaser()
    {
        return $this->teaser;
    }

    /**
     * Sets the start date of the deal.
     *
     * @param DateTime $startDate
     *
     * @return Epr_Deal
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Returns the start date of the deal.
     *
     * @return DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Sets the featured flag.
     *
     * @param $featured
     *
     * @return Epr_Deal
     */
    public function setAsFeatured($featured)
    {
        $this->featured = $featured;

        return $this;
    }

    /**
     * Returns the featured flag.
     *
     * @return bool
     */
    public function isFeatured()
    {
        return $this->featured;
    }

    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function getOldPrice()
    {
        return $this->oldPrice;
    }

    /**
     * @param $price
     * @return $this
     */
    public function setOldPrice($price)
    {
        $this->oldPrice = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getReminderText()
    {
        return $this->reminderText;
    }

    /**
     * @param $text
     * @return $this
     */
    public function setReminderText($text)
    {
        $this->reminderText = (is_null($text)) ? null : (string)$text;
        return $this;
    }

    public function isActive()
    {
        $active = parent::isActive();
        if ($active) {
            $active = $this->getOnsiteCodeModule()->isValid() || $this->getOnlineCodeModule()->isValid();
        }

        return $active;
    }

    public function getActiveOnsiteCodeModuleIdentifier()
    {
        return $this->activeOnsiteModuleIdentifier;
    }

    public function isManuallySoldOut() {
        return ($this->manuallySoldOut == true);
    }

    /**
     * @param bool $value
     * @return $this
     */
    public function setIsManuallySoldOut($value) {
        $this->manuallySoldOut = ($value == true);
        return $this;
    }

    /**
     * Returns the onsite code generation module.
     *
     * @return Epr_Module_DealCode_Abstract
     */
    public function getOnsiteCodeModule()
    {

        if (is_null($this->onsiteCodeModule)) {
            // TODO: now we staticly create a static code instance, but this should be a real setter which sets the id of the module
            if (!is_array($this->codeData)) {
                $this->codeData = array();
            }
            $this->onsiteCodeModule = new Epr_Modules_StaticCode();
            $dataKey = $this->onsiteCodeModule->getDataKey();
            if ($dataKey) {
                $data = (isset($this->codeData[$dataKey])) ? $this->codeData[$dataKey] : array();
                $this->onsiteCodeModule->setData($data);
            }
        }

        return $this->onsiteCodeModule;
    }

    /**
     * Returns the online code generation module.
     *
     * @return Epr_Module_DealCode_Abstract
     */
    public function getOnlineCodeModule()
    {
        if (is_null($this->onlineCodeModule)) {
            if (!is_array($this->codeData)) {
                $this->codeData = array();
            }
            $this->onlineCodeModule = new Epr_Modules_CodeList();
            $dataKey = $this->onlineCodeModule->getDataKey();
            if ($dataKey) {
                $data = (isset($this->codeData[$dataKey])) ? $this->codeData[$dataKey] : array();
                $this->onlineCodeModule->setData($data);
            }
        }

        return $this->onlineCodeModule;
    }

    public function setCodeModulesData($data)
    {
        $this->codeData = $data;
    }

    public function getCodeModulesData()
    {
        return $this->codeData;
    }

    public function getOnsiteRedeemURL()
    {

        $url = _app()->getPublicURL();
        return $url->addPathComponent('redeem')->addPathComponent($this->getId())->addPathComponent('onsite');
    }

    public function getOnlineRedeemURL()
    {

        $url = _app()->getPublicURL();
        return $url->addPathComponent('redeem')->addPathComponent($this->getId())->addPathComponent('online');
    }

    /**
     * Removes all dealer category information.
     *
     * @return $this
     */
    public function resetDealerCategories()
    {
        $this->dealerCategories = array();

        return $this;
    }

    /**
     * Marks the field with the given id of the category with the given id as selected.
     *
     * @param $categoryIndex
     * @param $fieldIndex
     *
     * @return $this
     */
    public function selectDealerCategoryField($categoryIndex, $fieldIndex)
    {

        if (is_integer($categoryIndex) && $categoryIndex >= 0) {

            if (is_integer($fieldIndex) && $fieldIndex >= 0) {

                $category = null;
                if (isset($this->dealerCategories[$categoryIndex])) {
                    $category = $this->dealerCategories[$categoryIndex];
                } else {
                    $category = array();
                }

                if (array_search($fieldIndex, $category) === false) {
                    $category[] = $fieldIndex;
                }

                $this->dealerCategories[$categoryIndex] = $category;
            }
        }

        return $this;
    }

    /**
     * Returns true if the field with the given index is selected in the category with the given index.
     * @param $categoryIndex
     * @param $fieldIndex
     *
     * @return bool
     */
    public function isDealerCategoryFieldSelected($categoryIndex, $fieldIndex)
    {

        if (isset($this->dealerCategories[$categoryIndex])) {
            return (array_search($fieldIndex, $this->dealerCategories[$categoryIndex]) !== false);
        }

        return false;
    }

    /**
     * Returns an array of selected field indexes in the category with the given index.
     * @param $categoryIndex
     *
     * @return array
     */
    public function selectedFieldIndexesInCategory($categoryIndex)
    {
        if (isset($this->dealerCategories[$categoryIndex])) {
            return $this->dealerCategories[$categoryIndex];
        } else {
            return array();
        }
    }
}

