<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 23.09.12
 * Time: 17:34
 */

use Doctrine\ODM\CouchDB\DocumentRepository;
use Doctrine\CouchDB\View\FolderDesignDocument;

/** @Document (repositoryClass="Epr_Content_Image_Collection") */
class Epr_Content_Image extends Epr_Document
{

	const DOCUMENTNAME = 'Epr_Content_Image';

	const BINARY_VERSION = 1.0;

	const ACCESS_GENERAL       = 1;
	const ACCESS_SYSTEM        = 2;
	const ACCESS_DEALER_LOGO   = 4;
	const ACCESS_IDEA_IMAGE    = 8;
	const ACCESS_IDEA_CATEGORY = 16;
	const ACCESS_CMS_IMAGE = 32;
	const ACCESS_TMP = 64;
    const ACCESS_THEME = 128;

	/** @Id */
	protected $id;

    /** @Version */
    protected $version;

	/** @Field(type="integer") */
	protected $binaryVersion;

	/** @Field(type="string") */
	protected $name;

	/**
	 * @Index
	 * @Field(type="string")
	 */
	protected $reference;

	/** @Field(type="string") */
	protected $copyright;

	/** @Attachments */
	protected $attachments;

	/** @var @Field(type="integer") */
	protected $access;

	/** @var @Field(type="string") */
	protected $hash;

	/** @Field(type="integer") */
	protected $width;

	/** @Field(type="integer") */
	protected $height;

    /** @Field(type="datetime") */
    protected $creationDate;

    /** @Field(type="datetime") */
    protected $trashedDate;

	/** @Field(type="float")  */
	protected $focalPointX;

	/** @Field(type="float")  */
	protected $focalPointY;

	private $lastURLWidth = false;
	private $lastURLHeight = false;

    public function __construct() {
      $this->creationDate = new DateTime();
    }

    public function getVersion() {
        return $this->version;
    }

	static public function imageFromPImage(\Poundation\PImage $image, $name = false)
	{
		$contentImage = null;
		if ($image) {
			$contentImage = new Epr_Content_Image();
			$contentImage->importImage($image, $name);
		}

		return $contentImage;
	}

	public function importImage(\Poundation\PImage $image, $name = false)
	{
		if ($name === false) {
			$name = $image->getName();
		}
		if ($name === false) {
			$name = 'unnamed';
		}

		$imageHash = $image->getHash();
		if ($this->hash != $imageHash) {
			// only import if the binary data is really different
			$this->attachments        = array();
			$this->attachments['raw'] = \Doctrine\CouchDB\Attachment::createFromBinaryData($image->getData(), $image->getMIME());
			$this->hash               = $image->getHash();
			$this->getWidth();
			$this->getHeight();
		}
		$this->name = $name;
	}

	/**
	 * @return \Doctrine\CouchDB\Attachment|null
	 */
	private function getAttachment($id = 'raw')
	{
		if ($id === 'raw') {
			// Legacy checks and transformations
			if (count($this->attachments) == 1) {

			}
		}

		return (isset($this->attachments[$id]) ? $this->attachments[$id] : null);
	}


	public function getData()
	{
		$attachment = $this->getAttachment();
		if ($attachment) {
			return $attachment->getRawData();
		}
	}

	/**
	 * Returns the hash of the image.
	 *
	 * @return string|null
	 */
	public function getHash()
	{
		if ($this->hash == null) {
			$this->hash = $this->getImage()->getHash();
		}

		return $this->hash;
	}

	public function getLength()
	{
		$attachment = $this->getAttachment();
		if ($attachment) {
			return $attachment->getLength();
		} else {
			return 0;
		}
	}

	public function getContentType()
	{
		$type = null;

		$attachment = $this->getAttachment();
		if ($attachment) {
			$type = $attachment->getContentType();
		}

		if ($type == 'application/octet-stream' || is_null($type)) {
			$type = \Poundation\PMIME::getTypeForFilename($this->getName());
		}

		return $type;

	}

	/**
	 * @return \Poundation\PMIME
	 */
	public function getMIME()
	{
		$type = $this->getContentType();

		return \Poundation\PMIME::createMIMEWithType($type);
	}

	public function setCopyright($copyright)
	{
		$this->copyright = $copyright;
	}

	public function getCopyright()
	{
		return $this->copyright;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getName()
	{
		return $this->name;
	}

    public  function getCreationDate() {
        return $this->creationDate;
    }

	/**
	 * Returns true if the given access flag is set.
	 *
	 * @param $access
	 *
	 * @return bool
	 */
	public function hasAccess($access)
	{
		// synchronize the db field with the internal flag
		if ($this->flags == null) {
			$this->flags = $this->access;
		}

		// default to ACCESS_GENERAL when no flag has been set yet
		if ($this->access == null) {
			$this->setAccess(self::ACCESS_GENERAL);
		}

		return $this->isFlagSet($access);
	}

	/**
	 * Sets the access flag.
	 *
	 * @param $access
	 *
	 * @return Epr_Content_Image
	 */
	public function setAccess($access)
	{
		$this->setFlag($access);

		// synchronizing the internal flag with the db field
		$this->access = $this->flags;

		return $this;
	}

	/**
	 * Removes the access flag.
	 *
	 * @param $access
	 *
	 * @return Epr_Content_Image
	 */
	public function removeAccess($access)
	{
		$this->setFlag($access, false);

		return $this;
	}

	/**
	 * Returns the reference string.
	 * @return string
	 */
	public function getReference() {
		return $this->reference;
	}

	/**
	 * Sets a reference string.
	 * @param $string
	 *
	 * @return $this
	 */
	public function setReference($string) {
		$this->reference = (string)$string;
		return $this;
	}

	public function getRelativePath()
	{
		$url = '/files/images/' . urlencode($this->getName());

		return $url;
	}

	/**
	 * Returns the width of the original image.
	 * @return int
	 */
	public function getWidth()
	{

		if ($this->width <= 0) {
			$this->width = $this->getImage()->getWidth();
		}

		return $this->width;

	}

	/**
	 * Returns the height of the original image.
	 * @return int
	 */
	public function getHeight()
	{

		if ($this->height <= 0) {
			$this->height = $this->getImage()->getHeight();
		}

		return $this->height;
	}

	/**
	 * Returns the image ratio.
	 * @return float
	 */
	public function getRatio() {
		return $this->getWidth() / $this->getHeight();
	}

	public function getFocalXPercentage() {
		if (is_null($this->focalPointX)) {
			$this->focalPointX = 0.5;
		}
		return $this->focalPointX;
	}

	public function getFocalYPercentage() {
		if (is_null($this->focalPointY)) {
			$this->focalPointY = 0.5;
		}
		return $this->focalPointY;
	}

	/**
	 * @param $XPercentage
	 * @param $YPercentage
	 * @return $this
	 */
	public function setFocalPoint($XPercentage, $YPercentage) {
		if ((0.0 <= $XPercentage && $XPercentage <= 1.0) && (0.0 <= $YPercentage && $YPercentage <= 1.0)) {
			if ($XPercentage != $this->focalPointX || $YPercentage != $this->focalPointY) {
				$this->focalPointX = $XPercentage;
				$this->focalPointY = $YPercentage;
				$this->removeAllThumbnails();
			}
		}
		return $this;
	}

	/**
	 * Returns the width of the image if scaled to the given height.
	 * @param $height
	 *
	 * @return float
	 */
	public function getWidthByHeight($height) {
		return (int)round($this->getRatio() * $height,0);
	}

	public function getHeightByWidth($width) {
		return (int)round($width / $this->getRatio(),0);
	}

	public function getPublicURL($width = false, $height = false)
	{
		$applicationPath = _app()->getPublicURL();
		$url             = $applicationPath . '/files/images/' . urlencode($this->getName());

		$this->lastURLWidth = false;
		$this->lastURLHeight = false;

		if (is_int($width) && $height == false) {
			$height = $this->getHeightByWidth($width);
		} else if (is_int($height) && $width == false) {
			$width = $this->getWidthByHeight($height);
		}

		if (is_numeric($width) && is_numeric($height)) {
			if ($width > 0 && $height > 0) {
				$url .= '-' . $width . 'x' . $height;
				$this->lastURLWidth = $width;
                $this->lastURLHeight = $height;
                $url .= '.' . $this->getMIME()->getExtension();
            }
        }

		return $url;
	}

	public function getPublicIDBasedURL($width = false, $height = false)
	{
		$applicationPath = _app()->getPublicURL();
		$url             = $applicationPath . '/files/images/' . urlencode($this->getId());

		$this->lastURLWidth = false;
		$this->lastURLHeight = false;

		if (is_int($width) && $height == false) {
			$height = $this->getHeightByWidth($width);
		} else if (is_int($height) && $width == false) {
			$width = $this->getWidthByHeight($height);
		}

		if (is_numeric($width) && is_numeric($height)) {
			if ($width > 0 && $height > 0) {
				$url .= '-' . $width . 'x' . $height;
				$this->lastURLWidth = $width;
				$this->lastURLHeight = $height;
			}
		}
		$url .= '.' . $this->getMIME()->getExtension();


		return $url;
	}

	/**
	 * @param string $tagName
	 * @return string
	 * @throws Exception
	 */
	public function getAPIXMLSnippet($tagName = 'image') {
		if (strlen($tagName) == 0) {
			throw new Exception('Canot create image XML snippet with an empty tag name.');
		}
		$output = '<' . $tagName . ' id="' . $this->getId() . '" width="' . $this->getWidth() . '" height="' . $this->getHeight() . '" type="' . $this->getMIME() . '">';
		$output.= '<![CDATA[' . $this->getPublicURL() . ']]>';
		$output.= '</' . $tagName . '>';
		return $output;
	}

	/**
	 * Returns the image object.
	 *
	 * @return null|Poundation\PImage
	 */
	public function getImage()
	{
		$raw           = $this->getAttachment();
		$originalImage = \Poundation\PImage::createImageFromString($raw->getRawData(), $this->getName());

		return $originalImage;
	}

	public function removeAllThumbnails() {
		$newAttachments = array(
			'raw' => $this->getAttachment('raw')
		);
		$this->attachments = $newAttachments;
		return $this;
	}

	/**
	 * @param $key
	 *
	 * @return null|Poundation\PImage
	 */
	private function getSavedImagedWithKey($key)
	{
		$savedAttachment = $this->getAttachment($key);

		if ($savedAttachment !== null) {
			return \Poundation\PImage::createImageFromString($savedAttachment->getRawData(), $this->getName());
		}

		return null;
	}

	private function setSavedImageWithKey($key, \Poundation\PImage $image)
	{
		if ($image && is_string($key) && strlen($key) > 0) {

			$imageMIME   = $image->getMIME();
			$imageBinary = $image->getData($imageMIME);

			$this->attachments[$key] = \Doctrine\CouchDB\Attachment::createFromBinaryData($imageBinary, $imageMIME);

			_dm()->flush();
		}
	}

	/**
	 * Returns a scaled image which has the given height.
	 *
	 * @param $height
	 *
	 * @return null|Poundation\PImage
	 */
	public function getImageByHeight($height, $persist = true)
	{
		$returnImage = null;

		if (is_numeric($height) && $height > 0) {

			$key         = 'h_' . $height;
			$returnImage = $this->getSavedImagedWithKey($key);
			if (is_null($returnImage)) {

				$raw   = $this->getAttachment();
				$image = \Poundation\PImage::createImageFromString($raw->getRawData(), $this->getName());
				if ($image && $image->resize(0, $height, \Poundation\PImage::RESIZE_BY_HEIGHT)) {
					$returnImage = $image;
					if ($persist) {
						$this->setSavedImageWithKey($key, $returnImage);
					}
				}
			}
		}

		return $returnImage;
	}

	/**
	 * Returns a scaled image which has the given width.
	 *
	 * @param $width
	 *
	 * @return null|Poundation\PImage
	 */
	public function getImageByWidth($width, $persist = true)
	{

		$returnImage = null;

		if (is_numeric($width) && $width > 0) {

			$key         = 'w_' . $width;
			$returnImage = $this->getSavedImagedWithKey($key);
			if (is_null($returnImage)) {

				$raw   = $this->getAttachment();
				$image = \Poundation\PImage::createImageFromString($raw->getRawData(), $this->getName());
				if ($image) {

					if ($image->resize($width, 0, \Poundation\PImage::RESIZE_BY_WIDTH)) {
						$returnImage = $image;
						if ($persist) {
							$this->setSavedImageWithKey($key, $returnImage);
						}
					}
				}
			}
		}

		return $returnImage;
	}

	public function getCropedImageBySize($width, $height, $persist = true)
	{
		$returnImage = null;

		if (is_numeric($width) && is_numeric($height)) {
			if ($width > 0 && $height > 0) {

				$key         = $width . 'x' . $height;
				$returnImage = $this->getSavedImagedWithKey($key);
				if (is_null($returnImage)) {
					$raw   = $this->getAttachment();
					$image = \Poundation\PImage::createImageFromString($raw->getRawData(), $this->getName());
					if ($image && $image->resize($width, $height, \Poundation\PImage::RESIZE_CROP, $this->getFocalXPercentage(), $this->getFocalYPercentage())) {
						$returnImage = $image;
						if ($persist) {
							$this->setSavedImageWithKey($key, $returnImage);
						}
					}
				}
			}
		}

		return $returnImage;
	}

	public function getCSSDimensionString($width = 0, $height = 0) {

		if ($width == 0 && $height == 0) {
			$width = (is_numeric($this->lastURLWidth)) ? $this->lastURLWidth : $this->getWidth();
			$height = (is_numeric($this->lastURLHeight)) ? $this->lastURLHeight : $this->getHeight();
		}

		return 'width: ' . $width . 'px; height: ' . $height . 'px;';
	}

    public function isInTrash() {
        return (!is_null($this->trashedDate));
    }

    /**
     * @return $this
     */
    public function moveToTrash() {
        $this->trashedDate = new DateTime();
        return $this;
    }

    public function usages() {
        $usages = null;

        $query = _dm()->createQuery('binary', 'contentImageConsumersByImageId');
        $query->setKey($this->getId());
        $query->onlyDocs(true);

        $result = $query->execute();
        if ($result) {
            $usages = $result;
        }

        return $usages;
    }
}
