<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 23.09.12
 * Time: 17:41
 */

use Doctrine\ODM\CouchDB\DocumentRepository;
use Doctrine\CouchDB\View\FolderDesignDocument;

class Epr_Content_Image_Collection extends Epr_Collection implements Epr_Paginatable
{
    const SORT_DESIGN_DOCUMENT_NAME = 'sortContentImage';

    const SEARCH_DESIGN_DOCUMENT_NAME = 'searchContentImages';
    const SEARCH_ALL = 'all';

    static function getSortKeys()
    {
        return array(
            'name',
            'access',
            'width',
            'height',
            'access'
        );
    }

    static function getSortExclusions()
    {
        return array('trashedDate' => null);
    }

    static function getSearchNames()
    {
        return array_merge(parent::getSearchNames(), array(
            self::SEARCH_ALL
        ));
    }

    public static function getSearchFieldsForName($name)
    {
        $fields = parent::getSearchFieldsForName($name);
        if ($name == self::SEARCH_ALL) {
            $fields = array_merge($fields, array(
                'name',
                'copyright'
            ));
        }
        return $fields;
    }

    static function getSearchFiltersForName($name)
    {
        $fields = parent::getSearchFiltersForName($name);
        if ($name == self::SEARCH_ALL) {
            $fields = array_merge($fields, array(
                'width',
                'height'
            ));
        }
        return $fields;
    }


    /**
     * @return \Doctrine\ODM\CouchDB\DocumentRepository
     */
    static function getRepository() {
        return _dm()->getRepository(Epr_Content_Image::DOCUMENTNAME);
    }

    /**
     * @param $name
     * @return Epr_Content_Image
     */
    static public function imageWithFilename($name) {

        $query = _dm()->createQuery('binary','contentImagesByName');
        $result = $query->setKey($name)->onlyDocs(true)->execute();
        return (count($result) > 0) ? $result[0] : null;

    }

    /**
     * @param $id
     * @return Epr_Content_Image|null
     */
    static public function imageWithId($id) {
        return _dm()->find(Epr_Content_Image::DOCUMENTNAME, $id);
    }

	/**
	 * Returns the content image with the given hash
	 * @param $hash
	 *
	 * @return Epr_Content_Image
	 */
	static public function imageWithHash($hash) {
		$query = _dm()->createQuery('binary', 'contentImagesByHash');
		$result = $query->setKey($hash)->onlyDocs(true)->execute();
		return (count($result) > 0) ? $result[0] : null;
	}

    static public function allImages() {

        $result = self::getRepository()->findAll();
        return $result;

    }

	static public function imagesWithReference($reference) {
		$result = self::getRepository()->findBy(array('reference' => $reference));
		return \Poundation\PArray::create($result);
	}

    /**
     * @param $access
     * @return \Doctrine\CouchDB\View\Result|null
     */
    static public  function imageWithAccess($access) {
        $query = _dm()->createQuery('binary', 'contentImagesByAccess');
        $result = $query->setKey($access)->onlyDocs(true)->execute();
        return (count($result) > 0) ? $result : null;
    }

    static public function getImagesByAccessSortedByName($access, $count = 0, $offset = 0, $sorting = SORT_ASC) {
        $images = null;

        $access = (int)$access;
        if ($access > 0) {
            $query = _dm()->createQuery('binary', 'contentImagesByAccessAndName');

            // startkey=["1"]&endkey=["1",{}]
            $startKey = array($access);
            $endKey = array($access, array());

            if ($count > 0) {
                $query->setLimit($count);
            }

            if ($offset > 0) {
                $query->setSkip($offset);
            }

            if ($sorting == SORT_ASC) {
                $query->setStartKey($startKey);
                $query->setEndKey($endKey);
            } else {
                $query->setStartKey($endKey);
                $query->setEndKey($startKey);
                $query->setDescending(true);
            }

            $query->onlyDocs(true);
            $result = $query->execute();
            if (!is_null($result)) {
                $images = $result;
            }
        }

        return $images;
    }

    static public function getImagesByAccessSortedByCreationDate($access, $count = 0, $offset = 0, $sorting = SORT_ASC) {
        $images = null;

        $access = (int)$access;
        if ($access > 0) {
            $query = _dm()->createQuery('binary', 'contentImagesByAccessAndCreationDate');

            // startkey=["1"]&endkey=["1",{}]
            $startKey = array($access);
            $endKey = array($access, array());

            if ($count > 0) {
                $query->setLimit($count);
            }

            if ($offset > 0) {
                $query->setSkip($offset);
            }

            if ($sorting == SORT_ASC) {
                $query->setStartKey($startKey);
                $query->setEndKey($endKey);
            } else {
                $query->setStartKey($endKey);
                $query->setEndKey($startKey);
                $query->setDescending(true);
            }


            $query->onlyDocs(true);
            $result = $query->execute();
            if (!is_null($result)) {
                $images = $result;
            }
        }

        return $images;
    }

    static public function countImagesByAccess($access) {
        $access = (int)$access;

        $query = _dm()->createQuery('binary', 'countContentImagesByAccess');
        $query->setKey($access);
        $result = $query->execute();

        if (count($result) > 0) {
            $row = $result[0];
            if (isset($row['value'])) {
                return (int)$row['value'];
            }
        }

        return 0;
    }

    static function getImagesMatching($query, $sortKey = null, $sortDirection = SORT_ASC, $count = 10, $offset = 0) {
        return self::getMatching(self::SEARCH_ALL, $query, $sortKey, $sortDirection, $count, $offset);
    }

}
