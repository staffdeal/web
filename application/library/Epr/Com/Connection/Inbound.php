<?php

use Airbrake\Exception;

use Poundation\PString;

use Poundation\PURL;

use Poundation\PClass;

class Epr_Com_Connection_Inbound extends Epr_Com_Connection {

	/**
	 * @var Zend_Controller_Request_Http
	 */
	private $request;
	
	static private $instance = null;
	
	/**
	 * @var Epr_Com_Message
	 */
	private $replyMessage = false;
	
	/**
	 * Returns a shared connection.
	 * @return Epr_Com_Connection_Inbound
	 */
	static public function connection() {
		
		if (!isset(self::$instance)) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	public function __construct() {

		$request = Zend_Controller_Front::getInstance()->getRequest();
		if ($request instanceof Zend_Controller_Request_Http) {
			$this->request = $request;
			if ($this->loadRequest() == false) {
				throw new Exception('Could not parse message from incoming connection.');
			}
			
		} else {
			throw new Exception(
					'Cannot create inbound connection since there is no HTTP request.');
		}
	}

	private function loadRequest() {
		$rawBody = $this->request->getRawBody();

        // format candidates
        $firstLetter = substr($rawBody,0,1);
        if ($firstLetter == '<') {
            $this->message = Epr_Com_Message::createMessageFromXML($rawBody);
        } else if ($firstLetter) {
            $this->message = Epr_Com_Message::createMessageFromJSON($rawBody);
        }

		return ($this->message !== false);
	}
	
	public function reply(Epr_Com_Message $message = NULL) {
		if ($message === NULL) {
			$message = $this->replyMessage;
		}
		
		if ($message === NULL) {
			throw new Exception('Cannot reply without a message. Call replyMessage first or pass a custom one.');
		} else {

			$xml = (string)$message;
			$front = Zend_Controller_Front::getInstance();
			
			// since we're sending pure XML we need to disable Zend's layout and renderer
			$layout = Zend_Layout::getMvcInstance();
			$layout->disableLayout();
			
			$response = $front->getResponse();
			$response->setHeader('Content-type', 'application/xml');
			$response->setBody($xml);
		}
	}
	
	
	/**
	 * Returns the message used to reply to the incoming connection.
	 * @return Epr_Com_Message
	 */
	public function replyMessage() {
		
		if ($this->replyMessage === false) {
			$this->replyMessage = Epr_Com_Message::createMessageWithName('reply', $this->getMessage()->getFormat());
            if ($this->getMessage()->signatureAuthority()) {
                $this->replyMessage->sign($this->getMessage()->signatureAuthority()->signingKey());
            }
		}
		
		return $this->replyMessage;
	}
	
	/**
	 * Returns a prepared success message as reply.
	 * @return Epr_Com_Message
	 */
	public function successReplyMessage() {
		$message = $this->replyMessage();
		$message->setValue('OK', 'status');
		return $message;
	}

	/**
	 * Returns a prepared error message.
	 * @param string $errorDescription
	 * @return Epr_Com_Message
	 */	
	public function errorReplyMessage($errorDescription = false) {
		$message = $this->replyMessage();
		$message->setValue('Error', 'status');
		if ($errorDescription !== false) {
			$message->setValue($errorDescription,'description');
		}
		return $message;
	}

    /**
     * Replies with an error containing the given message.
     * @param $errorMessage The error message used to reply.
     */
    public function replyWithErrorMessage($errorMessage) {
        $message = $this->errorReplyMessage($errorMessage);
        $this->reply();
    }
}

?>