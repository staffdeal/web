<?php

class Epr_Com_Connection_Error extends \Poundation\PError {

    public function __construct($description) {
        parent::__construct('epr.com.connection',$description);
    }

    /**
     * Creates a new connection error.
     * @param $description
     * @return Epr_Com_Connection_Error
     */
    public static function createConnectionError($description) {
        return new self($description);
    }

}
