<?php

use Poundation\PString;
use Poundation\PURL;
use Poundation\PClass;

class Epr_Com_Connection_Outbound extends Epr_Com_Connection {

	/**
	 * Creates a new outbound connection to the specified target URL.
	 * @param PURL $targetURL
	 * @param Epr_Com_Message $message
	 * @throws Exception
	 * @return Epr_Com_Connection_Outbound
	 */
	static public function createOutboundConnection($targetURL, $message = false) {
		
		$connection = false;
		
		try {
			$connection = new Epr_Com_Connection_Outbound($targetURL);
		} catch (Exception $e) {
			throw $e;
		}
		
		if ($connection !== false) {
			$connection->setMessage($message);
		}
		
		return $connection;
		
	}
	
	public function __construct($targetURL) {
	
		if (is_string($targetURL) || $targetURL instanceof PURL) {
			if (is_string($targetURL)) {
				$targetURL = new PURL($targetURL);
			}
				
			$this->targetURL = $targetURL;
				
		} else {
			throw new Exception('Cannot construct connection object without a valid target URL.');
		}
	
	}
	

	
	/**
	 * Sets the user agent of the outbound connection. Has no effect once it has been sent.
	 * @param string $userAgent
	 * @return Epr_Com_Connection_Outbound
	 */
	public function setUserAgent($userAgent) {
		$this->userAgent = $userAgent;
		return $this;
	}
	
	/**
	 * Starts the connection and returns a response object.
	 * @return Epr_Com_Connection_Response
	 */
	public function start() {
	
		$remoteURL = clone $this->getTargetURL();
		if (!$remoteURL->path()->hasPrefix('api/com')) {
			$remoteURL->addPathComponent('api/com');
		}

        $useDebugger = true;
		// debug_host=10.37.129.2%2C10.211.55.2%2C192.168.208.28%2C127.0.0.1&debug_fastfile=1&start_debug=1&debug_port=10137&use_remote=1&send_sess_end=1&debug_start_session=1&debug_no_cache=1359632727808&debug_session_id=1008
        if ($useDebugger) {
            $remoteURL->setParameter('debug_host', '10.37.129.2,10.211.55.2,192.168.208.28,127.0.0.1');
            $remoteURL->setParameter('debug_fastfile', '1');
            $remoteURL->setParameter('start_debug', '1');
            $remoteURL->setParameter('debug_port', '10137');
            $remoteURL->setParameter('use_remote', '1');
            $remoteURL->setParameter('send_sess_end', '1');
            $remoteURL->setParameter('debug_start_session', '1');
            $remoteURL->setParameter('debug_session_id', '12431');
            $remoteURL->setParameter('original_url', 'http://dw.meinepr.loc/api/com');
        }
		
		$userAgent = 'meinEPR Outbound Connection';
		if (is_string($this->getUserAgent())) {
			if (strlen($this->getUserAgent()) > 0) {
				$userAgent = $this->getUserAgent();
			}
		} else if ($this->getUserAgent() instanceof PString) {
			if ($this->getUserAgent()->length() > 0) {
				$userAgent = (string)$this->getUserAgent();
			}
		}
		
		$urlString = (string)$remoteURL;
		$client = new Zend_Http_Client($urlString, array(
					'useragent'		=> $this->getUserAgent()
				));
		
		if ($this->message) {

			$output = (string)$this->message;
            $mime = 'text/json';

            if ($this->getMessage()->getFormat() == Epr_Com_Message::FORMAT_XML) {
                $mime = 'text/xml';
            }

			$client->setRawData($output)->setEncType($mime);
		}
		
		$response = new Epr_Com_Connection_Response($client);
		return $response;
	}
	
	
}

?>