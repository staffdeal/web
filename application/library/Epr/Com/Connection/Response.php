<?php

class Epr_Com_Connection_Response {

	const STATUS_NOTLOADED = 0;
	const STATUS_GENERALERROR = 1;
	const STATUS_HOSTNOTFOUND = 2;
	const STATUS_CONNECTIONERROR = 3;
	const STATUS_RESPONSEFORMATERROR = 4;
	const STATUS_SUCCESS = 200;
	const STATUS_FORBIDDEN = 403;
	const STATUS_MESSAGEERROR = 404;
	const STATUS_PEERERROR = 500;
	
	/**
	 * @var Zend_Http_Client
	 */
	private $client;

	private $status;
    private $error;
	
	/**
	 * @var Epr_Com_Message
	 */
	private $receivedMessage = false;
	
	public function __construct(Zend_Http_Client $client) {
		$this->status = self::STATUS_NOTLOADED;
		if ($client instanceof Zend_Http_Client && $client !== NULL) {
			$this->client = $client;
			$this->load();
		} else {
			throw new Exception('Cannot create response without a http client.');
		}
	}
	
	private function load() {
		if ($this->client) {
			$hostname = false;
			$hostIP = false;
			$response = false;
			try {
				$hostname = $this->client->getUri()->getHost();
				$hostIP = gethostbyname($hostname);
			} catch (Exception $e) {
				
			}
			
			if ($hostIP === $hostname) {
				$this->status = self::STATUS_HOSTNOTFOUND;
			} else {
			
				try {
				  	$response = $this->client->request('POST');
				} catch (Zend_Http_Client_Adapter_Exception $e) {
					$this->status = self::STATUS_GENERALERROR;
				
					$message = __($e->getMessage());
					if ($message->hasPrefix('Unable to Connect')) {
						$this->status = self::STATUS_CONNECTIONERROR;
					}
				}
			}
			
			if ($response !== false) {
				$this->status = $response->getStatus();
				
				if ($this->status === self::STATUS_SUCCESS) {
					$headers = $response->getHeaders();
					$contentType = __($headers['Content-type']);
					if ($contentType->uppercase()->hasSuffix('XML')) {
					
						$rawContent = $response->getRawBody();
						$this->receivedMessage = Epr_Com_Message::createMessageFromXML($rawContent);
						
						if ($this->receivedMessage instanceof Epr_Com_Message) {
							$statusString = (string)__($this->receivedMessage->getValue('status'))->uppercase();
							if ($statusString != 'OK') {
								$this->status = self::STATUS_PEERERROR;
                                if ($statusString == 'ERROR') {
                                    $this->error = Epr_Com_Connection_Error::createConnectionError($this->receivedMessage->getValue('description'));
                                } else {
                                    $this->error = Epr_Com_Connection_Error::createConnectionError('An unspecified error occured.');
                                }

							}
						} else {
							$this->status = self::STATUS_RESPONSEFORMATERROR;
						}
						
					} else {
						$this->status = self::STATUS_RESPONSEFORMATERROR;
					}
				}
			}
		}
	}
	
	/**
	 * Returns the status of the connection.
	 */
	public function getStatus() {
		return $this->status;
	}

    public function wasSuccessful() {
        return (!$this->wasFaulty());
    }

    public function wasFaulty() {
        return ($this->getLastError() !== null);
    }

    /**
     * Returns the last error.
     * @return Epr_Com_Connection_Error
     */
    public function getLastError() {
        return (($this->error instanceof Epr_Com_Connection_Error) ? $this->error : null);
    }
	
	public function getReceivedMessage() {
		return $this->receivedMessage;
	}
	
}

?>