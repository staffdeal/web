<?php

class Epr_Com_Message_Attachment {

    private $data = null;
    private $filesize = 0;
    private $filename = null;
    private $useBase64 = true;

    /**
     * Creates an attachment from a DOM node.
     * @param DOMNode $attachmentNode
     * @return Epr_Com_Message_Attachment
     */
    public static function attachmentFromDOMNode(DOMNode $attachmentNode) {

        if ($attachmentNode->nodeName == 'attachment') {
            $attachment = new Epr_Com_Message_Attachment();

            $attributes = $attachmentNode->attributes;
            for ($i = 0; $i < $attributes->length; $i++) {
                $attribute = $attributes->item($i);
                $attributeName = $attribute->nodeName;
                $attributeValue = $attribute->nodeValue;

                if ($attributeName == 'name') {
                    if (strlen($attributeValue) > 0) {
                        $attachment->filename = $attributeValue;
                    }
                } else if ($attributeName == 'encoding') {
                    if ($attributeValue === 'base64') {
                        $attachment->useBase64 = true;
                    }
                } else if ($attributeName == 'size') {
                    $attachment->filesize = (int)$attributeValue;
                }
            }

            $attachment->data = ($attachment->useBase64) ? base64_decode($attachmentNode->nodeValue) : $attachmentNode->nodeValue;

            return $attachment;
        }

        return null;
    }

    /**
     * Creates a new attachment with the content of the file with the given filename.
     * @param $filename
     * @return Epr_Com_Message_Attachment|null
     */
    public static function attachmentFromFilename($filename) {

        if (file_exists($filename)) {
            $fileHandle = fopen($filename,'r');
            $attachment = self::attachmentFromFilehandle($fileHandle);
            if ($attachment) {
                $attachment->filename = basename($filename);
            }
            return $attachment;
        }
        return null;
    }

    public static function attachmentFromFilehandle($fileHandle) {
        if (is_resource($fileHandle)) {
            $attachment = new Epr_Com_Message_Attachment();
            if ($attachment->readFromFile($fileHandle)) {
                return $attachment;
            }
        }
        return null;
    }

    /**
     * Returns the name of the attachment. This should be "filename" like image1.jpg.
     * @return string
     */
    public function getName() {
        return $this->filename;
    }

    /**
     * Sets the name of the attachment. This should be "filename" like image1.jpg.
     * @param $name
     * @return Epr_Com_Message_Attachment
     */
    public function setName($name) {
        $this->filename = $name;
        return $this;
    }

    /**
     * Returns the size of the attachment.
     * @return int
     */
    public function getSize() {
        return $this->filesize;
    }

    /**
     * Returns the data of the attachment.
     * @return string
     */
    public function getData() {
       return $this->data;
    }

    /**
     * Returns a DOM element representing the attachement. Pass a parent element if the XML should be embedded.
     * @param DOMElement $parentElement
     * @return DOMElement|DOMNode|null
     */
    public function xml(DOMElement $parentElement = null)
    {

        if ($this->filesize > 0) {

            $document = null;

            if ($parentElement === null) {
                $document = new DOMDocument('1.0', 'UTF-8');

                $parentElement = $document->createElement('attachment');
                $parentElement = $document->appendChild($parentElement);
            } else if ($parentElement instanceof DOMElement) {
                $document = $parentElement->ownerDocument;
            }

            if ($document !== null && $parentElement !== null) {

                $filesizeAttribute = $document->createAttribute('size');
                $filesizeAttribute->value = $this->filesize;
                $parentElement->appendChild($filesizeAttribute);

                if (is_string($this->filename) && strlen($this->filename) > 0) {
                    $filenameAttribute = $document->createAttribute('name');
                    $filenameAttribute->value = $this->filename;
                    $parentElement->appendChild($filenameAttribute);
                }

                if ($this->useBase64) {
                    $encodingAttribute = $document->createAttribute('encoding');
                    $encodingAttribute->value = 'base64';
                    $parentElement->appendChild($encodingAttribute);
                }

                $parentElement->nodeValue = ($this->useBase64) ? base64_encode($this->data) : $this->data;

                return $parentElement;
            }
        }
        return null;
    }

    public function __toString()
    {
        return $this->xml()->ownerDocument->saveXML();
    }

    private function readFromFile($filehandle) {
        $success = false;
        if (is_resource($filehandle)) {

            $fileInfo = fstat($filehandle);
            $this->filesize = (int)$fileInfo['size'];

            $binary = fread($filehandle, $this->filesize);
            if ($binary !== false) {
                $this->data = $binary;
                $success = true;
            }
        }
        return $success;
    }

}
