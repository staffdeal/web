<?php

class Epr_Com_Message
{

    const DATETIMEFORMAT = DateTime::ISO8601;

    const FORMAT_XML = 1;
    const FORMAT_JSON = 2;

    protected $name;

    protected $values = array();
    protected $attachments = array();

    private $key = false;

    private $format = self::FORMAT_JSON;

    private $authority;

    /**
     * Creates a new Com message.
     * @param $name
     * @return Epr_Com_Message|null
     */
    static public function createMessageWithName($name, $format = self::FORMAT_JSON)
    {
        if (is_string($name) && strlen($name) > 0) {
            $newMessage = new Epr_Com_Message();
            $newMessage->name = $name;
            $newMessage->format = $format;
            return $newMessage;
        }

        return NULL;
    }

    /**
     * Creates a Com message from the given XML
     * @param $xml
     * @return Epr_Com_Message|null
     */
    static public function createMessageFromXML($xml)
    {

        $xml = trim($xml);

        if (is_string($xml) && substr($xml, 0, 1) == '<') {

            $document = new DOMDocument('1.0', 'UTF-8');
            try {
                $didLoad = ($document->loadXML($xml));
            } catch (Exception $e) {
                $didLoad = false;
            }
            if ($didLoad) {

                $nameElements = $document->getElementsByTagName('name');
                if ($nameElements->length > 0) {
                    $nameElement = $nameElements->item(0);
                    $name = $nameElement->nodeValue;
                    if (strlen($name) > 0) {
                        $message = self::createMessageWithName($name);
                        if ($message) {

                            $message->format = self::FORMAT_XML;

                            $keyElements = $document->getElementsByTagName('key');
                            if ($keyElements->length > 0) {
                                $keyElement = $keyElements->item(0);
                                $key = $keyElement->nodeValue;
                                if (strlen($key) > 8) {
                                    $message->key = $key;
                                }
                            }

                            $valuesContainerNodes = $document->getElementsByTagName('values');
                            if ($valuesContainerNodes->length > 0) {
                                $valuesContainerNode = $valuesContainerNodes->item(0);

                                $containerChildNodes = $valuesContainerNode->childNodes;
                                for ($i = 0; $i < $containerChildNodes->length; $i++) {

                                    $value = false;
                                    $key = false;

                                    $childNode = $containerChildNodes->item($i);
                                    if ($childNode->nodeName === 'value') {
                                        $value = $childNode->nodeValue;
                                        $attributes = $childNode->attributes;
                                        for ($u = 0; $u < $attributes->length; $u++) {
                                            $attribute = $attributes->item($u);
                                            if ($attribute->nodeName === 'key') {
                                                $key = $attribute->nodeValue;
                                            } else if ($attribute->nodeName === 'type') {
                                                $type = $attribute->nodeValue;
                                                if ($type == 'datetime') {
                                                    $date = new DateTime($value);
                                                    $value = $date;
                                                }
                                            }
                                        }
                                    }

                                    if ($value !== false && $key !== false) {
                                        $message->values[$key] = $value;
                                    }
                                }
                            }

                            $attachmentsNodes = $document->getElementsByTagName('attachment');
                            for ($i = 0; $i < $attachmentsNodes->length; $i++) {
                                $attachmentNode = $attachmentsNodes->item($i);

                                $attributes = $attachmentNode->attributes;
                                for ($u = 0; $u < $attributes->length; $u++) {
                                    $attribute = $attributes->item($u);
                                    if ($attribute->nodeName == 'id') {
                                        $identifier = $attribute->nodeValue;
                                        if (strlen($identifier) > 0) {
                                            $attachment = Epr_Com_Message_Attachment::attachmentFromDOMNode($attachmentNode);
                                            if ($attachment) {

                                                $expectedSize = $attachment->getSize();
                                                $receivedSize = strlen(($attachment->getData()));
                                                if ($expectedSize != $receivedSize) {
                                                    trigger_error('Attachment size did not match expected size. Expected size of attachment "' . $identifier . '" was ' . $expectedSize . ' but got ' . $receivedSize . '.', E_USER_WARNING);
                                                }

                                                $message->attachments[$identifier] = $attachment;
                                            }
                                        }
                                    }
                                }

                            }

                            return $message;
                        }
                    }
                }

            }
        }

        return NULL;
    }

    /**
     * Creates a message of a JSON string if possible
     * @param $rawJSON
     * @return Epr_Com_Message|null
     */
    static public function createMessageFromJSON($rawJSON)
    {

        $message = null;

        $data = json_decode($rawJSON);
        if ($data instanceof stdClass) {

            $messageData = (isset($data->message)) ? $data->message : null;
            if ($messageData instanceof stdClass) {

                $name = isset($messageData->name) ? $messageData->name : null;

                if ($name) {
                    $message = Epr_Com_Message::createMessageWithName($name);
                    $message->key = (isset($messageData->key)) ? $messageData->key : null;

                    $rawValues = (isset($messageData->values)) ? $messageData->values : null;
                    if ($rawValues instanceof stdClass) {

                        $rawValuesProperties = get_object_vars($rawValues);
                        foreach($rawValuesProperties as $key=>$value) {
                            $message->setValue($value, $key);
                        }
                    }
                }
            }
        }

        return $message;
    }

    /**
     * Returns the message name.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function getFormat() {
        return $this->format;
    }

    /**
     * Returns the value of a message parameter.
     * @param string $key
     * @return string|null
     */
    public function getValue($key)
    {
        return (isset($this->values[$key]) ? $this->values[$key] : null);
    }

    /**
     * Returns all message parameters.
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * Sets a message parameter.
     * @param $value
     * @param $key
     * @return Epr_Com_Message
     */
    public function setValue($value, $key)
    {

        if (is_string($key) || is_numeric($key)) {

            if ($value != false && $value != NULL) {
                $this->values[$key] = $value;
            } else {
                if (isset($this->values[$key])) {
                    unset($this->values[$key]);
                }
            }
        }
        return $this;
    }

    /**
     * Sets values from the given array. The array's keys are used as parameter keys.
     * @param $array
     * @return Epr_Com_Message
     */
    public function setValues($array)
    {
        if (is_array($array) || $array instanceof \Poundation\PCollection) {
            foreach ($array as $key => $value) {
                $this->setValue($value, $key);
            }
        }
        return $this;
    }

    /**
     * Sets an attachment with the given identifier.
     * @param Epr_Com_Message_Attachment $attachment
     * @param $identifier
     * @return Epr_Com_Message
     */
    public function setAttachment(Epr_Com_Message_Attachment $attachment, $identifier)
    {
        if (is_string($identifier) && strlen($identifier) > 0) {
            $this->attachments[$identifier] = $attachment;
        }
        return $this;
    }

    /**
     * Returns an array with all attachments of the message.
     * @return array
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Returns the attachment with the given identifier.
     * @param $identifier
     * @return Epr_Com_Message_Attachment
     */
    public function getAttachmentWithIdentifier($identifier)
    {
        return ((isset($this->attachments[$identifier]) ? $this->attachments[$identifier] : ''));
    }

    /**
     * Signs the message with the given key.
     * @param string $sendersPrivateKey
     * @return Epr_Com_Message
     */
    public function sign($sendersPrivateKey)
    {
        $this->key = $sendersPrivateKey;
        return $this;
    }

    /**
     * Returns true if the authoriy was used to sign the message.
     * @param Epr_Com_Message_Authority $autority
     * @return bool
     */
    public function checkSignatureFromAuthority($autority)
    {
        if ($this->authority === false || $this->authority === null) {
            if ($autority instanceof Epr_Com_Message_Authority) {
                if ($this->key === $autority->remoteKey()) {
                    $this->authority = $autority;
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    /**
     * Returns the authority that was used to check the signature.
     * @return Epr_Com_Message_Authority
     */
    public function signatureAuthority()
    {
        return $this->authority;
    }

    public function __toString()
    {
        if ($this->format == self::FORMAT_XML) {
            return $this->__toXMLString();
        } else if ($this->format == self::FORMAT_JSON) {
            return $this->__toJSONString();
        } else {
            return 'Message::' . $this->getName();
        }
    }

    private function __toXMLString()
    {

        $document = new DOMDocument('1.0', 'UTF-8');

        $xmlRoot = $document->createElement('message');
        $xmlRoot = $document->appendChild($xmlRoot);

        $nameElement = $document->createElement('name', $this->getName());
        $xmlRoot->appendChild($nameElement);

        if (count($this->values) > 0) {

            $valuesElement = $document->createElement('values');

            foreach ($this->values as $key => $value) {

                $type = 'string';

                if (!is_string($value)) {
                    if ($value instanceof DateTime) {
                        $value = '2013-03-10T00:00:00+0100'; // $value->format(self::DATETIMEFORMAT);
                        $type = 'datetime';
                    } else {
                        $value = (string)$value;
                    }
                }
                $valueElement = $document->createElement('value', $this->escape($value));

                $keyAttribute = $document->createAttribute('key');
                $keyAttribute->value = $key;
                $valueElement->appendChild($keyAttribute);

                if ($type != 'string') {
                    $typeAttribute = $document->createAttribute('type');
                    $typeAttribute->value = $type;
                    $valueElement->appendChild($typeAttribute);
                }

                $valuesElement->appendChild($valueElement);
            }

            $xmlRoot->appendChild($valuesElement);
        }

        if (count($this->attachments) > 0) {

            $attachmentsElement = $document->createElement('attachments');
            $xmlRoot->appendChild($attachmentsElement);

            foreach ($this->attachments as $identifier => $attachment) {
                if ($attachment instanceof Epr_Com_Message_Attachment) {
                    $attachmentElement = $document->createElement('attachment');
                    $attachmentElement = $attachment->xml($attachmentElement);

                    $idAttribute = $document->createAttribute('id');
                    $idAttribute->value = $identifier;
                    $attachmentElement->appendChild($idAttribute);

                    $attachmentsElement->appendChild($attachmentElement);
                }
            }

        }

        if ($this->key !== false) {
            $keyElement = $document->createElement('key', $this->key);
            $xmlRoot->appendChild($keyElement);
        }

        return $document->saveXML();
    }

    private function __toJSONString()
    {
        $data = array(
            'message'   => array(
                'name'      => $this->getName(),
                'key'       => $this->key,
                'values'    => $this->values
            )
        );

        $value = json_encode($data);
        return $value;
    }

    private function escape($source)
    {
        if (is_string($source)) {
            $source = str_replace('&', '&amp;', $source);
        }
        return $source;
    }

}

?>