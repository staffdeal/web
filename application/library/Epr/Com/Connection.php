<?php

use Poundation\PURL;

abstract class Epr_Com_Connection {

	const STATE_DISCONNECTED = 0;
	const STATE_CONNECTING = 1;
	const STATE_CONNECTED = 2;
	const STATE_FAILED = 3;
	
	protected $state = Epr_Com_Connection::STATE_DISCONNECTED;
	
	protected $peer;
	
	protected $userAgent;
	
	/**
	 * @var PURL
	 */
	protected $targetURL;
	
	protected $message;

    /**
     * Creates a key which can be used as a connection key.
     * @return string
     */
    static public function connectionKey() {

        mt_srand((double)microtime() * 10000);
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $uuid = substr($charid, 0, 8) . '-' . substr($charid, 8, 8) . '-' . substr($charid,16, 8) . '-' . substr($charid,24, 8);
        return $uuid;

    }
	
	/* (non-PHPdoc)
	 * @see Epr_Com_Connection_Outbound::createOutboundConnection
	 */
	static public function createOutboundConnection($targetURL, $message = false) {
		return Epr_Com_Connection_Outbound::createOutboundConnection($targetURL, $message);
	}
	
	/**
	 * Returns the incoming connection if the current request was an oubound connection.
	 * @return Epr_Com_Connection_Inbound|boolean
	 */
	static public function inboundConnection() {
		try {
			$connection = Epr_Com_Connection_Inbound::connection();
			return $connection;
		} catch (Exception $e) {
			return false;	
		}
	}
	
	/**
	 * Returns the targetURL of the connection.
	 * @return PURL
	 */
	public function getTargetURL() {
		return $this->targetURL;
	}
	
	/**
	 * Returns the user agent of the connection.
	 */
	public function getUserAgent() {
		return $this->userAgent;
	}
	
	/**
	 * Sets the message of the connection.
	 * @param Epr_Com_Message $message
	 * @return Epr_Com_Connection
	 */
	protected function setMessage(Epr_Com_Message $message) {
		$this->message = $message;
		return $this;
	}
	
	/**
	 * Returns the message of the connection.
	 * @return Epr_Com_Message
	 */
	public function getMessage() {
		return $this->message;
	}
	
}

?>