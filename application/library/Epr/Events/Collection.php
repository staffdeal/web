<?php

class Epr_Events_Collection extends Epr_Collection implements Epr_Paginatable, Epr_Searchable {

    const SORT_DESIGN_DOCUMENT_NAME = 'sortEvents';

    const SEARCH_DESIGN_DOCUMENT_NAME = 'searchEvents';
    const SEARCH_ALL = 'all';

    /**
     * @return \Doctrine\ODM\CouchDB\DocumentRepository
     */
    static public function getRepository() {
        return _dm()->getRepository(Epr_Event::DOCUMENTNAME);
    }

    static function getSortKeys() {
        return array('title', 'date');
    }

    static function getSearchNames()
    {
        $names = parent::getSearchNames();
        $names[] = self::SEARCH_ALL;
        return $names;
    }

    public static function getSearchFieldsForName($name)
    {
        $fields = parent::getSearchFieldsForName($name);
        if ($name == self::SEARCH_ALL) {
            $fields[] = 'title';
            $fields[] = 'text';
        }
        return $fields;
    }

    static function getSearchFiltersForName($name) {
        $field = parent::getSearchFiltersForName($name);
        if ($name == self::SEARCH_ALL) {
            $field[] = 'state';
        }
        return $field;
    }

    /**
     * @param $id
     * @return Epr_Event
     */
    static function eventWithId($id) {
        $event = null;
        if (is_string($id) && strlen($id) > 0) {
            $event = _dm()->find(Epr_Event::DOCUMENTNAME, $id);
        }

        return $event;
    }

    static function eventWithSlug($slug) {
        $event = null;
        if (strlen($slug) > 0) {
            $repo = _dm()->getRepository(Epr_Event::DOCUMENTNAME);
            $result = $repo->findBy(array('slug' => $slug));
            if (count($result) > 0) {
                $event = $result[0];
            }
        }
        return $event;
    }

    static function getPublishedEventsByDate($sorting = SORT_ASC, $skipPast = true) {
        $query = _dm()->createQuery('events', 'publishedByDate');
        $query->setDescending(($sorting == SORT_DESC));

        if ($skipPast) {
            $startDate = \Poundation\PDate::now();
            if ($sorting == SORT_ASC) {
                $query->setStartKey($startDate->getInISO8601Format());
            } else {
                $query->setEndKey($startDate->getInISO8601Format());
            }
        }

        $result = $query->onlyDocs(true)->execute();

        return ($result->count() > 0) ? $result : null;
    }

    static function  getAPIEvents() {
        return self::getPublishedEventsByDate(SORT_DESC);
    }

    static function getEventsMatching($query, $sortKey = null, $sortDirection = SORT_ASC, $count = 10, $offset = 0) {
        return self::getMatching(self::SEARCH_ALL, $query, $sortKey, $sortDirection, $count, $offset);
    }

}