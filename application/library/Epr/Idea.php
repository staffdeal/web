<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 14.09.12
 * Time: 12:26
 */
/** @Document (repositoryClass="Epr_Idea_Collection") */
class Epr_Idea extends Epr_Document
{

	const DOCUMENTNAME = 'Epr_Idea';

	/** @Id */
	public $id;

	/** @Field(type="string") */
	public $text;

	/** @Field(type="datetime") */
	public $submissionDate;

	/** @Field(type="string") */
	public $url;

	/** @Field(type="string") */
	public $title;

	/**
	 * @var Epr_Idea_Competition
	 * @ReferenceOne(targetDocument="Epr_Idea_Competition")
	 */
	public $competition;

	/** ReferenceOne(targetDocument="Epr_User") */

    /** @Field(type="string") */
	public $user;
    private $_userObject;

	/** @ReferenceMany(targetDocument="Epr_Content_Image") */
	public $images = array();

	public function __construct()
	{

		parent::__construct();
		$this->submissionDate = new DateTime();
		$this->activate();
	}

	/**
	 * Sets the date of submission.
	 *
	 * @param $publishDate
	 *
	 * @return Epr_Idea
	 */
	public function setSubmissionDate(DateTime $submissionDate)
	{
		$this->submissionDate = $submissionDate;

		return $this;
	}

	/**
	 * Returns the submission date.
	 *
	 * @return DateTime
	 */
	public function getSubmissioDate()
	{
		return $this->submissionDate;
	}

	/**
	 * Sets the text.
	 *
	 * @param $text
	 *
	 * @return Epr_Idea
	 */
	public function setText($text)
	{
		$this->text = $text;

		return $this;
	}

	/**
	 * Returns the text.
	 *
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}

	/**
	 * Sets the Title of the Idea
	 *
	 * @param $title
	 *
	 * @return Epr_Idea
	 */
	public function setTitle($title)
	{
		$this->title = $title;

		return $this;
	}

	/**
	 * Returns the title of the idea
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}


	/**
	 * Sets the URL.
	 *
	 * @param $url
	 *
	 * @return Epr_Idea
	 */
	public function setUrl($url)
	{
		if ($url instanceof \Poundation\PURL || is_null($url)) {
			$this->url = $url;
		}

		return $this;
	}

	/**
	 * Returns the URL.
	 *
	 * @return null|Poundation\PURL
	 */
	public function getUrl()
	{
		if (!$this->url instanceof \Poundation\PURL && is_string($this->url)) {
			$this->url = \Poundation\PURL::URLWithString($this->url);
		}

		return $this->url;
	}

	/**
	 * Sets the competition.
	 *
	 * @param $competition
	 *
	 * @return Epr_Idea
	 */
	public function setCompetition($competition)
	{
		$this->competition = $competition;

		return $this;
	}

	/**
	 * Returns the competition.
	 *
	 * @return Epr_Idea_Competition
	 */
	public function getCompetition()
	{
		return $this->competition;
	}

	/**
	 * Sets the user.
	 *
	 * @param $user
	 *
	 * @return Epr_Idea
	 */
	public function setUser($user)
	{
        if ($user instanceof Epr_User) {
            $this->user = $user->getId();
            $this->_userObject = $user;
        } else {
            $this->user = null;
            $this->_userObject = null;
        }

		return $this;
	}

	/**
	 * Returns the user.
	 *
	 * @return Epr_User
	 */
	public function getUser()
	{
        if (!$this->_userObject instanceof Epr_User) {
            if (is_string(($this->user)) && strlen($this->user) > 5) {
                $this->_userObject = _dm()->find(Epr_User::DOCUMENTNAME, $this->user);
            }
        }
		return $this->_userObject;
	}

	/**
	 * Adds an image to the idea.
	 *
	 * @param Epr_Content_Image $image
	 *
	 * @return Epr_Idea
	 */
	public function addImage(Epr_Content_Image $image)
	{
		$this->images[] = $image;

		return $this;
	}

	/**
	 * Returns an array of images.
	 *
	 * @return Poundation\PArray
	 */
	public function getImages()
	{
		$images = new \Poundation\PArray();

		foreach ($this->images as $image) {
			if ($image instanceof Epr_Content_Image) {
				$images[] = $image;
			}
		}

		return $images;
	}
}