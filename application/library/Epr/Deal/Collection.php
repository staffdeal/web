<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 08.09.12
 * Time: 12:56
 */
use Doctrine\ODM\CouchDB\DocumentRepository;
use Doctrine\CouchDB\View\FolderDesignDocument;

class Epr_Deal_Collection extends Epr_Collection implements Epr_Paginatable
{
    const SORT_DESIGN_DOCUMENT_NAME = 'sortDeals';

    const SEARCH_DESIGN_DOCUMENT_NAME = 'searchDeals';
    const SEARCH_ALL = 'all';

    static function getSortKeys()
    {
        return array(
            'title',
            'startDate',
            'featured'
        );
    }

    static function getSearchNames()
    {
        return array_merge(parent::getSearchNames(), array(
            self::SEARCH_ALL
        ));
    }

    public static function getSearchFieldsForName($name)
    {
        $fields = parent::getSearchFieldsForName($name);
        if ($name == self::SEARCH_ALL) {
            $fields = array_merge($fields, array(
                    'title',
                    'text',
                    'teaser',
                    'codeData'
                ));
        }
        return $fields;
    }

    static function getSearchFiltersForName($name)
    {
        $fields = parent::getSearchFiltersForName($name);
        if ($name == self::SEARCH_ALL) {
            $fields = array_merge($fields, array(
                'featured',
                'title',
                'isActive' => 'active',
                'manuallySoldOut' => 'soldout'
            ));
        }
        return $fields;
    }


    /**
     * @return \Doctrine\ODM\CouchDB\DocumentRepository
     */
    static function getRepository()
    {
        return _dm()->getRepository(Epr_Deal::DOCUMENTNAME);
    }

    static function getAll()
    {
        $coll = self::getRepository();

        return $coll->findAll();
    }

    static function getActive()
    {

        $startDate = new \Poundation\PDate();
        $startKey  = $startDate->getInDoctrineFormat();

        $query = _dm()->createQuery('deals', 'activeByEndDate');
        $query->setStartkey($startKey);

        $result = $query->onlyDocs(true)->execute();

        return $result;

    }

    static public function getCacheIdForProvidedDeals()
    {
        return 'providedDeals';
    }

    static public function invalidateCacheForProvidedDeals()
    {
        $cache  = Epr_Cache_Provider::getInstance()->getCache();
        $module = _app()->getModuleWithIdentifier(Epr_Modules_Dispatcher::identifier());
        if ($module instanceof Epr_Modules_Dispatcher) {
            $action = 'sync/' . $module->referenceId();
            $module->call($action);
        }
        $cache->remove(self::getCacheIdForProvidedDeals());
    }

    static public function getProvidedDeals($limit = -1)
    {
        /** @var $cache Zend_Cache_Core */
        $cache = Epr_Cache_Provider::getInstance()->getCache();
        $cache->setLifetime(60);

        $module = _app()->getModuleWithIdentifier(Epr_Modules_Dispatcher::identifier());
        if ($module instanceof Epr_Modules_Dispatcher) {
            $action  = 'deals/' . $module->referenceId();
            $cacheId = self::getCacheIdForProvidedDeals();

            $deals = $cache->load($cacheId);

            if (!$deals instanceof \Poundation\PArray || _app()->isDevelopment()) {

                $list = null;

                try {
                    $list = $module->call($action);
                } catch (Exception $e) {
                    _logger()->critical($e->getMessage());
                    return null;
                }

                if ($list instanceof stdClass) {

                    if (isset($list->status) && $list->status == 'OK') {

                        if (isset($list->deals) && is_array($list->deals)) {
                            $deals = new \Poundation\PArray();

                            foreach ($list->deals as $item) {
                                $deal = Epr_Dispatcher_Deal::createFromDispatcher($item);

                                if ($deal) {

                                    $now = \Poundation\PDate::now();

                                    if ($deal->getStartDate()->isBefore($now, true) && $deal->getEndDate()->addDays(1)->isAfter($now, true)) {
                                        $deals[] = $deal;
                                    }
                                }
                            }

                            $deals->sortByPropertyName('endDate', SORT_ASC);

                            if (!_app()->isDevelopment()) {
                                $cache->save($deals, $cacheId);
                            }

                            $cache->save($deals, $cacheId);
                        }
                    }
                }
            }

            if ($deals instanceof \Poundation\PArray) {

                if ($limit > -1) {
                    $deals->shrinkTo($limit - 1);
                }

                $deals = $deals->nativeArray();
            }


            return $deals;

        } else {
            _logger()->critical('Could not load dispatcher module');
        }

        return null;
    }

    /**
     * Returns the provided deal with the given id.
     *
     * @param $dealId
     * @return Epr_Deal|null
     */
    public static function getProvidedDealWithId($dealId) {

        $providedDeals = self::getProvidedDeals();
        if ($providedDeals) {
            foreach ($providedDeals as $deal) {
                if ($deal instanceof Epr_Dispatcher_Deal) {
                    if ($dealId == $deal->getReferenceDealId()) {
                        return $deal;
                    }
                }
            }
        }

        return null;

    }

    /**
     * Returns all deals that are currently provided and saved by the user.
     * @param $user
     * @return array|null
     */
    public static function getProvidedDealsSavedByUser($user) {
        $deals = null;

        if ($user instanceof Epr_User) {
            $allDeals = self::getProvidedDeals();
            if (is_array($allDeals)) {
                foreach ($allDeals as $candidate) {
                    if ($user->isDealSaved($candidate)) {
                        if (is_null($deals)) {
                            $deals = array();
                        }
                        $deals[] = $candidate;
                    }
                }
            }
        }

        return $deals;
    }

    public static function getCacheIdForDealersForDeal($dealId)
    {
        return 'dealersForDeal' . $dealId;
    }

    public static function invalidateCacheForDealersForDeal($dealId)
    {
        $cache = Epr_Cache_Provider::getInstance()->getCache();
        $cache->remove(self::getCacheIdForDealersForDeal($dealId));
    }

    public static function getDealersForDeal($dealId)
    {

        $list = null;

        $cache = Epr_Cache_Provider::getInstance()->getCache();

        $module = _app()->getModuleWithIdentifier(Epr_Modules_Dispatcher::identifier());
        if ($module instanceof Epr_Modules_Dispatcher) {

            $action  = 'dealers/forDeal/' . $dealId;
            $cacheId = self::getCacheIdForDealersForDeal($dealId);

            $list = $cache->load($cacheId);

            if (!$list) {
                try {
                    $response = $module->call($action);
                    if ($response && isset($response->status) && $response->status == 'OK') {

                        if (isset($response->dealers) && is_array($response->dealers)) {
                            $list = $response->dealers;

                            $cache->save($list, $cacheId);
                        }
                    }
                } catch (Exception $e) {
                }
            }
        }

        return $list;
    }

    static function getDealsMatching($query, $sortKey = null, $sortDirection = SORT_ASC, $count = 10, $offset = 0) {
        return self::getMatching(self::SEARCH_ALL, $query, $sortKey, $sortDirection, $count, $offset);
    }
}