<?php
/**
 * meinERP - Epr_Dispatcher_Deal
 *
 * This object wraps Deals given by a Dispatcher
 *
 * User: Jan Fanslau
 * Date: 16.04.13
 * Time: 12:17
 */

/** @Document */
class Epr_Dispatcher_Deal extends Epr_Document
{

	const DOCUMENTNAME = 'Epr_Dispatcher_Deal';
	/**
	 * @Id
	 * @var $id
	 */
	protected $id;

	/**
	 * @Field(type="string")
	 * @Index
	 * @var $referenceDealId
	 */
	public $referenceDealId;

	/**
	 * @Field(type="string")
	 * @var $title string
	 */
	public $title;

	/**
	 * @Field(type="string")
	 * @var $teaser string
	 */
	public $teaser;

	/**
	 * @Field(type="string")
	 * @var $text string
	 */
	public $text;

	/**
	 * @Field(type="datetime")
	 * @Index
	 * @var $startDate \Poundation\PDate
	 */
	public $startDate;

	/**
	 * @Field(type="datetime")
	 * @var $endDate \Poundation\PDate
	 */
	public $endDate;

	/**
	 * @Field(type="boolean")
	 * @var $featured bool
	 */
	public $featured;

	/**
	 * @Field(type="string")
	 * @var $provider string
	 */
	public $provider;

	/**
	 * @Field(type="string")
	 * @var $titleImage string
	 */
	public $titleImage;

	/**
	 * @Field(type="mixed")
	 * @var $pass StdClass
	 */
	public $pass;

	/**
	 * @ReferenceOne(targetDocument="Epr_Content_Image")
	 */
	public $codeImage;

    /** @Field(type="string") */
    private $price;

    /** @Field(type="string") */
    private $oldPrice;

    /** @Field(type="string")  */
    private $reminderText;


	/**
	 * @param Poundation\PDate
	 *
	 * @return $this
	 */
	public function setEndDate($endDate)
	{
		$this->endDate = $endDate;

		return $this;
	}

	/**
	 * @return Poundation\PDate
	 */
	public function getEndDate()
	{
		return $this->endDate;
	}

	/**
	 * @param boolean $featured
	 *
	 * @return $this
	 */
	public function setFeatured($featured)
	{
		$this->featured = $featured;

		return $this;
	}

	/**
	 * @return boolean
	 */
	public function getFeatured()
	{
		return $this->featured;
	}

	/**
	 * @param  $referenceDealId
	 *
	 * @return $this
	 */
	public function setReferenceDealId($referenceDealId)
	{
		$this->referenceDealId = $referenceDealId;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getReferenceDealId()
	{
		return $this->referenceDealId;
	}


	/**
	 * @param Datetime $startDate
	 *
	 * @return $this
	 */
	public function setStartDate($startDate)
	{
		$this->startDate = $startDate;

		return $this;
	}

	/**
	 * @return \Poundation\PDate
	 */
	public function getStartDate()
	{
		return $this->startDate;
	}

	/**
	 * @param string $teaser
	 *
	 * @return $this
	 */
	public function setTeaser($teaser)
	{
		$this->teaser = $teaser;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getTeaser()
	{
		return $this->teaser;
	}

	/**
	 * @param \StdClass $pass
	 *
	 * @return $this
	 */
	public function setPass($pass)
	{
		$this->pass = $pass;

		return $this;
	}

	/**
	 * @return \StdClass
	 */
	public function getPass()
	{
		return $this->pass;
	}

	/**
	 * @param string $provider
	 *
	 * @return $this
	 */
	public function setProvider($provider)
	{
		$this->provider = $provider;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getProvider()
	{
		return $this->provider;
	}

	/**
	 * @param string $title
	 *
	 * @return $this
	 */
	public function setTitle($title)
	{
		$this->title = $title;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string $titleImage
	 *
	 * @return $this
	 */
	public function setTitleImageURL($titleImage)
	{
		$this->titleImage = $titleImage;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitleImageURL()
	{
		return $this->titleImage;
	}

    public function getPrice() {
        return $this->price;
    }

    /**
     * @param $price
     * @return $this
     */
    public function setPrice($price) {
        $this->price = $price;
        return $this;
    }

    public function getOldPrice() {
        return $this->oldPrice;
    }

    /**
     * @param $price
     * @return $this
     */
    public function setOldPrice($price) {
        $this->oldPrice = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getReminderText() {
        return $this->reminderText;
    }

    /**
     * @param $text
     * @return $this
     */
    public function setReminderText($text) {
        $this->reminderText = (is_null($text)) ? null : (string)$text;
        return $this;
    }

	public function getTitleImage() {

		$image = null;

		$url = \Poundation\PURL::URLWithString($this->getTitleImageURL());

		if ($url) {
			$imageReference = 'deal_' . md5($this->getTitleImageURL());

			$images = Epr_Content_Image_Collection::imagesWithReference($imageReference);
			if (count($images) > 0) {
				$image = $images[0];
			}
			if (!$image instanceof Epr_Content_Image) {

				$rawImage = \Poundation\PImage::createImageFromURL($url);
				if ($rawImage) {
					$image = Epr_Content_Image::imageFromPImage($rawImage);
					if ($image) {
						$image->setReference($imageReference);
						$image->setAccess(Epr_Content_Image::ACCESS_TMP);
						_dm()->persist($image);
					}
				}
			}
		}

		return $image;

	}

	/**
	 * @param string $text
	 *
	 * @return $this
	 */
	public function setText($text)
	{
		$this->text = $text;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}


	/**
	 * @param \Poundation\PImage $image
	 *
	 * @return $this
	 */
	public function setCodeImage(\Poundation\PImage $image)
	{
		if ($image) {
			$imageToSave = Epr_Content_Image::imageFromPImage($image, "code");
			_dm()->persist($imageToSave);
			_dm()->flush();
			$this->codeImage = $imageToSave;
		}

		return $this;
	}

	/**
	 * @return Epr_Content_Image
	 */
	public function getCodeImage()
	{
		return $this->codeImage;

	}

	public function canBeSaved() {
		if ($this->pass) {
			if (isset($this->pass->onsite) || isset($this->pass->online)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @param $obj
	 *
	 * @return Epr_Dispatcher_Deal|null
	 */
	static function createFromDispatcher($obj)
	{

		$dispatcherDeal = null;
		if ($obj instanceof stdClass) {

			$dispatcherDeal = new self();
			$dispatcherDeal->setReferenceDealId($obj->id);
			$dispatcherDeal->setStartDate(new Poundation\PDate($obj->startDate));
			$dispatcherDeal->setEndDate(new Poundation\PDate($obj->endDate));
			$dispatcherDeal->setTeaser($obj->teaser);
			$dispatcherDeal->setText($obj->text);
			$dispatcherDeal->setTitle($obj->title);
			$dispatcherDeal->setProvider($obj->provider);
			$dispatcherDeal->setTitleImageURL($obj->titleImage);
			$dispatcherDeal->setPass($obj->pass);
			$dispatcherDeal->setFeatured($obj->featured);
            $dispatcherDeal->setPrice($obj->price);
            $dispatcherDeal->setOldPrice($obj->oldPrice);
            $dispatcherDeal->setReminderText(isset($obj->reminder) ? $obj->reminder : null);
		}

		return $dispatcherDeal;

	}

	/**
	 * @param bool $id
	 *
	 * @return Epr_Dispatcher_Deal | null
	 */
	static function getDealById($id = false)
	{
		if ($id === false) {
			return null;
		}

		$deals = Epr_Deal_Collection::getProvidedDeals();
		foreach ($deals as $deal) {
			/** @var Epr_Dispatcher_Deal $deal */
			if ($deal->getReferenceDealId() == $id) {
				return $deal;
			}
		}

		return null;
	}

    public function getRedeemCode($type) {
        $code = null;
        $passes = $this->getPass();
        if ($type == Epr_Deal::TYPE_ONSITE) {
            if (isset($passes->onsite)) {
                $url = \Poundation\PURL::URLWithString($passes->onsite);
                if ($url) {
                    $content = $url->getContent(array('X-API-Version' => 2));
                    $xml = null;
                    try {
                        $xml = new SimpleXMLElement($content);
                    } catch (Exception $e) {}
                    if ($xml && isset($xml->redeem) && isset($xml->redeem->ean)) {
                        $candidate = (string)$xml->redeem->ean;
                        if (strlen($candidate) >= 4) {
                            $code = $candidate;
                        }
                    }
                }
            }
        } else if ($type == Epr_Deal::TYPE_ONLINE) {
            if (isset($passes->online) && isset($passes->online->available)) {
                if (isset($passes->online->redeemURL)) {
                    $url = \Poundation\PURL::URLWithString($passes->online->redeemURL);
                    if ($url) {
                        $content = $url->getContent(array('X-API-Version' => 2));
                        $xml = null;
                        try {
                            $xml = new SimpleXMLElement($content);
                        } catch (Exception $e) {}
                        if ($xml && isset($xml->redeem) && isset($xml->redeem->code)) {
                            $candidate = (string)$xml->redeem->code;
                            if (strlen($candidate) >= 4) {
                                $code = $candidate;
                            }
                        }
                    }
                } else {
                    $code = Epr_Deal::ONLINE_NOCODE;
                }
            }
        }

        return $code;
    }

    public function getOnsitePass() {
        $passes = $this->getPass();
        return (isset($passes->onsite)) ? $passes->onsite : null;
    }

    public function getOnlinePass() {
        $passes = $this->getPass();
        return (isset($passes->online)) ? $passes->online : null;
    }

    /**
     * Returns true if the deal is provided onsite. This does not mean that it is still available.
     * @return bool
     */
    public function isOnsite() {
        $pass = $this->getOnsitePass();
        return ($pass != null);
    }

    /**
     * Returns true if the deal is provided online. This does not mean that it is still available.
     * @return bool
     */
    public function isOnline() {
        $pass = $this->getOnlinePass();
        return (isset($pass->available));
    }

    public function isSoldOutOnsite() {
       $result = !$this->isOnsite();
        return $result;
    }

    public function isSoldOutOnline() {
        $result = true;
        if ($this->isOnline()) {
            $pass = $this->getOnlinePass();
            if ($pass) {
                $result = !($pass->available);
            }
        }
        return $result;
    }

    public function isSoldOut() {
        $numberOfChannels = $this->isOnsite() + $this->isOnline();
        $numberOfAvailableChannels = !$this->isSoldOutOnsite() + !$this->isSoldOutOnline();
        $isSoldOut = ($numberOfAvailableChannels - $numberOfChannels) < 0;

        return $isSoldOut;
    }

}