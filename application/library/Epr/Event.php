<?php

/** @Document */
class Epr_Event extends Epr_Document_WebPage implements Epr_Document_Publishable {
    use Epr_Document_PublishableTrait;

    const DOCUMENTNAME = 'Epr_Event';

    /** @Id */
    protected $id;

    /** @Field(type="datetime") */
    protected $date;

    /** @Field(type="string")  */
    protected $description;

    /** @Field(type="boolean") */
    protected $canBeBooked = true;

    /** @EmbedMany */
    protected $bookings = array();


    /**
     * Sets the date of the event.
     * @param \Poundation\PDate $date
     * @return $this
     */
    public function setDate($date) {
        if (is_null($date) || $date instanceof \Poundation\PDate) {
            $this->date = $date;
        }

        return $this;
    }

    /**
     * @return \Poundation\PDate
     */
    public function getDate() {
        $result = null;
        if ($this->date) {
            $result = \Poundation\PDate::createDate($this->date);
        }
        return $result;

    }

    /**
     * @param $value
     * @return $this
     */
    public function setCanBeBooked($value) {
        $this->canBeBooked = ($value == true);
        return $this;
    }

    public function getCanBeBooked() {
        return $this->canBeBooked;
    }

    public function getBookings() {
        if (is_null($this->bookings)) {
            $this->bookings = array();
        }
        return $this->bookings;
    }

    /**
     * @param Epr_User $user
     * @return Epr_Event_Booking|null
     */
    public function getBookingForUser(Epr_User $user) {
        $booking = null;

        foreach($this->getBookings() as $candidate) {
            if ($candidate instanceof Epr_Booking) {
                if ($candidate->isForUser($user)) {
                    $booking = $candidate;
                    break;
                }
            }
        }

        return $booking;
    }

    /**
     * @param Epr_User $user
     * @return Epr_Event_Booking
     */
    public function bookUser(Epr_User $user) {
        $booking = $this->getBookingForUser($user);
        if (is_null($booking)) {
            $booking = new Epr_Event_Booking($user);
            $this->bookings[] = $booking;

            $message = 'A new user booked for an event:\n\n';
            $message.= 'Event: ' . $this->getTitle() . ' (' . $this->getDate()->getFormatedString('Y-m-d') . ')\nUser: ' . $user->getEmail() . '\n';
            $message.= 'Link: ' . _app()->getPublicURL()->addPathComponent('admin/events/bookings')->addPathComponent($this->getId());
            _app()->sendAdminMail(_t('New event booking'), $message);
        }
        return $booking;
    }

    public function allowAutomaticPublishing()
    {
        return false;
    }


}