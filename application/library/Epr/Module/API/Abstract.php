<?php

class Epr_Module_API_Abstract extends Epr_Module_Cron_Abstract {

	/** @Field (type="string") */
	protected $displayname;

	/** @Field(type="string") */
	protected $authlevel;

	/** @Field(type="mixed") */
	protected $templates = array();

	/**
	 * Returns the display name of the module.
	 *
	 * @return \Poundation\PString
	 */
	public function getDisplayname() {
		$name = __($this->displayname);
		if ($name->length() == 0) {
			$class = \Poundation\PClass::classFromString(get_called_class());
			$name  = $class->invokeMethod('title');
		}

		return $name;
	}

	public function setDisplayname($displayname) {
		$this->displayname = (string)$displayname;
	}

	public function getAPIPath() {
		return false;
	}

	public function getAPIAuthLevel() {
		$role = $this->authlevel;
		if (Epr_Roles::isValidRole($role)) {
			return $role;
		} else {
			return Epr_Roles::ROLE_NONE;
		}
	}

	public function setAPIAuthLevel($role) {
		if (Epr_Roles::isValidRole($role)) {
			$this->authlevel = $role;
		}
	}

	public function getSettingsFormFields() {

		$fields = parent::getSettingsFormFields();
		$displayName = new Zend_Form_Element_Text('displayname');
		$displayName->setLabel('Display Name');
		$displayName->setRequired(false);
		$displayName->setAttrib('class', 'inp-form');
		$displayName->setDecorators(array('Composite'));
		$displayName->setValue($this->getDisplayname());
		$displayName->setDescription('The text which is used to create menus on the web page and the mobile clients.');
		$fields[] = $displayName;

		$authLevel = new Zend_Form_Element_Select('authlevel');
		$authLevel->setLabel('Access Level');
		$authLevel->setMultiOptions(Epr_Roles::allRoles()->nativeArray());
		$authLevel->setAttrib('class', 'styledselect_form_1');
		$authLevel->setDecorators(array('Composite'));
		$authLevel->setValue($this->getAPIAuthLevel());
		$authLevel->setDescription('Specify the user level which is needed to access the module. When set to \'none\', the module is inactive.');
		$fields[] = $authLevel;

		foreach ($this->getTemplateData() as $data) {
			if (is_string($data['id']) && strlen($data['id']) > 0 && is_string($data['platform'])) {

				$id = $data['id'];
				$title = null;
				$description = null;

				if (is_string($id) && strlen($id) > 0) {

					if (isset($data['title']) && strlen($data['title']) > 0) {
						$title = $data['title'];
					}
					if (isset($data['description'])) {
						$description = $data['description'];
					}

					if (!is_null($id) && !is_null($title)) {
						$templateField = new Zend_Form_Element_Textarea(md5($id . $data['platform']));
						$templateField->setLabel($title);
						if (strlen($description) > 0) {
							$templateField->setDescription($description);
						}
						$templateField->setAttrib('class', 'inp-form wide');
						$templateField->setValue($this->getTemplateValueForIdentifier($id, $data['platform']));
						$fields[] = $templateField;
					}
				}

			}
		}

		return $fields;

	}

	public function saveSettingsField($field) {

		$fieldName = $field->getName();
		$value     = $field->getValue();

		$templates = array();
		foreach ($this->getTemplateData() as $data) {
			if (is_string($data['id']) && strlen($data['id']) > 0 && is_string($data['platform'])) {
				$templates[md5($data['id'] . $data['platform'])] = $data;
			}
		}
		unset($id);
		unset($data);

		if ($fieldName == 'displayname') {
			$this->setDisplayname($value);
			return true;
		} else if ($fieldName == 'authlevel') {
			$this->setAPIAuthLevel($value);
			return true;
		} else {

			if (array_key_exists($fieldName, $templates)) {
				$template = $templates[$fieldName];
				$this->setTemplateValueForIdentifier($value, $template['id'], $template['platform']);
				return true;
			}

			return parent::saveSettingsField($field);
		}
	}

	public function getTemplateData() {
		return array();
	}

	public function getTemplateValueForIdentifier($id, $platform) {
		$result = null;

		if (strlen($id) > 0) {
			$id = $id . '_' . $platform;
			if (isset($this->templates[$id])) {
				if (isset($this->templates[$id]['t'])) {
					$result = $this->templates[$id]['t'];
				}
			}
		}

		return $result;
	}

	public function getTemplateHashForIdentifier($id, $platform) {
		$result = null;

		if (strlen($id) > 0) {
			$id = $id . '_' . $platform;
			if (isset($this->templates[$id])) {
				if (isset($this->templates[$id]['h'])) {
					$result = $this->templates[$id]['h'];
				}
			}
		}

		return $result;
	}

	public function setTemplateValueForIdentifier($value, $id, $platform) {
		if (is_string($id) && strlen($id) > 0 && strlen($platform) > 0) {
			$id = $id . '_' . $platform;
			$hash = null;
			if (strlen($value) > 0) {
				$hash = md5($value);
			}
			$this->templates[$id] = array('t' => $value, 'h' => $hash);
		}

		return $this;
	}

	public function getTemplates() {
		$templates = array();

		foreach($this->getTemplateData() as $data) {
			$id = $data['id'];
			$platform = $data['platform'];

			if (strlen($id) > 0 && strlen($platform) > 0) {
				$text = $this->getTemplateValueForIdentifier($id, $platform);
				$hash = $this->getTemplateHashForIdentifier($id, $platform);
				if ($text && $hash) {
					$data['value'] = $text;
					$data['hash'] = $hash;
					$templates[] = $data;
				}
			}
		}

		return $templates;
	}
}