<?php

interface Epr_Module_Notification extends Epr_Module {

	/**
	 * Return true if the plugin can be used.
	 * @return true
	 */
	public function isUsable();
	public function notify();

}