<?php

use Poundation\PClass;

class Epr_Module_Abstract {

	/** @Id(strategy="ASSIGNED") */
	protected $id;

	protected $isStartedUp = false;

	static public function instance() {
		$classname = get_called_class();

		$instance = false;
		if (Zend_Registry::isRegistered($classname)) {
			$instance = Zend_Registry::get($classname);
		} else {
			$instance = new $classname;
			Zend_Registry::set($classname, $instance);
		}

		if ($instance === false) {
			throw new Exception('Could not instanciate module ' . $classname);
		} else {
			return $instance;
		}
	}

	static function identifier() {
		return (string)__(get_called_class())->substringFromPositionOfString('_')->removeLeadingCharactersWhenMatching('_');
	}

	public function __construct() {
		$this->id = self::identifier();
	}

	/**
	 * Starts up the module. Subclasses should call super and proceed with the startup code only if super return true.
	 * @return bool
	 */
	public function startup() {
		if (!$this->isStartedUp) {
			_logger()->info('Starting up module ' . $this->identifier());
			$this->isStartedUp = true;
			return true;
		} else {
			return false;
		}
	}

	public function getId() {
		return $this->id;
	}

	public function isActive() {
		return true;
	}

	public function getSettingsFormFields() {
		return array();
	}

	/* (non-PHPdoc)
	 * @see Epr_Module::saveSettingsField()
	*/
	public function saveSettingsField($field) {
		// we return false here since the was a field that has not been handled by a child class
		return false;
	}

	public function getSettingsSaveButtonCaption() {
		return 'Save';
	}

}

?>