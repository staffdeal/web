<?php

interface Epr_Module_Cron {

	/**
	 * Runs the cron job of the module and returns the output.
	 * @param DateInterval $intervalSinceLastRun
	 *
	 * @return string
	 */
	public function runCron(DateInterval $intervalSinceLastRun);

}