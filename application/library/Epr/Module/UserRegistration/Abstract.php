<?php

class Epr_Module_UserRegistration_Abstract extends Epr_Module_Abstract
{

    const FIELD_PASSWORD_LENGTH   = 'passwordLength';
    const FIELD_PASSWORD_LETTERS  = 'passwordLetters';
    const FIELD_PASSWORD_CAPITALS = 'passwordCapitals';
    const FIELD_PASSWORD_NUMBERS  = 'passwordNumbers';
    const FIELD_PASSWORD_SYMBOLS  = 'passwordSymbols';

    /** @Field(type="string") */
    protected $registrationHint;

    /** @Field(type="string") */
    protected $privacyPolicy;

    /** @Field(type="string") */
    protected $termsOfUse;

    /** @Field(type="boolean") */
    protected $emailVerification;

    /** @Field(type="string") */
    protected $emailVerificationText;

    /** @Field(type="string") */
    protected $registrationEmailSubject;

    /**
     * @Field(type="integer")
     * @var int
     */
    protected $passwordMinimumLength = 8;

    /**
     * @Field(type="boolean")
     * @var bool
     */
    protected $passwordContainsNumber = true;

    /**
     * @Field(type="boolean")
     * @var bool
     */
    protected $passwordContainsLetter = true;

    /**
     * @Field(type="boolean")
     * @var bool
     */
    protected $passwordContainsCapitals = true;

    /**
     * @Field(type="boolean")
     * @var bool
     */
    protected $passwordContainsSymbols = true;

    /** @Field(type="string") */
    protected $activationEmailText;

    /**
     * Returns the registration help text.
     *
     * @return string
     */
    public function getRegistrationHint()
    {
        return $this->registrationHint;
    }

    /**
     * Sets the registration help text.
     *
     * @param $text
     *
     * @return Epr_Module_UserRegistration_Abstract
     */
    public function setRegistrationHint($text)
    {
        $this->registrationHint = $text;

        return $this;
    }

    /**
     * Returns the privacy policy text-
     *
     * @return string
     */
    public function getPrivacyPolicy()
    {
        return $this->privacyPolicy;
    }

    /**
     * Sets the privacy policy text.
     *
     * @param $text
     *
     * @return Epr_Module_UserRegistration_Abstract
     */
    public function setPrivacyPolicy($text)
    {
        $this->privacyPolicy = $text;

        return $this;
    }

    /**
     * Returns the term of use text.
     *
     * @return string
     */
    public function getTermsOfUse()
    {
        return $this->termsOfUse;
    }

    /**
     * Sets the term of use text.
     *
     * @param $text
     *
     * @return Epr_Module_UserRegistration_Abstract
     */
    public function setTermsOfUse($text)
    {
        $this->termsOfUse = $text;

        return $this;
    }

    public function getVerifyEmail()
    {
        return $this->emailVerification;
    }

    public function setVerifyEmail($value)
    {
        $this->emailVerification = ($value == true);

        return $this;
    }

    public function getRegistrationEmailSubject()
    {
        return $this->registrationEmailSubject;
    }

    private function setRegistrationEmailSubject($text)
    {
        $this->registrationEmailSubject = $text;

        return $this;
    }

    public function getEmailVerificationTemplate()
    {
        return $this->emailVerificationText;
    }

    public function setEmailVerificationTemplate($text)
    {
        $this->emailVerificationText = $text;

        return $this;
    }

    public function getPasswordMinimumLength()
    {
        return $this->passwordMinimumLength;
    }

    public function getPasswordContainsNumber()
    {
        return $this->passwordContainsNumber;
    }

    public function getPasswordContainsLetter()
    {
        return $this->passwordContainsLetter;
    }

    public function getPasswordContainsCapital()
    {
        return $this->passwordContainsCapitals;
    }

    public function getPasswordContainsSymbol()
    {
        return $this->passwordContainsSymbols;
    }

    public function getActiviationEmailText()
    {
        return $this->activationEmailText;
    }

    public function setActivationEmailText($string)
    {
        $this->activationEmailText = (string)$string;

        return $this;
    }

    public function getSettingsFormFields()
    {
        $fields = array();

        $passwordLength = new Zend_Form_Element_Select(self::FIELD_PASSWORD_LENGTH);
        $passwordLength->setLabel('Minimum Password Length');
        $passwordLength->setMultiOptions(\Poundation\PArray::createProgressivArray(4, 20)->dictionary()->nativeArray());
        $passwordLength->setValue($this->getPasswordMinimumLength());
        $passwordLength->setRequired(true);
        $passwordLength->setDescription('The minimum password length.');
        $passwordLength->setAttrib('color', 'blue');
        $fields[] = $passwordLength;

        $passwordLetters = new Zend_Form_Element_Checkbox(self::FIELD_PASSWORD_LETTERS);
        $passwordLetters->setChecked($this->getPasswordContainsLetter());
        $passwordLetters->setLabel('Password contains letters');
        $passwordLetters->setDescription('If checked all passwords need to contain at least one letter.');
        $passwordLetters->setAttrib('color', 'blue');
        $fields[] = $passwordLetters;

        $passwordCapitals = new Zend_Form_Element_Checkbox(self::FIELD_PASSWORD_CAPITALS);
        $passwordCapitals->setChecked($this->getPasswordContainsCapital());
        $passwordCapitals->setLabel('Password contains capital letters');
        $passwordCapitals->setDescription('If checked all passwords need to contain at least one capital letter.');
        $passwordCapitals->setAttrib('color', 'blue');
        $fields[] = $passwordCapitals;

        $passwordNumbers = new Zend_Form_Element_Checkbox(self::FIELD_PASSWORD_NUMBERS);
        $passwordNumbers->setChecked($this->getPasswordContainsNumber());
        $passwordNumbers->setLabel('Password contains numbers');
        $passwordNumbers->setDescription('If checked all passwords need to contain at least one number.');
        $passwordNumbers->setAttrib('color', 'blue');
        $fields[] = $passwordNumbers;

        $passwordSymbols = new Zend_Form_Element_Checkbox(self::FIELD_PASSWORD_SYMBOLS);
        $passwordSymbols->setChecked($this->getPasswordContainsSymbol());
        $passwordSymbols->setLabel('Password contains symbols');
        $passwordSymbols->setDescription('If checked all passwords need to contain at least one symbol.');
        $passwordSymbols->setAttrib('color', 'blue');
        $fields[] = $passwordSymbols;


        $privacyPolicy = new Zend_Form_Element_Textarea('privacyPolicy');
        $privacyPolicy->setLabel('Privacy Policy');
        $privacyPolicy->setRequired(true);
        $privacyPolicy->setValue($this->getPrivacyPolicy());
        $privacyPolicy->setAttrib('class', 'wysiwyg wide');
        $privacyPolicy->setDescription('The HTML text is displayed as privacy policy. The user has to accept it during registration.');
        $fields[] = $privacyPolicy;

        $termsOfUse = new Zend_Form_Element_Textarea('termsOfUse');
        $termsOfUse->setLabel('Terms of Use');
        $termsOfUse->setRequired(true);
        $termsOfUse->setValue($this->getTermsOfUse());
        $termsOfUse->setAttrib('class', 'wysiwyg wide');
        $termsOfUse->setDescription('The HTML text is displayed as terms of use. The user has to accept it during registration.');
        $fields[] = $termsOfUse;

        $hintField = new Zend_Form_Element_Textarea('hint');
        $hintField->setLabel('Registration Hint');
        $hintField->setRequired(false);
        $hintField->setAttrib('rows', '5');
        $hintField->setValue($this->getRegistrationHint());
        $hintField->setDescription('This text is displayed when the user starts registration. It is intended to inform the user about the process.');
        $fields[] = $hintField;

        /* Register Email Subject Field */
        $regEmailSubject = new Zend_Form_Element_Textarea('registrationEmailSubject');
        $regEmailSubject->setLabel('Registration Email Subject');
        $regEmailSubject->setAttrib('rows', '2');
        $regEmailSubject->setValue($this->getRegistrationEmailSubject());
        $regEmailSubject->setDescription('This is the subject of the email sent upon registration.');
        $fields[] = $regEmailSubject;

        $verify = new Zend_Form_Element_Checkbox('verifyEmail');
        $verify->setLabel('Verify Email Address');
        $verify->setValue($this->getVerifyEmail());
        $verify->setDescription('Activate if you want the user to verify its mail address. The user may not be able to login before finishing the verification process.');
        $fields[] = $verify;

        $verifyText = new Zend_Form_Element_Textarea('verifyEmailText');
        $verifyText->setLabel('Verify Email Template');
        $verifyText->setValue($this->getEmailVerificationTemplate());
        $verifyText->setAttrib('class', 'wysiwyg wide');
        $verifyText->setDescription('This is the text of the email which is sent to the user. You can use the following placeholders: ' . implode(', ', array_keys($this->emailVerificationVariables())));
        $fields[] = $verifyText;

        $activationText = new Zend_Form_Element_Textarea('activationText');
        $activationText->setLabel('Activation Email Template');
        $activationText->setValue($this->getActiviationEmailText());
        $activationText->setAttrib('class', 'wysiwyg wide');
        $activationText->setDescription('This is the text of the email which is sent to the user when his account is activated for the first time. You can use the following placeholders: ' . implode(', ', array_keys($this->emailActivationsVariables())));
        $fields[] = $activationText;

        return $fields;
    }

    /* (non-PHPdoc)
     * @see Epr_Module::saveSettingsField()
    */
    public function saveSettingsField($field)
    {
        $fieldname  = $field->getName();
        $fieldvalue = $field->getValue();

        if ($fieldname == self::FIELD_PASSWORD_LENGTH) {
            $this->passwordMinimumLength = max(4, (int)$fieldvalue);

            return true;
        } else if ($fieldname == self::FIELD_PASSWORD_NUMBERS) {
            $this->passwordContainsNumber = (bool)$fieldvalue;

            return true;
        } else if ($fieldname == self::FIELD_PASSWORD_LETTERS) {
            $this->passwordContainsLetter = (bool)$fieldvalue;

            return true;
        } else if ($fieldname == self::FIELD_PASSWORD_CAPITALS) {
            $this->passwordContainsCapitals = (bool)$fieldvalue;

            return true;
        } else if ($fieldname == self::FIELD_PASSWORD_SYMBOLS) {
            $this->passwordContainsSymbols = (bool)$fieldvalue;

            return true;
        } else if ($fieldname == 'hint') {
            $this->setRegistrationHint($fieldvalue);

            return true;
        } else if ($fieldname == 'privacyPolicy') {
            $this->setPrivacyPolicy($fieldvalue);

            return true;
        } else if ($fieldname == 'termsOfUse') {
            $this->setTermsOfUse($fieldvalue);

            return true;
        } else if ($fieldname == 'verifyEmail') {
            $this->setVerifyEmail($fieldvalue);

            return true;
        } else if ($fieldname == 'registrationEmailSubject') {
            $this->setRegistrationEmailSubject($fieldvalue);

            return true;
        } else if ($fieldname == 'verifyEmailText') {
            $this->setEmailVerificationTemplate($fieldvalue);

            return true;
        } else if ($fieldname == 'activationText') {
            $this->setActivationEmailText($fieldvalue);

            return true;
        } else {
            return parent::saveSettingsField($field);
        }
    }

    public function getAPIPath()
    {
        return 'auth/register';
    }

    public function registrationFields()
    {
        return array(
            Epr_Module_UserRegistration_Field::createField('email', Epr_Module_UserRegistration_Field::TYPE_EMAIL, 'E-Mail'),
            Epr_Module_UserRegistration_Field::createField('password', Epr_Module_UserRegistration_Field::TYPE_PASSWORD, 'Passwort')
        );
    }

    public function isRegistrationDataSufficient(&$params, &$error = null, &$errorCode = null)
    {

        $fields = $this->registrationFields();
        foreach ($fields as $field) {
            if ($field instanceof Epr_Module_UserRegistration_Field) {
                if ($field->getRequired()) {
                    $fielname = $field->getName();
                    if (!isset($params[$fielname]) || strlen($params[$fielname]) == 0) {
                        $error     = 'Missing value for ' . $fielname;
                        $errorCode = 100;

                        return false;
                    }
                }
            }
        }

        $email    = isset($params['email']) ? $params['email'] : false;
        $password = isset($params['password']) ? $params['password'] : false;

        $success = true;
        if (!\Poundation\PMailAddress::verifyAddress($email)) {
            $success   = false;
            $error     = 'Email address invalid';
            $errorCode = 101;
        }

        if (!$this->isPasswordConsideredSafe($password)) {
            $success   = false;
            $error     = 'Password does not fulfill complexity policies';
            $errorCode = 102;
        }

        return $success;
    }

    public function isPasswordConsideredSafe($password)
    {

        $validator = new Epr_Validate_SecurePassword(
            array(
                'passwordMinimumLength'    => $this->passwordMinimumLength,
                'passwordContainsNumber'   => $this->passwordContainsNumber,
                'passwordContainsLetter'   => $this->passwordContainsLetter,
                'passwordContainsCapitals' => $this->passwordContainsCapitals,
                'passwordContainsSymbols'  => $this->passwordContainsSymbols,
            )
        );

        return $validator->isValid($password);

    }

    public function createNewUser($params, $context)
    {
        $email    = __($params['email'])->lowercase();
        $password = $params['password'];

        $newUser = new Epr_User($email, $context);
        $newUser->setPassword($password);

        return $newUser;

    }

    public function emailVerificationVariables($user = null)
    {

        $variables = array(
            '%URL%'    => ($user instanceof Epr_User) ? _app()->getPublicURL()->addPathComponent('user/verify/')
                    ->addPathComponent($user->getVerificationId()) : '',
            '%EMAIL%'  => ($user instanceof Epr_User) ? (string)$user->getEmail() : null,
            '%SENDER%' => _app()->getName()
        );

        return $variables;
    }

    public function emailActivationsVariables($user = null)
    {

        $variables = array(
            '%URL%'    => _app()->getPublicURL(),
            '%EMAIL%'  => ($user instanceof Epr_User) ? (string)$user->getEmail() : null,
            '%SENDER%' => _app()->getName()
        );

        return $variables;
    }


}
