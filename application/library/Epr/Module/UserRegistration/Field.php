<?php

/** @Document */
class Epr_Module_UserRegistration_Field {

    const TYPE_EMAIL = 'email';
    const TYPE_PASSWORD =  'password';
    const TYPE_TEXT = 'text';
    const TYPE_LOCATION = 'gps';
    const TYPE_SELECT = 'select';

    /**
     * @Field(type="string")
     * @var string
     */
    private $type = self::TYPE_TEXT;

    /**
     * @Field(type="string")
     * @var string
     */
    private $name;

    /**
     * @Field(type="string")
     * @var string
     */
    private $caption;

    /**
     * @Field(type="boolean")
     * @var bool
     */
    private $isRequired = true;

    /** @Field(type="mixed") */
    private $additionalFields;

    /**
     * Creates a new field.
     * @param $name
     * @param $type
     * @param $caption
     * @return Epr_Module_UserRegistration_Field
     */
    static function createField($name, $type, $caption, $required = true) {
        $field = new Epr_Module_UserRegistration_Field();
        $field->setName($name);
        $field->setType($type);
        $field->setCaption($caption);
        $field->setRequired($required);
        return $field;
    }

    /**
     * Sets the caption.
     * @param $caption
     * @return $this
     */
    public function setCaption($caption) {
        $this->caption = $caption;
        return $this;
    }

    /**
     * Returns the caption.
     * @return string
     */
    public function getCaption() {
        return $this->caption;
    }

    /**
     * Sets the name.
     * @param $name
     * @return $this
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * Returns the name.
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Sets the type.
     * @param $type
     * @return $this
     */
    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    /**
     * Returns the type.
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Defines if the field is required. The default is true.
     * @param $value
     * @return $this
     */
    public function setRequired($value) {
        $this->isRequired = ($value == true);
        return $this;
    }

    /**
     * Returns true of the field is required.
     * @return bool
     */
    public function getRequired() {
        return $this->isRequired;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function addAdditionalField($name, $value) {
        if (is_string($name) && strlen($name) > 0) {
            if (is_null($this->additionalFields)) {
                $this->additionalFields = array();
            }

            $this->additionalFields[$name] = $value;
        }

        return $this;
    }

    /**
     * @param $name
     * @return null|string
     */
    public function getAdditionField($name) {
        $result = null;

        if (!is_null($this->additionalFields)) {
            if (isset($this->additionalFields[$name])) {
                $result = $this->additionalFields[$name];
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getAdditionalFields() {
        return $this->additionalFields;
    }


}