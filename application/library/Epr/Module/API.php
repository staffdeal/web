<?php

interface Epr_Module_API extends Epr_Module {

	const PLATFORM_IOS = 'ios';
	const PLATFORM_ANDROID = 'android';
	const PLATFORM_ANY = 'any';

	public function getAPIPath();	// return false or empty string to disable API functionality

	public function getAPIAuthLevel();			// implemented by abstract, override only if you know what you're doing
	public function setAPIAuthLevel($role);

	public function getDisplayname();
	public function setDisplayname($displayname);

	public function getTemplateData(); // return an array with 'id' => {'platform' => Epr_Module_API::PLATFORM_IOS || Epr_Module_API::PLATFORM_IOS, 'title' => …, 'description' => …} This causes all UI and API data to be generated.
}