<?php

class Epr_Module_DealCode_Abstract extends Epr_Module_Abstract {

	protected $data;

	public function getData() {
		return $this->data;
	}

    public function setData($data) {
        if (is_array($data)) {
            $this->data = &$data;
        }
        return $this;
    }

	public function __construct(&$data = null) {
		parent::__construct();
        if (is_array($data)) {
            $this->setData($data);
        }
	}

	/**
	 * Sets the data for the key.
	 *
	 * @param $data
	 * @param $key
	 *
	 * @return $this
	 */
	public function setDataForKey($data, $key) {
		if (is_string($key)) {
			$this->data[$key] = $data;
		}

		return $this;
	}

	/**
	 * Returns the data for the key.
	 *
	 * @param $key
	 *
	 * @return null|mixed
	 */
	public function getDataForKey($key) {
		return (isset($this->data[$key])) ? $this->data[$key] : null;
	}

	public function isValid() {
		return false;
	}

	public function redeem() {
		return null;
	}

    public function getSettingsFormFields() {
        return array();
    }

    public function saveSettingsField($field) {
        return true;
    }

}