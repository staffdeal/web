<?php

interface Epr_Module_UserRegistration extends Epr_Module {

    /**
     * Returns an array of fields that should be requested upon registration.
     * The array may only have instances of Epr_Module_UserRegistrationField.
     * @return array
     */
    public function registrationFields();

	/**
	 * Returns a boolean value indicating if the registration may be processed with the given data.
	 * Returning false immediatly breaks the registratrion process.
	 * @param array $params
	 */
	public function isRegistrationDataSufficient(&$params, &$error = null); // default implementation checks for mail/password/name so you must call super and pass this value on (after &&ing).
	
	/**
	 * After the system has created the new user this method is called on the module.
	 * Perform any neccessary work on it (e.g. deactivating, send mails)
	 * @param Epr_User $user
	 * @param array $params
     * @return bool
	 */
	public function processNewUser($user, $params);			// there is no default implementation

	/**
	 * This method processes things right after the user has been verified.
	 * @param Epr_User $user
	 *
	 * @return bool
	 */
	public function processVerification(Epr_User $user);
}

?>