<?php

/** @Document */
class Epr_Module_Cron_Abstract extends Epr_Module_Abstract implements Epr_Module_Cron {

	/** @Field(type="datetime") */
	private $lastRunDate;

	/**
	 * Runs the cron job of the module and returns the output.
	 *
	 * @param DateInterval $intervalSinceLastRun
	 *
	 * @return string
	 */
	public function runCron(DateInterval $intervalSinceLastRun)
	{
		$this->lastRunDate = new DateTime();
        return '';
	}

	/**
	 * @return Datetime|null
	 */
	public function getLastRunDate() {
		return $this->lastRunDate;
	}


}