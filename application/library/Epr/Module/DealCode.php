<?php

interface Epr_Module_DealCode extends Epr_Module {

	public function isValid();
	public function redeem();
    public function hasWarning();

    public function getDataKey();

    public function getSettingsFormFields();

    public function saveSettingsField($field);

}