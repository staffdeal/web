<?php
class Epr_Log extends Zend_Log
{


    public function log($message, $priority, $extras = null)
    {
        if (func_num_args() > 2) {
            $backtrace = func_get_arg(2);
        } else {
            $backtrace = debug_backtrace();
        }
        $this->setEventItem('function', isset($backtrace[1]['function']) ? $backtrace[1]['function'] : '');
        $this->setEventItem('class', isset($backtrace[1]['class']) ? $backtrace[1]['class'] : '');
        $this->setEventItem('line', $backtrace[0]['line']);
        $this->setEventItem('file', $backtrace[0]['file']);


        parent::log($message, $priority);

    }

    public function __call($method, $params)
    {
        $priority = strtoupper($method);
        if (($priority = array_search($priority, $this->_priorities)) !== false) {
            $this->log(array_shift($params), $priority, array_slice(debug_backtrace(), 1));
        } else {
            throw new Zend_Log_Exception('Bad log priority');
        }
    }

    public function debugPrint($obj)
    {
        $this->debug(print_r($obj, true));
    }

    public function emerg($message)
    {
        $this->log($message, self::EMERG);
    }

    public function alert($message)
    {
        $this->log($message, self::ALERT);
    }

    public function critical($message)
    {
        $this->log($message, self::CRIT);
    }

    public function error($message)
    {
        $this->log($message, self::ERR);
    }

    public function warn($message)
    {
        $this->log($message, self::WARN);
    }
    public function notice($message)
    {
        $this->log($message, self::NOTICE);
    }
    public function info($message)
    {
        $this->log($message, self::INFO);
    }
    public function debug($message)
    {
        $this->log($message, self::DEBUG);
    }

}
