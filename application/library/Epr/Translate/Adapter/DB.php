<?php

class Epr_Translate_Adapter_DB extends Zend_Translate_Adapter
{

	private $documents = array();
	private $context;

	public function __construct($options) {
		parent::__construct($options);
	}

	/**
	 * Load translation data
	 *
	 * @param  mixed              $data
	 * @param  string|Zend_Locale $locale
	 * @param  array              $options (optional)
	 *
	 * @return array
	 */
	protected function _loadTranslationData($data, $locale, array $options = array())
	{
		$this->context = $data;
		$document = $this->getDocumentForLocale($locale, $data);
		return ($document instanceof Epr_Translate_Locale) ? array($locale => $document->getData()) : array('de' => array());
	}

	/**
	 * Returns a valid locale document.
	 * @param $locale
	 *
	 * @return Epr_Translate_Locale
	 */
	private function getDocumentForLocale($locale, $context = '') {

		$identifier = $locale . '_' . $context;

		if (is_string($locale) && strlen($locale) > 0) {

			if (!isset($this->documents[$identifier])) {
				$this->documents[$identifier] = Epr_Translate_Locale::getLocaleWithLocale($locale, $context);
			}

			return $this->documents[$identifier];
		}

		return null;
	}

	/**
	 * Returns the adapter name
	 *
	 * @return string
	 */
	public function toString()
	{
		return 'Epr Translate Adapter DB';
	}

	protected function _log($message, $locale)
	{
		$document = $this->getDocumentForLocale($locale, $this->context);
		if ($document instanceof Epr_Translate_Locale) {
			$document->addString($message);
		}

		parent::_log($message, $locale);
	}

}