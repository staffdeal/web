<?php

/** @Document */
class Epr_Translate_Locale extends Epr_Document
{

	const DOCUMENT_NAME = 'Epr_Translate_Locale';

	/** @Id */
	protected $id;

	/**
	 * @Index
	 * @Field(type="string")
	 */
	private $locale;

	/**
	 * @Index
	 * @Field(type="string")
	 */
	private $context;

	/**
	 * @Field(type="mixed")
	 */
	private $data = array();

	public static function getLocaleWithLocale($locale, $context = '')
	{

		$repo   = _dm()->getRepository(self::DOCUMENT_NAME);
		$result = $repo->findBy(array(
									 'locale'  => $locale,
									 'context' => $context
								));
		if (count($result) == 0) {
			$document = new Epr_Translate_Locale($locale, $context);
			_dm()->persist($document);

			$document->addString('Y/m/d', 'Y/m/d');

		} else {
			$document = $result[0];
		}

		return $document;

	}

	public function __construct($locale, $context = '')
	{
		if (!is_string($locale) || strlen($locale) == 0) {
			throw new Exception('Cannot create locale document without a locale string.');
		}

		$this->locale = $locale;
		$this->context = $context;
	}

	public function getLocale() {
		return $this->locale;
	}

	public function getContext() {
		return $this->context;
	}

	public function getData()
	{
		return $this->data;
	}

	public function count() {
		return count($this->data);
	}

	public function countMissing() {
		$missing = 0;

		foreach($this->data as $value) {
			if (is_null($value) || strlen($value) == 0) {
				$missing++;
			}
		}

		return $missing;
	}

	public function addString($name, $value = null)
	{
		if ($this->hasString($name)) {
			return false;
		} else {
			$this->setString($name, $value);

			return true;
		}
	}

	public function setString($name, $value = null)
	{
		if (strlen($name) > 0) {
			$this->data[$name] = $value;
		}
	}

	public function removeString($name) {
		$this->data[$name] = 'remove';
		unset($this->data[$name]);
	}

	public function hasString($name)
	{
		return (isset($this->data[$name]));
	}

}