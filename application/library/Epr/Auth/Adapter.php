<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 09.09.12
 * Time: 15:26
 */
class Epr_Auth_Adapter implements Zend_Auth_Adapter_Interface
{

    /** @var string */
    protected $_identity = null;

    /** @var string */
    protected $_credential = null;

    /** @var array */
    protected $_options = null;

    const DEFAULT_SESSION_NAMESPACE = "Epr_Auth_Adapter";

    public function __construct(array $options = array())
    {

    }

    public function setIdentity($identity)
    {
        $this->_identity = $identity;
    }

    public function getIdentity()
    {
        return $this->_identity;
    }

    public function setCredential($credential)
    {
        $this->_credential = $credential;
    }

    public function getCredential()
    {
        return $this->_credential;
    }

    public function authenticate()
    {

        if(!$this->getIdentity()){
            $code = Zend_Auth_Result::FAILURE;
            $message = array('A username is required');
            return new Zend_Auth_Result($code, '', $message);
        }

        if(!$this->getCredential())
        {
            $code = Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID;
            $message = array('A password is required.');
            return new Zend_Auth_Result($code, '', $message);
        }

        if (!isset($this->_options['noSession']) || ($this->_options['noSession'] != true)) {
        
			$namespace = self::DEFAULT_SESSION_NAMESPACE;

			if (isset($this->_options['sessionNamespace']) && $this->_options['sessionNamespace'] != '') {
            	$namespace = $this->_options['sessionNamespace'];
        	}

        	$session = new Zend_Session_Namespace($namespace);
        }
        
        try {

            $user = _dm()->getRepository('Epr_User')->findOneBy(array('email'=>$this->getIdentity()));

            if(!$user instanceof Epr_User || !$user->isActive() || !$user->isVerified()){
                $code = Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND;
                $message = array('Identity not found.');
                return new Zend_Auth_Result($code, '', $message);
            } else {
            	if ($user->checkPassword($this->getCredential())) {
                    $user->abortResetPassword();
            		return new Zend_Auth_Result(Zend_Auth_Result::SUCCESS, $user->getId(), array());
            	}
            }

        } catch (Exception $e) {
            $code = Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND;
            $message = array('Something went wrong.');
            return new Zend_Auth_Result($code, '', $message);
        }

        return new Zend_Auth_Result(Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID, $user, array());

    }
}
