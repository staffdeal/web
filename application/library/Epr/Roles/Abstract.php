<?php

class Epr_Roles_Abstract extends \Poundation\PEnum {

	/**
	 * Returns the translated name of the given role identifier. If the roleIdentifier is not defined, the argument's value is returned.
	 * @param string $roleIdentifier
	 * @return string
	 * @throws Exception
	 */
	public static function roleNameForRoleIdentifier($roleIdentifier) {
		return (string)__($roleIdentifier)->uppercaseAtBeginning();
	}
	
	/**
	 * Returns a dictionary with all known roles.
	 * The keys of the dict are the strings you pass Epr_User::setRole. The values are translated names you can display to the user.
	 * @return \Poundation\PDictionary
	 */
	public static function allRoles() {
		$roles = new \Poundation\PDictionary();
	
		$values = self::allConstants();
		foreach($values as $name=>$value) {
			$roles->setValueForKey(self::roleNameForRoleIdentifier($value), $value);
		}
	
		return $roles;
	}
	
	public static function isValidRole($role) {
		return self::allRoles()->allKeys()->contains($role);
	}
	
}

?>