<?php

class Epr_Document_WebPage extends Epr_Document
{

	/** @Field(type="string") */
	protected $title;

	/** @Field(type="string") */
	protected $text;


	/**
	 * @ReferenceMany(targetDocument="Epr_Content_Image")
	 */
	protected $contentImages;

	/**
	 * @ReferenceOne(targetDocument="Epr_Content_Image")
	 */
	protected $titleImage;

	/**
	 * @Index
	 * @Field(type="string")
	 */
	protected $slug;

	/**
	 * Returns the title.
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Sets the title.
	 *
	 * @param $title
	 *
	 * @return Epr_Document_WebPage
	 */
	public function setTitle($title)
	{
		$this->title = (string)$title;

		return $this;
	}

	/**
	 * Returns the text.
	 *
	 * @return string
	 */
	public function getText()
	{
		return $this->text;

	}

	/**
	 * Sets the text.
	 *
	 * @param $text
	 *
	 * @return Epr_Document_WebPage
	 */
	public function setText($text)
	{
		$this->text = (string)$text;

		return $this;
	}

	/**
	 * Returns an array of connected images.
	 *
	 * @return array
	 */
	public function getImages()
	{
		$images = array();
		foreach ($this->contentImages as $img) {
			array_push($images, $img);
		}

		return $images;
	}

	/**
	 * Adds an image to the news.
	 *
	 * @param $image
	 *
	 * @return Epr_News
	 */
	public function addImage($image)
	{

		if ($image instanceof Epr_Content_Image) {
			$this->contentImages[] = $image;
		} else if ($image instanceof \Poundation\PImage) {

			$contentImage = Epr_Content_Image::imageFromPImage($image);
			if ($contentImage) {
				_dm()->persist($contentImage);

				return $this->addImage($contentImage);
			}

		}

		return $this;

	}

	/**
	 * Removes an image and returns true if successful. You may pass an Epr_Content_Image instance or a string of the id.
	 *
	 * @param $image
	 *
	 * @return bool
	 */
	public function removeImage($image)
	{
		$key = false;
		if ($image instanceof Epr_Content_Image) {
			$key = array_search($image, $this->contentImages);
		} else if (is_string($image)) {
			// assuming it is the id of the image
			foreach ($this->contentImages as $arrayKey => $contentImage) {
				if ($contentImage->getId() == $image) {
					$key = $arrayKey;
					break;
				}
			}
		}
		if ($key !== false && isset($this->contentImages[$key])) {
			unset($this->contentImages[$key]);

			return true;
		}

		return false;
	}

	/**
	 * Sets the content image.
	 *
	 * @param Epr_Content_Image $image
	 *
	 * @return Epr_News
	 */
	public function setTitleImage($image)
	{
		if ($image instanceof Epr_Content_Image || is_null($image)) {
			$this->titleImage = $image;
		}
		return $this;
	}

	/**
	 * Removes the title image.
	 *
	 * @return Epr_Document_WebPage
	 */
	public function removeTitleImage()
	{
		$this->titleImage = null;
		return $this;
	}

	/**
	 * Returns the title image.
	 *
	 * @return Epr_Content_Image
	 */
	public function getTitleImage()
	{
		return $this->titleImage;
	}

	/**
	 * Returns the slug of the document.
	 *
	 * @return string
	 */
	public function getSlug()
	{
		return $this->slug;
	}

	/**
	 * Sets the slug of the document.
	 *
	 * @param $slug
	 *
	 * @return Epr_Document_WebPage
	 */
	public function setSlug($slug)
	{
		if (is_string($slug) || is_null($slug)) {
			$this->slug = $slug;
		}

		return $this;
	}

	public function createSlugFromText($text)
	{
		$result = false;

		if (is_string($text)) {

			$slugIsFree = false;

			// helper variables to create unique slug, used after first check
			$i      = 0;
			$prefix = '';
			while ($slugIsFree == false) {

				$slug = __($text)->appendString($prefix)->slug();

				$query  = _dm()->createQuery('helpers', 'slugs')->setKey((string)$slug);
				$result = $query->execute();

				$slugIsFree = (is_null($result));

				$prefix = '-' . ++$i;
			}
		}

		return $result;
	}

}
