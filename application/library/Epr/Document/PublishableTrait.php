<?php

/** @Document */
trait Epr_Document_PublishableTrait {

    /** @Field(type="string") */
    private $state = Epr_Document_Publishable::STATE_DRAFT;

    /** @Field(type="string") */
    private $stateChangeReason = Epr_Document_Publishable::STATE_CHANGE_REASON_AUTOMATIC;

    /** @Field(type="datetime") */
    private $automaticPublishDate = null;

    /** @Field(type="datetime") */
    private $manualPublishDate = null;

    /** @Field(type="datetime") */
    private $manualUnpublishDate = null;

    /** @Field(type="boolean") */
    private $publishAutomatically = true;

    /**
     * Returns true, if the document is a publishable document in the database.
     * @return bool
     */
    public function isDocumentPublishable() {
        return (!is_null($this->state));
    }

    public function getState() {
        return ($this->isDraft()) ? Epr_Document_Publishable::STATE_DRAFT : Epr_Document_Publishable::STATE_PUBLISHED;
    }

    /**
     * @return bool
     */
    public function isDraft() {
        return !$this->isPublished();
    }

    /**
     * @return bool
     */
    public function isPublished() {
        return ($this->state == Epr_Document_Publishable::STATE_PUBLISHED);
    }

    /**
     * Published the document MANUALLY! All automatics are getting disabled.
     * @return $this
     */
    public function publish() {

        if (!$this->isPublished()) {
            $this->manualPublishDate = Poundation\PDate::now();
            $this->state = Epr_Document_Publishable::STATE_PUBLISHED;
            $this->stateChangeReason = Epr_Document_Publishable::STATE_CHANGE_REASON_MANUAL;
        }

        return $this;
    }


    /**
     * Marks the document as draft, revoking all publish-related states.
     * Calling this method does NOT enable automatic publishing again. For this to work, you need to set a new automatic
     * publish date by calling publishAt.
     *
     * You can call this method to revoke any automatic publish dates, event those being in the future.
     *
     * @return $this
     */
    public function unpublish() {
        $this->manualUnpublishDate = \Poundation\PDate::now();
        $this->state = Epr_Document_Publishable::STATE_DRAFT;
        $this->automaticPublishDate = null;
        $this->stateChangeReason = Epr_Document_Publishable::STATE_CHANGE_REASON_MANUAL;
        return $this;
    }

    /**
     * Enables automatic publishing. You need to pass a date.
     *
     * @param \Poundation\PDate $date
     * @return $this
     */
    public function publishAt(\Poundation\PDate $date) {
        if (!$this->isPublished()) {
            $this->automaticPublishDate = $date;
            $this->state = Epr_Document_Publishable::STATE_DRAFT;
        }

        if ($date->isPast()) {
            $this->state = Epr_Document_Publishable::STATE_PUBLISHED;
            $this->stateChangeReason = Epr_Document_Publishable::STATE_CHANGE_REASON_MANUAL;
            $this->manualPublishDate = $date;
            $this->automaticPublishDate = null;
        }

        return $this;
    }

    /**
     * Returns the date of the last publishing.
     * @return \Poundation\PDate||null
     */
    public function getPublishedDate() {
        $result = null;

        if ($this->isPublished()) {
            if ($this->stateChangeReason == Epr_Document_Publishable::STATE_CHANGE_REASON_AUTOMATIC) {
                $result = \Poundation\PDate::createDate($this->automaticPublishDate);
            } else {
                $result = \Poundation\PDate::createDate($this->manualPublishDate);
            }
        } else {
            if ($this->willBePublishedAutomatically()) {
                $result = \Poundation\PDate::createDate($this->automaticPublishDate);
            }
        }

        return $result;
    }

    /**
     * Returns the date of the last unpublishing. Since unpublishing can only ne done manually,
     * this automatically means that the unpublishing was done manually.
     * @return \Poundation\PDate||null
     */
    public function getUnpublishedDate() {
        return \Poundation\PDate::createDate($this->manualUnpublishDate);
    }

    public function publishAutomatically() {
        if ($this->isDraft() && $this->willBePublishedAutomatically() && $this->automaticPublishDate) {
            $publishDate = \Poundation\PDate::createDate($this->automaticPublishDate);
            if ($publishDate->isPast()) {
                $this->state = Epr_Document_Publishable::STATE_PUBLISHED;
                $this->stateChangeReason = Epr_Document_Publishable::STATE_CHANGE_REASON_AUTOMATIC;
                $this->manualPublishDate = null;
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the reason of the last state change.
     * @return string
     */
    public function getStateChangeReason() {
        return $this->stateChangeReason;
    }

    /**
     * Returns true, if the object will be published automatically. Return false,
     * if the current settings do not allow this.
     *
     * @return bool
     */
    public function willBePublishedAutomatically() {
        return ($this->isDraft() && !is_null($this->automaticPublishDate));
    }

    /**
     *
     * Returns a human-readable. textual description of the current state.
     *
     * @param bool $includeDates
     * @return string
     */
    public function getStatusText($includeDates = false, $separator = '<br />') {
        $text =  ucfirst(_t($this->getState()));

        if ($includeDates) {
            if ($this->willBePublishedAutomatically()) {
                $text .= $separator;
                $text .= _t('Automatic publishing at ') . $this->getPublishedDate()->getFormatedString('Y-m-d H H:i');
            }
        }

        return $text;
    }

}