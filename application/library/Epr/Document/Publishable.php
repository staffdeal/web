<?php

interface Epr_Document_Publishable {

    const STATE_DRAFT = 'draft';
    const STATE_PUBLISHED = 'published';

    const STATE_CHANGE_REASON_MANUAL = 'manual';
    const STATE_CHANGE_REASON_AUTOMATIC = 'auto';

    public function isDocumentPublishable();

    public function getState();
    public function isDraft();
    public function isPublished();

    public function allowAutomaticPublishing();

    public function publish();
    public function unpublish();
    public function publishAt(\Poundation\PDate $date);

    public function getPublishedDate();
    public function getUnpublishedDate();
    public function getStateChangeReason();

    public function getStatusText($includeDates = false);
}