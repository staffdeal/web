<?php

/** @Document */
class Epr_Cms_Page extends Epr_Cms_Element
{

    const DOCUMENTNAME = 'Epr_Cms_Page';

    const CMS_PAGE_HEADER_CONTENT = 'pg_content';
    const CMS_PAGE_IMAGE = 'pg_image';

    private $elementsToSkip = array();


    /** @ReferenceOne(targetDocument="Epr_Content_Image") */
    private $image;

    static public function getTypeString()
    {
        return 'Web page';
    }

    /**
     * @return Epr_Content_Image|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param $image
     *
     * @return $this
     */
    public function setImage($image)
    {
        if (is_null($image) || $image instanceof Epr_Content_Image) {
            $this->image = $image;
        }

        return $this;
    }

    /**
     * @param $element
     *
     * @return $this
     */
    public function skipElement($element)
    {
        if ($element instanceof Epr_Cms_Element) {
            $element = $element->getPath();
        }

        if (is_string($element) && strlen($element) > 0) {
            $this->elementsToSkip[] = $element;
        }

        return $this;
    }


    public function getEditorFields()
    {
        $fields = parent::getEditorFields();

        $content = new Zend_Form_Element_Textarea(self::CMS_PAGE_HEADER_CONTENT);
        $content->setLabel('Content');
        $content->setValue($this->getContent());
        $content->setDescription('This is the HTML content of the CMS element.');
        $content->setAttrib('class', 'wysiwyg wide');
        $fields[] = $content;

        $imageField = new Epr_Form_Element_Image_Selector(self::CMS_PAGE_IMAGE);
        $imageField->setContext(Epr_Content_Image::ACCESS_CMS_IMAGE);
        $imageField->setLabel('Image');
        $imageField->setDescription('Set an image to be displayed');
        $imageField->setValue($this->getImage());
        $fields[] = $imageField;

        return $fields;
    }

    public function saveEditorField(Zend_Form_Element $field)
    {
        $name = $field->getName();
        $value = $field->getValue();

        if ($name == self::CMS_PAGE_HEADER_CONTENT) {
            $this->content = (string)$value;

            return true;
        } else {
            if ($name == self::CMS_PAGE_IMAGE) {

                $this->setImage($value);

                return true;

            } else {
                return parent::saveEditorField($field);
            }
        }
    }


    /**
     * @param $path
     *
     * @return Epr_Cms_Page
     */
    static public function getPageWithPath($path)
    {
        $page = null;

        $repo = _dm()->getRepository(self::DOCUMENTNAME);
        if ($repo instanceof \Doctrine\ODM\CouchDB\DocumentRepository) {
            $result = $repo->findBy(array('path' => $path));

            if (count($result) > 0) {
                $page = $result[0];
            }
        }

        return $page;

    }

    public function renderedContent($titleLevel = 1)
    {

        $output = '';

        if (strlen($this->getTitle() . $this->getContent()) > 0) {

            $output .= '<div class="hero-unit jumbotron">';

            if ($this->getImage()) {

                $width = min(250, $this->getImage()->getWidth());
                $height = $this->getImage()->getHeightByWidth($width);

                $output .= '<img src="' . $this->getImage()->getPublicIDBasedURL(
                        $width,
                        $height
                    ) . '" class="page-title" style="' . $this->getImage()->getCSSDimensionString() . '" />';
            }

            $output .= $this->renderedTitle($titleLevel, null, null, false);
            if (strlen($this->getContent()) > 0) {
                $output .= $this->getContent();
            }

            if ($this->getImage()) {
                $output .= '<div class="clearfix"></div>';
            }

            $output .= '</div>';

        }

        $elementsToRender = new \Poundation\PArray();

        $freeCols = 12;
        foreach ($this->getElements() as $element) {

            if ($element instanceof Epr_Cms_Element) {

                foreach($this->getSharedData() as $key => $value) {
                    $element->setSharedDataForKey($value, $key);
                }

                $freeCols -= $element->getWidth();

                if (array_search($element->getPath(), $this->elementsToSkip) === false) {
                    $elementsToRender[] = $element;
                }

            }

        }

        $elementsToRender->sortByPropertyName('order', SORT_ASC);

        if ($freeCols > 0) {
            $freeCols = (int)floor($freeCols / 2.0);
        }

        if (count($this->getElements()) > 0) {
            $output .= '<div class="row">';

            foreach ($elementsToRender as $element) {
                if ($element instanceof Epr_Cms_Element) {

                    $class = 'col-lg-' . $element->getWidth();

                    if ($freeCols > 0) {
                        $class .= ' col-lg-offset-' . $freeCols;
                        $freeCols = 0;
                    }

                    $output .= '<div class="' . $class . '">';
                    $output .= $element->renderedContent($titleLevel + 1);
                    $output .= '</div>';
                }
            }

            $output .= '</div>';
        }

        return $output;
    }

}