<?php

/** @Document */
class Epr_Cms_Element_TermsOfUse extends Epr_Cms_Element
{

	static public function getTypeString()
	{
		return 'terms of use';
	}


	public function getContent() {

		$content = '';

		$module = _app()->getActiveUserRegistrationModule();
		if ($module instanceof Epr_Module_UserRegistration_Abstract) {
			$content = $module->getTermsOfUse();
		}

		return $content;

	}

}