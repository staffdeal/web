<?php

/** @Document(repositoryClass="Epr_Cms_Element_Collection") */
class Epr_Cms_Element_HTML extends Epr_Cms_Element {

	const DOCUMENTNAME = 'Epr_Cms_Element_HTML';
	const CMS_ELEMENT_FORM_FIELD_CONTENT = 'content_content';

	static public function getTypeString() {
		return 'HTML snippet';
	}

	public function setContent($content) {
		$this->content = (string)$content;
		return $this;
	}

	public function renderedContent($titleLevel = 0) {

		$content = parent::renderedContent($titleLevel);

		return $content . '<div class="clearfix"></div>';

	}


	public function getEditorFields()
	{
		$fields = parent::getEditorFields();

		$content = new Zend_Form_Element_Textarea(self::CMS_ELEMENT_FORM_FIELD_CONTENT);
		$content->setLabel('Content');
		$content->setValue($this->getContent());
		$content->setDescription('This is the HTML content of the CMS element.');
		$content->setAttrib('class', 'wysiwyg wide');
		$fields[] = $content;

		return $fields;
	}

	public function saveEditorField(Zend_Form_Element $field)
	{
		$name = $field->getName();
		$value = $field->getValue();

		if ($name == self::CMS_ELEMENT_FORM_FIELD_CONTENT) {
			$this->setContent($value);
			return true;
		} else {
			return parent::saveEditorField($field);
		}

	}

}
