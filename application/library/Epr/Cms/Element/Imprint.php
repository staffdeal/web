<?php

/** @Document */
class Epr_Cms_Element_Imprint extends Epr_Cms_Element
{

	static public function getTypeString()
	{
		return 'imprint';
	}


	public function getContent() {

		$content = '';

		$module = _app()->getModuleWithIdentifier(Epr_Modules_Info::identifier());
		if ($module instanceof Epr_Modules_Info) {
			$content = $module->getImpressText();
		}

		return $content;

	}

}