<?php

class Epr_Cms_Element_Collection extends \Doctrine\ODM\CouchDB\DocumentRepository
{

	static public function getAllPages()
	{

		$repo = _dm()->getRepository(Epr_Cms_Page::DOCUMENTNAME);

		return $repo->findAll();

	}

	static public function getElementWithAbsolutePath($path)
	{

		$element = null;

		$components = explode('.', trim($path));
		if (count($components) > 0) {

			$element = Epr_Cms_Page::getPageWithPath($components[0]);

			for ($i = 1; $i < count($components); $i++) {

				$element = $element->getElementWithPath($components[$i]);
				if (is_null($element)) {
					break;
				}
			}
		}

		return $element;

	}

}