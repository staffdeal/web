<?php

/** @Document */
class Epr_Cms_Element_PrivacyPolicy extends Epr_Cms_Element
{

	static public function getTypeString()
	{
		return 'privacy policy';
	}


	public function getContent() {

		$content = '';

		$module = _app()->getActiveUserRegistrationModule();
		if ($module instanceof Epr_Module_UserRegistration_Abstract) {
			$content = $module->getPrivacyPolicy();
		}

		return $content;

	}

}