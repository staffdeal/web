<?php

/** @Document */
class Epr_Cms_Element extends Epr_Document
{

    const CMS_ELEMENT_FORM_FIELD_TITLE = 'title';

    /** @Id */
    protected $id;

    /** @Field(type="string") */
    private $parent_type = 'Epr_Cms_Element';

    /**
     * @Index
     * @Field(type="string")
     */
    private $path;

    /** @Field(type="string") */
    protected $title;

    /** @Field(type="string") */
    protected $content;

    /** @EmbedMany */
    protected $elements = array();

    /** @Field(type="integer") */
    protected $width = 12;

    /** @Field(type="integer") */
    protected $order = 1;

    private $sharedData = array();

    private $parentElement;
    private $didAssignParentElement = false;

    protected $titleRendered = false;

    protected $_automaticTitle = true;

    static public function getTypeString()
    {
        return 'The element has no real type.';
    }

    public function __construct($path)
    {
        parent::__construct();
        $this->path = $path;
    }

    public function __toString()
    {
        return $this->renderedContent(1);
    }

    /**
     * Returns the path of the element.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    public function getAbsolutePath()
    {

        $path = $this->getPath();

        $parent = $this->getParentElement();
        if ($parent) {
            $path = $parent->getAbsolutePath() . '.' . $path;
        }

        return $path;

    }

    /**
     * Returns the title of the element.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title of the element.
     *
     * @param $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = (string)$title;

        return $this;
    }

    /**
     * Sets a shared object for the given key.
     * @param $data
     * @param $key
     * @return $this
     */
    public function setSharedDataForKey($data, $key)
    {
        $this->sharedData[$key] = $data;
        return $this;
    }

    /**
     * Returns the shared object for the given key.
     * @param $key
     * @return null
     */
    public function getSharedDataForKey($key)
    {
        $data = null;

        if ($key && isset($this->sharedData[$key])) {
            return $this->sharedData[$key];
        }

        return $data;
    }

    /**
     * Sets the shared data array.
     * @param array $data
     * @return $this
     */
    public function setSharedData($data)
    {
        if (is_array($data) ||  is_null($data)) {
            $this->sharedData = $data;
        }
        return $this;
    }

    /**
     * Returns the shared data array.
     * @return array
     */
    public function getSharedData()
    {
        return $this->sharedData;
    }

    /**
     * Returns the content of the element.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Returns the width (1 - 12).
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     *
     * @return $this
     */
    public function setWidth($width)
    {

        $this->width = max(1, min(12, (int)$width));
        return $this;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setOrder($value)
    {
        $this->order = max(0, (int)$value);
        return $this;
    }

    /**
     * Adds an sub-element to the element.
     *
     * @param Epr_Cms_Element $element
     *
     * @return $this
     * @throws Exception
     */
    public function addElement(Epr_Cms_Element $element)
    {
        if (strlen($element->getPath()) == 0) {
            throw new Exception('You cannot add an element without a path.');
        } else {
            if ($this->getElementWithPath($element->getPath())) {
                throw new Exception('An element with the same path "' . $element->getPath() . '" does already exist');
            } else {
                $doesExist = false;
                if ($this->elements instanceof \Doctrine\Common\Collections\ArrayCollection) {
                    $doesExist = ($this->elements->contains($element));
                } else if (is_array($this->elements)) {
                   $doesExist = (array_search($element, $this->elements) !== false);
                }
                if  ($doesExist == false) {
                    $this->elements[] = $element;
                    $element->parentElement = $this;
                }
            }
        }

        return $this;
    }

    /**
     * Returns all sub-elements.
     *
     * @return array(Epr_Element)
     */
    public function getElements()
    {
        $this->_assignParentElement();

        return \Poundation\PArray::create($this->elements)->getSortedArrayByPropertyName('order', SORT_ASC);
    }

    /**
     * Returns the element with the given path or null of it does not exist.
     *
     * @param $path
     *
     * @return Epr_Cms_Element|null
     */
    public function getElementWithPath($path)
    {
        $element = null;

        foreach ($this->getElements() as $subElement) {
            if ($subElement instanceof Epr_Cms_Element) {
                if ($subElement->getPath() === $path) {
                    $element = $subElement;
                    break;
                }
            }
        }

        return $element;
    }

    private function _assignParentElement()
    {
        if (!is_null($this->elements)) {
            if ($this->didAssignParentElement == false) {
                foreach ($this->elements as $element) {
                    if ($element instanceof Epr_Cms_Element) {
                        $element->parentElement = $this;
                    }
                }
                $this->didAssignParentElement = true;
            }
        }
    }

    /**
     * @return Epr_Cms_Element
     */
    public function getParentElement()
    {
        $this->_assignParentElement();

        return $this->parentElement;
    }

    /**
     * @return Epr_Cms_Element
     */
    public function getRootElement() {

        $parent = $this;
        while($parent->getParentElement()) {
            $parent = $parent->getParentElement();
        }

        return $parent;
    }

    public function isOnStartPage() {
        $root = $this->getRootElement();
        $path = $root->getPath();
        return ($path == 'index');
    }

    public function renderedTitle(
        $titleLevel = 1,
        $prepend = null,
        $append = null,
        $useBranding = true,
        $titleOverride = null
    ) {

        $this->titleRendered = true;

        $output = '';

        $titleContent = (is_string($titleOverride)) ? $titleOverride : $this->getTitle();

        if (is_string($prepend)) {
            $output .= $prepend;
        }

        if (strlen($titleContent) > 0) {

            $classes = array();

            if ($useBranding) {
                $classes[] = 'branding';
            }

            if (outlineTranslation()) {
                $classes[] = "translate";
            }
            $class = ' class="' . implode(' ', $classes) . '"';
            $output .= '<h' . $titleLevel . $class . '>';
            $output .= $titleContent;
            $output .= '</h' . $titleLevel . '>';
        }

        if (is_string($append)) {
            $output .= $append;
        }

        return $output;
    }


    public function renderedContent($titleLevel = 0)
    {

        $output = '';

        if ($this->_automaticTitle && $this->titleRendered == false) {
            $output .= $this->renderedTitle($titleLevel, null, null, false);
        }

        $this->titleRendered = false;

        $content = $this->getContent();

        if (strlen($content) > 0) {
            $class = (outlineTranslation()) ? ' class="translate"' : '';
            $output .= '<div' . $class . '>' . $content . '</div>';
        }

        $this->titleRendered = false;

        return $output;
    }

    public function getEditorFields()
    {

        $fields = array();

        $titleField = new Zend_Form_Element_Text(self::CMS_ELEMENT_FORM_FIELD_TITLE);
        $titleField->setLabel('Title');
        $titleField->setDescription('This is the title of the CMS element.');
        $titleField->setAttrib('class', 'inp-form');
        $titleField->setValue($this->getTitle());
        $fields[] = $titleField;

        return $fields;
    }

    public function saveEditorField(Zend_Form_Element $field)
    {

        $name = $field->getName();
        $value = $field->getValue();

        if ($name == self::CMS_ELEMENT_FORM_FIELD_TITLE) {
            $this->setTitle($value);

            return true;
        }

        return false;

    }

    public function hasContentToDisplay()
    {
        return (strlen($this->getTitle() . $this->getContent()) > 0);
    }

    public  function getPixelWidth() {
        $pixelsPerCol = 95; // depends on the CSS
        return $this->getWidth() * $pixelsPerCol;
    }

}