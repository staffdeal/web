<?php

/**
 * Outputs an debug - log
 *
 * @param $message
 */
function debug($message)
{
	_logger()->debug($message);
}

/**
 * echos a styled print_r
 *
 * @param $obj mixed
 */
function debugPrint($obj)
{
	echo "<pre>";
	print_r($obj);
	echo "</pre>";
}

/**
 * Returns the Zend Logger
 *
 * @return Epr_Log
 */
function _logger()
{
	if (Zend_Registry::isRegistered('logger')) {
		return Zend_Registry::get('logger');
	} else {
		echo "Logger unregistred";
	}
}

function _timeSince($timestamp)
{
	return Epr_System::time_since($timestamp);
}

/**
 * Returns the current User
 *
 * @return Epr_User
 */

function _user()
{
    $user = Epr_System::getUser();
    if (!$user) {
        $token = _token();
        if ($token instanceof Epr_Token) {
            $user = $token->getUser();
        }
    };

    return $user;
}

/**
 * Returns the API token
 *
 * @return Epr_Token
 */
function _token()
{
	$token = null;
	if (Zend_Registry::isRegistered('apiToken')) {
		$tmpToken = Zend_Registry::get('apiToken');
		if ($tmpToken instanceof Epr_Token) {
			$user = $tmpToken->getUser();
			if ($user instanceof Epr_User) {
				if ($user->isActive()) {
					$token = $tmpToken;
				}
			}
		}
	}

	return $token;
}

function _requestedAPIVersion() {
    $version = 1;
    if (Zend_Registry::isRegistered('APIVersion')) {
        $version = Zend_Registry::get('APIVersion');
    } else {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        if ($request) {
            $version = max(1, (int)$request->getHeader('X-API-Version', 1));
            Zend_Registry::set('APIVersion', $version);
        }
    }
    return $version;
}

function _tempDir()
{
	return Epr_System::getTempDir();
}

/**
 * Returns the Database-Connection
 *
 * @return Doctrine\ODM\CouchDB\DocumentManager
 */
function _dm()
{
	return Epr_System::dm();
}

/**
 * Returns the Session
 */

function _session()
{
	return Epr_System::getSession();
}

/**
 * Returns the application object.
 *
 * @return Epr_Application
 */
function _app()
{
	return (Zend_Registry::isRegistered('app')) ? Zend_Registry::get('app') : Epr_Application::application();
}

/**
 * Returns the dispatcher's config.
 *
 * @return Zend_Config_Ini
 */
function _dispatcher()
{
	return Zend_Registry::get('dispatcher');
}

function _dbDateTimeFormat()
{
	return 'Y-m-d H:i:s.u';
}

/**
 * @return Epr_Theme
 */
function _theme()
{
	return Epr_Theme_Collection::getActiveTheme();
}

/**
 * Returns the mail-Transport
 *
 * @author janfanslau
 * @return Zend_Mail_Transport_Smtp
 */

function _mailTransport()
{
	return Epr_System::getMailTransport();
}

function xmlentities($string)
{
	return htmlspecialchars($string, ENT_QUOTES);
}

function _isDebugging()
{
	return (isset($_GET['start_debug']) && $_GET['start_debug'] == '1');
}

function _clientVersion() {

    $version = 0;
    if (Zend_Registry::isRegistered('APIVersion')) {
        $version = Zend_Registry::get('APIVersion');
    } else {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        if ($request) {
            $version = (int)$request->getHeader('X-API-Version', 0);
            if ($version <= 0) {
                $version = (int)$request->getParam('v', 1);
            }
        }
        $version = max(1, $version);
        Zend_Registry::set('APIVersion', $version);
    }

    return max($version, 1);
}

/**
 * Returns the translated value of the given string. If no translation is found, the string is return.
 * @param $string
 * @return null|string
 */
function _t($string)
{

	$result = null;

	if (strlen($string) > 0) {
		$translator = (Zend_Registry::isRegistered(Plugins_Translate::REGISTRY_KEY)) ? Zend_Registry::get(Plugins_Translate::REGISTRY_KEY) : null;
		if ($translator instanceof Zend_Translate) {
			$result = $translator->_($string);
		}
	}

	if (is_null($result)) {
		$result = $string;
	}

	if (outlineTranslation()) {
		$result = '<span class="translate">' . $result . '</span>';
	}

	return $result;
}

/**
 * Returns the translated and formatted string.
 * @param $string
 * @return mixed|null|string
 */
function _f($string) {

    $formatted = null;

    $arguments = func_get_args();
    if (count($arguments) >= 2) {
        $values = array_splice($arguments, 1);
        $translated = array(_t($string));
        $formatted = call_user_func_array('sprintf', array_merge($translated, $values));
    } else {
        $formatted = _t($string);
    }

    return $formatted;
}

function _n($singular, $plural, $number) {
    $translated = _t(($number == 1) ? $singular : $plural);
    return sprintf($translated, $number);
}

function outlineTranslation() {
	$session = new Zend_Session_Namespace('translation');

	$outline = false;
	if (isset($session->outline)) {
		$outline = ($session->outline == true);
	}
	return $outline;
}

function toggleOutlineTranslation() {

	$session = new Zend_Session_Namespace('translation');

	$outline = false;
	if (isset($session->outline)) {
		$outline = ($session->outline == true);
	}

	$outline = !$outline;
	$session->outline = $outline;

}

function is_iterable($var) {
	return (is_array($var) || $var instanceof Traversable || $var instanceof stdClass);
}

function getSortViewName($sortKey) {
    return 'sortBy' . ucfirst($sortKey);
}

/**
 * EPR System Class
 * (c)2012 by Jan Fanslau
 *
 * @version 1.0
 *
 */

class Epr_System
{

	/** @var Epr_User */
	static $user;

	static $geocoder;


	/**
	 * Returns the current logged in user
	 *
	 * @return Epr_User
	 */
	static function getUser()
	{

		if (isset(self::$user)) {
			return self::$user;
		}

		$auth = Zend_Auth::getInstance()->getIdentity();
		if (!is_null($auth)) {
			self::$user = _dm()->getRepository('Epr_User')->find($auth);

			return self::$user;
		}

		return false;
	}

	static function setUser($user)
	{
		$sesUser = new Zend_Session_Namespace('sesUser');
		if ($user instanceof Epr_User) {
			$sesUser->user = $user;
		} else {
			$sesUser->user = new Epr_User('info@gibts.net', Epr_User::REGISTRATION_CONTEXT_ADMIN);
		}
	}


	/**
	 * @return \Geocoder\Geocoder
	 */
	static public function getGeocoder()
	{
		if (is_null(self::$geocoder)) {

			$adapter        = new \Geocoder\HttpAdapter\CurlHttpAdapter();
			self::$geocoder = new \Geocoder\Geocoder();

			self::$geocoder->registerProviders(array(
													new \Geocoder\Provider\GoogleMapsProvider($adapter)
											   ));

		}

		return self::$geocoder;
	}


	/**
	 * Returns the databaseconnection
	 *
	 * @return Zend_Db
	 */
	static function db()
	{
		return Zend_Registry::get('db');
	}

	/**
	 * Returns the databaseconnection
	 *
	 * @return Doctrine\ODM\CouchDB\DocumentManager
	 */
	static function dm()
	{
		return Zend_Registry::get('dm');
	}

	static function dbCache()
	{
		return Zend_Registry::get('dbCache');
	}

	/**
	 * returns the Base URL
	 *
	 * @return String
	 */
	static function getBaseURL()
	{
		return Zend_Registry::get('getBaseURL');
	}


	/**
	 * Returns the Root Directory
	 *
	 * @return String
	 */
	static function getRootDir()
	{
		return Zend_Registry::get('rootDir');
	}

	/**
	 * Returns the Root Directory
	 *
	 * @return String
	 */
	static function getTempDir()
	{
		return Zend_Registry::get('tempDir');
	}

	/**
	 * Returns a string with the effective path of the given path with. If the path is relative, it is resolved with respect of the given root path.
	 *
	 * @param $path
	 * @param $rootPath
	 *
	 * @return string
	 */
	static function getEffectivePath($path, $rootPath = null)
	{

		$path = __($path);

		$startCharacter = $path->first();
		if ((string)$startCharacter !== '/') {
			if (is_null($rootPath)) {
				$rootPath = ROOT_PATH;
			}
			$path = __($rootPath)->ensureLastCharacter('/')->addString($path);
		}

		return (string)$path;

	}


	/**
	 * Return random String
	 *
	 * @return String
	 */

	static function getRandomString($lenth = 25)
	{
		// makes a random alpha numeric string of a given lenth
		$aZ09 = array_merge(range('A', 'Z'), range('a', 'z'), range(0, 9));
		$out  = '';
		for ($c = 0; $c < $lenth; $c++) {
			$out .= $aZ09[mt_rand(0, count($aZ09) - 1)];
		}

		return $out;
	}

	static function cleanString($string, $quote_style = ENT_NOQUOTES, $charset = false, $double_encode = false)
	{
		$string = (string)$string;

		if (0 === strlen($string)) {
			return '';
		}

		$string = str_replace(" ", "_", $string);

		return $string;
	}

	static function revertURL2Community($string)
	{
		$string = str_replace("_", " ", $string);

		return $string;
	}

	static function getTextForLink($text)
	{
		if (empty($text)) {
			return $text;
		}

		$aTransition = array(
			'$'  => '-',
			'@'  => 'at',
			'€'  => 'e',
			'.'  => '-',
			' '  => '-',
			'_'  => '-',
			'¡'  => 'i',
			'¢'  => '-',
			'£'  => '-',
			'¤'  => '-',
			'¥'  => 'Y',
			'¦'  => '-',
			'§'  => 'S',
			'¨'  => '-',
			'©'  => 'C',
			'ª'  => 'a',
			'«'  => '-',
			'¬'  => '-',
			'®'  => 'R',
			'¯'  => '-',
			'°'  => '-',
			'±'  => '-',
			'²'  => '-',
			'³'  => '3',
			'´'  => '-',
			'µ'  => 'mu',
			'¶'  => '-',
			'·'  => '-',
			'¸'  => '-',
			'¹'  => '1',
			'º'  => '-',
			'»'  => '-',
			'¼'  => '1-4',
			'½'  => '1-2',
			'¾'  => '3-4',
			'¿'  => '-',
			'À'  => 'A',
			'Á'  => 'A',
			'Â'  => 'A',
			'Ã'  => 'A',
			'Ä'  => 'A',
			'Å'  => 'A',
			'Æ'  => 'AE',
			'Ç'  => 'C',
			'È'  => 'E',
			'É'  => 'E',
			'Ê'  => 'E',
			'Ë'  => 'E',
			'Ì'  => 'I',
			'Í'  => 'I',
			'Î'  => 'I',
			'Ï'  => 'I',
			'Ð'  => 'D',
			'Ñ'  => 'N',
			'Ò'  => 'O',
			'Ó'  => 'O',
			'Ô'  => 'O',
			'Õ'  => 'O',
			'Ö'  => 'O',
			'×'  => 'x',
			'Ø'  => 'O',
			'Ù'  => 'U',
			'Ú'  => 'U',
			'Û'  => 'U',
			'Ü'  => 'U',
			'Ý'  => 'Y',
			'Þ'  => 'P',
			'ß'  => 'ss',
			'á'  => 'a',
			'â'  => 'a',
			'ã'  => 'a',
			'ä'  => 'a',
			'å'  => 'a',
			'æ'  => 'ae',
			'ç'  => 'c',
			'è'  => 'e',
			'é'  => 'e',
			'ê'  => 'e',
			'ë'  => 'e',
			'ì'  => 'i',
			'?*' => 'i',
			'î'  => 'i',
			'ï'  => 'i',
			'ð'  => 'o',
			'ñ'  => 'n',
			'ò'  => 'o',
			'ó'  => 'o',
			'ô'  => 'o',
			'õ'  => 'o',
			'ö'  => 'o',
			'÷'  => '-',
			'ø'  => 'o',
			'ù'  => 'u',
			'ú'  => 'u',
			'û'  => 'u',
			'ü'  => 'u',
			'ý'  => 'y',
			'þ'  => 'b',
			'ÿ'  => 'y'
		);

		$text = strtr($text, $aTransition);

		// restliche Zeichen: weg damit!
		$text = preg_replace("=[^[:alnum:]\-$]=is", '', $text);

		// doppelte -- weg
		$text = preg_replace("=\-{3}=is", '-', $text);
		$text = preg_replace("=\-{2}=is", '-', $text);

		return $text;
	}


	static function getMailTransport()
	{
		return Zend_Registry::get('mailTransport');
	}


	static function getSessionController()
	{
		return _session()->controller;
	}

	static function getSessionAction()
	{
		return _session()->action;
	}

	static function getSessionRequestParams()
	{
		return _session()->params;
	}

	static function setSessionController($controller)
	{
		$sesSystem = new Zend_Session_Namespace('sesSystem');
		unset($sesSystem->controller);

		$sesSystem->controller = $controller;
	}

	static function setSessionRequestParams($params)
	{
		$sesSystem = new Zend_Session_Namespace('sesSystem');
		unset($sesSystem->params);

		$sesSystem->params = $params;
	}

	static function setSessionAction($action)
	{
		$sesSystem = new Zend_Session_Namespace('sesSystem');
		unset($sesSystem->action);
		$sesSystem->action = $action;

	}


	static function getUniqueId()
	{
		return sprintf('%03d-%04x%04x-%04x-%03x4-%04x-%04x%04x%04x', Epr_System::ipv4AsArray(4), // 24 bit for the server's ip address
			mt_rand(0, 65535), mt_rand(0, 65535), // 32 bits for "time_low"
			mt_rand(0, 65535), // 16 bits for "time_mid"
			mt_rand(0, 4095), // 12 bits before the 0100 of (version) 4 for "time_hi_and_version"
			bindec(substr_replace(sprintf('%016b', mt_rand(0, 65535)), '01', 6, 2)), // 8 bits, the last two of which (positions 6 and 7) are 01, for "clk_seq_hi_res"
			// (hence, the 2nd hex digit after the 3rd hyphen can only be 1, 5, 9 or d)
			// 8 bits for "clk_seq_low"
			mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535) // 48 bits for "node"
		);
	}


	static function ipv4AsArray($block = 0)
	{
		$data = explode('.', $_SERVER['SERVER_ADDR']);
		if ($block == 0) {
			return $data;
		} elseif (1 <= $block && $block <= 4) {
			return $data[$block - 1];
		} else {
			return false;
		}
	}

	static function getSession()
	{
		$sesSystem = new Zend_Session_Namespace('sesSystem');

		return $sesSystem;
	}


	// ATOM Zeitstempel umwandeln
	static function time_since($your_timestamp)
	{
		$unix_timestamp = strtotime($your_timestamp);
		$seconds        = time() - $unix_timestamp;
		$minutes        = 0;
		$hours          = 0;
		$days           = 0;
		$weeks          = 0;
		$months         = 0;
		$years          = 0;

		if ($seconds == 0) {
			$seconds = 1;
		}
		if ($seconds > 60) {
			$minutes = $seconds / 60;
		} else {
			return Epr_System::germanize_time_since($seconds, 'Sekunde');
		}

		if ($minutes >= 60) {
			$hours = $minutes / 60;
		} else {
			return Epr_System::germanize_time_since($minutes, 'Minute');
		}

		if ($hours >= 24) {
			$days = $hours / 24;
		} else {
			return Epr_System::germanize_time_since($hours, 'Stunde');
		}

		if ($days >= 7) {
			$weeks = $days / 7;
		} else {
			return Epr_System::germanize_time_since($days, 'Tag');
		}

		if ($weeks >= 4) {
			$months = $weeks / 4;
		} else {
			return Epr_System::germanize_time_since($weeks, 'Woche');
		}

		if ($months >= 12) {
			$years = $months / 12;

			return Epr_System::germanize_time_since($years, 'Jahr');
		} else {
			return Epr_System::germanize_time_since($months, 'Monat');
		}
	}

	//Zeitangabe korrigieren
	static function germanize_time_since($num, $word)
	{
		$num   = floor($num);
		$addon = "n";
		if ($word == "Tag" || $word == "Monat") {
			$addon = "en";
		}

		if ($num == 1) {
			return "Vor " . $num . " " . $word;
		} else {
			return "Vor " . $num . " " . $word . $addon;
		}
	}


	static function http_file_exists($url)
	{
		$f = @fopen($url, "r");
		if ($f) {
			fclose($f);

			return true;
		}

		return false;
	}

	/**
	 * Returns the number of weeks in a year
	 */
	static function weeksPerYear($year)
	{
		return date("W", mktime(0, 0, 0, 12, 28, $year));
	}

	/**
	 * Returns an empty gif image for cookie
	 */
	static function getEmptyGif()
	{

		$id    = ImageCreate(1, 1);
		$black = ImageColorAllocate($id, 0, 0, 0);
		$white = ImageColorAllocate($id, 255, 255, 255);
		$trans = ImageColorTransparent($id, $white);

		imagefill($id, 0, 0, $white);
		Header("Content-type: image/gif");
		ImageGif($id);
	}

    static function getVerbalTimeRangeExpression(\Poundation\PDateRange $range, $filter = null, $onlyLongest = false) {

        if (is_null($filter)) {
            $filter = \Poundation\PString::createFromString(\Poundation\PDateRange::DURATION_YEARS . \Poundation\PDateRange::DURATION_MONTHS . \Poundation\PDateRange::DURATION_DAYS . \Poundation\PDateRange::DURATION_HOURS . \Poundation\PDateRange::DURATION_MINUTES . \Poundation\PDateRange::DURATION_SECONDS);
        } else {
            $filter = \Poundation\PString::createFromString($filter);
        }

        $textComponents = new \Poundation\PArray();
        $components = $range->getDurationComponents();
        $continue = true;

        if ($continue && $components[\Poundation\PDateRange::DURATION_YEARS] > 0 && $filter->contains(\Poundation\PDateRange::DURATION_YEARS)) {
            $textComponents[] = _n("%u year", "%u years", $components[\Poundation\PDateRange::DURATION_YEARS]);
            $continue = !$onlyLongest;
        }

        if ($continue && $components[\Poundation\PDateRange::DURATION_MONTHS] > 0 && $filter->contains(\Poundation\PDateRange::DURATION_MONTHS)) {
            $textComponents[] = _n("%u month", "%u months",$components[\Poundation\PDateRange::DURATION_MONTHS]);
            $continue = !$onlyLongest;
        }

        if ($continue && $components[\Poundation\PDateRange::DURATION_DAYS] > 0 && $filter->contains(\Poundation\PDateRange::DURATION_DAYS)) {
            $textComponents[] = _n("%u day", "%u days",$components[\Poundation\PDateRange::DURATION_DAYS]);
            $continue = !$onlyLongest;
        }

        if ($continue && $components[\Poundation\PDateRange::DURATION_HOURS] > 0 && $filter->contains(\Poundation\PDateRange::DURATION_HOURS)) {
            $textComponents[] = _n("%u hour", "%u hours",$components[\Poundation\PDateRange::DURATION_HOURS]);
            $continue = !$onlyLongest;
        }

        if ($continue && $components[\Poundation\PDateRange::DURATION_MINUTES] > 0 && $filter->contains(\Poundation\PDateRange::DURATION_MINUTES)) {
            $textComponents[] = _n("%u minute", "%u minutes",$components[\Poundation\PDateRange::DURATION_MINUTES]);
            $continue = !$onlyLongest;
        }

        if ($continue && $components[\Poundation\PDateRange::DURATION_SECONDS] > 0 && $filter->contains(\Poundation\PDateRange::DURATION_SECONDS)) {
            $textComponents[] = _n("%u second", "%u seconds",$components[\Poundation\PDateRange::DURATION_SECONDS]);
        }

        return $textComponents->string(', ');
    }
}