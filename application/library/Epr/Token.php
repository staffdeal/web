<?php

/** @Document (repositoryClass="Epr_Token_Collection") */
class Epr_Token extends Epr_Document
{

    const DOCUMENTNAME = 'Epr_Token';

    const TYPE_API_ACCESS  = 'api';
    const TYPE_IOS_APN     = 'iosapn';
    const TYPE_ANDROID_GCM = 'androidgcm';

    /** @Id */
    protected $id;

    /** @Field(type="datetime") */
    protected $creationDate;

    /** @Field(type="datetime") */
    private $lastUsageDate;

    /** ReferenceOne(targetDocument="Epr_User") */
    
    /** @Field(type="string") */
    private $user;

    /** @Field(type="string") */
    private $tokenType = self::TYPE_API_ACCESS;

    /** @Field(type="string") */
    private $additionalInformation;

    /** @Field(type="string") */
    private $deviceToken;

    /** @Field(type="mixed") */
    private $topics = array();

    public function __construct($user, $type = self::TYPE_API_ACCESS)
    {

        parent::__construct();

        $this->user          = $user;
        $this->lastUsageDate = new DateTime();

        if ($type == self::TYPE_API_ACCESS || $type == self::TYPE_IOS_APN || $type == self::TYPE_ANDROID_GCM) {
            $this->tokenType = $type;
        }
    }

    /**
     * Creates a new API token.
     *
     * @param $user
     *
     * @return Epr_Token
     */
    static public function createAPIToken($user)
    {
        return new self($user, self::TYPE_API_ACCESS);
    }

    /**
     * Creates a new iOS APN token and assigns the given device token.
     *
     * @param string $deviceToken
     * @param Epr_User $user
     *
     * @return Epr_Token
     */
    static public function createIOSAPNToken($deviceToken, $user = null)
    {
        $token              = new self($user, self::TYPE_IOS_APN);
        $token->deviceToken = (string)$deviceToken;

        return $token;
    }

    /**
     * Creates a new Android GCM token and assigns the given device token.
     *
     * @param      $deviceToken
     * @param null $user
     *
     * @return Epr_Token
     */
    static function createAndroidGCMToken($deviceToken, $user = null)
    {
        $token              = new self($user, self::TYPE_ANDROID_GCM);
        $token->deviceToken = $deviceToken;

        return $token;
    }

    /**
     * Returns true if the token is a device token.
     *
     * @return bool
     */
    public function isDeviceToken()
    {
        return ($this->getType() == self::TYPE_IOS_APN || $this->getType() == self::TYPE_ANDROID_GCM);
    }

    /**
     * Returns the datetime of the last usage.
     *
     * @return DateTime
     */
    public function getLastUsageDate()
    {
        return $this->lastUsageDate;
    }

    /**
     * Sets all internal counters so this token appears to be in use.
     *
     * @return Epr_Token
     */
    public function markAsUsed()
    {
        $this->lastUsageDate = new DateTime();

        return $this;
    }

    /**
     * Returns the user of the token.
     *
     * @return Epr_User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Returns the type if the token (valid values are constants).
     *
     * @return string
     */
    public function getType()
    {
        return $this->tokenType;
    }

    /**
     * Returns a string that has been saved by calling setAdditionalInformation.
     *
     * @return mixed
     */
    public function getAdditionInformation()
    {
        return $this->additionalInformation;
    }

    /**
     * Sets an arbitrary information.
     *
     * @param $text
     *
     * @return $this
     */
    public function setAdditionalInformation($text)
    {
        $this->additionalInformation = (string)$text;

        return $this;
    }

    /**
     * Returns the device token.
     *
     * @return string|null
     */
    public function getDeviceToken()
    {
        return ($this->tokenType == self::TYPE_IOS_APN || $this->tokenType == self::TYPE_ANDROID_GCM) ? $this->deviceToken : null;
    }

    /**
     * Returns true if the token contains the given topic.
     *
     * @param $topic
     *
     * @return bool
     */
    public function containsTopic($topic)
    {
        if (is_array($this->topics)) {
            return (array_search((string)$topic, $this->topics) !== false);
        } else {
            return false;
        }
    }

    /**
     * Adds the given topic to the token.
     *
     * @param $topic
     *
     * @return $this
     */
    public function addTopic($topic)
    {
        if (!$this->containsTopic($topic)) {
            $this->topics[] = $topic;
        }

        return $this;
    }

    /**
     * Removes the given topic from the token.
     *
     * @param $topic
     *
     * @return $this
     */
    public function removeTopic($topic)
    {
        $index = array_search((string)$topic, $this->topics);
        if ($index !== false) {
            unset($this->topics[$index]);
        }

        return $this;
    }

    /**
     * Returns an array with the topics of the token.
     *
     * @return array
     */
    public function getTopics()
    {
        return $this->topics;
    }

    /**
     * Assigns the given user if the token has not been assigned before. This means that a token cannot be assigned to a different user.
     * True is returned if the user has been assigned successfully.
     *
     * @param Epr_User $user
     *
     * @return bool
     */
    public function assignUser(Epr_User $user)
    {
        if (is_null($this->user)) {
            $this->user = $user;

            return true;
        }

        return false;
    }

    /**
     * Creates a notification if this token can be used as receiver.
     *
     * @param null $message
     * @param null $sender
     *
     * @return Epr_Notification_APN|null
     */
    public function createNotification($message = null, $sender = null, $context = null)
    {

        $notification = null;

        if ($this->getType() == Epr_Token::TYPE_IOS_APN) {
            $notification = Epr_Notification_APN::createMessage($this, $message, $sender, $context);
        } else if ($this->getType() == Epr_Token::TYPE_ANDROID_GCM) {
            $notification = Epr_Notification_GCM::createMessage($this, $message, $sender, $context);
        }

        if ($notification) {
            if ($this->getUser()) {
                $notification->setReceiverName($this->getUser()->getEmail());
            } else {
                $notification->setReceiverName('Unknown User');
            }

            $this->markAsUsed();
        }


        return $notification;
    }

}
