<?php

class Epr_Dealer_Collection extends Epr_Collection implements Epr_Paginatable
{
    const SORT_DESIGN_DOCUMENT_NAME = 'sortDealers';

	const SEARCH_DESIGN_DOCUMENT_NAME = 'searchDealers';
	const SEARCH_ALL = 'all';

    static function getSortKeys()
    {
        return array(
            'title',
            'address.zip'
        );
    }

	static function getSearchNames()
	{
		return array_merge(parent::getSearchNames(), array(
			self::SEARCH_ALL
		));
	}

	public static function getSearchFieldsForName($name)
	{
		$fields = parent::getSearchFieldsForName($name);
		if ($name == self::SEARCH_ALL) {
			$fields = array_merge($fields, array(
				'address',
				'telephone',
				'web',
				'email',
				'openingHours',
				'title',
				'text'
			));
		}
		return $fields;
	}

	static function getSearchFiltersForName($name)
	{
		$fields = parent::getSearchFieldsForName($name);
		if ($name == self::SEARCH_ALL) {
			$fields = array_merge($fields, array(
				'active',
				'web',
				'title',
				'email',
				'isActive' => 'active'
			));
		}
		return $fields;
	}


	/**
	 * @return \Doctrine\ODM\CouchDB\DocumentRepository
	 */
	static function getRepository() {
		return _dm()->getRepository(Epr_Dealer::DOCUMENTNAME);
	}

	/**
	 * Returns all dealers
	 *
	 * @return array
	 */
	public static function getAll() {
		$result = self::getRepository()->findAll();

		return $result;
	}

	/**
	 * Returns all active dealers.
	 *
	 * @return array
	 */
	public static function getAllActive() {
		$query  = _dm()->createQuery('dealers', 'activeByTitle');
		$result = $query->onlyDocs(true)->execute();

		return $result;
	}

	/**
	 * Returns all dealers around the given location within a radius of the given accuracy.
	 * The returned dealer instances have a new property 'distance' with the distance in meters.
	 * Calculating is done with the help of the Expediter so this method relies on a connected dispatcher.
	 *
	 * @param Poundation\PCoordinate  $location
	 * @param int                     $accuracy The radius in meters.
	 */
	public static function getDealerAroundLocation(\Poundation\PCoordinate $location, $accuracy = 1000, $sort = SORT_ASC) {
		$result = null;

		if (is_integer($accuracy) && $accuracy > 0) {

			$module = _app()->getModuleWithIdentifier(Epr_Modules_Dispatcher::identifier());
			if ($module instanceof Epr_Modules_Dispatcher) {

				$action = 'dealers/' . $module->referenceId();
				$params = array(
					'distanceFrom' => $location->getLatitude() . ',' . $location->getLongitude()
				);

				$list = null;

				try {
					$list   = $module->call($action, $params);
				} catch (Exception $e) {
					_logger()->critical('Could not load dealers from dispatcher: ' . $e->getMessage());
				}

				if ($list instanceof stdClass) {
					if (isset($list->status) && $list->status == 'OK') {
						if (isset($list->dealers)) {
							$receivedDealers = $list->dealers;

							if (is_array($receivedDealers)) {
								$result = array();
								foreach ($receivedDealers as $receivedItem) {
									if (isset($receivedItem->id) && isset($receivedItem->distance)) {
										$distance = (int)$receivedItem->distance;
										if ($distance <= $accuracy) {
											$dealerId = $receivedItem->id;
											$dealer   = _dm()->find(Epr_Dealer::DOCUMENTNAME, $dealerId);
											if ($dealer instanceof Epr_Dealer) {
												$dealer->distance = $distance;
												$result[]         = $dealer;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if (is_array($result)) {
			// for sorting we provide a closure with the sorting logic
			usort($result, function ($a, $b) use ($sort) {
				if (isset($a->distance) && isset($b->distance)) {
					$da = $a->distance;
					$db = $b->distance;

					if ($da == $db) {
						return 0;
					} else if ($da < $db) {
						return ($sort == SORT_ASC) ? -1 : 1;
					} else {
						return ($sort == SORT_ASC) ? 1 : -1;
					}
				} else {
					return 0;
				}
			});

		}

		return $result;
	}

	static function getDealersMatching($query, $sortKey = null, $sortDirection = SORT_ASC, $count = 10, $offset = 0) {
		return self::getMatching(self::SEARCH_ALL, $query, $sortKey, $sortDirection, $count, $offset);
	}

}
