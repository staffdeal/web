<?php

/** @Document (repositoryClass="Epr_Dealer_Collection") */
class Epr_Dealer extends Epr_Document_WebPage {

	const DOCUMENTNAME = 'Epr_Dealer';

	/** @Id */
	protected $id;

	/** @Field(type="address") */
	private $address = null;

	/** @Field(type="string") */
	private $telephone;

	/** @Field(type="url") */
	private $web;

	/** @Field(type="mail") */
	private $email;

	/** @Field(type="string") */
	private $openingHours;

	/** @var @Field(type="mixed") */
	private $additionCategories = array();

	static public function createFromDispatchDealer($data) {

		$dealer = null;

		if ($data instanceof stdClass && isset($data->name)) {
			$dealer = new self();

			$dealer->setTitle($data->name);

			if (isset($data->telephone)) {
				$dealer->setTelephone($data->telephone);
			}

			if (isset($data->url)) {
				$dealer->setWeb(\Poundation\PURL::URLWithString($data->url));
			}

			if (isset($data->email)) {
				$dealer->setEmail(\Poundation\PMailAddress::createFromString($data->email));
			}

			if (isset($data->address) && isset($data->address->street)) {

				$address = new \Poundation\PAddress();
			    if (isset($data->address->street)) {
					$address->setStreet($data->address->street);
				}
				if (isset($data->address->zip)) {
					$address->setZip($data->address->zip);
				}
				if (isset($data->address->city)) {
					$address->setCity($data->address->city);
				}
				if (isset($data->address->country)) {
					$address->setCountry($data->address->country);
				}

				if (isset($data->location) && isset($data->location->lat) && isset($data->location->lon)) {
					$lat = $data->location->lat;
					$lon = $data->location->lon;

					if (is_float($lat) && is_float($lon)) {
						$coordinate = \Poundation\PCoordinate::createCoordinate($lat, $lon);

						if ($coordinate) {
							  $address->setCoordinate($coordinate);
						}
					}

				}

				$dealer->setAddress($address);
			}




		}

		return $dealer;

	}

	/**
	 * Returns the address object of the dealer.
	 *
	 * @return Poundation\PAddress
	 */
	public function getAddress() {
		return $this->address;
	}

	/**
	 * Sets the address object.
	 *
	 * @param Poundation\PAddress $address
	 *
	 * @return Epr_Dealer
	 */
	public function setAddress(\Poundation\PAddress $address) {
		$this->address = $address;

		return $this;
	}

	/**
	 * Removes the address.
	 *
	 * @return Epr_Dealer
	 */
	public function removeAddress() {
		$this->address = null;

		return $this;
	}

	/**
	 * Returns the telefon string.
	 *
	 * @return string
	 */
	public function getTelephone() {
		return $this->telephone;
	}

	/**
	 * Sets the telefon string.
	 *
	 * @param $telefon string
	 *
	 * @return Epr_Dealer
	 */
	public function setTelephone($telephone) {
		$this->telephone = $telephone;

		return $this;
	}

	/**
	 * Returns the web URL or null if it is empty or invalid.
	 *
	 * @return null|Poundation\PURL
	 */
	public function getWeb() {
		if (!$this->web instanceof \Poundation\PURL && !is_null($this->web)) {
			$this->web = \Poundation\PURL::URLWithString($this->web);
		}

		return $this->web;
	}

	/**
	 * Sets the web URL.
	 *
	 * @param $url
	 *
	 * @return Epr_Dealer
	 */
	public function setWeb($url) {
		if ($url instanceof \Poundation\PURL) {
			$this->web = $url;
		} else {
			$this->web = \Poundation\PURL::URLWithString($url);
		}

		return $this;
	}

	/**
	 * Returns the mail address.
	 *
	 * @return null|Poundation\PMailAddress
	 */
	public function getEmail() {
		if (!$this->email instanceof \Poundation\PMailAddress && !is_null($this->email)) {
			$this->email = \Poundation\PMailAddress::createFromString($this->email);
		}

		return $this->email;
	}

	/**
	 * Sets the mail address-
	 *
	 * @param $email
	 *
	 * @return Epr_Dealer
	 */
	public function setEmail($email) {
		if ($email instanceof \Poundation\PMailAddress) {
			$this->email = $email;
		} else {
			$this->email = \Poundation\PMailAddress::createFromString($email);
		}

		return $this;
	}

	/**
	 * Returns the text describing the opening hours.
	 *
	 * @return string
	 */
	public function getOpeningHours() {
		return $this->openingHours;
	}

	/**
	 * Sets the text describing the opening hours.
	 *
	 * @param $text
	 *
	 * @return Epr_Dealer
	 */
	public function setOpeningHours($text) {
		$this->openingHours = $text;

		return $this;
	}

	/**
	 * Sets a value of an additional category.
	 *
	 * @param $index
	 * @param $value
	 *
	 * @return Epr_Dealer
	 */
	public function setAdditionCategoryValue($index, $value) {
		if (is_integer($index) && is_integer($value)) {
			if ($index >= 0 && $value >= 0) {
				$this->additionCategories[$index] = $value;
			}
		}

		return $this;
	}

	/**
	 * Returns the value if the addition category with the given index.
	 *
	 * @param $index
	 *
	 * @return integer|null
	 */
	public function getAdditionCategoryValue($index) {
		return (isset($this->additionCategories[$index]) ? $this->additionCategories[$index] : null);
	}

	public function hasCoordinate() {
		if ($this->address instanceof \Poundation\PAddress) {
			return ($this->address->getCoordinate() instanceof \Poundation\PCoordinate);
		} else {
			return false;
		}
	}
}
