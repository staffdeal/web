<?php
use Poundation\PArray;

/**
 * meinERP
 * User: Jan Fanslau
 * Date: 08.09.12
 * Time: 12:56
 */
use Doctrine\ODM\CouchDB\DocumentRepository;
use Doctrine\CouchDB\View\FolderDesignDocument;

class Epr_User_Collection extends Epr_Collection implements Epr_Paginatable, Epr_Searchable
{

    const SORT_DESIGN_DOCUMENT_NAME = 'sortUsers';

    const SEARCH_DESIGN_DOCUMENT_NAME = 'searchUsers';
    const SEARCH_ALL = 'all';

	/**
	 * @return \Doctrine\ODM\CouchDB\DocumentRepository
	 */
	static function getRepository() {
		return _dm()->getRepository(Epr_User::DOCUMENTNAME);
	}

    static function getSortKeys()
    {
        return array(
            'firstname',
            'lastname',
            'email',
            'role',
            'lastLogin',
            'lastMobileLogin'
        );
    }

    static function getSearchNames()
    {
        return array_merge(parent::getSearchNames(), array(
            self::SEARCH_ALL
        ));
    }

    public static function getSearchFieldsForName($name)
    {
        $fields = parent::getSearchFieldsForName($name);
        if ($name == self::SEARCH_ALL) {
            $fields = array_merge($fields, array(
                'firstname',
                'lastname',
                'email',
                'registrationInformation'
            ));
        }
        return $fields;
    }

    static function getSearchFiltersForName($name)
    {
        $fields = parent::getSearchFiltersForName($name);
        if ($name == self::SEARCH_ALL) {
            $fields = array_merge($fields, array(
                'role',
                'firstname',
                'lastname',
                'email',
                'isActive' => 'active'
            ));
        }
        return $fields;
    }


    /**
     * @param $id
     * @return null|Epr_User
     */
    static function getUserWithIdentifier($id) {
        return (strlen($id) > 0) ? _dm()->find(Epr_User::DOCUMENTNAME, $id) : null;
    }

    static function getAllGuests()
    {
		throw new Exception('deprecated call of getAllGuests');
    }

    static function getByRole($role)
    {
		if (Epr_Roles::allRoles()->allKeys()->contains($role)) {
			$roleUsers = self::getRepository()->findBy(array('role' => $role));
			return new \Poundation\PArray($roleUsers);
		} else {
			throw new Exception('Querying unknown user role ' . $role . '.');
		}
    }
    
    static function hasActiveRootUser() {
    	$repo = self::getRepository();
    	$activeAdmins = new \Poundation\PArray($repo->findBy(array('role' => Epr_Roles::ROLE_ROOT)));
    	return ($activeAdmins->count() > 0);
    }

    static function getAllUsers()
    {
		$repo = self::getRepository();
		$users = $repo->findAll();
		return new \Poundation\PArray($users);
    }

    static function getUsersMatching($query, $sortKey = null, $sortDirection = SORT_ASC, $count = 10, $offset = 0) {
        return self::getMatching(self::SEARCH_ALL, $query, $sortKey, $sortDirection, $count, $offset);
    }
}