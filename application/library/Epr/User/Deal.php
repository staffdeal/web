<?php
/**
 * Epr_User_Deal
 *
 * This is the wrapper Class for saving Deals in a User
 *
 * User: Jan Fanslau
 * Date: 21.04.13
 * Time: 16:42
 */

/** @Document */
class Epr_User_Deal extends Epr_Dispatcher_Deal
{

	const DOCUMENTNAME = "Epr_User_Deal";

	/** @Id */
	protected $id;

	/**
	 * @Field(type="datetime")
	 * @var $savingTime \DateTime
	 */
	public $savingTime;

	public function __construct(Epr_Dispatcher_Deal $deal)
	{
		//        parent::__construct();
		$this->setEndDate($deal->getEndDate());
		$this->setReferenceDealId($deal->getReferenceDealId());
		$this->setStartDate($deal->getStartDate());
		$this->setProvider($deal->getProvider());
		$this->setTeaser($deal->getTeaser());
		$this->setFeatured($deal->getFeatured());
		$this->setText($deal->getText());
		$this->setTitleImageURL($deal->getTitleImageURL());
		$this->setTitle($deal->getTitle());

		return $this;
	}

	/**
	 * @param \DateTime $savingTime
	 */
	public function setSavingTime($savingTime)
	{
		$this->savingTime = $savingTime;
	}

	/**
	 * @return \DateTime
	 */
	public function getSavingTime()
	{
		return $this->savingTime;
	}

}
