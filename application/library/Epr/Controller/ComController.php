<?php

class Epr_Controller_ComController extends Epr_Controller_ApiController {

    public function preDispatch() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

}