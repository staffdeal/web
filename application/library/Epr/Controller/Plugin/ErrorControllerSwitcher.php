<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 05.10.12
 * Time: 17:21
 */
class Epr_Controller_Plugin_ErrorControllerSwitcher extends Zend_Controller_Plugin_Abstract
{

	public function routeShutdown (Zend_Controller_Request_Abstract $request)
    {
        $front = Zend_Controller_Front::getInstance();

		$errorHandler = $front->getPlugin('Zend_Controller_Plugin_ErrorHandler');

        if (!($errorHandler instanceof Zend_Controller_Plugin_ErrorHandler)) {
            return;
        }
        $error = $front->getPlugin('Zend_Controller_Plugin_ErrorHandler');
        $testRequest = new Zend_Controller_Request_Http();
        $testRequest->setModuleName($request->getModuleName())
            ->setControllerName($error->getErrorHandlerController())
            ->setActionName($error->getErrorHandlerAction());
        if ($front->getDispatcher()->isDispatchable($testRequest)) {
            $error->setErrorHandlerModule($request->getModuleName());
        }
    }
}