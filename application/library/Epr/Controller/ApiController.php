<?php

class Epr_Controller_ApiController extends Epr_Controller_Action
{

    protected $requestedClientVersion = 1;

    public function init()
    {

        /** @var $request Zend_Controller_Request_Http */
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $this->view->assign('requestedApiVersion', $this->requestedClientVersion);
        $this->getResponse()->setHeader('X-Document-Version', Epr_Application::application()->getDocumentVersion());
    }

    public function preDispatch()
    {
        $this->_helper->layout()->setLayout('api-xml');
    }

    protected function registerContextForAction($action)
    {
        $contextSwitch = $this->_helper->getHelper('contextSwitch');
        $contextSwitch->addActionContext($action, 'xml')
            ->addActionContext($action, 'json')
            ->setAutoJsonSerialization(false)
            ->initContext(
                $this->getRequest()
                    ->getParam('format', 'xml')
            );
    }

    protected function registerContextsForAction($actions)
    {
        if (is_array($actions)) {
            foreach ($actions as $action) {
                $this->registerContextForAction($action);
            }
        }
    }

}
