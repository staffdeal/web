<?php

class Epr_Controller_Action_Helper_CustomUrl extends Zend_Controller_Action_Helper_Url
{

    /**
     * Route to be used for creating custom urls.
     *
     * @var string
     */
    protected $_routeName = 'lang_default';

    /**
     * Builds an custom URL, including language parameter.
     *
     * @param string $action
     * @param string $controller
     * @param string $module
     * @param string $options
     * @return Zend_Controller_Action_Helper_Url
     */
    public function custom($action, $controller = null, $module = null, $options = array())
    {
        $options['action'] = $action;

        if (!isset($options['language']))
        {
            $options['language'] = $this->getRequest()->getParam('language', 'de');
        }

        if (!is_null($controller)) {
            $options['controller'] = $controller;
        }

        if (!is_null($action)) {
            $options['action'] = $action;
        }

        if (!is_null($module)) {
            $options['module'] = $module;
        }

        return $this->url($options, $this->_routeName, true);
    }


}