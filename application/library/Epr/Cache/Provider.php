<?php


class Epr_Cache_Provider
{

	static private $frontendOptions;
	static private $backendOptions;

	static private $instance;

	protected $cache;

	/**
	 * Returns the shared cache provider instance.
	 *
	 * @return Epr_Cache_Provider
	 */
	static function getInstance()
	{
		if (is_null(self::$instance)) {

			if (is_array(self::$backendOptions) && is_array(self::$frontendOptions)) {
				self::$instance        = new self();
				self::$instance->cache = Zend_Cache::factory('Core', 'File', self::$frontendOptions, self::$backendOptions);
			}

		}

		return self::$instance;
	}


	/**
	 * Sets up the cache from Bootstrap.php
	 *
	 * @param array $options
	 */
	static public function setupCache($options = array())
	{

		self::$frontendOptions = array(
			'lifetime'                => $options['lifetime'],
			'automatic_serialization' => true
		);

		self::$backendOptions = array(
			'cache_dir' => $options['cacheDir']
		);

	}

	/**
	 * @return Zend_Cache
	 */
	public function getCache()
	{
		return $this->cache;
	}

}