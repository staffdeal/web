<?php

use Poundation\PMailAddress;

use Poundation\PDictionary;

/** @Document (repositoryClass="Epr_User_Collection") */
class Epr_User extends Epr_Document
{

    const DOCUMENTNAME = 'Epr_User';

    const REGISTRATION_CONTEXT_ADMIN = 0;
    const REGISTRATION_CONTEXT_WEB   = 1;
    const REGISTRATION_CONTEXT_API   = 2;

    /** @Id */
    protected $id;

    /**
     * @Field(type="string")
     */
    private $lastname;

    /** @Field(type="string") */
    private $firstname;

    private $fullname = null;

    /**
     * @Index
     * @Field(type="string")
     */
    private $email;

    /**
     * @index
     * @Field(type="string")
     */
    private $password;

    /**
     * @Index
     * @Field(type="string")
     */
    private $role;

    /** @var bool */
    private $hasAuth;

    /**
     * @var string
     * @Field(type="string")
     */
    private $salt;

    /** @Field(type="datetime") */
    private $lastLogin;

    /** @Field(type="datetime") */
    private $lastMobileLogin;

    private $associatedDealer;

    /** @Field(type="integer") */
    private $registrationContext;

    /** @Field(type="string") */
    private $registrationInformation;

    /** @Field(type="string") */
    private $verificationId;

    /** @ReferenceMany(targetDocument="Epr_User_Deal") */
    private $deals;

    /** @Field(type="string") */
    private $resetPasswordId;

    private $showBackendHints = true;

    /** @Field(type="mixed") */
    private $adminSettings;

    /** @Field(type="mixed") */
    private $dealCodes;

    /** @Field(type="boolean") */
    private $sendActivationMail;

    /**
     * @param PMailAddress $mail
     *
     * @return Epr_User|null
     */
    static public function getUserWithEmail(PMailAddress $mail)
    {

        $repo   = _dm()->getRepository(self::DOCUMENTNAME);
        $result = $repo->findBy(array('email' => (string)$mail));

        return (count($result) > 0) ? $result[0] : null;

    }

    public function __construct($email, $registrationContext)
    {
        parent::__construct();

        if (__($email)->length() > 0) {
            $this->setEmail($email);
        } else {
            throw new Exception('A user can only be instantiated with an email address.');
        }

        if (is_int($registrationContext)) {
            $this->registrationContext = $registrationContext;
        } else {
            throw new Exception('Registration context must be an integer.');
        }

        if ($registrationContext == Epr_User::REGISTRATION_CONTEXT_WEB) {
            $this->sendActivationMail = true;
        }
    }

    /**
     * Sets the last name of the user.
     *
     * @param string $lastname
     * @returns $this
     */
    public function setLastname($lastname)
    {
        $this->lastname = (strlen($lastname) > 0) ? $lastname : null;
        $this->fullname = null;
        return $this;
    }

    /**
     * Returns the last name of the user.
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Sets the firstname of the user.
     *
     * @param string $firstname
     * @returns $this
     */
    public function setFirstname($firstname)
    {
        $this->firstname = (strlen($firstname) > 0) ? $firstname : null;
        $this->fullname = null;
        return $this;
    }

    /**
     * Returns the firstname of the user.
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    public function getFullname() {

        if (is_null($this->fullname)) {
            $components = \Poundation\PArray::create();
            $firstName = $this->getFirstname();
            if ($firstName) {
                $components->add($firstName);
            }
            $lastName = $this->getLastname();
            if ($lastName) {
                $components->add($lastName);
            }
            $this->fullname = $components->string(' ');
        }

        return $this->fullname;
    }

    /**
     * Sets the mail address of the user.
     *
     * @param Poundation\PMailAddress $email
     */
    public function setEmail($email)
    {
        if (!$email instanceof PMailAddress) {
            $email = PMailAddress::createFromString($email);
        }
        $this->email = $email;
    }

    /**
     * Returns the mail address of the user.
     *
     * @return \Poundation\PMailAddress
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the context used to create the user.
     *
     * @return int
     */
    public function getRegistrationContext()
    {
        return (int)$this->registrationContext;
    }

    public function setPassword($password)
    {
        $globalSalt = false;
        if (Zend_Registry::isRegistered('salt')) {
            $globalSalt = Zend_Registry::get('salt');
        } else {
            throw new Exception('No global salt found. How on hell did you bypass the bootstrapper?');
        }

        $this->generateSalt();
        $userSalt = $this->getSalt();

        $this->password        = md5($globalSalt . $password . $userSalt);
        $this->resetPasswordId = null;
    }

    public function checkPassword($input)
    {

        $globalSalt = false;
        $userSalt   = $this->getSalt();

        if (Zend_Registry::isRegistered('salt')) {
            $globalSalt = Zend_Registry::get('salt');
        } else {
            throw new Exception('No global salt found. How on hell did you bypass the bootstrapper?');
        }

        $hash = md5($globalSalt . $input . $userSalt);

        return ($this->password === $hash);
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function generateSalt()
    {
        $this->salt = mt_rand();
    }


    public function resetPassword()
    {
        $this->resetPasswordId = \Poundation\PString::createUUID();

        return (string)$this->resetPasswordId;
    }

    public function abortResetPassword()
    {
        $this->resetPasswordId = null;

        return true;
    }

    /**
     * Returns the role identifier.
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Sets the role identifier if it is known.
     *
     * @param string $role
     *
     * @throws Exception
     */
    public function setRole($role)
    {
        _logger()->debugPrint(Epr_Roles::allRoles());
        if (Epr_Roles::allRoles()->allKeys()->contains($role)) {

            $this->role = $role;
        } else {
            throw new Exception('Unknown user role ' . $role . '.');
        }

    }

    public function isAdministrator()
    {
        return ($this->getRole() === Epr_Roles::ROLE_ADMIN || $this->getRole() === Epr_Roles::ROLE_ROOT);
    }

    public function isRoot()
    {
        return ($this->getRole() === Epr_Roles::ROLE_ROOT);
    }

    public function setAsAuthenticated($hasAuth)
    {
        $this->hasAuth = $hasAuth;
        $this->markAsLoggedIn();
    }

    public function hasAuth()
    {
        return $this->hasAuth;
    }

    public function markAsWebLoggedIn()
    {
        $this->lastLogin = new DateTime();
        $this->hasAuth = true;
    }

    /**
     * Returns the last login or null.
     *
     * @return null|DateTime
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }


    /**
     * Returns the last mobile login or null.
     *
     * @return null|DateTime
     */
    public function getLastMobileLogin()
    {
        return $this->lastMobileLogin;
    }

    /**
     * Sets the mobile login timestamp.
     *
     * @return Epr_User
     */
    public function markAsMobileLoggedIn()
    {
        $this->lastMobileLogin = new DateTime();
        return $this;
    }

    /**
     * Returns the associated dealer.
     *
     * @return Epr_Dealer
     */
    public function associatedDealer()
    {
        return $this->associatedDealer;
    }

    /**
     * Associates the user with the given dealer.
     *
     * @param Epr_Dealer $dealer
     *
     * @return Epr_User
     */
    public function associateWithDealer(Epr_Dealer $dealer, $selectedDealer = null)
    {
        $this->associatedDealer = $dealer;
        $text = 'Associated Dealer: ' . $dealer->getTitle() . ', ' . $dealer->getAddress();
        if ($selectedDealer instanceof Epr_Dealer) {
            $text .= "\n" . 'Selected Dealer: ' . $selectedDealer->getTitle() . ', ' . $dealer->getAddress();
        }
        $this->setRegistrationInformation($text);

        return $this;
    }

    /**
     * @return Epr_Dealer
     */
    public function getAssociatedDealer()
    {
        return $this->associatedDealer;
    }

    /**
     * Returns true if the user has been verified.
     *
     * @return bool
     */
    public function isVerified()
    {
        return (is_null($this->verificationId));
    }

    /**
     * Returns the verification id. If none is present, a new one will be generated.
     * Use isVerificationIdValid to check if an id correct.
     *
     * @return string
     */
    public function getVerificationId()
    {
        if ($this->verificationId == null) {

            $this->verificationId = (string)\Poundation\PString::createUUID();

        }

        return $this->verificationId;
    }

    /**
     * Returns true of the given id matches the saved verification id.
     *
     * @param $id
     *
     * @return bool
     */
    public function isVerificationIdValid($id)
    {

        if (is_null($this->verificationId)) {
            return false;
        } else {
            return ($id === $this->verificationId);
        }

    }

    /**
     * Marks the user as verified.
     *
     * @return $this
     */
    public function markAsVerified()
    {
        $this->verificationId = null;

        return $this;
    }

    public function getRegistrationInformation()
    {
        return $this->registrationInformation;
    }

    public function setRegistrationInformation($value)
    {
        $this->registrationInformation = $value;

        return $this;
    }


    /**
     * Get either a Gravatar URL or complete image tag for a specified email address.
     *
     * @param string $email The email address
     * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
     * @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
     * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
     *
     * @return String containing either just a URL or a complete image tag
     * @source http://gravatar.com/site/implement/images/php/
     */
    function getAvatarUrl($s = 80, $d = 'mm', $r = 'g')
    {
        $url = 'http://www.gravatar.com/avatar/';
        $url .= md5(strtolower(trim((string)$this->getEmail())));
        $url .= "?s=$s&d=$d&r=$r";

        return $url;
    }


    /**
     * @param $deals
     * @deprecated
     */
    public function setDeals($deals)
    {
        $this->deals = $deals;
    }

    /**
     * @return array
     */
    public function getDeals()
    {
        return Epr_Deal_Collection::getProvidedDealsSavedByUser($this);
    }

    /**
     * @param $id
     * @return Epr_User_Deal|null
     * @deprecated
     */
    public function getDealWithId($id)
    {

        $deal = null;

        foreach ($this->getDeals() as $candidate) {
            if ($candidate instanceof Epr_User_Deal) {
                if ($candidate->getReferenceDealId() == $id || $candidate->getId() == $id) {
                    $deal = $candidate;
                    break;
                }
            }
        }

        return $deal;
    }

    /**
     * @param Epr_User_Deal $deal
     * @deprecated
     */
    private function addDeal(Epr_User_Deal $deal)
    {
        $this->deals[] = $deal;
    }


    public function sendMail($subject, $body, $isHTML = false)
    {

        $subject = (string)$subject;
        $body    = (string)$body;

        if (strlen($subject) > 0 && strlen($body) > 0) {

            $mail = new Zend_Mail();
            $mail->addTo((string)$this->getEmail());
            $mail->setSubject($subject);

            if ($isHTML) {
                $body = strip_tags($body);
                $mail->setBodyText($body);
            } else {
                $mail->setBodyHtml($body);
            }

            try {
                $mail->send();
            } catch (Exception $e) {
                _logger()->critical('Failed to send mail to user ' . $this->getEmail() . ': ' . $e->getMessage());

                return false;
            }

            return true;
        }

        return false;
    }


    /**
     * Sends a redeem Request to external Backend and retrieves false or an image with a barcode
     *
     * @param $deal
     *
     * @return Epr_User_Deal
     */
    public function saveDeal(Epr_Dispatcher_Deal $deal)
    {
        $dealForUser = null;

        if (!isset($deal->getPass()->onsite)) {
            throw new Zend_Controller_Action_Exception('Deal not configured properly', 500);
        } else {

            $url = \Poundation\PURL::URLWithString($deal->getPass()->onsite);
            try {
                $image = \Poundation\PImage::createImageFromURL($url);
            } catch (Exception $e) {
                $image = false;
            }


            if ($image) {

                $dealForUser = new Epr_User_Deal($deal);
                $dealForUser->setCodeImage($image);
                $dealForUser->setSavingTime(\Poundation\PDate::now());

                _dm()->persist($dealForUser);
                $this->addDeal($dealForUser);
            } else {
                _logger()->debug('Deal failed!');
            }
        }

        return $dealForUser;

    }

    public function getShowBackendHints()
    {
        return $this->showBackendHints;
    }

    public function getAdminSetting($key, $default = null)
    {
        if (isset($this->adminSettings[$key])) {
            return $this->adminSettings[$key];
        } else {
            return $default;
        }
    }

    public function setAdminSetting($key, $value)
    {
        if (!is_array($this->adminSettings)) {
            $this->adminSettings = array();
        }

        $this->adminSettings[$key] = $value;
    }

    /**
     * Saves a deal code for the given type. If this deal has already been saved the old code is overwritten.
     * @param Epr_Dispatcher_Deal $deal
     * @param $type
     * @param $code
     * @return $this
     * @throws Exception
     */
    public function setDealCode(Epr_Dispatcher_Deal $deal, $type, $code)
    {

        if (strlen($code) >= 4) {
            if (is_null($this->dealCodes)) {
                $this->dealCodes = array();
            }

            $passes = $deal->getPass();
            if ($type == Epr_Deal::TYPE_ONSITE) {
                if (isset($passes->onsite)) {

                    if (!isset($this->dealCodes[$deal->getReferenceDealId()])) {
                        $this->dealCodes[$deal->getReferenceDealId()] = array();
                    }

                    $this->dealCodes[$deal->getReferenceDealId()][Epr_Deal::TYPE_ONSITE] = $code;

                } else {
                    throw new Exception('Cannot save an onsite code of a deal which has no onsite pass.');
                }
            } else if ($type == Epr_Deal::TYPE_ONLINE) {
                if (isset($passes->online)) {

                    if (!isset($this->dealCodes[$deal->getReferenceDealId()])) {
                        $this->dealCodes[$deal->getReferenceDealId()] = array();
                    }

                    $this->dealCodes[$deal->getReferenceDealId()][Epr_Deal::TYPE_ONLINE] = $code;

                } else {
                    throw new Exception('Cannot save an online code of a deal which has no online pass.');
                }
            }
        }

        return $this;
    }

    /**
     * Returns the saved deal code for the given deal and type.
     * @param Epr_Dispatcher_Deal $deal
     * @param $type
     * @return null|string
     */
    public function getDealCode(Epr_Dispatcher_Deal $deal, $type)
    {
        $code = null;

        if (!is_null($this->dealCodes)) {
            if (isset($this->dealCodes[$deal->getReferenceDealId()])) {

                if ($type == Epr_Deal::TYPE_ONSITE && isset($this->dealCodes[$deal->getReferenceDealId()][Epr_Deal::TYPE_ONSITE])) {
                    $code = $this->dealCodes[$deal->getReferenceDealId()][Epr_Deal::TYPE_ONSITE];
                } else if ($type == Epr_Deal::TYPE_ONLINE && isset($this->dealCodes[$deal->getReferenceDealId()][Epr_Deal::TYPE_ONLINE])) {
                    $code = $this->dealCodes[$deal->getReferenceDealId()][Epr_Deal::TYPE_ONLINE];
                }

            }
        }

        return $code;
    }

    /**
     * Returns true if the user has a code for the given type.
     * @param Epr_Dispatcher_Deal $deal
     * @param $type
     * @return bool
     */
    public function hasDealCode(Epr_Dispatcher_Deal $deal, $type)
    {
        $code = $this->getDealCode($deal, $type);

        return (is_string($code) && strlen($code) >= 4);
    }

    /**
     * Returns true if the user has any type of saved code.
     * @param Epr_Dispatcher_Deal $deal
     * @return bool
     */
    public function isDealSaved(Epr_Dispatcher_Deal $deal)
    {
        return ($this->hasDealCode($deal, Epr_Deal::TYPE_ONSITE) || $this->hasDealCode($deal, Epr_Deal::TYPE_ONLINE));
    }

    public function setActive($isActive)
    {
        $wasActive = $this->isActive();
        parent::setActive($isActive);

        if (!$wasActive && $this->isActive()) {
            // was inactive and is active now
            if ($this->sendActivationMail) {

                $usersModule = _app()->getModuleWithIdentifier(Epr_Modules_Users::identifier());
                if ($usersModule instanceof Epr_Modules_Users) {
                    $module = $usersModule->getUserRegistrationModule();
                    if ($module instanceof Epr_Module_UserRegistration_Abstract) {
                        $activationText = $module->getActiviationEmailText();
                        if (strlen($activationText) > 0) {
                            $fields = $module->emailActivationsVariables($this);
                            $template = new \Poundation\PTemplate($activationText);
                            $content = $template->setFields($fields)->renderedString();
                            if ($this->sendMail($module->getRegistrationEmailSubject(), $content)) {
                                $this->sendActivationMail = false;
                            } else {
                                throw new Exception('Could not send activation email.');
                            }
                        }
                    }
                }
            }
        }

        return $this;
    }


}
