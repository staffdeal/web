<?php

/** @Document (repositoryClass="Epr_Feed_Collection") */
class Epr_Feed extends Epr_Document implements Epr_Categorizable
{

	const DOCUMENTNAME = 'Epr_Feed';

	/** @Id */
	protected $id;

	/** @Field(type="string") */
	private $title;

	/**
	 * @Field(type="url")
	 * @Index
	 */
	private $url;

	/** @ReferenceOne(targetDocument="Epr_News_Category") */
	private $category;

	/** @var Exception */
	private $lastError;

	/** @var string */
	private $feedTitle;

	/** @var string */
	private $feedDescription;

	/** @var \Poundation\PURL */
	private $feedURL;

	/** @var \Poundation\PDate */
	private $feedModifiedDate;

	/** @var array */
	private $feedAuthors = array();

	/** @EmbedMany */
	private $feedItems = array();


	/**
	 * Creates a new feed with the given url.
	 *
	 * @param \Poundation\PURL $url
	 *
	 * @return $this
	 */
	static public function createFeedWithURL(\Poundation\PURL $url)
	{
		$feed = new self();

		return $feed->setUrl($url);
	}

	/**
	 * Sets the title.
	 *
	 * @param $title
	 *
	 * @return $this
	 */
	public function setTitle($title)
	{
		$this->title = (string)$title;

		return $this;
	}

	/**
	 * Returns the title.
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Sets the URL.
	 *
	 * @param $url
	 *
	 * @return $this
	 */
	public function setUrl($url)
	{
		if (!$url instanceof \Poundation\PURL) {
			$url = \Poundation\PURL::URLWithString($url);
		}
		$this->url = $url;

		return $this;
	}

	/**
	 * Returns the URL.
	 *
	 * @return \Poundation\PURL
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * Sets the assigned category.
	 *
	 * @param Epr_News_Category $category
	 *
	 * @return $this
	 */
	public function setCategory($category)
	{
		if ($category instanceof Epr_News_Category || is_null($category)) {
			$this->category = $category;
		}

		return $this;
	}

	/**
	 * Removes the assigned category.
	 *
	 * @return $this
	 */
	public function removeCategory()
	{
		$this->category = null;

		return $this;
	}

	/**
	 * Returns the assigned category.
	 *
	 * @return Epr_News_Category|null
	 */
	public function getCategory()
	{
		return $this->category;
	}

	/**
	 * Returns the entries of the feed (onced it has been read).
	 *
	 * @return array
	 */
	public function getEntries()
	{
		return $this->feedItems;
	}

	/**
	 * Returns true of the feed is valid which includes a parsing test.
	 *
	 * @return bool
	 */
	public function isValid(&$error = null)
	{
		$valid = $this->read();
		if ($valid == false) {
			$error = $this->lastError;
		} else {
			if (is_null($this->title) || strlen($this->title) == 0) {
				$this->setTitle($this->feedTitle);
			}
		}

		return $valid;
	}

	/**
	 * Returns the last error.
	 *
	 * @return Exception|null
	 */
	public function getLastError()
	{
		return $this->lastError;
	}

	private function maxAge()
	{
		$module = _app()->getModuleWithIdentifier(Epr_Modules_RSS::identifier());
		if ($module instanceof Epr_Modules_RSS) {

			return $module->getMaximumItemAge();

		}

		return 14;
	}

	/**
	 * Reads the feeds and returns true if this was successful.
	 *
	 * @return bool
	 */
	public function read($maxAge = 0)
	{

		if ($maxAge < 1) {
			$maxAge = $this->maxAge();
		}

		$success         = false;
		$this->lastError = null;

		if ($this->url instanceof \Poundation\PURL) {

			$cacheProvider = Epr_Cache_Provider::getInstance();
			if (!is_null($cacheProvider)) {
				Zend_Feed_Reader::setCache($cacheProvider->getCache());
				Zend_Feed_Reader::useHttpConditionalGet(true);
			}

			try {
				$reader  = Zend_Feed_Reader::import((string)$this->getUrl());
				$success = true;
			} catch (Exception $e) {
				_logger()->error('Error validating feed ' . $this->url . ': "' . $e->getMessage() . '"');
				$this->lastError = $e;
			}

			if ($success) {
				$this->feedTitle       = $reader->getTitle();
				$this->feedDescription = $reader->getDescription();
				$this->feedURL         = \Poundation\PURL::URLWithString($reader->getFeedLink());

				$modifiedDate = $reader->getDateModified();
				if ($modifiedDate) {
					$this->feedModifiedDate = new \Poundation\PDate($modifiedDate->getTimestamp());
				}
				$this->feedAuthors = $reader->getAuthors();

				$limit = \Poundation\PDate::now();
				$limit->addDays(0 - $maxAge);

				if (is_null($this->feedItems)) {
					$this->feedItems = array();
				}

				foreach ($reader as $entry) {
					if ($entry instanceof Zend_Feed_Reader_EntryInterface) {

						$date = $entry->getDateCreated();
						if ($date) {
							$releaseDate = new \Poundation\PDate($date->getTimestamp());

							if (!$releaseDate->isBefore($limit)) {

								$id = md5($entry->getId());

								if (is_null($this->getItemWithId($id))) {

									$item = new Epr_Feed_Item($entry->getId(), $id); // we pass the id since there will be none because the document is embedded
									if ($item) {
										$item->setTitle($entry->getTitle());
										$item->setText($entry->getContent());
										$item->setUrl($entry->getPermalink());
										$item->setAuthors($entry->getAuthors());

										$item->setReleaseDate($releaseDate->getDateTime());

										$encl = $entry->getEnclosure();
										if ($encl) {
											$item->importEnclosure($encl);
										}

										$this->feedItems[] = $item;

									}
								}
							}
						}
					}

				}

				$this->cleanup($maxAge);
			}
		}

		return $success;
	}

	private function cleanup($maxAge = 0)
	{

		if ($maxAge < 1) {
			$maxAge = $this->maxAge();
		}

		$limit = \Poundation\PDate::now();
		$limit->addDays(0 - $maxAge);

		$indexesToRemove = array();
		for ($i = 0; $i < count($this->feedItems); $i++) {
			$item = $this->feedItems[$i];
			if ($item instanceof Epr_Feed_Item) {
				$releaseDate = $item->getReleaseDate();
				if (!$releaseDate instanceof \Poundation\PDate) {
					$releaseDate = new \Poundation\PDate($releaseDate);
				}
				if ($releaseDate instanceof \Poundation\PDate) {
					if ($releaseDate->isBefore($limit)) {
						$indexesToRemove[$i] = $item;
					}
				}
			}
		}

        if ($this->feedItems instanceof \Doctrine\Common\Collections\ArrayCollection) {
            foreach ($indexesToRemove as $index => $itemToRemove) {
                $this->feedItems->remove($index);
            }
        }

	}

	/**
	 * Returns the item with the given id.
	 *
	 * @param $id
	 *
	 * @return Epr_Feed_Item|null
	 */
	public function getItemWithId($id)
	{
		$result = null;

		foreach ($this->getEntries() as $item) {
			if ($item instanceof Epr_Feed_Item) {
				if ($item->getId() == $id) {
					$result = $item;
					break;
				}
			}
		}

		return $result;
	}

}