<?php

/** @Document */
abstract class Epr_Booking {

    /** @Id */
    protected $id;

    /** @Field(type="string")  */
    protected $userId;
    // we only keep the id because user's can get deleted

    /** @Field(type="datetime") */
    protected $date;

    private $user;

    public function __construct(Epr_User $user) {
        $this->userId = $user->getId();
        $this->date = new DateTime();
    }

    /**
     * Returns the user.
     * @return Epr_User|null
     */
    public function getUser() {

        if (is_null($this->user)) {
            if ($this->userId) {
                $this->user = Epr_User_Collection::getUserWithIdentifier($this->userId);
            }
        }
        return $this->user;
    }

    /**
     * Returns true if the booking is for the given user.
     * This method only compares the ids and is therefore pretty fast.
     * @param Epr_User $user
     * @return bool
     */
    public function isForUser(Epr_User $user) {
        return ($this->userId == $user->getId());
    }

    /**
     * @return null|\Poundation\PDate
     */
    public function getDate() {
        $date = null;
        if ($this->date) {
            $date = \Poundation\PDate::createDate($this->date);
        }
        return $date;
    }

}