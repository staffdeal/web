<?php

use Poundation\PURL;
use Poundation\PClass;
use Poundation\PArray;

use Doctrine\CouchDB\View\FolderDesignDocument;

/** @Document */
class Epr_Application extends Epr_Document
{

    const DOCUMENTVERSION = 52;
    const APIVERSION = 2;

    const RUNTIMETYPE_DEVELOPMENT = 'development';
    const RUNTIMETYPE_TESTING = 'testing';
    const RUNTIMETYPE_PRODUCTION = 'production';

    const DOCUMENTNAME = 'Epr_Application';
    const ID = 'application';

    /** @Id(strategy="ASSIGNED") */
    public $id = Epr_Application::ID;

    /** @Field(type="integer") */
    private $documentsVersion = 0;

    private static $instance;

    private $moduleClasses = false;

    /** @EmbedMany */
    private $modules = array();

    private $commitRev;
    private $commitBranch;
    private $commitDate;

    private $theme;

    /**
     * Returns the singleton application object.
     *
     * @return Epr_Application
     */
    static function application()
    {

        if (!isset(self::$instance)) {
            $dm = Zend_Registry::get('dm');
            self::$instance = $dm->find(Epr_Application::DOCUMENTNAME, Epr_Application::ID);
            if (!isset(self::$instance)) {
                self::$instance = new Epr_Application();
                $dm->persist(self::$instance);
                $dm->flush(self::$instance);
            }
        }

        return self::$instance;
    }

    public function __destruct()
    {
        _dm()->flush();
    }

    /**
     * Returns the name of the instance.
     *
     * @return string|null
     */
    public function getName()
    {
        if (Zend_Registry::isRegistered('client')) {
            $config = Zend_Registry::get('client');
            if (isset($config->info->name)) {
                return $config->info->name;
            }
        }

        return null;
    }

    /**
     * Returns true if the application has been started in production mode.
     *
     * @return bool
     */
    public function isProduction()
    {
        return (APPLICATION_MODE == self::RUNTIMETYPE_PRODUCTION);
    }

    /**
     * Returns true if the application has been started in development mode.
     *
     * @return bool
     */
    public function isDevelopment()
    {
        return (APPLICATION_MODE == self::RUNTIMETYPE_DEVELOPMENT);
    }

    /**
     * Returns true if the application has been started in testing mode.
     *
     * @return bool
     */
    public function isTesting()
    {
        return (APPLICATION_MODE == self::RUNTIMETYPE_TESTING);
    }

    /**
     * Returns the public URL of the application. The URL is read from the configuration and has not been determined by network analysis.
     *
     * @return PURL
     */
    public function getPublicURL()
    {
        $url = new PURL();
        $url->setHost($_SERVER['SERVER_NAME'])->setPort($_SERVER['SERVER_PORT']);
        $url->setScheme((isset($_SERVER['HTTPS'])) ? PURL::SCHEME_HTTPS : PURL::SCHEME_HTTP);

        return $url;

    }

    public function getEANImageURL($code)
    {
        $baseURL = $this->getPublicURL();
        return $baseURL->addPathComponent('api/barcode/ean/')->addPathComponent((string)$code);
    }

    public function performUpdateActionsIfNeeded()
    {

        $forceUpdate = (\Poundation\Server\PRequest::request()->getURL()->getParameter('forceUpdate') == 'true') ? true : false;

        if ($this->getDocumentsVersion() < $this->getDocumentVersion() || $forceUpdate) {

            $documentsVersionBeforeUpdating = $this->getDocumentsVersion();
            $this->performUpdateActions();

            if ($documentsVersionBeforeUpdating < 152) {
                // this is the version where news got the publishable trait

                $allNews = Epr_News_Collection::getAll();
                foreach ($allNews as $news) {
                    if ($news instanceof Epr_News && !$news->isDocumentPublishable()) {
                        if ($news->isActive()) {
                            $news->publishAt(\Poundation\PDate::createDate($news->getPublishDate()));
                        } else {
                            $news->unpublish();
                        }
                    }
                }
            }
        }
    }

    private function performUpdateActions()
    {
        $this->removeOldUserDeals();
        $this->registerDBViews();
        $this->registerSortDBViews();
        $this->registerSearchDBViews();
        $this->registerModules();
        $this->loadDummyImage();
        $this->loadCmsPages();
    }

    private function registerDBViews()
    {
        $couchClient = _dm()->getCouchDBClient();

        $basePath = APPLICATION_PATH . 'library/couch/';
        $files = glob($basePath . '/*', GLOB_ONLYDIR);
        foreach ($files as $dirName) {
            $key = basename($dirName);
            $designDocument = new FolderDesignDocument($dirName);
            $couchClient->createDesignDocument($key, $designDocument);
        }

        $this->documentsVersion = $this->getDocumentVersion();
    }

    private function registerSortDBViews()
    {

        $client = _dm()->getCouchDBClient();
        Epr_News_Collection::registerSortDesignDocument(Epr_News::DOCUMENTNAME, $client);
        Epr_News_Category_Collection::registerSortDesignDocument(Epr_News_Category::DOCUMENTNAME, $client);
        Epr_Feed_Collection::registerSortDesignDocument(Epr_Feed::DOCUMENTNAME, $client);
        Epr_User_Collection::registerSortDesignDocument(Epr_User::DOCUMENTNAME, $client);
        Epr_Token_Collection::registerSortDesignDocument(Epr_Token::DOCUMENTNAME, $client);
        Epr_Deal_Collection::registerSortDesignDocument(Epr_Deal::DOCUMENTNAME, $client);
        Epr_Dealer_Collection::registerSortDesignDocument(Epr_Dealer::DOCUMENTNAME, $client);
        Epr_Idea_Competition_Collection::registerSortDesignDocument(Epr_Idea_Competition::DOCUMENTNAME, $client);
        Epr_Idea_Category_Collection::registerSortDesignDocument(Epr_Idea_Category::DOCUMENTNAME, $client);
        //Epr_Idea_Collection::registerSortDesignDocument(Epr_Idea::DOCUMENTNAME, $client); // this is yet done by code
        Epr_Content_Image_Collection::registerSortDesignDocument(Epr_Content_Image::DOCUMENTNAME, $client);
        Epr_Events_Collection::registerSortDesignDocument(Epr_Event::DOCUMENTNAME, $client);

    }

    private function registerSearchDBViews()
    {
        $client = _dm()->getCouchDBClient();

        Epr_Events_Collection::registerSearchDesignDocument(Epr_Event::DOCUMENTNAME, $client);
        Epr_News_Collection::registerSearchDesignDocument(Epr_News::DOCUMENTNAME, $client);
        Epr_User_Collection::registerSearchDesignDocument(Epr_User::DOCUMENTNAME, $client);
        Epr_Deal_Collection::registerSearchDesignDocument(Epr_Deal::DOCUMENTNAME, $client);
        Epr_Dealer_Collection::registerSearchDesignDocument(Epr_Dealer::DOCUMENTNAME, $client);
        Epr_Idea_Competition_Collection::registerSearchDesignDocument(Epr_Idea_Competition::DOCUMENTNAME, $client);
        Epr_Content_Image_Collection::registerSearchDesignDocument(Epr_Content_Image::DOCUMENTNAME, $client);
    }

    private function removeOldUserDeals()
    {

        if ($this->documentsVersion < 44) {      // we can use the magic number here since this was the version we changed this data
            $deals = _dm()->getRepository(Epr_User_Deal::DOCUMENTNAME)->findAll();
            foreach ($deals as $deal) {
                _dm()->remove($deal);
            }
        }

    }

    private function loadDummyImage()
    {
        $path = ROOT_PATH . '/public/images/dummy.jpg';
        if (file_exists($path)) {

            $contentImage = Epr_Content_Image_Collection::imageWithFilename('dummy.jpg');
            if ($contentImage == null) {
                $fileChanged = true;
            } else {
                $fileHash = \Poundation\PImage::hashFromFilename($path);
                $existingHash = $contentImage->getHash();
                $fileChanged = ($fileHash !== $existingHash);
            }

            if ($fileChanged) {
                $image = \Poundation\PImage::createImageFromFilename($path);
                if ($image) {
                    if ($contentImage == null) {
                        $contentImage = Epr_Content_Image::imageFromPImage($image);
                        $contentImage->setAccess(Epr_Content_Image::ACCESS_SYSTEM);
                        _dm()->persist($contentImage);
                    } else {
                        $contentImage->importImage($image);
                    }
                }
            }

        } else {
            throw new Exception('Could not load dummy image at "' . $path . '".');
        }
    }

    public function registerLocalizationStringsIfNeeded()
    {
        if ($this->getDocumentsVersion() < $this->getDocumentVersion() || $this->isDevelopment()) {
            $this->registerLocalizationStrings();
        }
    }

    private function registerLocalizationStrings()
    {
        _t('%u year');
        _t('%u years');
        _t('%u month');
        _t('%u months');
        _t('%u day');
        _t('%u days');
        _t('%u hour');
        _t('%u hours');
        _t('%u minute');
        _t('%u minutes');
        _t('%u second');
        _t('%u seconds');
    }

    private function loadCmsPages()
    {
        $cmsModule = $this->getModuleWithIdentifier(Epr_Modules_Cms::identifier());
        if ($cmsModule instanceof Epr_Modules_Cms) {

            $pages = $cmsModule->getPages();
        }
    }

    /**
     * Returns the version of the application's source code. It should be increased if the supporting database views have been changed.
     *
     * @return int
     */
    public function getDocumentVersion()
    {
        return Epr_Application::DOCUMENTVERSION;
    }

    /**
     * Returns the version of the API.
     *
     * @return int
     */
    public function getAPIVersion()
    {
        return self::APIVERSION;
    }

    /**
     * Returns the version of the documents saved to the database. If it differs from the source code's version a migration should be started.
     *
     * @return int
     */
    public function getDocumentsVersion()
    {
        return $this->documentsVersion;
    }


    private function readCommitData()
    {
        if (is_null($this->commitDate)) {
            $path = ROOT_PATH . '/commit.txt';
            if (file_exists($path)) {
                $data = file_get_contents($path);
                if ($data) {

                    $components = explode("\n", $data);
                    if (count($components) >= 3) {
                        $this->commitRev = $components[0];
                        $this->commitDate = new \Poundation\PDate((int)$components[1]);
                        if (is_null($this->commitDate)) {
                            $this->commitDate = new \Poundation\PDate('1970-01-01 00:00:00');
                        }
                        $this->commitBranch = $components[2];
                    }

                }
            } else {

                $this->commitRev = exec("git log --pretty=format:'%h' -n 1");
                $commitDate = exec("git log --pretty=format:'%ct' -n 1");
                if ($commitDate) {
                    $this->commitDate = new \Poundation\PDate((int)$commitDate);
                }
                if (is_null($this->commitDate)) {
                    $this->commitDate = new \Poundation\PDate('1970-01-01 00:00:00');
                }

                $this->commitBranch = exec('git branch | grep "*" | sed "s/* //"');

            }
        }
    }

    /**
     * Returns the revision of the last git commit.
     *
     * @return string|null
     */
    public function getCommitRevision()
    {
        $this->readCommitData();

        return $this->commitRev;
    }

    /**
     * Returns the name of the current git branch.
     *
     * @return string|null
     */
    public function getCommitBranch()
    {
        $this->readCommitData();

        return $this->commitBranch;
    }

    /**
     * Returns the date of the last commit.
     *
     * @return null|Poundation\PDate
     */
    public function getCommitDate()
    {
        $this->readCommitData();

        return $this->commitDate;
    }

    /**
     * Returns an array of all loaded modules.
     *
     * @return array
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * Returns all modules that expose an API.
     *
     * @return array
     */
    public function getAPIModules()
    {
        $modules = array();
        foreach ($this->getModules() as $module) {
            if (PClass::classFromObject($module)->implementsInterface('Epr_Module_API')) {
                $apiPath = $module->getAPIPath();
                if ($apiPath !== false && $module->getAPIAuthLevel() != Epr_Roles::ROLE_NONE) {
                    $modules[] = $module;
                }
            }
        }

        return $modules;
    }

    /**
     * Returns the module with the given API path.
     * @param $path
     * @return Epr_Module_API_Abstract|null
     */
    public function getAPIModuleWithPath($path)
    {
        $module = null;

        foreach ($this->getAPIModules() as $candidate) {
            if ($candidate instanceof Epr_Module_API_Abstract) {
                if ($candidate->getAPIPath() == $path) {
                    $module = $candidate;
                    break;
                }
            }
        }

        return $module;
    }

    /**
     * Returns an array of all modules that perform user registrations.
     *
     * @return array
     */
    public function getUserRegistrationModules()
    {

        $modules = array();

        foreach ($this->getModules() as $module) {
            if (PClass::classFromObject($module)->implementsInterface('Epr_Module_UserRegistration')) {
                $modules[] = $module;
            }
        }

        return $modules;
    }

    /**
     * Returns the currently active user registration module.
     * @return Epr_Module_UserRegistration_Abstract|null
     */
    public function getActiveUserRegistrationModule()
    {
        $usersModule = $this->getModuleWithIdentifier(Epr_Modules_Users::identifier());
        if ($usersModule instanceof Epr_Modules_Users) {
            $module = $usersModule->getUserRegistrationModule();
            if ($module instanceof Epr_Module_UserRegistration_Abstract) {
                return $module;
            }
        }
        return null;
    }

    /**
     * Returns an array of all modules that perform deal code management and generation.
     *
     * @return array
     */
    public function getDealCodesModules()
    {

        $modules = array();

        foreach ($this->getModules() as $module) {
            if (PClass::classFromObject($module)->implementsInterface('Epr_Module_DealCode')) {
                $modules[] = $module;
            }
        }

        return $modules;

    }

    public function getCronModules()
    {
        $modules = array();

        foreach ($this->getModules() as $module) {
            if (PClass::classFromObject($module)->implementsInterface('Epr_Module_Cron')) {
                $modules[] = $module;
            }
        }

        return $modules;
    }

    /**
     * Returns class objects (no instances) of all known modules.
     *
     * @return \Poundation\PArray
     */
    private function getModulesClasses()
    {

        if ($this->moduleClasses === false) {

            // first, we grap every file in the modules directory
            $path = ROOT_PATH . '/application/library/Epr/Modules';
            $files = scandir($path);

            $sourceFiles = new PArray();
            if (is_array($files)) {
                foreach ($files as $filename) {
                    $filename = __($filename);
                    if ($filename->hasSuffix('.php')) {
                        $sourceFiles->add($filename);
                    }
                }
            }

            // then we create PClass objects with every file/class.
            $this->moduleClasses = new PArray();
            foreach ($sourceFiles as $sourceFilename) {
                if ($sourceFilename instanceof \Poundation\PString) {
                    $fullPath = $path . '/' . $sourceFilename;
                    /** @noinspection PhpIncludeInspection */
                    include_once($fullPath);
                    $className = 'Epr_Modules_' . $sourceFilename->removeTrailingCharactersWhenMatching('.php');

                    $class = PClass::classFromString($className);
                    if ($class->implementsInterface('Epr_Module') && $class->isKindOfClass('Epr_Module_Abstract')) {
                        $this->moduleClasses->add($class);
                    }
                }
            }
        }

        return $this->moduleClasses;
    }

    /**
     * @return PClass[]
     */
    public function getDealCodeModuleClasses()
    {
        $classes = array();

        foreach ($this->getModulesClasses() as $class) {
            if ($class instanceof PClass) {
                if ($class->implementsInterface('Epr_Module_DealCode')) {
                    $classes[] = $class;
                }
            }
        }

        return $classes;
    }

    /**
     * Returns the module with the given id.
     *
     * @param $id
     *
     * @return null|Epr_Module_Abstract
     */
    public function getModuleWithIdentifier($id, $avoidStartup = false)
    {
        foreach ($this->modules as $module) {
            /** @var $module Epr_Module_Abstract */
            if (PClass::classFromObject($module)->implementsInterface('Epr_Module')) {
                $moduleIdentifier = $module->identifier();
                if ($moduleIdentifier == $id || $moduleIdentifier == 'Modules_' . $id) {
                    if ($avoidStartup == false) {
                        ($module->startup());
                    }

                    return $module;
                }
            }
        }

        return null;
    }

    private function registerModules()
    {
        $needFlush = false;
        foreach ($this->getModulesClasses() as $class) {
            if ($class instanceof PClass) {

                $identifier = $class->invokeMethod('identifier');
                if ($identifier && is_null($this->getModuleWithIdentifier($identifier, true))) {
                    $instance = $class->invokeMethod('instance', array());
                    $this->modules[] = $instance;
                    $needFlush = true;
                }
            }
        }
        if ($needFlush) {
            _dm()->flush();
        }
    }

    public function runCronJobs()
    {

        $modules = $this->getCronModules();
        $outputs = array();

        foreach ($modules as $module) {
            if ($module instanceof Epr_Module && $module instanceof Epr_Module_Cron) {

                $lastDate = null;

                if ($module instanceof Epr_Module_Cron_Abstract) {
                    $lastDate = $module->getLastRunDate();
                }

                if (!$lastDate instanceof DateTime) {
                    $lastDate = new DateTime('1970-01-01');
                }

                $output = $module->runCron($lastDate->diff(new DateTime()));
                if ($output && strlen($output)) {
                    $outputs[$module->title()] = $output;
                }
            }
        }

        return $outputs;
    }

    public function sendAdminMail($subject, $body)
    {

        $adminMail = (Zend_Registry::isRegistered('adminMail')) ? Zend_Registry::get('adminMail') : null;
        if ($adminMail instanceof \Poundation\PMailAddress) {
            $mail = new Zend_Mail();
            $mail->addTo((string)$adminMail);
            $mail->setSubject($subject);
            $mail->setBodyHtml($body);
            try {
                $mail->send();
                return true;
            } catch (Exception $e) {
                _logger()->critical('Failed to send mail to user ' . $adminMail . ': ' . $e->getMessage());
            }

        }

        return false;
    }
}
