<?php


class Epr_Validate_UniqueUser extends Zend_Validate_Abstract {

	const MAIL_ALREADY_EXISTS = 'mailAlreadyExist';

	protected $_messageTemplates = array(
			self::MAIL_ALREADY_EXISTS => "'%value%' already exist.",
	);
	
	public function isValid($value)
    {
		$valueString = (string) $value;
		$this->_setValue($valueString);

        // we check of the mail address is already known
        $email           = (string)__($valueString)->lowercase();
        $usersCollection = Epr_User_Collection::getRepository();
        $existingUsers   = $usersCollection->findBy(array('email' => $email));

        if (count($existingUsers) > 0) {
			$this->_messageTemplates[self::MAIL_ALREADY_EXISTS] = sprintf(_t('The mail address %s has already been registered'),$value);
            $this->_error(self::MAIL_ALREADY_EXISTS);
			return false;
        }
		return true;
	}
	
}

?>