<?php


class Epr_Validate_URL extends Zend_Validate_Abstract {

	const INVALID_URL = 'invalidUrl';
	
	protected $_messageTemplates = array();

	private $allowEmailURLs = false;
	
	public function isValid($value) {
		$valueString = (string)$value;
		$this->_setValue($valueString);

		if ($this->getAllowEmailURLs()) {
			$url = \Poundation\PURL::URLWithString($valueString);
			$isValid = ($url instanceof \Poundation\PURL);
			if ($isValid && is_null($url->getMailAddress())) {
				$isValid = Zend_Uri::check($valueString);
			}

		} else {
			$isValid = Zend_Uri::check((string)$value);
		}

		if (!$isValid) {
			$this->_messageTemplates[self::INVALID_URL] = sprintf(_t('"%s" is not a valid URL.'), $value);
			$this->_error(self::INVALID_URL);
		}

		return $isValid;
	}

	public function setAllowEmailURLs($value) {
		$this->allowEmailURLs = ($value == true);
		return $this;
	}

	public function getAllowEmailURLs() {
		return ($this->allowEmailURLs == true);
	}

}

?>