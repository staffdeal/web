<?php


class Epr_Validate_SecurePassword extends Zend_Validate_Abstract {

	const INSECURE_PASSWORD = 'insecurePassword';

	/** @var int */
	protected $passwordMinimumLength = 8;

	/** @var bool */
	protected $passwordContainsNumber = true;

	/** @var bool */
	protected $passwordContainsLetter = true;

	/** @var bool */
	protected $passwordContainsCapitals = true;

	/** @var bool */
	protected $passwordContainsSymbols = true;


	protected $_messageTemplates = array(
		self::INSECURE_PASSWORD => "'%value%' is not a secure password.",
	);

	public function __construct($options = array()) {

		$this->setDisableTranslator(true);

		if (count($options) > 0) {
			if (array_key_exists('passwordMinimumLength', $options)) {
				$this->setPasswordMinimumLength($options['passwordMinimumLength']);
			}

			if (array_key_exists('passwordContainsNumber', $options)) {
				$this->setPasswordContainsNumber($options['passwordContainsNumber']);
			}

			if (array_key_exists('passwordContainsLetter', $options)) {
				$this->setPasswordContainsLetter($options['passwordContainsLetter']);
			}

			if (array_key_exists('passwordContainsCapitals', $options)) {
				$this->setPasswordContainsCapitals($options['passwordContainsCapitals']);
			}

			if (array_key_exists('passwordContainsSymbols', $options)) {
				$this->setPasswordContainsSymbols($options['passwordContainsSymbols']);
			}
		}
	}

    public function getHintString() {
        return $this->createErrorString();
    }

	private function createErrorString() {
		$validationSettingsString = _t("The password needs") . ' ';
		if ($this->getPasswordContainsCapitals()) {
			$validationSettingsString .= _t('at least one capital letter') . ', ';
		}
		if ($this->getPasswordContainsLetter()) {
			$validationSettingsString .= _t('at least one letter') . ', ';
		}
		if ($this->getPasswordContainsNumber()) {
			$validationSettingsString .= _t('at least one number') . ', ';
		}
		if ($this->getPasswordContainsSymbols()) {
			$validationSettingsString .= _t('at least one symbol') . ', ';
		}
		if ($this->getPasswordMinimumLength()) {
			$validationSettingsString .= sprintf(_t('a minimum length of %d characters'), $this->getPasswordMinimumLength());
		}

		return $validationSettingsString;
	}

	public function isValid($value) {
		$valueString = (string)$value;
		$this->_setValue($valueString);

		if (!$this->validate($value)) {
			$this->_messageTemplates[self::INSECURE_PASSWORD] = $this->createErrorString();
			$this->_error(self::INSECURE_PASSWORD);

			return false;
		}

		return true;
	}


	/**
	 * Do the final Check here.
	 *
	 * @param $password
	 *
	 * @return bool
	 */
	private function validate($password) {

		if (strlen($password) < $this->getPasswordMinimumLength()) {
			return false;
		}

		if ($this->getPasswordContainsNumber()) {
			if (!preg_match("#[0-9]+#", $password)) {
				return false;
			}
		}

		if ($this->getPasswordContainsLetter()) {
			if (!preg_match("#[a-z]+#", $password)) {
				return false;
			}
		}

		if ($this->getPasswordContainsCapitals()) {
			if (!preg_match("#[A-Z]+#", $password)) {
				return false;
			}
		}

		if ($this->getPasswordContainsSymbols()) {
			if (!preg_match("#\W+#", $password)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * @param boolean $passwordContainsCapitals
	 */
	public function setPasswordContainsCapitals($passwordContainsCapitals) {
		$this->passwordContainsCapitals = $passwordContainsCapitals;
	}

	/**
	 * @return boolean
	 */
	public function getPasswordContainsCapitals() {
		return $this->passwordContainsCapitals;
	}

	/**
	 * @param boolean $passwordContainsLetter
	 */
	public function setPasswordContainsLetter($passwordContainsLetter) {
		$this->passwordContainsLetter = $passwordContainsLetter;
	}

	/**
	 * @return boolean
	 */
	public function getPasswordContainsLetter() {
		return $this->passwordContainsLetter;
	}

	/**
	 * @param boolean $passwordContainsNumber
	 */
	public function setPasswordContainsNumber($passwordContainsNumber) {
		$this->passwordContainsNumber = $passwordContainsNumber;
	}

	/**
	 * @return boolean
	 */
	public function getPasswordContainsNumber() {
		return $this->passwordContainsNumber;
	}

	/**
	 * @param boolean $passwordContainsSymbols
	 */
	public function setPasswordContainsSymbols($passwordContainsSymbols) {
		$this->passwordContainsSymbols = $passwordContainsSymbols;
	}

	/**
	 * @return boolean
	 */
	public function getPasswordContainsSymbols() {
		return $this->passwordContainsSymbols;
	}

	/**
	 * @param int $passwordMinimumLength
	 */
	public function setPasswordMinimumLength($passwordMinimumLength) {
		$this->passwordMinimumLength = $passwordMinimumLength;
	}

	/**
	 * @return int
	 */
	public function getPasswordMinimumLength() {
		return $this->passwordMinimumLength;
	}

}
