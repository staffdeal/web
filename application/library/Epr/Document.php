<?php

abstract class Epr_Document extends \Poundation\PObject
{

    /**
     * @Index
     * @Field(type="boolean")
     */
    protected $isActive;

    /**
     * @Field(type="mixed")
     */
    protected $associates = null;

    public function __construct()
    {
        if (property_exists(get_called_class(), 'creationDate')) {
            $this->_derivedClassPropertySet('creationDate', new DateTime());
        }
    }

    private function _derivedClassPropertyGet($propertyName)
    {
        // getting the value of an instance of a derived class is a bit tricky
        if (property_exists(get_called_class(), $propertyName)) {
            return $this->$propertyName;
        } else {
            throw new Exception('Though Epr_Document provides the getter for "' . $propertyName . '", the class ' . get_called_class() . ' need to have a property "' . $propertyName . '".');
        }
    }

    private function _derivedClassPropertySet($propertyName, $value)
    {
        // getting the value of an instance of a derived class is a bit tricky
        if (property_exists(get_called_class(), $propertyName)) {
            return $this->$propertyName = $value;
        } else {
            throw new Exception('Though Epr_Document provides the setter for "' . $propertyName . '", the class ' . get_called_class() . ' need to have a property "' . $propertyName . '".');
        }
    }


    /**
     * Returns the id of the document.
     *
     * @return mixed
     * @throws Exception
     */
    public function getId()
    {
        return $this->_derivedClassPropertyGet('id');
    }

    /**
     * @deprecated Calling Epr_Document::save is deprecated.
     */
    public function save()
    {
        trigger_error('Epr_Document::save is deprecated');
        _dm()->persist($this);
        _dm()->flush();

    }

    /**
     * Sets is active.
     *
     * @param $isActive
     * @deprecated
     *
     * @return Epr_Document_WebPage
     */
    public function setActive($isActive)
    {
        $this->isActive = ($isActive == true);

        return $this;
    }

    /**
     * Activates the document.
     *
     * @deprecated
     * @return Epr_Document_WebPage
     */
    public function activate()
    {
        return $this->setActive(true);
    }

    /**
     * Deactivates the document.
     *
     * @deprecated
     * @return Epr_Document_WebPage
     */
    public function deactivate()
    {
        return $this->setActive(false);
    }

    /**
     * Return true if the document is active.
     *
     * @deprecated
     * @return bool
     */
    public function isActive()
    {
        return ($this->isActive == true);
    }

    /**
     * Returns the datetime of creation.
     *
     * @return DateTime
     */
    public function getCreationDate()
    {
        $creationDate = $this->_derivedClassPropertyGet('creationDate');
        if (is_null($creationDate)) {
            $creationDate = new DateTime();
        }

        return $creationDate;
    }

    public function setCreationDate($date)
    {
        if ($date instanceof DateTime || is_null($date)) {
            $this->_derivedClassPropertySet('creationDate', $date);
        }

        return $this;
    }

    /**
     * Sets the associate with the given key.
     * @param $associate
     * @param string $key
     * @return $this
     * @throws Exception
     */
    public function setAssociateForKey($associate, $key)
    {
        if (!is_array($this->associates)) {
            $this->associates = [];
        }

        if (is_string($key)) {
            if (is_null($associate)) {
                return $this->removeAssociateForKey($key);
            } else {
                $type = gettype($associate);
                if ($type == 'object') {
                    $type = strtolower(get_class($associate));

                    if (\Doctrine\ODM\CouchDB\Types\Type::hasType($type)) {
                        $conv = \Doctrine\ODM\CouchDB\Types\Type::getType($type);
                        if ($conv) {
                            $associate = $conv->convertToCouchDBValue($associate);
                        }
                    }
                }
                $this->associates[$key] = ['type' => $type, 'data' => $associate];
            }
        } else {
            throw new Exception('The associate key must be a string.');
        }

        return $this;
    }


    /** Removes the associate with the given key.
     * @param $key
     * @return string $this
     * @throws Exception
     */
    public function removeAssociateForKey($key)
    {
        if (is_string($key)) {
            if (is_array($this->associates) && isset($this->associates[$key])) {
                unset($this->associates[$key]);
            }
        } else {
            throw new Exception('The associate key must be a string.');
        }

        return $this;
    }


    /**
     * Returns the associate for a key or null if it doesn't exist.
     * @param $key
     * @return null|mixed
     */
    public function getAssociateForKey($key)
    {
        $associate = null;

        if (is_string($key) && is_array($this->associates)) {
            if (isset($this->associates[$key])) {
                $data = $this->associates[$key];
                if (isset($data['type']) && isset($data['data'])) {
                    $type  = $data['type'];
                    $value = $data['data'];

                    if (\Doctrine\ODM\CouchDB\Types\Type::hasType($type)) {
                        $conv = \Doctrine\ODM\CouchDB\Types\Type::getType($type);
                        if ($conv) {
                            $value = $conv->convertToPHPValue($value);
                        }
                    }

                    return $value;
                }
            }

            return $associate;
        }
    }
}
