<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 14.09.12
 * Time: 12:31
 */
/** @Document (repositoryClass="Epr_Idea_Category_Collection") */
class Epr_Idea_Category extends Epr_Document_WebPage
{
	const DOCUMENTNAME = 'Epr_Idea_Category';
	
    /** @Id */
    public $id;

}
