<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 08.09.12
 * Time: 12:56
 */
use Doctrine\ODM\CouchDB\DocumentRepository;
use Doctrine\CouchDB\View\FolderDesignDocument;

class Epr_Idea_Category_Collection extends Epr_Collection implements Epr_Paginatable
{

    const SORT_DESIGN_DOCUMENT_NAME = 'sortIdeaCategory';

    static function getSortKeys()
    {
        return array(
            'title',
            'slug'
        );
    }

	/**
	 * @return \Doctrine\ODM\CouchDB\DocumentRepository
	 */
	static function getRepository() {
		return _dm()->getRepository(Epr_Idea_Category::DOCUMENTNAME);
	}
	
    static function getAll()
    {
        $coll = self::getRepository();
        return $coll->findAll();
    }

    static function getActive() {
       $coll = self::getRepository();
       return $coll->findBy(array(
            'isActive' => true
       ));
    }
}