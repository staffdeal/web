<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 08.09.12
 * Time: 12:56
 */
use Doctrine\ODM\CouchDB\DocumentRepository;
use Doctrine\CouchDB\View\FolderDesignDocument;

class Epr_Idea_Competition_Collection extends Epr_Collection implements Epr_Paginatable
{

    private static $validUntilToday = null;
    private static $categoriesUntilToday = null;

    const SORT_DESIGN_DOCUMENT_NAME = 'sortIdeaCompetitions';

    const SEARCH_DESIGN_DOCUMENT_NAME = 'searchIdeaCompetitons';
    const SEARCH_ALL = 'all';

    static function getSortKeys()
    {
        return array(
            'title',
            'company',
            'category',
            'startDate'
        );
    }

    static function getSearchNames()
    {
        return array_merge(parent::getSearchNames(), array(
            self::SEARCH_ALL
        ));
    }

    public static function getSearchFieldsForName($name)
    {
        $fields = parent::getSearchFieldsForName($name);
        if ($name == self::SEARCH_ALL) {
            $fields = array_merge($fields, array(
                'title',
                'text',
                'teaser',
                'rules',
                'endText',
                'company'
            ));
        }
        return $fields;
    }

    static function getSearchFiltersForName($name)
    {
        $fields = parent::getSearchFiltersForName($name);
        if ($name == self::SEARCH_ALL) {
            $fields = array_merge($fields, array(
                'title',
                'company',
                'isActive' => 'active'
            ));
        }
        return $fields;
    }


    /**
     * @return \Doctrine\ODM\CouchDB\DocumentRepository
     */
    static function getRepository()
    {
        return _dm()->getRepository(Epr_Idea_Competition::DOCUMENTNAME);
    }

    static function getAll()
    {
        $coll = self::getRepository();

        return $coll->findAll();
    }

    static function getActive()
    {
        $coll = self::getRepository();

        return $coll->findBy(
            array(
                'isActive' => true
            )
        );
    }

    static function getActiveUntilToday()
    {

        if (is_null(self::$validUntilToday)) {
            self::$validUntilToday = self::getActiveUntilDate(\Poundation\PDate::now());
        }

        return self::$validUntilToday;
    }

    static function getActiveCategoriesUntilDate($date)
    {

        if (is_null(self::$categoriesUntilToday)) {

            self::$categoriesUntilToday = array();

            $competitions = self::getActiveUntilDate($date);

            foreach ($competitions as $competition) {
                if ($competition instanceof Epr_Idea_Competition) {
                    $category = $competition->getCategory();
                    if ($category instanceof Epr_Idea_Category) {
                        if (!isset(self::$categoriesUntilToday[$category->getId()])) {
                            self::$categoriesUntilToday[$category->getId()] = $competition->getCategory();
                        }
                    }
                }
            }
        }

        return self::$categoriesUntilToday;

    }

    static function getActiveUntilDate(\Poundation\PDate $endDate)
    {

        $query = _dm()->createQuery('ideas', 'activeCompetitionsByEndDate')->onlyDocs(true);
        $query->setStartkey($endDate);
        $result = $query->execute();

//        if ($automaticSorting) {
//            $result = self::getSortedCompetitions(\Poundation\PArray::create($result));
//        }

        return $result;
    }


    static function getByCategoryId($id, $endDate, $automaticSorting = true)
    {

        $competitions = null;
        $query        = _dm()->createQuery('ideas', 'competitionsByCategory');
        $result       = $query->setKey($id)->onlyDocs(true)->execute();
        if (is_null($endDate)) {
            $competitions = $result;
        } else {

            if ($endDate instanceof DateTime) {
                $endDate = \Poundation\PDate::createDate($endDate);
            }

            if ($endDate instanceof \Poundation\PDate) {

                $competitions = array();
                foreach ($result as $competition) {
                    if ($competition instanceof Epr_Idea_Competition) {
                        if (!\Poundation\PDate::createDate($competition->getEndDate())->isBefore($endDate)) {
                            $competitions[] = $competition;
                        }
                    }
                }
            }
        }

        if ($competition && $automaticSorting) {
            $competition = self::getSortedCompetitions($competition);
        }

        return $competitions;
    }

    static function getSortedCompetitions($unsortedCompetitions)
    {

        $sortedCompetitions = null;

        if (is_array($unsortedCompetitions)) {
            $unsortedCompetitions = \Poundation\PArray::create($unsortedCompetitions);
        }

        if ($unsortedCompetitions instanceof \Poundation\PArray) {

            $unsortedCompetitions->sortUsingSortDescriptor(new \Poundation\PSortDescriptor('endDate', SORT_ASC));

            $futureCompetitions = array();
            $pastCompetitions   = array();
            foreach ($unsortedCompetitions as $competition) {
                if ($competition instanceof Epr_Idea_Competition) {
                    if (\Poundation\PDate::createDate($competition->getEndDate())->isPast()) {
                        $pastCompetitions[] = $competition;
                    } else {
                        $futureCompetitions[] = $competition;
                    }
                }
            }

            $sortedCompetitions = array_merge($futureCompetitions, array_reverse($pastCompetitions));
        }

        return $sortedCompetitions;
    }


    static function getCompetitionsMatching($query, $sortKey = null, $sortDirection = SORT_ASC, $count = 10, $offset = 0) {
        return self::getMatching(self::SEARCH_ALL, $query, $sortKey, $sortDirection, $count, $offset);
    }

}
