<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 05.10.12
 * Time: 10:56
 */

/** @Document (repositoryClass="Epr_Idea_Competition_Collection") */
class Epr_Idea_Competition extends Epr_Document_WebPage
{
    const DOCUMENTNAME = 'Epr_Idea_Competition';
    /** @Id */
    public $id;

    /** @Field(type="string") */
    private $company;

    /** @Field(type="string") */
    private $teaser;

    /** @Field(type="string") */
    private $rules;

    /** @Field(type="datetime") */
    private $startDate;

    /** @Field(type="datetime") */
    private $endDate;

	/** @Field(type="string") */
	private $endText;

    /** @ReferenceOne(targetDocument="Epr_Idea_Category") */
    private $category;

    /**
     * Sets the category.
     * @param $category
     * @return Epr_Idea_Competition
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Returns the category or an empty dummy if none is set.
     * @return Epr_Idea_Category
     */
    public function getCategory()
    {
        if (!$this->category instanceof Epr_Idea_Category) {
            $category = new Epr_Idea_Category();
            $category->setTitle('Uncategorized');
            $category->setActive(true);
            return $category;
        }
        return $this->category;
    }

    /**
     * Sets the company text.
     * @param $company
     * @return Epr_Idea_Competition
     */
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * Returns the company text.
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Sets the start date.
     * @param $startDate
     * @return Epr_Idea_Competition
     */
    public function setStartDate($startDate)
    {
        if (!$startDate instanceof DateTime) {
            $startDate = new DateTime($startDate);
        }
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * Returns the start date.
     * @return DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Sets the end date.
     * @param $endDate
     * @return Epr_Idea_Competition
     */
    public function setEndDate($endDate)
    {
        if (!$endDate instanceof DateTime) {
            $endDate = new DateTime($endDate);
        }
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * Returns the end date.
     * @return DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    public function doesAcceptIdeas()
    {

        $accepts = $this->isActive();

        if ($accepts) {
            $accepts = !(\Poundation\PDate::createDate($this->getStartDate())->isFuture());
        }

        if ($accepts) {
            $accepts = !(\Poundation\PDate::createDate($this->getEndDate())->isPast());
        }


        return $accepts;

    }

    /**
     * Sets the rules text.
     * @param $rules
     * @return Epr_Idea_Competition
     */
    public function setRules($rules)
    {
        $this->rules = $rules;
        return $this;
    }

    /**
     * Returns the rules text.
     * @return string
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * Sets the teaser text.
     * @param $teaser
     * @return Epr_Idea_Competition
     */
    public function setTeaser($teaser)
    {
        $this->teaser = $teaser;
        return $this;
    }

	/**
	 * Sets the teaser text.
	 * @param $endText
	 * @return Epr_Idea_Competition
	 */
	public function setEndText($endText)
	{
		$this->endText = $endText;
		return $this;
	}

    /**
     * Returns the teaser text.
     * @return string
     */
    public function getTeaser()
    {
        return $this->teaser;
    }

	/**
	 * Returns the end text.
	 * @return string
	 */
	public function getEndText()
	{
		return $this->endText;
	}

    public function getIdeas()
    {
        return Epr_Idea_Collection::findByCompetitionId($this->getId());
    }

    public function getActiveIdeas()
    {
        return Epr_Idea_Collection::findByCompetitionId($this->getId(), true);
    }
}
