<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 08.09.12
 * Time: 12:56
 */
use Doctrine\ODM\CouchDB\DocumentRepository;
use Doctrine\CouchDB\View\FolderDesignDocument;

class Epr_Idea_Collection extends Epr_Collection implements Epr_Paginatable
{

    const SORT_DESIGN_DOCUMENT_NAME = 'sortIdeas';

    static function getSortKeys()
    {
        return array(
            'submissionDate'
        );
    }

    /**
     * @return \Doctrine\ODM\CouchDB\DocumentRepository
     */
    static function getRepository() {
        return _dm()->getRepository(Epr_Idea::DOCUMENTNAME);
    }

    static function getAll() {
//        $coll = new self(_dm(), 'Epr_Idea_Collection');
        return new \Poundation\PArray(self::getRepository()->findAll());
//        return $coll->findAll();
    }

    static function findByCategoryId($id) {
        
        $query = _dm()->createQuery('ideas', 'byCategory');
        $result = $query->setKey($id)->execute();

        return $result;

    }

    static function findByCompetitionId($id, $onlyActive = false) {
        $viewName = ($onlyActive) ? 'activeByCompetition' : 'byCompetition';
        $query = _dm()->createQuery('ideas', $viewName);
        $result = $query->setKey($id)->onlyDocs(true)->execute();

        return $result;
    }
}