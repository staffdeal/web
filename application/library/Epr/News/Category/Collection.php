<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 08.09.12
 * Time: 12:56
 */
use Doctrine\ODM\CouchDB\DocumentRepository;
use Doctrine\CouchDB\View\FolderDesignDocument;

class Epr_News_Category_Collection extends Epr_Collection implements Epr_Paginatable
{

    const SORT_DESIGN_DOCUMENT_NAME = 'sortNewsCategory';

    private static $activeCategories = null;

    static function getSortKeys()
    {
        return array(
            'title',
            'slug',
            'isSticky'
        );
    }

    /**
     * @return \Doctrine\ODM\CouchDB\DocumentRepository
     */
    static function getRepository()
    {
        return _dm()->getRepository(Epr_News_Category::DOCUMENTNAME);
    }

    static function getAll()
    {
        $coll = self::getRepository();
        return $coll->findAll();
    }

    static function getActiveCategoriesInRange(\Poundation\PDateRange $range, $limit = 0)
    {

        if (is_null(self::$activeCategories)) {

            self::$activeCategories = array();

            $activeNews = Epr_News_Collection::getActiveInDateRange($range, $limit);
            foreach ($activeNews as $news) {

                if ($news instanceof Epr_News) {
                    $category = $news->getCategory();
                    if ($category->isEffectiveActive()) {
                        self::$activeCategories[$category->getId()] = $category;

                        while ($category->getCategory()) {
                            $category = $category->getCategory();
                            self::$activeCategories[$category->getId()] = $category;
                        }

                    }
                }
            }
        }

        return self::$activeCategories;
    }

    static function getCategoryByName($name)
    {
        $query = _dm()->createQuery('news', 'categoryByName');
        $result = $query->setKey($name)->onlyDocs(true)->execute();
        return (count($result) > 0) ? $result[0] : null;
    }


}
