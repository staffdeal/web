<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 14.09.12
 * Time: 12:31
 */

/** @Document (repositoryClass="Epr_News_Category_Collection") */
class Epr_News_Category extends Epr_Document_WebPage implements Epr_Categorizable
{

    const DOCUMENTNAME = 'Epr_News_Category';

    /** @Id */
    protected $id;

    /** @Field(type="color") */
    protected $color;

	/** @Field(type="boolean") */
	protected $isSticky;

    /** @ReferenceOne(targetDocument="Epr_News_Category")
     * @var Epr_News_Category
     * */
    protected $parentCategory;

    /**
     * Loads the category with the given id.
     * @param $id
     *
     * @return null|Epr_News_Category
     */
    static function getCategoryWithId($id)
    {

        $category = null;

        if (strlen($id) > 0) {
            $category = _dm()->find(self::DOCUMENTNAME, $id);
        }

        return $category;
    }

    /**
     * Sets the color.
     * @param Poundation\PColor $color
     * @return Epr_News_Category
     */
    public function setColor(\Poundation\PColor $color)
    {
        $this->color = $color;
        return $this;
    }

    /**
     * Removes the color.
     * @return Epr_News_Category
     */
    public function removeColor()
    {
        $this->color = null;
        return $this;
    }

    /**
     * Returns the color.
     * @return null|Poundation\PColor
     */
    public function getColor()
    {
        $color = ($this->color instanceof \Poundation\PColor) ? $this->color : \Poundation\PColor::whiteColor();
        return $color;
    }

	/**
	 * @return bool
	 */
	public function isSticky()
	{
		return ($this->isSticky == true);
	}


    /**
     * @return Epr_News_Category
     */
    public function getCategory()
    {
        return $this->parentCategory;
    }

    /**
     * Returns true if any of the parent category chain is the given candidate category.
     * @param Epr_News_Category $candidate
     *
     * @return bool
     */
    public function hasCategory(Epr_News_Category $candidate)
    {

        if ($this->parentCategory instanceof Epr_News_Category) {

            if ($this->parentCategory == $candidate) {
                return true;
            } else {
                return $this->getCategory()->hasCategory($candidate);
            }

        }
        return false;
    }


    /**
     * @param $category Epr_News_Category
     */
    public function setCategory($category)
    {
        $this->parentCategory = $category;
    }

    public function getNewsCount()
    {
        $newsCount = Epr_News_Collection::countByCategoryId($this->getId());
        return $newsCount;
    }

    public function getNews()
    {
        $news = Epr_News_Collection::getByCategoryId($this->getId());
        return $news;
    }

    public function getNewsInRange(\Poundation\PDateRange $range, $limit = 0, $skip=0)
    {
		$allNews = Epr_News_Collection::getByCategorySorted($range, true,$this->getId(),$limit,$skip);
		return $allNews;
    }

    public function isEffectiveActive()
    {

        if (is_null($this->parentCategory)) {
            return $this->isActive();
        } else {
            return $this->parentCategory->isEffectiveActive();
        }

    }

	/**
	 * @param $isSticky
	 * @return $this
	 */
	public function setSticky($isSticky)
	{
		$this->isSticky = ($isSticky == true);

		return $this;
	}



}
