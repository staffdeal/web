<?php

use Doctrine\ODM\CouchDB\DocumentRepository;
use Doctrine\CouchDB\View\FolderDesignDocument;
use Doctrine\CouchDB\View\Result;

class Epr_News_Collection extends Epr_Collection implements Epr_Paginatable
{
    const SORT_DESIGN_DOCUMENT_NAME = 'sortNews';

    const SEARCH_DESIGN_DOCUMENT_NAME = 'searchNews';
    const SEARCH_ALL = 'all';

    static function getSortKeys()
    {
        return array(
            'title',
            'hitsCounter',
            'viewCounter',
            'publishDate' => array('manualPublishDate', 'automaticPublishDate'),
            'category'
        );
    }

    static function getSearchNames()
    {
        return array_merge(parent::getSearchNames(), array(
            self::SEARCH_ALL
        ));
    }

    public static function getSearchFieldsForName($name)
    {
        $fields = array();
        if ($name == self::SEARCH_ALL) {
            $fields = array(
                'publicUrl',
                'source',
                'title',
                'text',
            );
        }
        return array_merge(parent::getSearchFieldsForName($name), $fields);

    }

    static function getSearchFiltersForName($name)
    {
        $fields = array();
        if ($name == self::SEARCH_ALL) {
            $fields = array(
                'state'
            );
        }
        return array_merge(parent::getSearchFiltersForName($name), $fields);
    }


    /**
     * @return \Doctrine\ODM\CouchDB\DocumentRepository
     */
    static function getRepository()
    {
        return _dm()->getRepository(Epr_News::DOCUMENTNAME);
    }

    static function getAll($reverse = true, $start = 0, $count = 0)
    {
        return new \Poundation\PArray(self::getRepository()->findAll());
    }

    static function getAllActive($reverse = true, $start = 0, $count = 0)
    {
        $query = _dm()->createQuery('news', 'activeByPublishDate')->setDescending(($reverse == true));

        if ($count > 0) {
            $query->setLimit($count);
        }

        if ($start > 0) {
            $query->setSkip($start);
        }

        $result = $query->onlyDocs(true)->execute();

        return $result;
    }

    static function getActiveInDateRange(\Poundation\PDateRange $range, $limit = 0, $reverse = true, $skip = 0)
    {

        $result = null;
        static $cache;

        if (!$cache) {
            $cache = array();
        }

        $firstKey = \Poundation\PDate::createDate($range->getStartDate())->getInISO8601Format();
        $lastKey  = \Poundation\PDate::createDate($range->getEndDate())->getInISO8601Format();

        $cacheKey = $firstKey . $lastKey . ($reverse ? 'r' : '') . $limit . '_' . $skip;
        if (isset($cache[$cacheKey])) {
            $result = $cache[$cacheKey];
        } else {

            $query = _dm()->createQuery('news', 'activeByPublishDate')->setDescending(($reverse == true));


            if (!$reverse) {
                $query->setStartKey($firstKey);
                $query->setEndKey($lastKey);
            } else {
                $query->setStartKey($lastKey);
                $query->setEndKey($firstKey);
            }

            if ($limit > 0) {
                $query->setLimit($limit);
            }

            if ($skip > 0) {
                $query->setSkip($skip);
            }

            $result = $query->onlyDocs(true)->execute();
            $cache[$cacheKey] = $result;
        }
        return $result;
    }

    static function getByCategoryId($id)
    {
        $query  = _dm()->createQuery('news', 'byCategory');
        $query->setKey($id);
		$result = $query->onlyDocs(true)->execute();
        return $result;
    }

	static function getByCategorySorted(\Poundation\PDateRange $range, $reverse = true,$id,$limit,$skip)
	{
		$query  = _dm()->createQuery('news', 'byCategorySorted')->setDescending(($reverse == true));

        $startDate = \Poundation\PDate::createDate($range->getStartDate())->getInISO8601Format();
        $endDate = \Poundation\PDate::createDate($range->getEndDate())->getInISO8601Format();

        if (!$reverse) {
            $query->setStartKey(array($id, $endDate));
            $query->setEndKey(array($id, $startDate));
        } else {
            $query->setStartKey(array($id, $endDate));
            $query->setEndKey(array($id, $startDate));
        }


        if ($limit > 0) {
			$query->setLimit($limit);
		}
		if ($skip > 0) {
			$query->setSkip($skip);
		}
		$query->onlyDocs(true);

        $result = $query->execute();
		return $result;

	}

    static function countByCategoryId($id)
    {
        $query  = _dm()->createQuery('news', 'countByCategory');
        $result = $query->setKey($id)->execute();

        if (count($result) > 0) {
            return $result[0]['value'];
        }

        return 0;
    }

    static function getByApiID($apiID)
    {
        $result = self::getRepository()->findOneBy(array('apiID' => $apiID));

        return $result;
    }

    static function getDraftsUntilDate(\Poundation\PDate $date) {

        $query = _dm()->createQuery('news', 'draftsByAutomaticPublishDate');
        $query->onlyDocs(true);
        $query->setEndKey($date->getInISO8601Format());
        $result = $query->execute();

        return (count($result) > 0 ) ? $result : null;
    }

    static function getDraftsUntilNow() {
        return self::getDraftsUntilDate(\Poundation\PDate::now());
    }

    static function getMatchingNews($query, $sortKey = null, $sortDirection = SORT_ASC, $count = 10, $offset = 0) {
        return self::getMatching(self::SEARCH_ALL, $query, $sortKey, $sortDirection, $count, $offset);
    }

}