<?php

interface Epr_Module
{
    const CONTEXT_GLOBAL = 'global';
    const CONTEXT_OBJECT = 'object';

    /* Implent these methods */
    static public function identifier();

    static public function title();

    static public function description();

    static public function instance();

    public function getSettingsFormFields();

    /**
     * @param Zend_Form_Element $field
     */
    public function saveSettingsField($field);

    public function getSettingsSaveButtonCaption();

    /* These methods are implemented by Epr_Module_Abstract */
    public function getId();

    public function isActive();

}
