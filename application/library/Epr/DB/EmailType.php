<?php

class Epr_DB_EmailType extends \Doctrine\ODM\CouchDB\Types\Type {

	/**
	 * @param Poundation\PMailAddress $value
	 * @return string
	 */
	public function convertToCouchDBValue($value) {
		if ($value instanceof \Poundation\PMailAddress) {
			return (string)$value;
		}
		return null;
	}

	/**
	 * @param string $value
	 * @return Poundation\PMailAddress
	 */
	public function convertToPHPValue($value) {
		$mail = \Poundation\PMailAddress::createFromString($value);
		return $mail;
	}

}
