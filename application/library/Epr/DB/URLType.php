<?php

class Epr_DB_URLType extends \Doctrine\ODM\CouchDB\Types\Type {

    /**
     * @param Poundation\PURL $value
     * @return string
     */
    public function convertToCouchDBValue($value) {
        if ($value instanceof \Poundation\PURL) {
            return (string)$value;
        }
        return null;
    }

    /**
     * @param string $value
     * @return Poundation\PURL
     */
    public function convertToPHPValue($value) {
        $url = \Poundation\PURL::URLWithString($value);
        return $url;
    }

}
