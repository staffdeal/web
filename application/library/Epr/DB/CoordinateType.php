<?php

class Epr_DB_CoordinateType extends \Doctrine\ODM\CouchDB\Types\Type
{

	/**
	 * @param \Poundation\PCoordinate $value
	 *
	 * @return string
	 */
	public function convertToCouchDBValue($value)
	{

		$dbValue = null;

		if ($value instanceof \Poundation\PCoordinate) {

			$dbValue = (string)$value;
		}

		return $dbValue;
	}

	/**
	 * @param string $value
	 *
	 * @return \Poundation\PCoordinate
	 */
	public function convertToPHPValue($value)
	{

		$coordinate = null;

		return \Poundation\PCoordinate::createCoordinateFromString($value);

	}

}
