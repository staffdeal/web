<?php

class Epr_DB_AddressType extends \Doctrine\ODM\CouchDB\Types\Type
{

    const FIELDNAME_STREET = 'street';
    const FIELDNAME_ZIP = 'zip';
    const FIELDNAME_CITY = 'city';
    const FIELDNAME_COUNTRY = 'country';
    const FIELDNAME_COORDINATE = 'coordinate';
    const FIELDNAME_COORDINATE_LAT = 'lat';
    const FIELDNAME_COORDINATE_LON = 'lon';

    /**
     * @param \Poundation\PAddress $value
     * @return string
     */
    public function convertToCouchDBValue($value) {
        if ($value instanceof \Poundation\PAddress) {
            $data = array();
            if ($value->getStreet()) {
                $data[self::FIELDNAME_STREET] = $value->getStreet();
            }
            if ($value->getZip()) {
                $data[self::FIELDNAME_ZIP] = $value->getZip();
            }
            if ($value->getCity()) {
                $data[self::FIELDNAME_CITY] = $value->getCity();
            }
            if ($value->getCountry()) {
                $data[self::FIELDNAME_COUNTRY] = $value->getCountry();
            }
            if ($value->getCoordinate()) {
                $coordinate = $value->getCoordinate();
                if ($coordinate instanceof \Poundation\PCoordinate) {
                    $data[self::FIELDNAME_COORDINATE][self::FIELDNAME_COORDINATE_LAT] = $coordinate->getLatitude();
                    $data[self::FIELDNAME_COORDINATE][self::FIELDNAME_COORDINATE_LON] = $coordinate->getLongitude();
                }
            }
            return $data;
        } else {
            return '';
        }
    }

    /**
     * @param string $value
     * @return \Poundation\PAddress
     */
    public function convertToPHPValue($value) {
        $address = null;
        if (is_array($value)) {
            $address = new \Poundation\PAddress();
            if (isset($value[self::FIELDNAME_STREET])) {
                $address->setStreet($value[self::FIELDNAME_STREET]);
            }
            if (isset($value[self::FIELDNAME_ZIP])) {
                $address->setZip($value[self::FIELDNAME_ZIP]);
            }
            if (isset($value[self::FIELDNAME_CITY])) {
                $address->setCity($value[self::FIELDNAME_CITY]);
            }
            if (isset($value[self::FIELDNAME_COUNTRY])) {
                $address->setCountry($value[self::FIELDNAME_COUNTRY]);
            }
            if (isset($value[self::FIELDNAME_COORDINATE])) {
                $rawCoordinate = $value[self::FIELDNAME_COORDINATE];
                if (is_array($rawCoordinate) && count($rawCoordinate) == 2) {
                    if (isset($rawCoordinate[self::FIELDNAME_COORDINATE_LAT]) && isset($rawCoordinate[self::FIELDNAME_COORDINATE_LON])) {
                        $rawLat = $rawCoordinate[self::FIELDNAME_COORDINATE_LAT];
                        $rawLon = $rawCoordinate[self::FIELDNAME_COORDINATE_LON];
                        if (strlen($rawLat) > 0 && strlen($rawLon) > 0) {
                            $coordinate = \Poundation\PCoordinate::createCoordinate((float)$rawLat, (float)$rawLon);
                            if ($coordinate) {
                                $address->setCoordinate($coordinate);
                            }
                        }
                    }
                }
            }
        }
        return $address;
    }

}
