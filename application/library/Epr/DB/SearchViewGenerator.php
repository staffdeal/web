<?php

class Epr_DB_SearchViewGenerator implements Doctrine\CouchDB\View\DesignDocument {

    private $documentName;
    private $searchFields = array();
    private $filterFields = array();
    private $sortKeys = array();

    public function __construct($documentName, $searchFields, $filterFields = null, $sortKeys = null) {
        $this->documentName = $documentName;
        if (is_array($searchFields)) {
            $this->searchFields = $searchFields;
        }
        if (is_array($filterFields)) {
            $this->filterFields = $filterFields;
        }
        if (is_array($sortKeys)) {
            $this->sortKeys = $sortKeys;
        }
    }

    public function getData()
    {

        $searches = array();
        foreach($this->searchFields as $searchName => $fields) {
            $searches[$searchName] = array();
            $function = "function(doc) {if (doc.type == \"" . $this->documentName . "\") { var ret=new Document(); ";
            foreach ($fields as $field) {
                $function.= "ret.add(doc." . $field ."); ";
            }

            foreach($this->sortKeys as $key => $sortKey) {
                $sortExpression = '';
                $sortName = '';
                if (is_string($key) && is_array($sortKey)) {
                    foreach($sortKey as $subKey => $subField) {
                        $sortKey[$subKey] = 'doc.' . $subField;
                    }
                    $sortExpression = implode(' || ', $sortKey);
                    $sortName = $key;
                } else {
                    $sortExpression = 'doc.' . $sortKey;
                    $sortName = $sortKey;
                }
                $function.= "ret.add(" . $sortExpression .", {field: \"sort_" . $sortName . "\", index: \"not_analyzed\"}); ";
            }

            $filterFields = (isset($this->filterFields[$searchName])) ? $this->filterFields[$searchName] : array();
            foreach($filterFields as $filterKey => $filterField) {
                if (is_integer($filterKey)) {
                    $filterKey = $filterField;
                }
                $function.= "ret.add(doc." . $filterKey .", {field: \"" . $filterField . "\", store: \"yes\"}); ";
            }

            $function.= "ret.add(doc.type, {field: \"type\", store: \"yes\"}); return ret;} else { return null; }}";
            $searches[$searchName]['index'] = $function;
        }

        $data = array('fulltext' => $searches);
        return $data;
    }


}