<?php

class Epr_DB_DateTimeType extends \Doctrine\ODM\CouchDB\Types\Type {


    /**
     * @param mixed $value
     * @return mixed
     */
    public function convertToCouchDBValue($value)
    {
        if (!$value instanceof \Poundation\PDate) {
            $value = new \Poundation\PDate($value);
        }

        return $value->getInISO8601Format();
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    public function convertToPHPValue($value)
    {
        $date = new \Poundation\PDate($value);

        if ($date == null) {
            $date = DateTime::createFromFormat('Y-m-d H:i:s.u', $value);
            return $date;
        } else {
            return $date->getDateTime();
        }
    }


}
