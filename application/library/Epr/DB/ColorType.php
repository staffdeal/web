<?php

class Epr_DB_ColorType extends \Doctrine\ODM\CouchDB\Types\Type {

    /**
     * @param \Poundation\PColor $value
     * @return string
     */
    public function convertToCouchDBValue($value) {
        if ($value instanceof \Poundation\PColor) {
            return $value->getHexString(true, false);
        } else {
            return '';
        }
    }

    /**
     * @param string $value
     * @return \Poundation\PColor
     */
    public function convertToPHPValue($value) {
        $color = \Poundation\PColor::colorFromString($value);
        return $color;
    }


}
