<?php

class Epr_DB_SortViewGenerator implements Doctrine\CouchDB\View\DesignDocument
{

    private $_documentName;
    private $_keys;
    private $_excludes;

    public function __construct($documentName, $keys, $excludes = null)
    {

        if (strlen($documentName) == 0 || count($keys) == 0) {
            throw new \Epr_Exception('Need a document name and keys');
        }

        $this->_documentName = $documentName;
        $this->_excludes     = $excludes;
        $this->_keys         = array();
        if (is_array($keys)) {
            foreach($keys as $key => $fields) {
                if (is_integer($key)) {
                    $key = $fields;
                }
                if (is_string($fields)) {
                    $fields = array($fields);
                }
                $this->_keys[$key] = $fields;
            }
        }
    }

    public function getData()
    {

        $views = array();
        foreach ($this->_keys as $name => $fields) {
            $viewName         = getSortViewName($name);
            $views[$viewName] = array(
                'map' => self::generateSortView($this->_documentName, $name, $fields, $this->_excludes)
            );
        }

        return array('views' => $views);
    }


    static public function generateSortView($documentName, $name, $fields, $exclusions)
    {

        $exclusionStrings = array();
        if (is_array($exclusions) && count($exclusions) > 0) {
            foreach($exclusions as $field => $value) {
                if (is_null($value)) {
                    $exclusionStrings[] = '!doc.' . $field;
                } else if (is_bool($value)) {
                    $exclusionStrings[] = '!doc.' . $field;
                } else {
                    $exclusionStrings[] = 'doc.' . $field . " == '" . $value . "'";
                }

            }
        }

        $filterString = null;
        if (count($exclusionStrings) > 0) {
            $filterString = implode(' && ', $exclusionStrings);
        }

        foreach($fields as $key => $field) {
            $fields[$key] = 'doc.' . $field;
        }
        $emitString = implode(' || ', $fields);


        $template = "function (doc) {
                if (doc.type == '%s'" . (($filterString) ? (' && (' . $filterString . ')') : '') . ") {
                    emit(%s, doc);
                }
            }";

        $view = sprintf($template, $documentName, $emitString);

        return $view;
    }

}