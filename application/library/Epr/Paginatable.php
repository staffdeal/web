<?php
/**
 * @package Backend
 * @author daniel 
 */

interface Epr_Paginatable {

    static function getSortKeys();
    static function registerSortDesignDocument($documentName, $client);
    static function getSorted($key, $direction = SORT_ASC, $count = 10, $offset = 0);

}