<?php

class Epr_Feed_Collection extends Epr_Collection implements Epr_Paginatable
{

    const SORT_DESIGN_DOCUMENT_NAME = 'sortFeeds';

    static function getSortKeys()
    {
        return array(
            'title',
            'url'
        );
    }


	/**
	 * @return \Doctrine\ODM\CouchDB\DocumentRepository
	 */
	static function getRepository()
	{
		return _dm()->getRepository(Epr_Feed::DOCUMENTNAME);
	}

	static function getAll()
	{
		return self::getRepository()->findAll();
	}

	/**
	 * Returns the feed with the given feed.
	 * @param \Poundation\PURL $url
	 *
	 * @return Epr_Feed
	 */
	static public function getFeedWithURL(\Poundation\PURL $url)
	{

		$query = _dm()->createQuery('news', 'feedsByUrl')->onlyDocs(true);
		$query->setKey((string)$url);

		$result = $query->execute();
		return (count($result) > 0) ? $result[0] : null;

	}

}