<?php

/** @Document (repositoryClass="Epr_Feed_Item_Collection") */
class Epr_Feed_Item extends Epr_Document
{

	const DOCUMENTNAME = 'Epr_Feed_Item';

	/** @Id */
	protected $id;

	/** @Field(type="string") */
	protected $title;

	/** @Field(type="string") */
	protected $text;

	/** @Field(type="string") */
	private $sourceId;

	/** @Field(type="url") */
	private $url;

	/** @Field(type="datetime") */
	private $releaseDate;

	/** @Field(type="mixed") */
	private $authors;

	/** @Field(type="boolean") */
	private $convertedToNews = false;

	/** @Field(type="mixed") */
	private $imageURLs;

	public function __construct($sourceId, $documentId = null)
	{

		parent::__construct();

		if (!is_null($documentId)) {
			$this->id = $documentId;
		}
		$this->sourceId = $sourceId;
		$this->releaseDate = \Poundation\PDate::today();
	}

	public function getSourceId()
	{
		return $this->sourceId;
	}

	/**
	 * Sets the url.
	 *
	 * @param \Poundation\PURL $url
	 *
	 * @return $this
	 */
	public function setUrl($url)
	{
		if (!$url instanceof \Poundation\PURL) {
			$url = \Poundation\PURL::URLWithString((string)$url);
		}
		$this->url = $url;

		return $this;
	}

	/**
	 * Returns the url.
	 *
	 * @return \Poundation\PURL|null
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * Returns the title.
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Sets the title.
	 *
	 * @param $title
	 *
	 * @return Epr_Document_WebPage
	 */
	public function setTitle($title)
	{
		$this->title = (string)$title;

		return $this;
	}

	/**
	 * Returns the text.
	 *
	 * @return string
	 */
	public function getText()
	{
		return $this->text;

	}

	/**
	 * Sets the text.
	 *
	 * @param $text
	 *
	 * @return Epr_Document_WebPage
	 */
	public function setText($text)
	{
		$this->text = (string)$text;

		return $this;
	}


	/**
	 * Sets the release date.
	 *
	 * @param Datetime $releaseDate
	 *
	 * @return $this
	 */
	public function setReleaseDate($releaseDate)
	{
		$this->releaseDate = $releaseDate;

		return $this;
	}

	/**
	 * Returns the relase date.
	 *
	 * @return Datetime
	 */
	public function getReleaseDate()
	{
		return $this->releaseDate;
	}

	/**
	 * Sets on or multiple authors.
	 *
	 * @param $authors
	 *
	 * @return $this
	 */
	public function setAuthors($authors)
	{
		if (!is_array($this->authors)) {
			$this->authors = array();
		}
		if (is_array($authors) || $authors instanceof ArrayObject) {
			foreach ($authors as $author) {
				$authorName = null;
				if (is_string($author)) {
					$authorName = $author;
				} else if (is_array($author)) {
					if (isset($author['name'])) {
						$authorName = $author['name'];
					}
				}

				if (is_string($authorName)) {
					$this->authors[] = $authorName;
				}
			}
		} else {
			$this->authors[] = (string)$authors;
		}

		return $this;
	}

	/**
	 * Returns all or a specific author.
	 *
	 * @param $index
	 *
	 * @return mixed
	 */
	public function getAuthors($index = -1)
	{
		if (0 <= $index && $index < count($this->authors)) {
			return $this->authors[$index];
		} else {
			return $this->authors;
		}
	}

	/**
	 * Returns true if the item has been converted to a news item.
	 *
	 * @return bool
	 */
	public function getConvertedToNews()
	{
		return $this->convertedToNews;
	}

	/**
	 * Marks the item as converted to a news item.
	 *
	 * @return $this
	 */
	public function markAsConvertedToNews()
	{
		$this->convertedToNews = true;

		return $this;
	}

	public function getImages()
	{

		if (is_null($this->imageURLs) && strpos($this->getText(), '<img') !== false) {

			$stripedText = strip_tags($this->getText(), '<img>');
			$cleanedText = preg_replace('#&(?=[a-z_0-9]+=)#', '&amp;', $stripedText);
			unset($stripedText);

			$wrappedText = '<div>' . $cleanedText . '</div>';
			unset($cleanedText);

			$xml = null;
			try {

				$doc = new DOMDocument();
				$doc->strictErrorChecking = FALSE;
				$doc->loadHTML($wrappedText);
				$xml = simplexml_import_dom($doc);

				//$xml = new SimpleXMLElement($wrappedText);
			} catch (Exception $e) {

			}



			if ($xml) {
				$rawImages = $xml->xpath('//img');
				foreach ($rawImages as $rawImage) {

					if ($rawImage instanceof SimpleXMLElement) {

						// first, we read all the attributes
						$urlString = null;
						$width     = -1;
						$height    = -1;

						foreach ($rawImage->attributes() as $name => $value) {

							$name = strtolower($name);

							if ($name == 'src') {
								$urlString = (string)$value;
							} else if ($name == 'width') {
								$width = (int)$value;
							} else if ($name == 'height') {
								$height = (int)$value;
							}

						}

						// then we filter images that seem inappropriate

						$useImage = true;

						$sharingTestString = __($urlString)->lowercase();
						if ($sharingTestString->containsOneOrMore(array(
																	   'sharing',
																	   'share',
																	   'social'
																  ))
						) {
							if ($sharingTestString->containsOneOrMore(array(
																		   'facebook',
																		   'twitter',
																		   'email',
																		   'linkedin',
																		   'googleplus',
																		   'xing',
																		   'weibo'
																	  ))
							) {
								$useImage = false;
							}
						}

						if ($useImage) {
							if (!\Poundation\PURL::isValidURLString($urlString)) {
								$useImage = false;
							}
						}

						if ($useImage) {
							if ($width != -1 || $height != -1) {
								if ($width < 50 || $height < 50) {
									$useImage = false;
								}
							}
						}

						// if we can use we add it to the list
						if ($useImage) {
							$this->imageURLs[] = $urlString;
						}

					}

				}
			}
		}

		return $this->imageURLs;

	}

	public function importEnclosure($data) {
		if (isset($data->type)) {
			$mime = \Poundation\PMIME::createMIMEWithType($data->type);
			if ($mime && $mime->isImage()) {
				if (isset($data->url)) {
					if (\Poundation\PURL::isValidURLString($data->url)) {
						$this->imageURLs = array();
						$this->imageURLs[] = $data->url;
					}
				}
			}
		}
	}

	static function compareByReleaseDate(Epr_Feed_Item $item1, Epr_Feed_Item $item2)
	{
		if ($item1->getReleaseDate() > $item2->getReleaseDate()) {
			return 1;
		} else if ($item1->getReleaseDate() < $item2->getReleaseDate()) {
			return -1;
		} else {
			return 0;
		}
	}

}