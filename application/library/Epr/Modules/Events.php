<?php

/** @Document */
class Epr_Modules_Events extends Epr_Module_API_Abstract implements Epr_Module_API, Epr_Module_Cms {

    const PAGE_INDEX_PATH = 'events';
    const INDEX_SIDEBAR_LEFT_PATH = 'sidebar-left';
    const INDEX_LIST_PATH = 'list';

    const PAGE_SHOW_PATH = 'show-event';
    const SHOW_LIST_PATH = 'list';
    const SHOW_DETAILS_PATH = 'details';
    const DETAILS_BOOKING_SENT_PATH = 'booking-sent';

    static public function title()
    {
        return 'Events';
    }

    static public function description()
    {
        return 'Manages events';
    }

    /**
     * @return Epr_Modules_Events
     */
    static public function instance() {
        return parent::instance();
    }

    public function getAPIPath() {
        return 'events';
    }

    public function getTemplateData()
    {
        return array(
            array(
                'id' => 'eventDetails',
                'platform' => Epr_Module_API::PLATFORM_IOS,
                'title' => 'Event Template iOS',
                'description' => 'Enter the template HTML for displaying event details (iOS).'
            ),
            array(
                'id' => 'eventDetails',
                'platform' => Epr_Module_API::PLATFORM_ANDROID,
                'title' => 'Event Template Android',
                'description' => 'Enter the template HTML for displaying event details (Android).'
            )
        );
    }

    public function getPages() {

        $pages = array();

        // ----------------------------------------------------

        $indexPage = Epr_Cms_Page::getPageWithPath(self::PAGE_INDEX_PATH);
        if (is_null($indexPage)) {
            $indexPage = new Epr_Cms_Page(self::PAGE_INDEX_PATH);
            _dm()->persist($indexPage);
        }

        $indexLeft = $indexPage->getElementWithPath(self::INDEX_SIDEBAR_LEFT_PATH);
        if (is_null($indexLeft)) {
            $indexLeft = new Epr_Cms_Element_HTML(self::INDEX_SIDEBAR_LEFT_PATH);
            $indexLeft->setWidth(3);
            $indexLeft->setOrder(0);
            $indexPage->addElement($indexLeft);
        }

        $listElement = $indexPage->getElementWithPath(self::INDEX_LIST_PATH);
        if (is_null($listElement)) {
            $listElement = new Epr_Modules_Events_ListElement(self::INDEX_LIST_PATH);
            $listElement->setWidth(9);
            $listElement->setOrder(10);
            $listElement->setTitle(_t('events'));
            $listElement->setNumberOfItemsToDisplay(100);
            $indexPage->addElement($listElement);
        }

        $pages[] = $indexPage;

        // ----------------------------------------------------

        $showPage = Epr_Cms_Page::getPageWithPath(self::PAGE_SHOW_PATH);
        if (is_null($showPage)) {
            $showPage = new Epr_Cms_Page(self::PAGE_SHOW_PATH);
            _dm()->persist($showPage);
        }

        $showPageLeft = $showPage->getElementWithPath(self::SHOW_LIST_PATH);
        if (is_null($showPageLeft)) {
            $showPageLeft = new Epr_Modules_Events_ListElement(self::SHOW_LIST_PATH);
            $showPageLeft->setWidth(3);
            $showPageLeft->setOrder(10);
            $showPageLeft->setTitle(_t('events'));
            $showPageLeft->setShowImages(false);
            $showPageLeft->setNumberOfItemsToDisplay(100);
            $showPage->addElement($showPageLeft);
        }

        $showPageDetails = $showPage->getElementWithPath(self::SHOW_DETAILS_PATH);
        if (is_null($showPageDetails)) {
            $showPageDetails = new Epr_Modules_Events_DetailsElement(self::SHOW_DETAILS_PATH);
            $showPageDetails->setWidth(9);
            $showPageDetails->setOrder(10);
            $showPage->addElement($showPageDetails);
        }

        $showPageDetailsBookingSent = $showPageDetails->getElementWithPath(self::DETAILS_BOOKING_SENT_PATH);
        if (is_null($showPageDetailsBookingSent)) {
            $showPageDetailsBookingSent = new Epr_Cms_Element_HTML(self::DETAILS_BOOKING_SENT_PATH);
            $showPageDetailsBookingSent->setContent(_t("We received your booking request. We'll get in touch with you soon."));
            $showPageDetails->addElement($showPageDetailsBookingSent);
        }

        $pages[] = $showPage;

        // ----------------------------------------------------

        return $pages;
    }

}