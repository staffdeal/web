<?php

/** @Document */
class Epr_Modules_MessageAPI extends Epr_Module_Builtin_Abstract implements Epr_Module_Builtin, Epr_Com_Message_Authority
{

    /** @Field(type="string") */
    private $backendKey;

    /** @Field(type="string") */
    private $consumerKey;


    public function getBackendKey()
    {
        if ($this->backendKey == null) {
            $this->backendKey = Epr_Com_Connection::connectionKey();
        }
        return $this->backendKey;
    }

    public function getConsumerKey()
    {
        if ($this->consumerKey == null) {
            $this->consumerKey = Epr_Com_Connection::connectionKey();
        }
        return $this->consumerKey;
    }

    /**
     * Returns the key that is used to sign a message.
     * @return string
     */
    public function signingKey()
    {
        return $this->backendKey;
    }

    /**
     * Returns the key the communication partner is supposed to use.
     * @return string
     */
    public function remoteKey()
    {
        return $this->consumerKey;
    }


    static public function title()
    {
        return 'Message API Access';
    }

    static public function description()
    {
        return 'Controls access to the Message API.';
    }

    public function getSettingsFormFields()
    {
        $fields = parent::getSettingsFormFields();

        $backendKeyField = new Zend_Form_Element_Text('key1');
        $backendKeyField->setLabel('Backend Key');
        $backendKeyField->setAttrib('class', 'inp-form-noborder');
        $backendKeyField->setAttrib('disabled', 'disabled');
        $backendKeyField->setAttrib('size',50);
        $backendKeyField->setDecorators(array('Composite'));
        $backendKeyField->setValue($this->getBackendKey());
        $fields[] = $backendKeyField;

        $consumerKeyField = new Zend_Form_Element_Text('key2');
        $consumerKeyField->setLabel('Consumer Key');
        $consumerKeyField->setAttrib('class', 'inp-form-noborder');
        $consumerKeyField->setAttrib('disabled', 'disabled');
        $consumerKeyField->setAttrib('size',50);
        $consumerKeyField->setDecorators(array('Composite'));
        $consumerKeyField->setValue($this->getConsumerKey());
        $fields[] = $consumerKeyField;

        return $fields;
    }

    public function getSettingsSaveButtonCaption() {
        return "Back to modules list";
    }


}