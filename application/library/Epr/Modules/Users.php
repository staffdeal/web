<?php

/** @Document */
class Epr_Modules_Users extends Epr_Module_Builtin_Abstract implements Epr_Module_Builtin, Epr_Module_Cms {

	const PAGE_LOGIN_PATH = 'login';
	const LOGIN_INFO_PATH = 'info';

	const PAGE_REGISTRATION_FINISH_PATH = 'registration-finish';
	const REGISTRATION_FINISH_CONTENT = 'content';

	const PAGE_VERIFICATION_PATH = 'verification';
	const VERIFICATION_SUCCESS_PATH = 'success';
	const VERIFICATION_FAIL_PATH = 'fail';

	const PAGE_ACCOUNT_PATH = 'account';

	const PAGE_RESET_PASSWORD_PATH = 'reset';
	const RESET_DESCRIPTION_PATH = 'description';

	const PAGE_DEALS = 'user-deals';
	const DEALS_INDEX = 'index';
	const DEALS_DESCRIPTION = 'description';

	/** @Field(type="string") */
	private $userRegistrationModuleId;

	/** @Field(type="string") */
	private $resetUserMailContent;

    public static function title() {
		return "Users";
	}

	public static function description() {
		return "Manages users and access control";
	}

	public function getUserRegistrationModule() {
		$module = false;
		$id = $this->userRegistrationModuleId;
		if ($id) {
			$module = _app()->getModuleWithIdentifier($id);
		}
		return $module;
	}
	
	public function setUserRegistrationModule($module) {
		if (Poundation\PClass::classFromObject($module)->implementsInterface('Epr_Module_UserRegistration')) {
			$this->userRegistrationModuleId = $module->getId();
		}
	}

	public function getResetUserMailContent() {
		return $this->resetUserMailContent;
	}

	/**
	 * @param $text
	 *
	 * @return $this
	 */
	public function setResetUserMailContent($text) {
		if (is_string($text) || is_null($text)) {
			$this->resetUserMailContent = $text;
		}
		return $this;
	}

	private function getUserRegistrationModuleNames() {

		$modules = array(''=>'');

		foreach(_app()->getUserRegistrationModules() as $existingModule) {
			if ($existingModule instanceof Epr_Module_UserRegistration_Abstract) {
				$moduleId = $existingModule->getId();
				$moduleTitle = Poundation\PClass::classFromObject($existingModule)->invokeMethod('title');
				$modules[$moduleId] = $moduleTitle;
			}
		}

		return $modules;

	}
	
	public function getSettingsFormFields() {
		$fields = array();
		
		$registrationModules = new Zend_Form_Element_Select('registrationModule');
		$registrationModules->setLabel('Registration Module');
		// TODO: Using the styled class causes the selected element not to be 'selected'
		//$registrationModules->setAttrib('class','styledselect_form_1');
		$registrationModules->setMultiOptions($this->getUserRegistrationModuleNames());
			
		$registrationModule = $this->getUserRegistrationModule();
		if ($registrationModule) {
			$registrationModules->setValue($registrationModule->getId());
		}

		$registrationModules->setDescription('Choose the module which is used to handle user registrations.');

		$fields['registrationModule'] = $registrationModules;

		$resetPasswordMail = new Zend_Form_Element_Textarea('resetPWMail');
		$resetPasswordMail->setValue($this->getResetUserMailContent());
		$resetPasswordMail->setLabel('Reset a user password mail content');
		$resetPasswordMail->setDescription('Enter the mail that is sent to the user when the password is reseted. The variable %LINK% gets replaced with the actual link. Supported variables: %EMAIL%, %LINK%.');
		$resetPasswordMail->setAttrib('class', 'inp-form wysiwyg wide');
		$fields[] = $resetPasswordMail;

		return $fields;
	}
	
	
	
	/* (non-PHPdoc)
	 * @see Epr_Module::saveSettingsField()
	 */
	public function saveSettingsField($field) {
		if ($field->getName() == 'registrationModule') {

			$selectedModuleId = $field->getValue();

			$registrationModule = _app()->getModuleWithIdentifier($selectedModuleId);

			if ($registrationModule) {
				$this->setUserRegistrationModule($registrationModule);
                return true;
			}
            return false;
        } else if ($field->getName() == 'resetPWMail') {
			$this->resetUserMailContent = $field->getValue();
			return true;
		} else {
			return parent::saveSettingsField($field);
		}
	}

	public function canResetUser() {

		$hasMailContent = strlen($this->resetUserMailContent) > 0;

		return ($hasMailContent);
	}

	public function resetUser(Epr_User $user) {

		$success = false;

		$resetID = $user->resetPassword();
		if (is_string($resetID) && strlen($resetID) > 0) {

			$url = _app()->getPublicURL();
			$url->addPathComponent('user/new-password/' . $resetID);

			$mailContent = '%LINK%';
			if (strlen($this->getResetUserMailContent()) > 0) {
				//$mailContent = $mailTemplate = new \Poundation\PTemplate($this->getResetUserMailContent());
			}
			$mailTemplate = new \Poundation\PTemplate($mailContent);
			$mailTemplate->setField('link', $url);
			$mailTemplate->setField('email', $user->getEmail());

			$success = $user->sendMail('Your new password', $mailTemplate);

		}

		return $success;
	}

	public function getPages()
	{
		$pages = array();

		// Login Page
		$loginPage = Epr_Cms_Page::getPageWithPath(self::PAGE_LOGIN_PATH);
		if (is_null($loginPage)) {
			$loginPage = new Epr_Cms_Page(self::PAGE_LOGIN_PATH);
			$loginPage->setTitle('Login');
			_dm()->persist($loginPage);
		}

		$loginInfoElement = $loginPage->getElementWithPath(self::LOGIN_INFO_PATH);
		if (is_null($loginInfoElement)) {
			$loginInfoElement = new Epr_Cms_Element_HTML(self::LOGIN_INFO_PATH);
			$loginInfoElement->setTitle('Information');
			$loginInfoElement->setContent('Some <b>details</b> about the registration.');
			$loginPage->addElement($loginInfoElement);
		}

		$pages[] = $loginPage;

		// Registration
		$registrationFinishedPage = Epr_Cms_Page::getPageWithPath(self::PAGE_REGISTRATION_FINISH_PATH);
		if (is_null($registrationFinishedPage)) {
			$registrationFinishedPage = new Epr_Cms_Page(self::PAGE_REGISTRATION_FINISH_PATH);
			$registrationFinishedPage->setTitle('Registration finished');
			_dm()->persist($registrationFinishedPage);
		}

		$registrationContent = $registrationFinishedPage->getElementWithPath(self::REGISTRATION_FINISH_CONTENT);
		if (is_null($registrationContent)) {
			$registrationContent = new Epr_Cms_Element_HTML(self::REGISTRATION_FINISH_CONTENT);
			$registrationContent->setTitle('Registration done');
			$registrationContent->setContent('A nice text telling the user that the registration has been finished. You need to tell him <b>which steps</b> he needs to do now.');
			$registrationFinishedPage->addElement($registrationContent);
		}

		$pages[] = $registrationFinishedPage;

		// Verification
		$verificationPage = Epr_Cms_Page::getPageWithPath(self::PAGE_VERIFICATION_PATH);
		if (is_null($verificationPage)) {
			$verificationPage = new Epr_Cms_Page(self::PAGE_VERIFICATION_PATH);
			$verificationPage->setTitle('Verification');
			_dm()->persist($verificationPage);
		}

		$verificationSuccess = $verificationPage->getElementWithPath(self::VERIFICATION_SUCCESS_PATH);
		if (is_null($verificationSuccess)) {
			$verificationSuccess = new Epr_Cms_Element_HTML(self::VERIFICATION_SUCCESS_PATH);
			$verificationSuccess->setTitle('Verification done');
			$verificationSuccess->setContent('A <b>nice</b> text explaining that the verification has been done and the user is now able to login.');
			$verificationPage->addElement($verificationSuccess);
		}

		$verificationFail = $verificationPage->getElementWithPath(self::VERIFICATION_FAIL_PATH);
		if (is_null($verificationFail)) {
			$verificationFail = new Epr_Cms_Element_HTML(self::VERIFICATION_FAIL_PATH);
			$verificationFail->setTitle('Verification failed');
			$verificationFail->setContent('A <b>nice</b> text explaining that the verification did actually fail.');
			$verificationPage->addElement($verificationFail);
		}

		$pages[] = $verificationPage;

		$accountPage = Epr_Cms_Page::getPageWithPath(self::PAGE_ACCOUNT_PATH);
		if (is_null($accountPage)) {
			$accountPage = new Epr_Cms_Page(self::PAGE_ACCOUNT_PATH);
			$accountPage->setTitle('Account');
			_dm()->persist($accountPage);
		}


		$resetPasswordPage = Epr_Cms_Page::getPageWithPath(self::PAGE_RESET_PASSWORD_PATH);
		if (is_null($resetPasswordPage)) {
			$resetPasswordPage = new Epr_Cms_Page(self::PAGE_RESET_PASSWORD_PATH);
			$resetPasswordPage->setTitle('reset the password');
			_dm()->persist($resetPasswordPage);
		}

		$resetPasswordDescription = $resetPasswordPage->getElementWithPath(self::RESET_DESCRIPTION_PATH);
		if (is_null($resetPasswordDescription)) {
			$resetPasswordDescription = new Epr_Cms_Element_HTML(self::RESET_DESCRIPTION_PATH);
			$resetPasswordDescription->setTitle('claim a new password');
			$resetPasswordDescription->setContent('Tell the user that he need to enter the <b>mail address</b>. He will then receive an email with a <b>link</b> which opens a page where a new password can be set.');
			$resetPasswordPage->addElement($resetPasswordDescription);
		}

		$pages[] = $resetPasswordPage;

		$dealsPage = Epr_Cms_Page::getPageWithPath(self::PAGE_DEALS);
		if (is_null($dealsPage)) {
			$dealsPage = new Epr_Cms_Page(self::PAGE_DEALS);
			$dealsPage->setTitle('my deals');
			_dm()->persist($dealsPage);
		}

		$dealsDescription = $dealsPage->getElementWithPath(self::DEALS_DESCRIPTION);
		if (is_null($dealsDescription)) {
			$dealsDescription = new Epr_Cms_Element_HTML(self::DEALS_DESCRIPTION);
			$dealsDescription->setWidth(3);
			$dealsDescription->setTitle('Description');
			$dealsDescription->setContent('This s the <b>perfect</b> place to say something about saved deals. For example, it might be a good idea to explain that the deal will indeed expire.');
			$dealsPage->addElement($dealsDescription);
		}

		$dealsList = $dealsPage->getElementWithPath(self::DEALS_INDEX);
		if (is_null($dealsList)) {
			$dealsList = new Epr_Modules_Deals_ListElement(self::DEALS_INDEX);
			$dealsList->setWidth(9);
			$dealsList->setOrder(2);
			$dealsList->setNumberOfItemsToDisplay(100);
			$dealsList->setTitle('my saved deals');
			$dealsPage->addElement($dealsList);
		}

		$pages[] = $dealsPage;

		return $pages;
	}

}
