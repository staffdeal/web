<?php

/** @Document */
class Epr_Modules_CodeList extends Epr_Module_DealCode_Abstract implements Epr_Module_DealCode
{
    const MODE_DISABLED = 0;
    const MODE_NONE     = 1;
    const MODE_SHARED   = 2;
    const MODE_UNIQUE   = 3;

    const LIST_WARNING_THRESHOLD_ABSOLUTE = 10;
    const LIST_WARNING_THRESHOLD_PERCENT  = 10;


    const FIELD_CODELIST    = 'codeList';
    const FIELD_URL         = 'shopURL';
    const FIELD_DESCRIPTION = 'codeText';
    const FIELD_MODE        = 'codeMode';

    const FIELD_COUNT = 'count';

    const FIELD_USED_CODELIST = 'usedCodeList';

    static public function title()
    {
        return "Online Code List";
    }

    static public function description()
    {
        return "Manages a list of code for online deals.";
    }

    public function getDataKey()
    {
        return 'codeList';
    }

    public function getCodeList()
    {
        return $this->getDataForKey(self::FIELD_CODELIST);
    }

    public function setCodeList($list)
    {
        $newList = array();
        foreach ($list as $code) {
            $code = trim($code);
            if (strlen($code) >= 4) {
                $newList[] = $code;
            }
        }
        $unnusedCodes = array_diff($newList, $this->getUsedCodesList());
        $this->setDataForKey($unnusedCodes, self::FIELD_CODELIST);
    }

    public function getUsedCodesList()
    {
        $list = $this->getDataForKey(self::FIELD_USED_CODELIST);
        if (!is_array($list)) {
            $list = array();
        }

        return $list;
    }

    public function getURL()
    {
        $url = null;

        $data = $this->getDataForKey(self::FIELD_URL);
        if ($data) {
            $url = \Poundation\PURL::URLWithString($data);
        }

        return $url;
    }

    public function setURL($url)
    {
        if (is_null($url) || $url instanceof \Poundation\PURL) {
            $this->setDataForKey($url, self::FIELD_URL);
        }
    }

    public function getDescription()
    {
        return $this->getDataForKey(self::FIELD_DESCRIPTION);
    }

    public function setDescription($text)
    {
        $this->setDataForKey((string)$text, self::FIELD_DESCRIPTION);
    }

    public function getMode()
    {
        return $this->getDataForKey(self::FIELD_MODE);
    }

    public function setMode($mode)
    {
        if ($mode == self::MODE_DISABLED || $mode == self::MODE_NONE || $mode == self::MODE_SHARED || $mode == self::MODE_UNIQUE) {
            $this->setDataForKey($mode, self::FIELD_MODE);
        }
    }

    public function isValid()
    {
        if ($this->getMode() == self::MODE_DISABLED) {
            return false;
        } else if ($this->getMode() == self::MODE_NONE) {
            return ($this->getURL() || strlen($this->getDescription()) > 0);
        } else {
            return ($this->getURL() || strlen($this->getDescription()) > 0) && (count($this->getCodeList()) > 0 || count($this->getUsedCodesList()) > 0);
        }
    }

    public function usesCodes()
    {
        return ($this->getMode() != self::MODE_NONE);
    }

    public function isAvailable()
    {
        if ($this->usesCodes()) {
            return (count($this->getCodeList()) > 0);
        } else {
            return true;
        }
    }

    public function hasWarning()
    {
        if ($this->getMode() == self::MODE_DISABLED || $this->getMode() == self::MODE_NONE) {
            return false;
        }
        if ($this->getMode() == self::MODE_SHARED) {
            return (count($this->getCodeList()) < 1);
        } else if ($this->getMode() == self::MODE_UNIQUE) {
            if (count($this->getCodeList()) <= self::LIST_WARNING_THRESHOLD_ABSOLUTE) {
                return true;
            }

            $unusedPercentage = 1 - $this->getUsedPercentage();
            if (self::LIST_WARNING_THRESHOLD_PERCENT / 100 >= $unusedPercentage) {
                return true;
            }
        }

        return false;
    }

    public function getUsedPercentage() {
        $numberOfCodesTotal = $this->getTotalNumberOfCodes();
        $numberOfCodesUsed = count($this->getUsedCodesList());
        $percentage = $numberOfCodesUsed / $numberOfCodesTotal;
        return round($percentage, 3);
    }

    public function getTotalNumberOfCodes() {
        return count($this->getCodeList()) + count($this->getUsedCodesList());
    }

    public function increaseCount()
    {
        $count = (int)$this->getDataForKey(self::FIELD_COUNT);
        $count++;
        $this->setDataForKey($count, self::FIELD_COUNT);

        return $count;
    }

    public function redeem()
    {
        if ($this->getMode() == self::MODE_NONE) {
            return true;
        } else if ($this->getMode() == self::MODE_SHARED) {
            $codeList = $this->getCodeList();
            if (count($codeList) > 0) {
                $this->increaseCount();

                return $codeList[0];
            }
        } else if ($this->getMode() == self::MODE_UNIQUE) {
            $codeList = $this->getCodeList();
            $code     = $codeList[0];

            array_shift($codeList);
            $this->setDataForKey($codeList, self::FIELD_CODELIST);

            $usedCodes   = $this->getUsedCodesList();
            $usedCodes[] = $code;
            $this->setDataForKey($usedCodes, self::FIELD_USED_CODELIST);

            $this->increaseCount();

            return $code;

        }

        return false;
    }

    public function getSettingsFormFields($context = Epr_Module::CONTEXT_OBJECT)
    {
        $fields = parent::getSettingsFormFields();

        if ($context == Epr_Module::CONTEXT_OBJECT) {

            $mode = new Zend_Form_Element_Select(self::FIELD_MODE);
            $mode->setMultiOptions(
                array(
                    self::MODE_DISABLED => 'Online Deal disabled',
                    self::MODE_NONE     => 'No code',
                    self::MODE_SHARED   => 'One code (shared)',
                    self::MODE_UNIQUE   => 'One code per user (unique)'
                )
            );
            $mode->setAttrib('class', 'styledselect_form_1');
            $mode->setLabel('Online Code Mode');
            $mode->setDescription('Select the mode.');
            $mode->setValue($this->getMode());
            $mode->setAttrib('color', 'blue');
            $fields[] = $mode;

            $codes = new Zend_Form_Element_Textarea(self::FIELD_CODELIST);
            $codes->setLabel('Online Codes');
            $codes->setAttrib('class', 'wide');
            $codes->setDescription('Enter all valid codes, one per line. Do not use , and . within the codes.');
            $codes->setAttrib('color', 'blue');
            $codesList = $this->getCodeList();
            if (is_array($codesList)) {
                $codes->setValue(implode("\n", $codesList));
            }
            $fields[] = $codes;

            $url = new Epr_Form_Element_URL(self::FIELD_URL);
            $url->setAttrib('class', 'inp-form wide');
            $url->setLabel('Shop URL');
            $url->setDescription('Enter the URL of the shop. Use __CODE__ as a placeholder. It is replaced with the code of the user.');
            $url->setAllowEmailURLs(true);
            $url->setValue($this->getURL());
            $url->setAttrib('color', 'blue');
            $fields[] = $url;

            $description = new Zend_Form_Element_Textarea(self::FIELD_DESCRIPTION);
            $description->setLabel('Online Deal Description');
            $description->setAttrib('class', 'wide wysiwyg');
            $description->setDescription('Enter a text that is displayed when the deal has no URL.');
            $description->setValue($this->getDescription());
            $description->setAttrib('color', 'blue');
            $fields[] = $description;


        }

        return $fields;
    }

    public function saveSettingsField($field)
    {
        $name  = $field->getName();
        $value = $field->getValue();

        if ($name == self::FIELD_MODE) {
            $this->setMode((int)$value);

            return true;
        } else if ($name == self::FIELD_CODELIST) {
            $value = trim(str_replace(';', "\n", $value));
            $value = str_replace(',', "\n", $value);
            $value = str_replace(' ', "\n", $value);
            $codes = preg_split('/[\s,]+/', $value);
            if (is_array($codes)) {
                $this->setCodeList($codes);
            }

            return true;
        } else if ($name == self::FIELD_DESCRIPTION) {
            $this->setDescription($value);

            return true;
        } else if ($name == self::FIELD_URL) {
            $this->setURL($value);

            return true;
        }


        return parent::saveSettingsField($field); // TODO: Change the autogenerated stub
    }


}