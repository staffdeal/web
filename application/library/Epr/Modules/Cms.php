<?php

/** @Document */
class Epr_Modules_Cms extends Epr_Module_Builtin_Abstract implements Epr_Module_Builtin
{

	private $cmsModules = false;
	private $cmsPages = false;

	static public function title()
	{
		return 'Web CMS';
	}

	static public function description()
	{
		return 'This modules manages all web-related CMS content pages.';
	}

	private function getCmsModules() {
		if ($this->cmsModules === false) {
			$this->cmsModules = array();

			foreach (_app()->getModules() as $module) {
				if (\Poundation\PClass::classFromObject($module)->implementsInterface('Epr_Module_Cms')) {
					$this->cmsModules[] = $module;
				}
			}
		}

		return $this->cmsModules;

	}

	public function getPages()
	{
		if ($this->cmsPages === false) {
			$this->cmsPages = $this->getMyPages();

			foreach($this->getCmsModules() as $module) {

				if ($module instanceof Epr_Module_Cms) {

					$modulesPages = $module->getPages();
					if (count($modulesPages) > 0) {
						$this->cmsPages = array_merge($this->cmsPages, $modulesPages);
					}
				}
			}
		}

		return $this->cmsPages;
	}

	private function getMyPages() {

		$pages = array();

		$indexPage = Epr_Cms_Page::getPageWithPath('index');

		if (is_null($indexPage)) {
			$indexPage = new Epr_Cms_Page('index');
			$indexPage->setTitle('Home');
			_dm()->persist($indexPage);
		}

		$indexNewsList = $indexPage->getElementWithPath('news');
		if (is_null($indexNewsList)) {
			$indexNewsList = new Epr_Modules_News_ListElement('news');
			$indexNewsList->setTitle('News');
			$indexNewsList->setWidth(6);
			$indexPage->addElement($indexNewsList);
		}

		$indexDealsList = $indexPage->getElementWithPath('deals');
		if (is_null($indexDealsList)) {
			$indexDealsList = new Epr_Modules_Deals_ListElement('deals');
			$indexDealsList->setTitle('Deals');
			$indexDealsList->setWidth(6);
			$indexPage->addElement($indexDealsList);
		}

		$pages[] = $indexPage;

		return $pages;

	}

}