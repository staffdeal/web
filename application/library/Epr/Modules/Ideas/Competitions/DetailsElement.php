<?php

/** @Document */
class Epr_Modules_Ideas_Competitions_DetailsElement extends Epr_Cms_Element
{

    private $competition;

    private $showEditLink;

    private $showVerbalDate = true;

    static public function getTypeString()
    {
        return "news article details";
    }

    /**
     * @param $flag
     * @return $this
     */
    public function setShowVerbalDate($flag) {
        $this->showVerbalDate = ($flag == true);
        return $this;
    }

    public function getShowVerbalDate() {
        return $this->showVerbalDate;
    }

    /**
     * @return Epr_Idea_Competition
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * @param $competition
     *
     * @return $this
     */
    public function setCompetition($competition)
    {
        if ($competition instanceof Epr_Idea_Competition || is_null($competition)) {
            $this->competition = $competition;
        }

        return $this;
    }

    /**
     * @param $showEditLink
     *
     * @return $this
     */
    public function setShowEditLink($showEditLink)
    {
        $this->showEditLink = ($showEditLink == true);
        return $this;
    }

    /**
     * @return bool
     */
    public function getShowEditLink()
    {
        return $this->showEditLink;
    }

    public function hasContentToDisplay()
    {
        return ($this->getCompetition() instanceof Epr_Idea_Competition);
    }


    public function renderedContent($titleLevel = 0)
    {
        $output = '';

        if ($this->getCompetition() instanceof Epr_Idea_Competition) {

            $url = '/ideas/' . $this->getCompetition()->getSlug() . '/submit';

            $output .= $this->renderedTitle($titleLevel, null, null, false, $this->getCompetition()->getTitle());

            $output .= '<div class="content details">';

            if ($this->getCompetition()->getTitleImage()) {

                $image = $this->getCompetition()->getTitleImage();

                $width = max(100, min($image->getWidth(), 300));
                $output .= '<img src="' . $image->getPublicIDBasedURL(
                        $width
                    ) . '" class="pull-left" style="' . $image->getCSSDimensionString() . '" />';
            }

            $output .= '<div class="media">';

            $output .= '<div class="info small">';

            $acceptsSubmissions = true;

            if ($this->getShowVerbalDate()) {
                $endDate = \Poundation\PDate::createDate($this->getCompetition()->getEndDate());
                $range = $endDate->getBalancedTimeRange();
                if ($endDate->isFuture()) {
                    $output .= _f('ends in %s', Epr_System::getVerbalTimeRangeExpression($range, null, true));
                } else {
                    $output .= _f('ended %s ago', Epr_System::getVerbalTimeRangeExpression($range, null, true));
                    $acceptsSubmissions = false;
                }
            } else {
                $output .= $this->getCompetition()->getStartDate()->format(_t('Y/m/d')) . ' – ' . $this->getCompetition()->getEndDate()->format(_t('Y/m/d'));
            }
            $output .= '<span class="tint">&nbsp/&nbsp;</span>' . $this->getCompetition()->getCategory()->getTitle();
            $output .= '</div>';

            $output .= $this->getCompetition()->getText();

            if (!$acceptsSubmissions) {
                $output .= '<div class="bg-info">' .$this->getCompetition()->getEndText() . '</div>';
            }

            if (strlen($this->getCompetition()->getRules()) > 0) {
                $output .= '<h' . ($titleLevel + 1) . '>' . _t('rules & prizes') . '</h' . ($titleLevel + 1) . '>';
                $output .= $this->getCompetition()->getRules();
            }

            if ($acceptsSubmissions) {
                $output .= '<a href="' . $url . '" class="btn btn-lg btn-primary submit-idea">' . _t('submit your idea') . '</a>';
            }


            if ($this->getShowEditLink() && _user() && _user()->isAdministrator()) {
                $output .= '&nbsp;<a href="/admin/ideas/update-competition/id/' . $this->getCompetition()->getId(
                    ) . '" class="btn btn-lg btn-primary">' . _t('Edit') . '</a>';
            }


            $output .= '</div>'; // media

            if ($this->getCompetition()->getTitleImage()) {
                $output .= '<div class="clearfix"></div>';
            }

            $output .= '</div>'; // content details
            $output .= '<div class="content details">';

            foreach ($this->getElements() as $subElement) {
                if ($subElement instanceof Epr_Cms_Element) {
                    $subElement->setSharedDataForKey($this->getCompetition(), 'competition');
                    $output .= '<div class="sub-content">' . $subElement->renderedContent($titleLevel) . '</div>';
                }
            }

            $output .= '</div>'; // content details

            $output .= '<div id="socialshareprivacy" class="sharing-addition"></div>';

        }

        return $output;
    }

}