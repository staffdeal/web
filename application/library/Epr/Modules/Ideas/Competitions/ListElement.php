<?php

/** @Document */
class Epr_Modules_Ideas_Competitions_ListElement extends Epr_Cms_Element_List_Abstract
{

    const CACHE_NAME = 'ideas_competitions_list_element';

    private $competitionsToDisplay = false;
    private $currentCategory;

    private $showVerbalDate = true;


    static public function getTypeString()
    {
        return 'Idea Competition List';
    }

    /**
     * @return Epr_Ideas_Category
     */
    public function getCurrentCategory()
    {
        return $this->currentCategory;
    }

    /**
     * @param $category
     *
     * @return $this
     */
    public function setCurrentCategory($category)
    {
        if ($category instanceof Epr_Idea_Category || is_null($category)) {
            $this->currentCategory = $category;
        }

        return $this;
    }

    /**
     * @param $flag
     * @return $this
     */
    public function setShowVerbalDate($flag)
    {
        $this->showVerbalDate = ($flag == true);

        return $this;
    }

    public function getShowVerbalDate()
    {
        return $this->showVerbalDate;
    }

    private function isFullWidth()
    {
        $width = $this->getWidth();

        return ($width > 6);
    }

    private function getCompetitions()
    {

        if ($this->competitionsToDisplay === false) {

            $maxAge = 0;
            $ideasModule = _app()->getModuleWithIdentifier('Ideas');
            if ($ideasModule instanceof Epr_Modules_Ideas) {
                $maxAge = $ideasModule->getCompetitionsMaximumNewsAge();
                $maxAge = ($maxAge > 0) ? $maxAge : 0;
            }

            $endDate = \Poundation\PDate::now()->addDays(0 - $maxAge);

            if ($this->getCurrentCategory()) {
                $this->competitionsToDisplay = \Poundation\PArray::create(Epr_Idea_Competition_Collection::getByCategoryId($this->getCurrentCategory()->getId(), $endDate), true);
                $this->competitionsToDisplay->filterByProperty('active', true);
            } else {
                $this->competitionsToDisplay = \Poundation\PArray::create(Epr_Idea_Competition_Collection::getActiveUntilDate($endDate), true);
            }

        }

        return $this->competitionsToDisplay;

    }


    public function hasContentToDisplay()
    {
        return (count($this->getCompetitions()) > 0);
    }


    public function renderedContent($titleLevel = 1)
    {
        if ($this->getNumberOfItemsToDisplay() > 0) {

            $cache   = Epr_Cache_Provider::getInstance()->getCache();
            $cacheId = self::CACHE_NAME . ($titleLevel) ? '_title' : '';

            $output = $cache->load($cacheId);

            // TODO: enable caching
            if (5 == 5 || strlen($output) == 0 || _app()->isDevelopment()) {

                $output = '';

                $output .= $this->renderedTitle(
                    $titleLevel, null, null, true, ($this->getCurrentCategory()) ? $this->getCurrentCategory()
                        ->getTitle() : null
                );

                $competitions = $this->getCompetitions();

                if (count($competitions) > 0) {

                    $output .= '<div class="media-list element ideas-competitions">';

                    $isFirst = (!$this->getCurrentCategory() instanceof Epr_Idea_Category);

                    foreach ($competitions as $competition) {

                        if ($competition instanceof Epr_Idea_Competition) {

                            if ($isFirst) {
                                $output .= $this->firstCompetitionContent($competition, $titleLevel + 1);
                                $isFirst = false;
                            } else {
                                $output .= $this->competitionContent($competition, $titleLevel + 1);
                            }

                        }
                    }

                    $output .= '</div>';

                    if ($this->isFullWidth()) {
                        $output .= '</div>';
                        $output .= '</div>';
                    }
                } else {
                    $output .= '<div class="alert alert-warning">' . _t('There is no competition currently.') . '</div>';
                }

                $cache->save($output, $cacheId);
            }

            return $output;
        }
    }


    public function firstCompetitionContent(Epr_Idea_Competition $competition, $titleLevel = 2)
    {
        $output = '';

        $url = '/ideas/' . $competition->getCategory()->getSlug() . '/' . $competition->getSlug();
        //$publishDate = new \Poundation\PDate($news->getPublishDate());

        $output .= '<div class="media first-element">';

        if ($competition->getTitleImage()) {
            $imageWidth  = max(100, min($competition->getTitleImage()->getWidth(), 250));
            $imageHeight = $competition->getTitleImage()->getHeightByWidth($imageWidth);

            $output .= '<img src="' . $competition->getTitleImage()
                    ->getPublicIDBasedURL($imageWidth, $imageHeight) . '" style="' . $competition->getTitleImage()
                    ->getCSSDimensionString() . '"/>';
        }

        $output .= '<div class="info">';

        $output .= $competition->getCategory()->getTitle();
        $output .= '</div>';
        $output .= '<div class="info small"><span class="tint">';

        if ($this->getShowVerbalDate()) {
            $endDate = \Poundation\PDate::createDate($competition->getEndDate());
            $range = $endDate->getBalancedTimeRange();
            if ($endDate->isFuture()) {
                $output .= _f('ends in %s', Epr_System::getVerbalTimeRangeExpression($range, null, true));
            } else {
                $output .= _f('ended %s ago', Epr_System::getVerbalTimeRangeExpression($range, null, true));
            }
        } else {
            $output .= $competition->getStartDate()->format(_t('Y/m/d')) . ' – ' . $competition->getEndDate()
                    ->format(_t('Y/m/d'));
        }
        $output .= '</span></div>';

        if ($this->isFullWidth()) {
            $output .= '<a href="' . $url . '">';
        }
        $output .= '<h' . $titleLevel . '>' . $competition->getTitle() . '</h' . $titleLevel . '>';
        if ($this->isFullWidth()) {
            $output .= '</a>';
        }

        $output .= '<div class="clearfix"></div>';

        $output .= '<div class="content">';
        $output .= $competition->getTeaser();
        $output .= '</div>';
        $output .= '<a href="' . $url . '" class="btn btn-lg btn-primary ' . (($this->isFullWidth()) ? 'pull-right' : '') . '">';
        //$output .= '<span class="glyphicon glyphicon-arrow-right"></span>';
        $output .= _t('read more');
        $output .= '</a>';
        $output .= '</div>';

        return $output;
    }

    public function competitionContent(Epr_Idea_Competition $competition, $titleLevel = 2)
    {
        $output = '';

        $url = '/ideas/' . $competition->getCategory()->getSlug() . '/' . $competition->getSlug();
        //$publishDate = new \Poundation\PDate($news->getPublishDate());

        $hideOnMobileClass = ($this->isFullWidth()) ? '' : 'hide-on-mobile';

        $output .= '<div class="media not-first-element ' . $hideOnMobileClass . '">';

        if ($competition->getTitleImage()) {

            $imageWidth  = 180;
            $imageHeight = 120;
            $output .= '<img src="' . $competition->getTitleImage()
                    ->getPublicIDBasedURL($imageWidth, $imageHeight) . '" style="width:' . $imageWidth . 'px; height:' . $imageHeight . 'px;">';
        }

        $output .= '<div class="info small">';
        if ($this->getShowVerbalDate()) {
            $endDate = \Poundation\PDate::createDate($competition->getEndDate());
            $range = $endDate->getBalancedTimeRange();
            if ($endDate->isFuture()) {
                $output .= _f('ends in %s', Epr_System::getVerbalTimeRangeExpression($range, null, true));
            } else {
                $output .= _f('ended %s ago', Epr_System::getVerbalTimeRangeExpression($range, null, true));
            }
        } else {
            $output .= $competition->getStartDate()->format(_t('Y/m/d')) . ' – ' . $competition->getEndDate()
                    ->format(_t('Y/m/d'));
        }
        $output .= '<span class="tint">&nbsp/&nbsp;</span>' . $competition->getCategory()->getTitle();
        $output .= '</div>';

        if ($this->isFullWidth()) {
            $output .= '<a href="' . $url . '">';
        }
        $output .= '<h' . $titleLevel . '>' . $competition->getTitle() . '</h' . $titleLevel . '>';
        if ($this->isFullWidth()) {
            $output .= '</a>';
        }

        if ($this->isFullWidth()) {
            $output .= '<div class="content">';
            $output .= $competition->getTeaser();
            $output .= '</div>';
        }

        if (!$this->isFullWidth()) {
            $output .= '<a href="' . $url . '">';
            $output .= '<span class="glyphicon glyphicon-arrow-right"></span>';
            $output .= _t('Read more');
            $output .= '</a>';
        }
        $output .= '</div>';

        return $output;
    }
}