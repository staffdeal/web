<?php
/**
 * @package Backend
 * @author daniel
 */

/** @Document */
class Epr_Modules_Ideas_Submissions_ListElement extends Epr_Cms_Element_List_Abstract
{

    private $competition;

    private $showVerbalDate = true;

    static public function getTypeString()
    {
        return 'Ideas Submission List';
    }

    /**
     * @param Epr_Idea_Competition $competition
     * @return $this
     */
    public function setCompetition($competition)
    {
        if (is_null($competition) || $competition instanceof Epr_Idea_Competition) {
            $this->competition = $competition;
        }

        return $this;
    }

    /**
     * @return Epr_Idea_Competition|null
     */
    public function getCompetition()
    {

        $competition = null;

        if (!is_null($this->competition)) {
            $competition = $this->competition;
        } else {
            $competition = $this->getSharedDataForKey('competition');
        }

        return $competition;
    }

    /**
     * @param $flag
     * @return $this
     */
    public function setShowVerbalDate($flag) {
        $this->showVerbalDate = ($flag == true);
        return $this;
    }

    public function getShowVerbalDate() {
        return $this->showVerbalDate;
    }

    public function renderedTitle(
        $titleLevel = 1,
        $prepend = null,
        $append = null,
        $useBranding = true,
        $titleOverride = null
    ) {
        return parent::renderedTitle($titleLevel, $prepend, $append, true, $titleOverride);
    }

    public function renderedContent($titleLevel = 0)
    {
        $output = '';

        $submissions = array();
        $competition = $this->getCompetition();
        if ($competition instanceof Epr_Idea_Competition) {
            $submissions = \Poundation\PArray::create($competition->getActiveIdeas());
            $submissions->sortByPropertyName('submissionDate', SORT_DESC);
        }

        if (count($submissions) > 0) {
            $output = $this->renderedTitle($titleLevel);

            $output .= '<div class="media-list element ideas-competitions">';
            foreach ($submissions as $submission) {
                if ($submission instanceof Epr_Idea) {
                    $output .= $this->renderedSubmission($submission);
                }
            }

            $output .= '</div>';
        }

        return $output;
    }

    private function renderedSubmission(Epr_Idea $submission)
    {

        $output = '';

        $output .= '<div class="media not-first-element">';

        $images = $submission->getImages();
        $image = null;
        if (count($images) > 0) {
            if ($images[0] instanceof Epr_Content_Image) {
                $image = $images[0];
            }
        }

        if ($image) {
            $width = max(100, min($image->getWidth(), 300));
            $output .= '<img src="' . $image->getPublicIDBasedURL(
                    $width
                ) . '" class="pull-left" style="' . $image->getCSSDimensionString() . '" />';
        }

        $output .= '<div class="info small">';
        if ($this->getShowVerbalDate()) {
            $range = \Poundation\PDate::createDate($submission->getSubmissioDate())->getBalancedTimeRange();
            $filter = \Poundation\PDateRange::DURATION_YEARS . \Poundation\PDateRange::DURATION_MONTHS . \Poundation\PDateRange::DURATION_DAYS . \Poundation\PDateRange::DURATION_HOURS . \Poundation\PDateRange::DURATION_MINUTES;
            $output .= sprintf(_t('%s ago'), Epr_System::getVerbalTimeRangeExpression($range, $filter, true));
        } else {
            $output .= $submission->getSubmissioDate()->format(_t('Y/m/d'));
        }
        $output .= '<span class="tint">&nbsp/&nbsp;</span>' . $submission->getUser()->getFirstname() . '&nbsp;' . $submission->getUser()->getLastname();
        $output .= '</div>';

        $output .= $submission->getText();

        if ($submission->getUrl()) {
            $output .= '<br /><a href="' . $submission->getUrl() . '" class="" target="_blank">' . $submission->getUrl() . '</a>';
        }

        $output .= '</div>';

        return $output;

    }

}