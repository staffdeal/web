<?php

/** @Document */
class Epr_Modules_Ideas_CategoryMenuElement extends Epr_Cms_Element
{

    private $currentCategory;

    static public function getTypeString()
    {
        return 'ideas category menu';
    }


    /**
     * @return Epr_News_Category
     */
    public function getCurrentCategory()
    {
        return $this->currentCategory;
    }

    /**
     * @param $category
     *
     * @return $this
     */
    public function setCurrentCategory($category)
    {
        if ($category instanceof Epr_Idea_Category || is_null($category)) {
            $this->currentCategory = $category;
        }

        return $this;
    }

    public function getCategories($hierarchal = false)
    {

        $maxAge      = 0;
        $ideasModule = _app()->getModuleWithIdentifier('Ideas');
        if ($ideasModule instanceof Epr_Modules_Ideas) {
            $maxAge = $ideasModule->getCompetitionsMaximumNewsAge();
            $maxAge = ($maxAge > 0) ? $maxAge : 0;
        }

        $endDate = \Poundation\PDate::now()->addDays(0 - $maxAge);


        $allCategories = \Poundation\PArray::create(Epr_Idea_Competition_Collection::getActiveCategoriesUntilDate($endDate));
        $allCategories->sortByPropertyName('title', SORT_ASC);

        $hierarchal = false; // we do this since there are no parent categories yet.
        if ($hierarchal) {

            $categories = array();
            foreach ($allCategories as $category) {
                if ($category instanceof Epr_Idea_Category) {
                    $category->subCategories        = array();
                    $categories[$category->getId()] = $category;
                }
            }

            foreach ($allCategories as $category) {
                if ($category instanceof Epr_Idea_Category) {
                    $parentCategory = $category->getCategory();
                    if ($parentCategory) {
                        $parentCategory->subCategories[] = $category;
                        unset($categories[$category->getId()]);
                    }
                }
            }

            return $categories;

        } else {
            return $allCategories;
        }

    }


    private function menuEntry(Epr_Idea_Category $category, $intendLevel = 0)
    {

        $output = '';

        $url         = '/ideas/' . $category->getSlug();
        $activeClass = ($category == $this->getCurrentCategory()) ? 'active' : '';

        $output .= '<a href="' . $url . '" class="list-group-item ' . $activeClass . '">';

        for ($i = 0; $i < $intendLevel * 4; $i++) {
            $output .= '&nbsp;';
        }

        //$output .= '<span class="colorwell" style="background-color: ' . $category->getColor() . '" /></span>';
        if ($category->getTitleImage()) {
            $image = $category->getTitleImage();

            $width  = 32;
            $height = 32;

            $output .= '<img src="' . $image->getPublicIDBasedURL($width, $height) . '" style="' . $image->getCSSDimensionString() . '" />';
        }

        $output .= $category->getTitle();
        $output .= '</a>';

        if (isset($category->subCategories) && is_array($category->subCategories)) {
            foreach ($category->subCategories as $subCategory) {
                if ($subCategory instanceof Epr_Idea_Category) {
                    $output .= $this->menuEntry($subCategory, $intendLevel + 1);
                }
            }
        }

        return $output;
    }

    public function renderedTitle($titleLevel = 1, $prepend = null, $append = null, $useBranding = true, $titleOverride = null)
    {
        $this->titleRendered = true;

        $classes = array('panel-title');
        if (outlineTranslation()) {
            $classes[] = 'translate';
        }

        $output = '';
        $output .= '<div class="panel-heading">';
        $output .= '<h' . $titleLevel . ' class="' . implode(' ', $classes) . '">' . $this->getTitle() . '</h' . $titleLevel . '>';
        $output .= '</div>';

        return $output;
    }


    public function renderedContent($titleLevel = 3)
    {

        $categories = $this->getCategories(true);

        $output = '';

        $output .= '<div class="panel">';

        $output .= $this->renderedTitle($titleLevel);
        $this->titleRendered = false;

        $output .= '<div class="list-group">';
        $activeClass = (is_null($this->getCurrentCategory())) ? 'active' : '';
        $output .= '<a href="/ideas" class="list-group-item ' . $activeClass . '">';
        $output .= _t('all competitions');
        $output .= '</a>';

        foreach ($categories as $category) {
            if ($category instanceof Epr_Idea_Category) {

                $output .= $this->menuEntry($category);

            }
        }

        $output .= '</div>';
        $output .= '</div>';

        return $output;


    }


}