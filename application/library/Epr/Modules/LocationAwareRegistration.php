<?php

/** @Document */
class Epr_Modules_LocationAwareRegistration extends Epr_Module_UserRegistration_Abstract implements Epr_Module_UserRegistration
{

	/** @Field(type="integer") */
	private $allowedPerimeter = 20;

	static public function title()
	{
		return 'Location Aware';
	}

	static public function description()
	{
		return 'User registration depending on the location.';
	}

	public function setAllowedPerimeter($value)
	{
		if (is_int($value) && $value > 0) {
			$this->allowedPerimeter = $value;
		}

		return $this;
	}

	public function getAllowedPerimeter()
	{
		return $this->allowedPerimeter;
	}

	public function getSettingsFormFields()
	{
		$fields = parent::getSettingsFormFields();

		$perimeter = new Zend_Form_Element_Select('perimeter');
		$perimeter->setLabel('Allowed Perimeter');

		$perimeterValues = \Poundation\PArray::createProgressivArray(5, 50, 5);
		$perimeter->setMultiOptions($perimeterValues->nativeArray());
		$perimeter->setValue($perimeterValues->indexOfObject($this->getAllowedPerimeter()));
		$perimeter->setDescription('The allowed perimeter in km. Registration is only allowed in this range.');
		$fields[] = $perimeter;

		return $fields;
	}

	public function saveSettingsField($field)
	{
		$fieldname  = $field->getName();
		$fieldvalue = $field->getValue();

		if ($fieldname == 'perimeter') {

			$perimeter = $field->getMultiOption($fieldvalue);
			$this->setAllowedPerimeter($perimeter);

			return ($perimeter == $this->getAllowedPerimeter());
		} else {
			return parent::saveSettingsField($field);
		}
	}

	public function registrationFields()
	{
		$fields = parent::registrationFields();

		$fields[] = Epr_Module_UserRegistration_Field::createField('firstname', Epr_Module_UserRegistration_Field::TYPE_TEXT, 'Vorname', true);
		$fields[] = Epr_Module_UserRegistration_Field::createField('lastname', Epr_Module_UserRegistration_Field::TYPE_TEXT, 'Nachname', true);
		$fields[] = Epr_Module_UserRegistration_Field::createField('location', Epr_Module_UserRegistration_Field::TYPE_LOCATION, '', true);
        $fields[] = Epr_Module_UserRegistration_Field::createField('dealer', Epr_Module_UserRegistration_Field::TYPE_SELECT, 'Händler', true)->addAdditionalField('url', _app()->getPublicURL()->addPathComponent('api/info/dealers'));

		return $fields;
	}

	public function isRegistrationDataSufficient(&$params, &$error = null, &$errorCode = null)
	{

		$valid = parent::isRegistrationDataSufficient($params, $error, $errorCode);
		if ($valid) {
			$valid = false;

			if (isset($params['dealer']) && $params['dealer'] instanceof Epr_Dealer) {
				$valid = true;
			} else {

                $params['selectedDealer'] = null;
                if (isset($params['dealer']) && strlen($params['dealer']) > 0) {
                    $params['selectedDealer'] = _dm()->find(Epr_Dealer::DOCUMENTNAME, $params['dealer']);
                }

                if ($params['selectedDealer'] instanceof Epr_Dealer || _clientVersion() < 2) {

                    $location   = isset($params['location']) ? $params['location'] : false;
                    $components = explode(',', $location);
                    if (count($components) == 2) {
                        $lat        = (float)trim($components[0]);
                        $lon        = (float)trim($components[1]);
                        $coordinate = \Poundation\PCoordinate::createCoordinate($lat, $lon);
                        if ($coordinate) {

                            $perimeter     = $this->getAllowedPerimeter() * 1000;
                            $dealersAround = Epr_Dealer_Collection::getDealerAroundLocation($coordinate, $perimeter);

                            if (is_array($dealersAround) && count($dealersAround) > 0) {
                                // we take the first one
                                $nearestDealer = $dealersAround[0];
                                if ($nearestDealer instanceof Epr_Dealer) {
                                    $params['dealer'] = $dealersAround[0];
                                    $valid            = true;
                                }
                            } else {
                                $error     = 'No dealer found within allowed perimeter';
                                $errorCode = 303;
                            }
                        } else {
                            $error     = 'Coordinates are invalid';
                            $errorCode = 302;
                        }
                    } else {
                        $error     = 'Location format invalid';
                        $errorCode = 301;
                    }
                } else {
                    $error = 'Provided dealer id invalid';
                    $errorCode = 304;
                }
			}
		}

		return $valid;
	}

	/**
	 * After the system has created the new user this method is called on the module.
	 * Perform any necessary work on it (e.g. deactivating, send mails)
	 *
	 * @param Epr_User $user
	 * @param array    $params
	 */
	public function processNewUser($user, $params)
	{

		// we do not call super here since there is no implementation

		$user->deactivate();

		$dealer = (isset($params['dealer'])) ? $params['dealer'] : null;
        $selectedDealer = (isset($params['selectedDealer'])) ? $params['selectedDealer'] : null;
		if ($dealer instanceof Epr_Dealer) {
			$user->associateWithDealer($dealer, $selectedDealer);

			$firstName = (isset($params['firstname'])) ? $params['firstname'] : null;
			$lastName  = (isset($params['lastname'])) ? $params['lastname'] : null;

			if ($firstName) {
				$user->setFirstname($firstName);
			}
			if ($lastName) {
				$user->setLastname($lastName);
			}

			if ($this->emailVerification) {
				if ($this->getEmailVerificationTemplate()) {
					$fields   = $this->emailVerificationVariables($user);
					$template = new \Poundation\PTemplate($this->getEmailVerificationTemplate());

					$content = $template->setFields($fields)->renderedString();
					if (!$user->sendMail($this->getRegistrationEmailSubject(), $content)) {
 						throw new Exception('Could not send verification email');
					}
				}
			}

			return true;

		}

		return false;
	}

	/**
	 * This method processes things right after the user has been verified.
	 *
	 * @param Epr_User $user
	 *
	 * @return bool
	 */
	public function processVerification(Epr_User $user)
	{
		switch($user->getRegistrationContext()) {
			case Epr_User::REGISTRATION_CONTEXT_ADMIN:
				$user->activate();
				return true;
				break;
			case Epr_User::REGISTRATION_CONTEXT_API:
				$user->activate();
				return true;
				break;
			case Epr_User::REGISTRATION_CONTEXT_WEB:

				$link = _app()->getPublicURL()->addPathComponent('/admin/user/update/id/' . $user->getId());
				_app()->sendAdminMail('Please activate a new user', sprintf('<p>A new user has verified the mail address. Since the user has been registered on the web page, you need to activate it manually.</p><ul><li>Email: %s</li><li>%s</li><li>Link: %s</li></ul>', $user->getEmail(), $user->getRegistrationInformation(), $link));
				return true;
				break;
		}

		return false;
	}

}
