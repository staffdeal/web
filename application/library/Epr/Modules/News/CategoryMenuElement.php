<?php

/** @Document */
class Epr_Modules_News_CategoryMenuElement extends  Epr_Cms_Element {

	private $currentCategory;

	static public function getTypeString()
	{
		return 'news category menu';
	}


	/**
	 * @return Epr_News_Category
	 */
	public function getCurrentCategory()
	{
		return $this->currentCategory;
	}

	/**
	 * @param $category
	 *
	 * @return $this
	 */
	public function setCurrentCategory($category)
	{
		if ($category instanceof Epr_News_Category || is_null($category)) {
			$this->currentCategory = $category;
		}

		return $this;
	}

	public function getCategories($hierarchal = false)
	{

        $newsModule = _app()->getModuleWithIdentifier(Epr_Modules_News::identifier());
        $maxAge     = $newsModule->getMaximumNewsAge();

        $range = $newsModule->getNewsDateRange();

		$allCategories = \Poundation\PArray::create(Epr_News_Category_Collection::getActiveCategoriesInRange($range));
		$allCategories->sortByPropertyName('title', SORT_ASC);

		if ($hierarchal) {

			$categories = array();
			foreach ($allCategories as $category) {
				if ($category instanceof Epr_News_Category) {
					$category->subCategories        = array();
					$categories[$category->getId()] = $category;
				}
			}

			foreach ($allCategories as $category) {
				if ($category instanceof Epr_News_Category) {
					$parentCategory = $category->getCategory();
					if ($parentCategory) {
						$parentCategory->subCategories[] = $category;
						unset($categories[$category->getId()]);
					}
				}
			}

			return $categories;

		} else {
			return $allCategories;
		}

	}


	private function menuEntry(Epr_News_Category $category, $intendLevel = 0)
	{

        $intendLevel = 0;
		$output = '';

		$url         = '/news/' . $category->getSlug();
		$activeClass = ($category == $this->getCurrentCategory()) ? 'active' : '';

		$output .= '<a href="' . $url . '" class="list-group-item ' . $activeClass . '">';

		for ($i = 0; $i < $intendLevel * 4; $i++) {
			$output .= '&nbsp;';
		}

		//$output .= '<span class="colorwell" style="background-color: ' . $category->getColor() . '" /></span>';

		$output .= $category->getTitle();
		$output .= '</a>';

		if (isset($category->subCategories) && is_array($category->subCategories)) {
			foreach ($category->subCategories as $subCategory) {
				if ($subCategory instanceof Epr_News_Category) {
					$output .= $this->menuEntry($subCategory, $intendLevel + 1);
				}
			}
		}

		return $output;
	}

	public function renderedTitle($titleLevel = 1, $prepend = null, $append = null, $useBranding = true, $titleOverride = null)
	{
		$this->titleRendered = true;

		$classes = array('panel-title');
		if (outlineTranslation()) {
			$classes[] = 'translate';
		}

		$output = '';
		$output .= '<div class="panel-heading">';
		$output .= '<h' . $titleLevel . ' class="' . implode(' ', $classes) . '">' . $this->getTitle() . '</h' . $titleLevel . '>';
		$output .= '</div>';
		return $output;
	}


	public function renderedContent($titleLevel = 3) {

		$categories = $this->getCategories(true);

		$output = '';

		$output .= '<div class="panel">';

		$output .= $this->renderedTitle($titleLevel);
		$this->titleRendered = false;

		$output .= '<div class="list-group">';
		$activeClass = (is_null($this->getCurrentCategory())) ? 'active' : '';
		$output .= '<a href="/news" class="list-group-item ' . $activeClass . '">';
		$output .= _t('All news');
		$output .= '</a>';

		foreach ($categories as $category) {
			if ($category instanceof Epr_News_Category) {

				$output .= $this->menuEntry($category);

			}
		}

		$output .= '</div>';
		$output .= '</div>';

		return $output;


	}



}