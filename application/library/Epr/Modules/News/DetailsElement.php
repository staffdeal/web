<?php

/** @Document */
class Epr_Modules_News_DetailsElement extends Epr_Cms_Element
{

	private $newsItem;
	private $showEditLink = false;

	private $showTitleImage = false;

    private $showSharingButtons = true;

	static public function getTypeString()
	{
		return "news article details";
	}

	/**
	 * @return Epr_News
	 */
	public function getNewsItem()
	{
		return $this->newsItem;
	}

	/**
	 * @param $newsItem
	 *
	 * @return $this
	 */
	public function setNewsItem($newsItem)
	{
		if ($newsItem instanceof Epr_News || is_null($newsItem)) {
			$this->newsItem = $newsItem;
		}

		return $this;
	}

	/**
	 * @param $showEditLink
	 *
	 * @return $this
	 */
	public function setShowEditLink($showEditLink) {
		$this->showEditLink = ($showEditLink == true);
		return $this;
	}

	/**
	 * @return bool
	 */
	public function getShowEditLink() {
		return $this->showEditLink;
	}

	/**
	 * @param $flag
	 *
	 * @return $this
	 */
	public function setShowTitleImage($flag) {
		$this->showTitleImage = ($flag == true);
		return $this;
	}

	/**
	 * @return bool
	 */
	public function getShowTitleImage() {
		return $this->showTitleImage;
	}

    /**
     * @param $flag
     * @return $this
     */
    public function setShowSharingButtons($flag) {
        $this->showSharingButtons = ($flag == true);
        return $this;
    }

    /**
     * @return bool
     */
    public function getShowSharingButtons() {
        return $this->showSharingButtons;
    }

	public function hasContentToDisplay()
	{
		return ($this->getNewsItem() instanceof Epr_News);
	}


	public function renderedContent($titleLevel = 0)
	{
		$output = '';

		if ($this->getNewsItem() instanceof Epr_News) {

			$output .= $this->renderedTitle($titleLevel, null, null, false, $this->getNewsItem()->getTitle());

			$output .= '<div class="content details">';

			if ($this->getShowTitleImage() && $this->getNewsItem()->getTitleImage()) {

				$image = $this->getNewsItem()->getTitleImage();

				$width = max(100, min($image->getWidth(), 300));
				$output .= '<img src="' . $image->getPublicIDBasedURL($width) . '" class="pull-left" style="' . $image->getCSSDimensionString() . '" />';
			}

			$output .= '<div class="media">';

			$output .= '<div class="info small">';
			$output .= $this->getNewsItem()->getPublishedDate()->getFormatedString(_t('Y/m/d'));
			$output .= '<span class="tint">&nbsp/&nbsp;</span>' . $this->getNewsItem()->getCategory()->getTitle();

			if (strlen($this->getNewsItem()->getSource()) > 0) {
				$output .= '<span class="tint">&nbsp/&nbsp;</span>';
				$output .= $this->getNewsItem()->getSource();
			}

			$output .= '</div>';

			$output .= $this->getNewsItem()->getText();

			if ($this->getShowTitleImage() && $this->getNewsItem()->getTitleImage()) {
				$output .= '<div class="clearfix"></div>';
			}

            $url = $this->getSharedDataForKey('moreUrl');

			$output .= '</div>';

			if ($url) {
				$output .= '<a href="' . $url . '" class="btn btn-lg btn-primary news-addition view-source" target="_blank">' . _t('view source') . '</a>';
			}

			if ($this->getShowEditLink() && _user() instanceof Epr_User && _user()->isAdministrator()) {
				$output .= '&nbsp;<a href="/admin/news/update/id/' . $this->getNewsItem()->getId() . '" class="btn btn-lg btn-primary">' . _t('Edit') . '</a>';
			}

			$output .= '</div>';

			if ($this->getShowSharingButtons()) {
				$output .= '<div id="socialshareprivacy" class="sharing-addition"></div>';
			}

		}

		return $output;
	}

}