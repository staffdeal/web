<?php

/** @Document */
class Epr_Modules_News_ListElement extends Epr_Cms_Element_List_Abstract
{

    const CACHE_NAME = 'news_list_element';
    private $newsToDisplay = false;
    private $currentCategory;
    private $totalRows = 0;


    static public function getTypeString()
    {
        return 'News List';
    }

    /**
     * @return Epr_News_Category
     */
    public function getCurrentCategory()
    {
        return $this->currentCategory;
    }

    /**
     * @param $category
     *
     * @return $this
     */
    public function setCurrentCategory($category)
    {
        if ($category instanceof Epr_News_Category || is_null($category)) {
            $this->currentCategory = $category;
        }

        return $this;
    }

    private function isFullWidth()
    {
        $width = $this->getWidth();

        return ($width > 6);
    }

    private function getNews()
    {

        if ($this->newsToDisplay === false) {

            $this->newsToDisplay = new \Poundation\PArray();

            $newsModule = _app()->getModuleWithIdentifier(Epr_Modules_News::identifier());
            $maxAge     = $newsModule->getMaximumNewsAge();

            $range = $newsModule->getNewsDateRange();

            $request = Zend_Controller_Front::getInstance()->getRequest();
            $page    = (max(0, (int)$request->getParam('page', 1)));

            $num2display = $this->getNumberOfItemsToDisplay();
            $skip        = $num2display * ($page - 1);

            if ($this->getCurrentCategory() instanceof Epr_News_Category) {
                if ($this->getCurrentCategory()->isEffectiveActive()) {
                    $allNews = $this->getCurrentCategory()->getNewsInRange($range, $num2display, $skip);
                }
                $this->totalRows = $this->getCurrentCategory()->getNewsCount();

            } else {
                $allNews         = Epr_News_Collection::getActiveInDateRange($range, $num2display, true, $skip);
                $this->totalRows = $allNews->getTotalRows();
            }

            if (isset($allNews)) {
                foreach ($allNews as $news) {
                    if ($news instanceof Epr_News) {
                        $this->newsToDisplay[] = $news;
                    }
                }
            }
        }

        return $this->newsToDisplay;

    }


    public function hasContentToDisplay()
    {
        return (count($this->getNews()) > 0);
    }


    public function renderedContent($titleLevel = 1)
    {
        $output      = '';
        $num2display = $this->getNumberOfItemsToDisplay();
        if ($num2display) {

            //$cache   = Epr_Cache_Provider::getInstance()->getCache();
            //$cacheId = self::CACHE_NAME . ($titleLevel) ? '_title' : '';

            //$output = $cache->load($cacheId);


            // TODO: enable caching
            if (5 == 5 || strlen($output) == 0 || _app()->isDevelopment()) {

                $allNews = $this->getNews();

                $page = 1;
                if ($this->getEnablePagination()) {
                    $page = max(0, (int)Zend_Controller_Front::getInstance()->getRequest()->getParam('page', 1));
                    //isset($_REQUEST['page']) && is_numeric($_REQUEST['page']) ? $page = $_REQUEST['page'] : $page = 1;
                    $nextpage   = $page + 1;
                    $prevpage   = $page - 1;
                    $totalPages = ceil($this->totalRows / $num2display);
                }


                $output .= $this->renderedTitle(
                    $titleLevel,
                    null,
                    null,
                    true,
                    ($this->getCurrentCategory()) ? $this->getCurrentCategory()->getTitle() : null
                );

                //$output.= '<div class="news_pagination_info">'._t('Page').' '.$page.' '._t('of').' '.$totalPages.'</div>';

                if (count($allNews) > 0) {

                    $output .= '<div class="media-list element news">';

                    $isFirstNews = (!$this->getCurrentCategory() instanceof Epr_News_Category);
                    //$isFirstNews = true;
                    foreach ($allNews as $news) {
                        if ($news instanceof Epr_News) {
                            if ($isFirstNews && $page == 1) {
                                $output .= $this->firstNewsContent($news, $titleLevel + 1);
                                $isFirstNews = false;
                            } else {
                                $output .= $this->newsContent($news, $titleLevel + 1);
                            }

                        }
                    }

                    $output .= '</div>';

                    if ($this->isFullWidth()) {
                        $output .= '</div>';
                        $output .= '</div>';
                    }

                    if ($this->getEnablePagination()) {
                        if (count($allNews) == $num2display) {
                            $output .= '<a href="?page=' . $nextpage . '" class="btn btn-lg btn-primary ' . (($this->isFullWidth()) ? 'pull-right' : '') . '">';
                            $output .= _t('older news');
                            $output .= '</a>';
                        }

                        if ($page > 1) {
                            $output .= '<a href="?page=' . $prevpage . '" class="btn btn-lg btn-primary btn-news-prev ' . (($this->isFullWidth()) ? 'pull-right' : '') . '">';
                            $output .= _t('earlier news');
                            $output .= '</a>';
                        }
                    } else if ($this->isOnStartPage()) {
                        $newsPage = Epr_Cms_Page::getPageWithPath(Epr_Modules_News::PAGE_PATH);
                        if ($newsPage instanceof Epr_Cms_Page) {
                            $output.= '<a href="/' . $newsPage->getPath(). '" class="btn btn-primary list-more btn-lg">' . _t('More news') . '</a>';
                        }

                    }

                } else {
                    $output .= '<div class="alert alert-warning">' . _t('There are no news currently.') . '</div>';
                }

                //$cache->save($output, $cacheId);
            }
        }

         return $output;
    }


    public function firstNewsContent(Epr_News $news, $titleLevel = 2)
    {
        $output = '';

        $url         = '/news/' . $news->getCategory()->getSlug() . '/' . $news->getSlug();
        $publishDate = new \Poundation\PDate($news->getPublishDate());

        $output .= '<div class="media first-element link-to" data-link-to="' . $url . '"">';

        if ($news->getTitleImage()) {
            $imageWidth  = max(100, min($news->getTitleImage()->getWidth(), 250));
            $imageHeight = $news->getTitleImage()->getHeightByWidth($imageWidth);

            $output .= '<img src="' . $news->getTitleImage()->getPublicIDBasedURL(
                    $imageWidth,
                    $imageHeight
                ) . '" style="' . $news->getTitleImage()->getCSSDimensionString() . '"/>';
        }

        $output .= '<div class="info">';

        $output .= $news->getCategory()->getTitle();
        $output .= '</div>';
        $output .= '<div class="info small">';
        $output .= $publishDate->getFormatedString(_t('Y/m/d'));
        $output .= '</div>';

        $output .= '<h' . $titleLevel . '>' . $news->getTitle() . '</h' . $titleLevel . '>';

        $output .= '<div class="clearfix"></div>';

        $output .= '<div class="content">';
        $output .= __($news->getText())->stripTags()->shortenAfterSentence(300, '. ', '')->trim();
        $output .= '</div>';
        //$output .= '<a href="' . $url . '" class="btn btn-lg btn-primary ' . (($this->isFullWidth()) ? 'pull-right' : '') . '">';
        //$output .= '<span class="glyphicon glyphicon-arrow-right"></span>';
        //$output .= _t('read more');
        //$output .= '</a>';
        $output .= '</div>';

        return $output;
    }

    public function newsContent(Epr_News $news, $titleLevel = 2)
    {
        $output = '';

        $url         = '/news/' . $news->getCategory()->getSlug() . '/' . $news->getSlug();
        $publishDate = new \Poundation\PDate($news->getPublishDate());

        $hideOnMobileClass = ($this->isFullWidth()) ? '' : 'hide-on-mobile';

        $output .= '<div class="media not-first-element ' . $hideOnMobileClass . ' link-to" data-link-to="' . $url . '"">';

        if ($news->getTitleImage()) {

            $imageWidth  = 180;
            $imageHeight = 120;
            $output .= '<img src="' . $news->getTitleImage()->getPublicIDBasedURL(
                    $imageWidth,
                    $imageHeight
                ) . '" style="width:' . $imageWidth . 'px; height:' . $imageHeight . 'px;">';
        }

        $output .= '<div class="info small">';
        $output .= $publishDate->getFormatedString(_t('Y/m/d')) . '<span class="tint">&nbsp;/&nbsp;</span>';
        $output .= $news->getCategory()->getTitle();
        $output .= '</div>';

        $output .= '<h' . $titleLevel . '>' . $news->getTitle() . '</h' . $titleLevel . '>';

        if ($this->isFullWidth()) {
            $output .= '<div class="content">';
            $output .= __($news->getText())->stripTags()->shortenAfterSentence(100, '. ', '')->trim();
            $output .= '</div>';
        }

        $output .= '</div>';

        return $output;
    }
}