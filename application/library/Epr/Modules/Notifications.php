<?php

/** @Document */
class Epr_Modules_Notifications extends Epr_Module_Cron_Abstract implements Epr_Module_Builtin, Epr_Module_Cron
{

    const NUMBER_OF_NOTIFICATIONS_PER_CRON = 20;

    static public function title()
    {
        return "Notifications";
    }

    static public function description()
    {
        return 'This module send notifications and manages the message queue.';
    }

    public function casUseAPNSModule()
    {
        $apnsModule = _app()->getModuleWithIdentifier(Epr_Modules_APNS::identifier());

        return ($apnsModule instanceof Epr_Module_Notification && $apnsModule->isUsable());
    }

    public function canUseGCMMOdule() {
        $gcmModule = _app()->getModuleWithIdentifier(Epr_Modules_GCM::identifier());

        return ($gcmModule instanceof Epr_Modules_GCM && $gcmModule->isUsable());
    }

    public function hasUsableNotificationModules()
    {
        $apnsModule = _app()->getModuleWithIdentifier(Epr_Modules_APNS::identifier());

        return $this->casUseAPNSModule();
    }

    public function runCron(DateInterval $intervalSinceLastRun)
    {
        $output = parent::runCron($intervalSinceLastRun);

        $queue         = Epr_Notification_Queue::getQueue();
        $notifications = $queue->getNotifications(Epr_Notification::STATUS_PENDING);

        $messagesToSend = array();

        if (count($notifications) > 0) {

            $apnsModule  = _app()->getModuleWithIdentifier(Epr_Modules_APNS::identifier());
            $apnsEnabled = ($apnsModule instanceof Epr_Module_Notification && $apnsModule->isUsable());

            $gcmModule  = _app()->getModuleWithIdentifier(Epr_Modules_GCM::identifier());
            $gcmEnabled = ($gcmModule instanceof Epr_Module_Notification && $gcmModule->isUsable());

            foreach ($notifications as $notification) {
                if ($notification instanceof Epr_Notification) {
                    if (($apnsEnabled && $notification->getType() == Epr_Notification::TYPE_IOS_APN) || ($gcmEnabled && $notification->getType() == Epr_Notification::TYPE_ANDROID_GCM)) {
                        if ($notification->isValid()) {
                            $messagesToSend[] = $notification;
                        }
                    }
                }

                if (count($messagesToSend) >= self::NUMBER_OF_NOTIFICATIONS_PER_CRON) {
                    break;
                }

                unset($notification);
            }

        }
        $output .= 'Found ' . count($messagesToSend) . ' ' . ((count($messagesToSend) == 1) ? 'message' : 'messages') . ' to send.';

        foreach ($messagesToSend as $notification) {
            if ($notification instanceof Epr_Notification) {

                $output .= "\n";

                $output .= 'Sending message ' . $notification->getTransportId() . ' to ' . $notification->getReceiverName();
                if ($notification->getType() == Epr_Notification::TYPE_IOS_APN) {
                    $output .= ' (iOS)';
                } else if ($notification->getType() == Epr_Notification::TYPE_ANDROID_GCM) {
                    $output .= ' (Android)';
                }

                $output .= ' ... ';

                if ($notification->send()) {
                    $output .= 'Success';
                } else {
                    $output .= 'Fail';
                }
            }
        }

        return $output;
    }

    public function createNotificationsForContext($context, $message, $sender = null)
    {

        $notifications = array();

        if ($this->hasUsableNotificationModules()) {

            // first, we take all notifications which have been sent with this context
            $sentNotifications = Epr_Notification_Collection::getNotificationsWithContext($context);

            // then we extract the device ids
            $sentTokens = array();
            foreach ($sentNotifications as $notification) {
                if ($notification instanceof Epr_Notification) {
                    $sentTokens[$notification->getDeviceId()] = true;
                }

                unset($notification);
            }

            unset($sentNotifications);

            // then we collect all tokens which did not sent a notification with the context
            $tokens = array();

            if ($this->casUseAPNSModule()) {
                $APNStokens = Epr_Token_Collection::getTokenWithType(Epr_Token::TYPE_IOS_APN);

                foreach ($APNStokens as $token) {
                    if ($token instanceof Epr_Token) {

                        $deviceId = $token->getDeviceToken();
                        if (!isset($sentTokens[$deviceId])) {
                            $tokens[] = $token;
                        }
                    }

                    unset($token);
                }
            }

            if ($this->canUseGCMMOdule()) {
                $GCMTokens = Epr_Token_Collection::getTokenWithType(Epr_Token::TYPE_ANDROID_GCM);
                foreach($GCMTokens as $token) {

                    if ($token instanceof Epr_Token) {
                        $deviceId = $token->getDeviceToken();
                        if (!isset($sentTokens[$deviceId])) {
                            $tokens[] = $token;
                        }
                    }

                    unset($token);
                }
            }

            unset($sentTokens);

            // and finally, we create the notifications
            foreach ($tokens as $token) {
                if ($token instanceof Epr_Token) {

                    $notification    = $token->createNotification($message, $sender, $context);
                    $notifications[] = $notification;

                }
            }

        }

        return $notifications;
    }

}