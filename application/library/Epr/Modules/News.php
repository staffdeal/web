<?php

/** @Document */
class Epr_Modules_News extends Epr_Module_API_Abstract implements Epr_Module_API, Epr_Module_Cms, Epr_Module_Cron {

	const PAGE_PATH = 'news';
	const NEWS_LIST_PATH = 'list';
	const NEWS_CATEGORY_MENU_PATH = 'menu';

	const PAGE_DETAILS = 'show-news';
	const DETAILS_NEWS = 'details';
	const DETAILS_MENU = 'menu';

	/** @Field(type="integer") */
	private $maximumNewsAge = 10; // in days

	public static function title() {
		return 'News';
	}
	public static function description() {
		return 'Manages news, categories and RSS gathering.';
	}
	
	/**
	 * @return Epr_Modules_News
	 */
	static public function instance() {
		return parent::instance();
	}
	
	/* (non-PHPdoc)
	 * @see Epr_Module_Builtin_Abstract::apiPath()
	 */
	 public function getAPIPath() {
		return 'news';	
	}
	
	public function getSettingsFormFields() {
		$fields = parent::getSettingsFormFields();
		
		$ageField = new Zend_Form_Element_Select('age');
		$ageField->setLabel('Remove after days');
		$ageField->setMultiOptions(Poundation\PArray::createProgressivArray(2, 60, 2)->dictionary()->nativeArray());
		$ageField->setAttrib('class','styledselect_form_1');
		$ageField->setDecorators(array('Composite'));
		$ageField->setValue($this->getMaximumNewsAge());
		$ageField->setDescription('The number of days after which news are removed automatically from the clients.');
		$fields[] = $ageField;

		return $fields;
	}
	
	/* (non-PHPdoc)
	 * @see Epr_Module_Abstract::saveSettingsField()
	 */
	public function saveSettingsField($field) {
		$name = $field->getName();
		$value = $field->getValue();
	
		if ($name == 'age') {
			$numericValue = (int)$value;
			if ($value == (string)$numericValue) {
				$this->setMaximumNewsAge($numericValue);
                return true;
			}

            return false;
		} else {
			return parent::saveSettingsField($field);
		}
	}
	
	public function getMaximumNewsAge() {
		return $this->maximumNewsAge;
	}
	
	public function setMaximumNewsAge($age) {
		if (is_integer($age) && $age > 0) {
			$this->maximumNewsAge = $age;
		}
	}

	public function getTemplateData()
	{
		return array(
			array(
				'id' => 'newsDetails',
				'platform' => Epr_Module_API::PLATFORM_IOS,
				'title' => 'Details Template iOS',
				'description' => 'Enter the template HTML for displaying news details (iOS).'
			),
			array(
				'id' => 'newsDetails',
				'platform' => Epr_Module_API::PLATFORM_ANDROID,
				'title' => 'Details Template Android',
				'description' => 'Enter the template HTML for displaying news details (Android).'
			)
		);
	}


	public function getPages()
	{

		$pages = array();

		$page = Epr_Cms_Page::getPageWithPath(self::PAGE_PATH);
		if (is_null($page)) {
			$page = new Epr_Cms_Page(self::PAGE_PATH);
			_dm()->persist($page);
		}
		$pages[] = $page;

		$menuElement = $page->getElementWithPath(self::NEWS_CATEGORY_MENU_PATH);
		if (is_null($menuElement)) {
			$menuElement = new Epr_Modules_News_CategoryMenuElement(self::NEWS_CATEGORY_MENU_PATH);
			$menuElement->setTitle('news topics');
			$menuElement->setWidth(3);
			$menuElement->setOrder(0);
			$page->addElement($menuElement);
		}

		$listElement = $page->getElementWithPath(self::NEWS_LIST_PATH);
		if (is_null($listElement)) {
			$listElement = new Epr_Modules_News_ListElement(self::NEWS_LIST_PATH);
			$listElement->setTitle('Current News');
			$listElement->setWidth(9);
			$listElement->setOrder(10);
			$listElement->setNumberOfItemsToDisplay(20);
			$page->addElement($listElement);
		}

		$detailsPage = Epr_Cms_Page::getPageWithPath(self::PAGE_DETAILS);
		if (is_null($detailsPage)) {
			$detailsPage = new Epr_Cms_Page(self::PAGE_DETAILS);
			_dm()->persist($detailsPage);
		}

		$detailsMenuElement = $detailsPage->getElementWithPath(self::DETAILS_MENU);
		if (is_null($detailsMenuElement)) {
			$detailsMenuElement = new Epr_Modules_News_CategoryMenuElement(self::DETAILS_MENU);
			$detailsMenuElement->setTitle('news topics');
			$detailsMenuElement->setWidth(3);
			$detailsMenuElement->setOrder(0);
			$detailsPage->addElement($detailsMenuElement);
		}


		$detailsElement = $detailsPage->getElementWithPath(self::DETAILS_NEWS);
		if (is_null($detailsElement)) {
			$detailsElement = new Epr_Modules_News_DetailsElement(self::DETAILS_NEWS);
			$detailsElement->setWidth(9);
			$detailsElement->setOrder(10);
			$detailsPage->addElement($detailsElement);
		}

		$pages[] = $detailsPage;

		return $pages;
	}

    /**
     * Returns the date range of news that should be visible currently.
     * @return null|\Poundation\PDateRange
     */
    public function getNewsDateRange() {
        $endDate = new \Poundation\PDate();
        $endDate->addSeconds(0 - $endDate->seconds());
        $startDate = clone $endDate;
        $startDate->setToMidnight();
        $startDate->addDays($this->getMaximumNewsAge() * -1);

        $range = \Poundation\PDateRange::createRange($startDate, $endDate);
        return $range;
    }

	public function runCron(DateInterval $intervalSinceLastRun)
	{
		$output = parent::runCron($intervalSinceLastRun);

		$newsToActivate = Epr_News_Collection::getDraftsUntilNow();
		if ($newsToActivate) {
			$output .= 'Found ' . count($newsToActivate) . ' news to publish:' . "\n";
			foreach($newsToActivate as $news) {
				if ($news instanceof Epr_News) {
					$output.= ' - ' . $news->getTitle();
					if ($news->publishAutomatically()) {
						$output.= '...OK' . "\n";
						$output.= '   => Creating notifications...';
						$count = $news->sendNotifications();
						$output.= $count;
					} else {
						$output.= '...not OK';
					}

					$output.= "\n";
				}
			}
		}

		return $output;
	}


}

