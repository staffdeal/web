<?php

/** @Document */
class Epr_Modules_StraightForwardRegistration extends Epr_Module_UserRegistration_Abstract implements Epr_Module_UserRegistration {


	/* (non-PHPdoc)
	 * @see Epr_Module::title()
	 */
	public static function title() {
		return 'Straight Forward';
	}

	/* (non-PHPdoc)
	 * @see Epr_Module::description()
	 */
	public static function description() {
		return 'Activates freshly registered users without any action from you side.';
	}

	public function processNewUser($user, $params) {

		$user->activate();

		$client      = Zend_Registry::get('client');
		$mailAddress = $client->info->mail;
		if ($mailAddress) {
			try {
				$mail = new Zend_Mail();
				$mail->addTo($mailAddress);
				$mail->setSubject('New user registration');
				$mail->setBodyText('A new user has been activated (' . $user->getEmail() . ', ' . $user->getFirstname() . ' ' . $user->getLastname() . ').');
				$mail->setFrom('webmaster@danielkbx.com', 'meinEPR');
				$mail->send();

				return true;
			} catch (Exception $e) {
				return false;
			}
		} else {
			return true;
		}
	}

	/**
	 * This method processes things right after the user has been verified.
	 *
	 * @param Epr_User $user
	 *
	 * @return bool
	 */
	public function processVerification(Epr_User $user)
	{
		return true;
	}

}