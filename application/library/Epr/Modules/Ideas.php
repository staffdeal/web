<?php 

/** @Document */
class Epr_Modules_Ideas extends Epr_Module_API_Abstract implements Epr_Module_API, Epr_Module_Cms {

	const PAGE_PATH = 'ideas';
	const MENU_PATH = 'menu';
	const LIST_PATH = 'list';

	const PAGE_COMPETITION_DETAILS = 'competition-show';
	const COMPETITION_DETAILS_PATH = 'details';
    const COMPETITION_SUBMISSIONS_PATH = 'submissions';

	const PAGE_SUBMIT_PATH = 'submit';
	const SUBMIT_MANUAL_PATH = 'manual';

    /** @Field(type="integer") */
    private $competitionsMaximumNewsAge = 10; // in days

	public static function title() {
		return 'Ideas';
	}
	public static function description() {
		return 'Manages competitions, categories and ideas.';
	}

	/* (non-PHPdoc)
	 * @see Epr_Module_Builtin_Abstract::apiPath()
	*/
	public function getAPIPath() {
		return 'ideas';
	}

    public function getCompetitionsMaximumNewsAge() {
        return $this->competitionsMaximumNewsAge;
    }

    public function setCompetitionsMaximumNewsAge($age) {
        if (is_integer($age) && $age > 0) {
            $this->competitionsMaximumNewsAge = $age;
        }
    }

    public function getSettingsFormFields() {
        $fields = parent::getSettingsFormFields();

        $ageField = new Zend_Form_Element_Select('age');
        $ageField->setLabel('Remove competitions after days');
        $ageField->setMultiOptions(Poundation\PArray::createProgressivArray(2, 60, 2)->dictionary()->nativeArray());
        $ageField->setAttrib('class','styledselect_form_1');
        $ageField->setDecorators(array('Composite'));
        $ageField->setValue($this->getCompetitionsMaximumNewsAge());
		$ageField->setDescription('The number of days after which competitions are automatically removed from clients.');
        $fields[] = $ageField;

        return $fields;
    }

    /* (non-PHPdoc)
     * @see Epr_Module_Abstract::saveSettingsField()
     */
    public function saveSettingsField($field) {
        $name = $field->getName();
        $value = $field->getValue();

        if ($name == 'age') {
            $numericValue = (int)$value;
            if ($value == (string)$numericValue) {
                $this->setCompetitionsMaximumNewsAge($numericValue);
                return true;
            }
            return false;
        } else {
            return parent::saveSettingsField($field);
        }
    }

	public function getTemplateData()
	{
		return array(
			array(
				'id' => 'competitionDescription',
				'platform' => Epr_Module_API::PLATFORM_IOS,
				'title' => 'Competition Description Template iOS',
				'description' => 'Enter the template HTML for displaying the competition description (iOS).'
			),
			array(
				'id' => 'competitionDescription',
				'platform' => Epr_Module_API::PLATFORM_ANDROID,
				'title' => 'Competition Description Template Android',
				'description' => 'Enter the template HTML for displaying the competition description (Android).'
			),
			array(
				'id' => 'competitionEnded',
				'platform' => Epr_Module_API::PLATFORM_IOS,
				'title' => 'Competition Ended Template iOS',
				'description' => 'Enter the template HTML for displaying the information after a competition ended (iOS).'
			),
			array(
				'id' => 'competitionEnded',
				'platform' => Epr_Module_API::PLATFORM_ANDROID,
				'title' => 'Competition Ended Template Android',
				'description' => 'Enter the template HTML for displaying the information after a competition ended (Android).'
			),
			array(
				'id' => 'competitionIdeaDraft',
				'platform' => Epr_Module_API::PLATFORM_IOS,
				'title' => 'Idea Draft Template iOS',
				'description' => 'Enter the template HTML for the idea draft displayed when the used creates a new idea (iOS).'
			),
			array(
				'id' => 'competitionIdeaDraft',
				'platform' => Epr_Module_API::PLATFORM_ANDROID,
				'title' => 'Idea Draft Template iOS',
				'description' => 'Enter the template HTML for the idea draft displayed when the used creates a new idea (Android).'
			),
			array(
				'id' => 'competitionIdeaDetails',
				'platform' => Epr_Module_API::PLATFORM_IOS,
				'title' => 'Idea Details Template iOS',
				'description' => 'Enter the template HTML for the idea details (iOS).'
			),
			array(
				'id' => 'competitionIdeaDetails',
				'platform' => Epr_Module_API::PLATFORM_ANDROID,
				'title' => 'Idea Draft Template iOS',
				'description' => 'Enter the template HTML for the idea details (Android).'
			)
		);
	}


	public function getPages()
	{
		$pages = array();

		// Index page
		$indexPage = Epr_Cms_Page::getPageWithPath(self::PAGE_PATH);
		if (is_null($indexPage)) {
			$indexPage = new Epr_Cms_Page(self::PAGE_PATH);
			_dm()->persist($indexPage);
		}

		$indexMenu = $indexPage->getElementWithPath(self::MENU_PATH);
		if (is_null($indexMenu)) {
			$indexMenu = new Epr_Modules_Ideas_CategoryMenuElement(self::MENU_PATH);
			$indexMenu->setTitle('categories');
			$indexMenu->setWidth(3);
			$indexMenu->setOrder(0);
			$indexPage->addElement($indexMenu);
		}

		$indexList = $indexPage->getElementWithPath(self::LIST_PATH);
		if (is_null($indexList)) {
			$indexList = new Epr_Modules_Ideas_Competitions_ListElement(self::LIST_PATH);
			$indexList->setWidth(9);
			$indexList->setOrder(10);
			$indexList->setTitle('idea contests');
			$indexPage->addElement($indexList);
		}

		$pages[] = $indexPage;


		// competition page
		$competitionPage = Epr_Cms_Page::getPageWithPath(self::PAGE_COMPETITION_DETAILS);
		if (is_null($competitionPage)) {
			$competitionPage = new Epr_Cms_Page(self::PAGE_COMPETITION_DETAILS);
			_dm()->persist($competitionPage);
		}

		$competitionMenu = $competitionPage->getElementWithPath(self::MENU_PATH);
		if (is_null($competitionMenu)) {
			$competitionMenu = new Epr_Modules_Ideas_CategoryMenuElement(self::MENU_PATH);
			$competitionMenu->setTitle('categories');
			$competitionMenu->setWidth(3);
			$competitionMenu->setOrder(0);
			$competitionPage->addElement($competitionMenu);
		}

		$competitionDetails = $competitionPage->getElementWithPath(self::COMPETITION_DETAILS_PATH);
		if (is_null($competitionDetails)) {
			$competitionDetails = new Epr_Modules_Ideas_Competitions_DetailsElement(self::COMPETITION_DETAILS_PATH);
			$competitionDetails->setWidth(9);
			$competitionDetails->setOrder(10);
			$competitionPage->addElement($competitionDetails);
		}

        $competitionSubmissions = $competitionDetails->getElementWithPath(self::COMPETITION_SUBMISSIONS_PATH);
        if (is_null($competitionSubmissions)) {
            $competitionSubmissions = new Epr_Modules_Ideas_Submissions_ListElement(self::COMPETITION_SUBMISSIONS_PATH);
            $competitionSubmissions->setNumberOfItemsToDisplay(0);
            $competitionSubmissions->setWidth(9);
            $competitionSubmissions->setOrder(11);
            $competitionSubmissions->setTitle('Submitted ideas');
            $competitionDetails->addElement($competitionSubmissions);
        }

		$pages[] = $competitionPage;

		// submit page
		$submitPage = Epr_Cms_Page::getPageWithPath(self::PAGE_SUBMIT_PATH);
		if (is_null($submitPage)) {
			$submitPage = new Epr_Cms_Page(self::PAGE_SUBMIT_PATH);
			$submitPage->setTitle('Submit an idea');
			_dm()->persist($submitPage);
		}

		$manualElement = $submitPage->getElementWithPath(self::SUBMIT_MANUAL_PATH);
		if (is_null($manualElement)) {
			$manualElement = new Epr_Cms_Element_HTML(self::SUBMIT_MANUAL_PATH);
			$manualElement->setTitle('Manual');
			$manualElement->setContent('Here you can explain what the user <b>need to do</b> and how this form actually works.');
			$submitPage->addElement($manualElement);
		}

		$pages[] = $submitPage;



		return $pages;
	}

}