<?php

/** @Document */
class Epr_Modules_GCM extends Epr_Module_Abstract implements Epr_Module_Notification {

    const FIELD_ACTIVE        = 'active';
    const FIELD_API_KEY = 'apiKey';

    /** @Field(type="boolean") */
    private $active;

    /** @Field(type="string") */
    private $apiKey;

	static public function title()
	{
		return 'GCM';
	}

	static public function description()
	{
		return 'This plugin handles notificitions to Google Cloud Massaging.';
	}

	/**
	 * Return true if the plugin can be used.
	 *
	 * @return true
	 */
	public function isUsable()
	{
		$usable = $this->getActive() && (strlen($this->getApiKey()) > 0);
        return $usable;
	}

	public function notify()
	{
		return false;
	}

    /**
     * Sets the active state.
     *
     * @param $active
     *
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = ($active == true);

        return $this;
    }

    /**
     * Returns the active state.
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Sets the API key.
     * @param $key
     * @return $this
     */
    public function setApiKey($key) {
        if (is_string($key) || is_null($key)) {
            $this->apiKey = $key;
        }

        return $this;
    }

    /**
     * Returns the API key.
     * @return string
     */
    public function getApiKey() {
        return $this->apiKey;
    }

    public  function getSettingsFormFields() {
        $fields = parent::getSettingsFormFields();

        $active = new Zend_Form_Element_Checkbox(self::FIELD_ACTIVE);
        $active->setLabel('Active');
        $active->setChecked($this->getActive());
        $active->setDescription('Set to active when you want to use push notifications to Android devices.');
        $fields[] = $active;

        $apiKey = new Zend_Form_Element_Text(self::FIELD_API_KEY);
        $apiKey->setLabel('API Key');
        $apiKey->setAttrib('class', 'inp-form middle');
        $apiKey->setValue($this->getApiKey());
        $apiKey->setDescription('The GCM API Key can be created at Google Developer pages.');
        $fields[] = $apiKey;

        return $fields;
    }

    public  function saveSettingsField($field) {

        $name = $field->getName();
        $value = $field->getValue();

        if ($name == self::FIELD_ACTIVE) {
            $this->setActive($value);

            return true;
        } else if ($name === self::FIELD_API_KEY) {
            $this->setApiKey($value);
            return true;
        } else {
            return parent::saveSettingsField($field);
        }

    }

}