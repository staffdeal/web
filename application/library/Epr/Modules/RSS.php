<?php

/** @Document */
class Epr_Modules_RSS extends Epr_Module_Builtin_Abstract implements Epr_Module_Builtin
{

	const MAXIMUM_MAXIMUMITEMAGE = 60;

	/** @Field(type="integer") */
	private $maximumItemAge = 30; // number of days before a feed item is not imported

	static public function title()
	{
		return 'RSS Feed Reader';
	}

	static public function description()
	{
		return 'This module reads RSS feeds.';
	}


	/**
	 * Returns the number of days after a RSS feed item is removed.
	 *
	 * @return int
	 */
	public function getMaximumItemAge()
	{
		return $this->maximumItemAge;
	}

	/**
	 * Sets the number of days after a RSS feed item is removed.
	 *
	 * @param $age
	 *
	 * @return $this
	 */
	public function setMaximumItemAge($age)
	{
		$numericAge = (int)$age;
		if (1 <= $numericAge && $numericAge <= self::MAXIMUM_MAXIMUMITEMAGE) {
			$this->maximumItemAge = $numericAge;
		}

		return $this;
	}

	public function getSettingsFormFields()
	{
		$fields = parent::getSettingsFormFields();

		$ageField = new Zend_Form_Element_Select('age');
		$ageField->setLabel('Remove after days');
		$ageField->setMultiOptions(Poundation\PArray::createProgressivArray(2, self::MAXIMUM_MAXIMUMITEMAGE, 2)->dictionary()->nativeArray());
		$ageField->setAttrib('class', 'styledselect_form_1');
		$ageField->setDecorators(array('Composite'));
		$ageField->setValue($this->getMaximumItemAge());
		$ageField->setDescription('Only feed items younger than this number of days are listed. Older items cannot be converted into a news entry.');
		$fields[] = $ageField;

		return $fields;
	}

	public function saveSettingsField($field)
	{
		$name  = $field->getName();
		$value = $field->getValue();

		if ($name == 'age') {

			$numericValue = (int)$value;
			$this->setMaximumItemAge($numericValue);

			return true;

		} else {
			return parent::saveSettingsField($field);
		}

	}

}