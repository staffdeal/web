<?php

/** @Document */
class Epr_Modules_Dealers extends Epr_Module_API_Abstract implements Epr_Module_API, Epr_Module_Cms
{

	const PAGE_DEAL = 'dealers-for-deal';
	const DEAL_DESCRIPTION = 'description';

    /** @Field(type="string") */
    private $category1Caption;

    /** @Field(type="string") */
    private $category1Fields;

    /** @Field(type="string") */
    private $category2Caption;

    /** @Field(type="string") */
    private $category2Fields;

    /** @Field(type="string") */
    private $category3Caption;

    /** @Field(type="string") */
    private $category3Fields;

    /**
     * Returns the Dealers module or null of none is loaded.
     * @return null|Epr_Modules_Dealers
     */
    public static function sharedModule() {
        return _app()->getModuleWithIdentifier(self::identifier());
    }

    public static function title()
    {
        return 'Dealers';
    }

    public static function description()
    {
        return 'Manages dealers and locations.';
    }

    public function getAPIPath()
    {
        return 'dealers';
    }

    public function getSettingsFormFields()
    {

        $fields = parent::getSettingsFormFields();

        $cat1Caption = new Zend_Form_Element_Text('category1Caption');
        $cat1Caption->setLabel('Additional Category 1 Caption');
        $cat1Caption->setAttrib('class', 'inp-form');
        $cat1Caption->setValue($this->category1Caption);
		$cat1Caption->setDescription('The name of the additonal category 1.');
        $fields[] = $cat1Caption;

        $fieldListHelpText = 'Comma separated list of possible values.';

        $cat1Fields = new Zend_Form_Element_Text('category1Fields');
        $cat1Fields->setLabel('Additional Category 1 Fields');
        $cat1Fields->setAttrib('class', 'inp-form wide');
        $cat1Fields->setValue($this->category1Fields);
        $cat1Fields->setDescription($fieldListHelpText);
        $fields[] = $cat1Fields;

        $cat2Caption = new Zend_Form_Element_Text('category2Caption');
        $cat2Caption->setLabel('Additional Category 2 Caption');
        $cat2Caption->setAttrib('class', 'inp-form');
        $cat2Caption->setValue($this->category2Caption);
		$cat2Caption->setDescription('The name of the additonal category 2.');
        $fields[] = $cat2Caption;

        $cat2Fields = new Zend_Form_Element_Text('category2Fields');
        $cat2Fields->setLabel('Additional Category 2 Fields');
        $cat2Fields->setAttrib('class', 'inp-form wide');
        $cat2Fields->setDescription($fieldListHelpText);
        $cat2Fields->setValue($this->category2Fields);
        $fields[] = $cat2Fields;

        $cat3Caption = new Zend_Form_Element_Text('category3Caption');
        $cat3Caption->setLabel('Additional Category 3 Caption');
        $cat3Caption->setAttrib('class', 'inp-form');
        $cat3Caption->setValue($this->category3Caption);
		$cat3Caption->setDescription('The name of the additonal category 3.');
        $fields[] = $cat3Caption;

        $cat3Fields = new Zend_Form_Element_Text('category3Fields');
        $cat3Fields->setLabel('Additional Category 3 Fields');
        $cat3Fields->setAttrib('class', 'inp-form wide');
        $cat3Fields->setDescription($fieldListHelpText);
        $cat3Fields->setValue($this->category3Fields);
        $fields[] = $cat3Fields;


        return $fields;
    }

	/**
	 * Returns the captions of all additional categories.
	 * @return array
	 */
	public function getCategoriesCaptions()
    {
        $captions = array();
        if ($this->category1Caption) {
            $captions[] = $this->category1Caption;
        }
        if ($this->category2Caption) {
            $captions[] = $this->category2Caption;
        }
        if ($this->category3Caption) {
            $captions[] = $this->category3Caption;
        }

		return $captions;
    }

    public function getCategories() {
        $categories = array();

        if ($this->category1Caption && $this->category1Fields) {
            $cat = array();
            $catFields = explode(',', $this->category1Fields);
            foreach($catFields as $field) {
                $cat[] = trim($field);
            }
            if (count($cat) > 0) {
                $categories[$this->category1Caption] = $cat;
            }
        }

        if ($this->category2Caption && $this->category2Fields) {
            $cat = array();
            $catFields = explode(',', $this->category2Fields);
            foreach($catFields as $field) {
                $cat[] = trim($field);
            }
            if (count($cat) > 0) {
                $categories[$this->category2Caption] = $cat;
            }
        }

        return $categories;
    }

    public function saveSettingsField($field)
    {
        $fieldName = $field->getName();
        $fieldValue = $field->getValue();
        if ($fieldName == 'category1Caption') {
            $this->category1Caption = $fieldValue;
            return true;

        }
        if ($fieldName == 'category1Fields') {
            if ($fieldValue) {
                if ($this->category1Caption) {
                    $this->category1Fields = $fieldValue;
                    return true;
                }
                return false;
            } else {
                $this->category1Fields = $fieldValue;
                return true;
            }

        } else if ($fieldName == 'category2Caption') {
            $this->category2Caption = $fieldValue;
            return true;

        } else if ($fieldName == 'category2Fields') {
            if ($fieldValue) {
                if ($this->category2Caption) {
                    $this->category2Fields = $fieldValue;
                    return true;
                }
                return false;
            } else {
                $this->category2Fields = $fieldValue;
                return true;
            }

        } else if ($fieldName == 'category3Caption') {
            $this->category3Caption = $fieldValue;
            return true;

        } else if ($fieldName == 'category3Fields') {
            if ($fieldValue) {
                if ($this->category3Caption) {
                    $this->category3Fields = $fieldValue;
                    return true;
                }
                return false;
            } else {
                $this->category3Fields = $fieldValue;
                return true;
            }

        } else {
            return parent::saveSettingsField($field);
        }
    }

	public function getPages()
	{
		$pages = array();

		$dealersForDealPage = Epr_Cms_Page::getPageWithPath(self::PAGE_DEAL);
		if (is_null($dealersForDealPage)) {
			$dealersForDealPage = new Epr_Cms_Page(self::PAGE_DEAL);
			$dealersForDealPage->setTitle('find dealers');
			_dm()->persist($dealersForDealPage);
		}

		$dealersForDealContent = $dealersForDealPage->getElementWithPath(self::DEAL_DESCRIPTION);
		if (is_null($dealersForDealContent)) {
			$dealersForDealContent = new Epr_Cms_Element_HTML(self::DEAL_DESCRIPTION);
			$dealersForDealContent->setTitle('find dealers near you');
			$dealersForDealContent->setContent('This is a nice place to explain the user what to with the map.');
			$dealersForDealContent->setWidth(12);
			$dealersForDealPage->addElement($dealersForDealContent);
		}

		$pages[] = $dealersForDealPage;

		return $pages;
	}

}
