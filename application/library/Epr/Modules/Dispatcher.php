<?php

/** @Document */
class Epr_Modules_Dispatcher extends Epr_Module_Builtin_Abstract implements Epr_Module_Builtin, Epr_Com_Message_Authority
{

	/** @Field(type="string") */
	private $url;

	/** @Field(type="string") */
	private $dispatcherKey;

	/** @Field(type="string") */
	private $backendKey;

	/** @Field(type="string") */
	private $referenceId;

	/** @Field(type="url") */
	private $feedURL;



	static public function title()
	{
		return "Dispatcher Interconnection";
	}

	static public function description()
	{
		return "Maintains the connection to a dispatcher system.";
	}

	public function isPaired()
	{
		return ($this->url && \Poundation\PURL::isValidURLString($this->url));
	}

	/**
	 * Returns the URL of the dispatcher.
	 *
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * Returns the key if the dispatcher.
	 *
	 * @return string
	 */
	public function getDispatcherKey()
	{
		return $this->dispatcherKey;
	}

	/**
	 * Returns the key if this backend instance.
	 *
	 * @return string
	 */
	public function getBackendKey()
	{
		return $this->backendKey;
	}

	/**
	 * Returns the key that is used to sign a message.
	 *
	 * @return string
	 */
	public function signingKey()
	{
		return $this->backendKey;
	}

	/**
	 * Returns the key the communication partner is supposed to use.
	 *
	 * @return string
	 */
	public function remoteKey()
	{
		return $this->dispatcherKey;
	}

	public function referenceId()
	{
		return $this->referenceId;
	}


	/**
	 * Saves the url and the dispatcher's key for pairing. If successful, the freshly created backendKey is returned.
	 *
	 * @param string $url
	 * @param string $dispatcherKey
	 *
	 * @return null|string
	 */
	public function pair($url, $dispatcherKey = null, $referenceId = null)
	{

		if ($dispatcherKey == null) {
			$dispatcherKey = $this->dispatcherKey;
		}

		if ($url && \Poundation\PURL::isValidURLString($url)) {
			if (is_string($dispatcherKey) && strlen($dispatcherKey) > 8) {

				$this->url           = $url;
				$this->dispatcherKey = $dispatcherKey;
				if (!is_string($this->backendKey) || strlen($this->backendKey) < 10) {
					$this->backendKey = Epr_Com_Connection::connectionKey();
				}
				$this->referenceId = $referenceId;

				return $this->backendKey;
			}
		}

		return null;
	}

	public function getSettingsFormFields()
	{
		$fields = parent::getSettingsFormFields();

		if ($this->isPaired()) {

			$urlField = new Zend_Form_Element_Text('url');
			$urlField->setLabel('Dispatcher URL');
			$urlField->setAttrib('class', 'inp-form');
			$urlField->setAttrib('disabled', 'disabled');
			$urlField->setValue($this->url);
			$urlField->setDescription('The URL of the dispatcher. You cannot change this.');
			$fields[] = $urlField;

		}

		$dispatcherKeyField = new Zend_Form_Element_Text('key1');
		$dispatcherKeyField->setLabel('Dispatcher Key');
		$dispatcherKeyField->setAttrib('class', 'inp-form middle');
		$dispatcherKeyField->setDecorators(array('Composite'));
		$dispatcherKeyField->setValue($this->dispatcherKey);
		$dispatcherKeyField->setDescription('The public key of the dispatcher. Do not change this unless you are told to do so.');
		$fields[] = $dispatcherKeyField;

		if ($this->isPaired()) {

			$backendKeyField = new Zend_Form_Element_Text('key2');
			$backendKeyField->setLabel('Backend Key');
			$backendKeyField->setAttrib('class', 'inp-form middle');
			$backendKeyField->setAttrib('disabled', 'disabled');
			$backendKeyField->setValue($this->backendKey);
			$backendKeyField->setDescription('The public key of this backend. Do not change this unless you are told to do so.');
			$fields[] = $backendKeyField;

			$backendIdField = new Zend_Form_Element_Text('referenceId');
			$backendIdField->setLabel('Reference ID');
			$backendIdField->setAttrib('class', 'inp-form middle');
			$backendIdField->setAttrib('disabled', 'disabled');
			$backendIdField->setValue($this->referenceId());
			$backendIdField->setDescription('The internal backend id of the dispatcher. You cannot change this.');
			$fields[] = $backendIdField;

			$feedURL = new Epr_Form_Element_URL('feedURL');
			$feedURL->setLabel('Feed URL');
			$feedURL->setAttrib('class', 'inp-form middle');
			$feedURL->setAttrib('disabled', 'disabled');
			$feedURL->setValue($this->getFeedURL());
			$feedURL->setDescription('The dispatcher feed URL used to show platform news. You cannot change this.');
			$fields[] = $feedURL;

		}

		return $fields;
	}

	public function saveSettingsField($field)
	{
		$fieldName = $field->getName();
		if ($fieldName == 'key1') {
			$this->dispatcherKey = $field->getValue();

			return true;

		} else {
			return parent::saveSettingsField($field);
		}
	}

	public function call($action, $params = null)
	{

		$result = null;

		$url = \Poundation\PURL::URLWithString($this->getUrl());
		if ($url) {

			$url->addPathComponent('api')->addPathComponent($action);
			if (is_array($params)) {
				foreach ($params as $name => $value) {
					$url->addPathComponent($name . '/' . $value);
				}
			}

			$request  = new Zend_Http_Client((string)$url);
            $response = null;

            try {
			    $response = $request->request(Zend_Http_Client::GET);
            } catch (Exception $e) {
                _logger()->critical('Could not connect to dispatch: ' . $e->getMessage());
            }

			if ($response) {
				if ($response->getStatus() == 200) {
					if ($response->getHeader('Content-type') == 'application/json') {
						$result = json_decode($response->getBody());
					}
				}
			}
		}

		return $result;
	}

	/**
	 * Returns the feed URL.
	 *
	 * @return \Poundation\PURL
	 */
	public function getFeedURL()
	{
		return $this->feedURL;
	}

	/**
	 * Sets the feed URL.
	 *
	 * @param \Poundation\PURL $url
	 *
	 * @return $this
	 */
	public function setFeedURL($url)
	{
		if (!is_null($url) && !$url instanceof \Poundation\PURL) {
			$url = \Poundation\PURL::URLWithString($url);
		}
		$this->feedURL = $url;

		return $this;
	}

}

