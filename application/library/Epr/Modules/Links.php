<?php

/** @Document */
class Epr_Modules_Links extends Epr_Module_Builtin_Abstract implements Epr_Module_Builtin
{

	static public function title()
	{
		return 'Link List';
	}

	static public function description()
	{
		return 'Manages list of links of websites, videos and more';
	}

//	public  function getAPIPath() {
//		return 'links';
//	}
//
	public function getSettingsFormFields() {
		$fields = parent::getSettingsFormFields();

		return $fields;
	}

}