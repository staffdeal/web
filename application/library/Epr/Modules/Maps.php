<?php

/** @Document */
class Epr_Modules_Maps extends Epr_Module_Builtin_Abstract implements Epr_Module_Builtin
{

	/** @Field(type="string") */
	private $googleApiKey;

	/** @Field(type="coordinate") */
	private $centerLocation;

	private $markers = array();

	static public function title()
	{
		return 'Maps';
	}

	static public function description()
	{
		return 'This modules manages and creates maps.';
	}

	/**
	 * Returns the Google API key.
	 *
	 * @return string
	 */
	public function getGoogleApiKey()
	{
		return $this->googleApiKey;
	}

	/**
	 * Sets the Google API Key.
	 *
	 * @param string $key
	 *
	 * @return $this
	 */
	public function setGoogleApiKey($key)
	{
		if (is_string($key) || is_null($key)) {
			$this->googleApiKey = $key;
		}

		return $this;
	}

	/**
	 * @return \Poundation\PCoordinate
	 */
	public function getCenterLocation()
	{
		return $this->centerLocation;
	}

	/**
	 * @param $coordinate
	 *
	 * @return $this
	 */
	public function setCenterLocation($coordinate)
	{

		if ($coordinate instanceof \Poundation\PCoordinate || is_null($coordinate)) {
			$this->centerLocation = $coordinate;
		}

		return $this;

	}

	public function getSettingsFormFields()
	{
		$fields = parent::getSettingsFormFields();

		$apiKey = new Zend_Form_Element_Text('gapi');
		$apiKey->setLabel('Google API Key (Console)');
		$apiKey->setValue($this->getGoogleApiKey());
		$apiKey->setDescription('Enter your Google Maps Console API Key.');
		$apiKey->setAttrib('class', 'inp-form wide');
		$fields[] = $apiKey;

		$centerLocation = new Zend_Form_Element_Text('centerLocation');
		$centerLocation->setLabel('Center Coordinates');
		$centerLocation->setValue($this->getCenterLocation());
		$centerLocation->setDescription("Enter the location of the map\'s center.");
		$centerLocation->setAttrib('class', 'inp-form middle');
		$fields[] = $centerLocation;

		return $fields;
	}

	public function saveSettingsField($field)
	{
		$name  = $field->getName();
		$value = $field->getValue();

		if ($name == 'gapi') {
			$this->setGoogleApiKey($value);

			return true;
		} else if ($name == 'centerLocation') {
			if (strlen($value) == 0) {
				$this->centerLocation = null;

				return true;
			} else {
				$location = \Poundation\PCoordinate::createCoordinateFromString($value);
				if ($location) {
					$this->centerLocation = $location;

					return true;
				} else {
					return false;
				}
			}

		} else {
			return parent::saveSettingsField($field);
		}

	}

	public function addMarker(\Poundation\PCoordinate $coordinate, $title = null, $content = null)
	{

		$this->markers[] = array(
			'coordinate' => $coordinate,
			'title'      => $title,
			'content'    => $content
		);

	}

	public function getMapsHTML($containerID = null, $useSensor = true)
	{

		if (is_null($containerID)) {
			$containerID = 'map_' . rand(1000, 10000);
		}

		$output = '';

		$output .= '<div id="' . $containerID . '" class="map"></div>';

		$params = array(
			'sensor=' . (($useSensor == true) ? 'true' : 'false')
		);

		if (strlen($this->getGoogleApiKey()) > 0) {
			$params[] = 'key=' . $this->getGoogleApiKey();
		}

		$output .= '<script src="https://maps.googleapis.com/maps/api/js?' . implode('&', $params) . '" type="text/javascript"></script>';

		$centerLocation = $this->getCenterLocation();
		if (is_null($centerLocation)) {
			$centerLocation = '(51.04139389812637, 10.052490234375)';
		} else {
			$centerLocation = (string)$centerLocation;
		}

		$script = '$(document).ready(function() {
					"use strict";
					var mapOptions =  {
						zoom: 6,
						center: new google.maps.LatLng' . $centerLocation . ',
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};


					var map = new google.maps.Map(document.getElementById("' . $containerID . '"), mapOptions);' . "\n\n";

		foreach ($this->markers as $index => $marker) {

			$coordinate = (isset($marker['coordinate'])) ? $marker['coordinate'] : null;
			$title      = (isset($marker['title'])) ? $marker['title'] : null;
			$content    = (isset($marker['content'])) ? $marker['content'] : null;

			if ($coordinate instanceof \Poundation\PCoordinate) {

				$includeInfoWindow = (strlen($title . $containerID) > 0);

				$infoId   = 'info_' . $index;
				$markerId = 'marker_' . $index;

				if ($includeInfoWindow) {

					$title = (strlen($title) > 0) ? '<h1>' . $title . '</h1>' : '';
					$content = $title . ((strlen($content) > 0) ? '<div>' . $content . '</div>' : '');

					$script .= 'var ' . $infoId . " = new google.maps.InfoWindow({
      							content: " . json_encode($content) . "
  							});" . "\n";
				}

				$script .= 'var ' . $markerId . ' = new google.maps.Marker({
      							position: new google.maps.LatLng' . $coordinate . ',
      							map: map
  							});' . "\n";

				if ($includeInfoWindow) {
					$script .= 'google.maps.event.addListener(' . $markerId . ', "click", function() {
   						 		' . $infoId . '.open(map,' . $markerId . ');
  							});' . "\n";
				}

			}

		}

		$script .= '});';

		$output .= '<script type="text/javascript">' . $script . '</script>';

		return $output;
	}

}