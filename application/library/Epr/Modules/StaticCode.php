<?php

/** @Document */
class Epr_Modules_StaticCode extends Epr_Module_DealCode_Abstract implements Epr_Module_DealCode
{

    const STATIC_EAN_KEY = 'staticEAN';

    static public function title()
    {
        return 'Static EAN Code';
    }

    static public function description()
    {
        return 'This module manages static EAN deal codes.';
    }

    public function getDataKey()
    {
        return 'staticEAN';
    }

    public function setData($data)
    {
        if (is_string($data)) {
            $data = array(self::STATIC_EAN_KEY => $data);
        }
        return parent::setData($data);
    }


    /**
     * Returns the EAN code.
     *
     * @return string
     */
    public function getStaticEANCode()
    {
        return $this->getDataForKey(self::STATIC_EAN_KEY);
    }

    /**
     * Sets the EAN code.
     *
     * @param $code
     *
     * @return Epr_Modules_StaticCode
     */
    public function setStaticEANCode($code)
    {
        $this->setDataForKey($code, self::STATIC_EAN_KEY);

        return $this;
    }

    public function isValid()
    {
        $code = $this->getStaticEANCode();

        return (is_string($code) && strlen($code) >= 10);
    }

    public function hasWarning() {
        return (!$this->isValid());
    }

    public function redeem()
    {
        return $this->getStaticEANCode();
    }

    public function getSettingsFormFields($context = Epr_Module::CONTEXT_OBJECT)
    {
        $fields = parent::getSettingsFormFields();

        if ($context == Epr_Module::CONTEXT_OBJECT) {

            $ean = new Zend_Form_Element_Text(self::STATIC_EAN_KEY);
            $ean->setLabel('Onsite EAN Code');
            $ean->setDescription('The EAN code of the deal (not the product itself). It is used to create a barcode which can be redeemed at the shop.');
            $ean->setAttrib('class', 'inp-form narrow');
            $ean->setValue($this->getStaticEANCode());

            $eanValidator = new Zend_Validate_Barcode('EAN13');
            $ean->addValidator($eanValidator);

            $fields[] = $ean;
        }

        return $fields;
    }

    public function saveSettingsField($field)
    {
        $name = $field->getName();
        $value = $field->getValue();

        if ($name == self::STATIC_EAN_KEY) {
            $this->setStaticEANCode($value);
            return true;
        } else {
            return parent::saveSettingsField($field);
        }
    }


}