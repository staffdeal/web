<?php

/** @Document */
class Epr_Modules_Info extends Epr_Module_Builtin_Abstract implements Epr_Module_Builtin, Epr_Module_Cms
{

    const PAGE_PATH           = 'info';
    const PAGE_ERROR_404_PATH = '404';
    const ERROR_404_CONTENT   = 'content';

    const PAGE_IMPRINT    = 'imprint';
    const IMPRINT_CONTENT = 'content';

    const PAGE_PRIVACY_POLICY    = 'privacy-policy';
    const PRIVACY_POLICY_CONTENT = 'content';

    const PAGE_TERMS_OF_USE    = 'terms-of-use';
    const TERMS_OF_USE_CONTENT = 'content';

    const BASEURL_FACEBOOK = 'http://www.facebook.de/';
    const BASEURL_TWITTER  = 'http://www.twitter.com/';

    /** @field(type="string") */
    private $aboutText;

    /** @field(type="string") */
    private $impressText;

    /** @field(type="url") */
    private $mediaURL;

    /** @field(type="string") */
    private $facebookName;

    /** @field(type="string") */
    private $facebookProfileID;

    /** @field(type="string") */
    private $twitterName;

    /** @field(type="string") */
    private $reasonsToRegisterMobile;

    static public function title()
    {
        return 'General Information';
    }

    static public function description()
    {
        return 'This module manages static texts for information purpose.';
    }

    public function getFacebookPageUrl()
    {
        return self::BASEURL_FACEBOOK . $this->getFacebookName();
    }

    public function getTwitterPageUrl()
    {
        return self::BASEURL_TWITTER . $this->getTwitterName();
    }

    public function getSettingsFormFields()
    {

        // we do not call super here since we do not want the defaults fields
        $fields = array();

        $mediaURL = new Epr_Form_Element_URL('mediaURL');
        $mediaURL->setLabel('Media URL');
        $mediaURL->setAttrib('class', 'inp-form middle');
        $mediaURL->setValue($this->getMediaURL());
        $mediaURL->setDescription('The URL of the media channel.');
        $fields[] = $mediaURL;

        $facebookName = new Zend_Form_Element_Text('facebookName');
        $facebookName->setLabel('Facebook Page Name');
        $facebookName->setAttrib('class', 'inp-form middle');
        $facebookName->setValue($this->getFacebookName());
        $facebookName->setDescription('The Facebook Page Name.');
        $fields[] = $facebookName;

        $facebookName = new Zend_Form_Element_Text('facebookID');
        $facebookName->setLabel('Facebook Profile ID');
        $facebookName->setAttrib('class', 'inp-form middle');
        $facebookName->setValue($this->getFacebookProfileID());
        $facebookName->setDescription('The Facebook profile ID. It is needed to support the facebook app on mobile devices. Use http://graph.facebook.com/[NAME] to get the ID.');
        $fields[] = $facebookName;

        $twitterName = new Zend_Form_Element_Text('twitterName');
        $twitterName->setLabel('Twitter User Name');
        $twitterName->setAttrib('class', 'inp-form middle');
        $twitterName->setValue($this->getTwitterName());
        $twitterName->setDescription('The Twitter User Name.');
        $fields[] = $twitterName;

        $about = new Zend_Form_Element_Textarea('about');
        $about->setLabel('About');
        $about->setAttrib('class', 'wysiwyg wide');
        $about->setValue($this->getAboutText());
        $about->setDescription('The text which explains what the app and the webpage is all about.');
        $fields[] = $about;

        $impress = new Zend_Form_Element_Textarea('impress');
        $impress->setLabel('Impress');
        $impress->setAttrib('class', 'wysiwyg wide');
        $impress->setValue($this->getImpressText());
        $impress->setDescription('The impress of the app and the webpage.');
        $fields[] = $impress;

        $reasonsMobile = new Zend_Form_Element_Textarea('reasonsMobile');
        $reasonsMobile->setLabel('Reasons to register (mobile)');
        $reasonsMobile->setAttrib('class', 'wide');
        $reasonsMobile->setValue($this->getReasonsToRegisterMobile());
        $reasonsMobile->setDescription('The text which explains the user why he should register for the platform.');
        $fields[] = $reasonsMobile;


        return $fields;

    }

    public function saveSettingsField($field)
    {

        $name  = $field->getName();
        $value = $field->getValue();

        if ($name == 'about') {
            $this->setAboutText($value);

            return true;
        } else if ($name == 'impress') {
            $this->setImpressText($value);

            return true;
        } else if ($name == 'mediaURL') {
            if ($value instanceof \Poundation\PURL) {
                $this->setMediaURL($value);
            } else {
                $this->removeMediaURL();
            }

            return true;
        } else if ($name == 'reasonsMobile') {
            $this->setReasonsToRegisterMobile($value);

            return true;
        } else if ($name == 'facebookName') {
            $this->setFacebookName($value);

            return true;
        } else if ($name == 'facebookID') {
            $this->setFacebookProfileID($value);
            return true;
        } else if ($name == 'twitterName') {
            $this->setTwitterName($value);

            return true;
        } else {
            return parent::saveSettingsField($field);
        }

    }

    /**
     * Returns the about text.
     *
     * @return string
     */
    public function getAboutText()
    {
        return $this->aboutText;
    }

    /**
     * Sets the about text.
     *
     * @param $text
     *
     * @return $this
     */
    public function setAboutText($text)
    {
        $this->aboutText = (string)$text;

        return $this;
    }

    /**
     * Returns the impress text.
     *
     * @return string
     */
    public function getImpressText()
    {
        return $this->impressText;
    }

    /**
     * Sets the impress text.
     *
     * @param $text
     *
     * @return $this
     */
    public function setImpressText($text)
    {
        $this->impressText = (string)$text;

        return $this;
    }


    /** Returns the media URL
     *
     * @return null|\Poundation\PURL
     */
    public function getMediaURL()
    {
        return $this->mediaURL;
    }

    /** Returns the facebook name
     *
     * @return string|null
     */
    public function getFacebookName()
    {
        return $this->facebookName;
    }

    /** Returns the twitter name
     *
     * @return string|null
     */
    public function getTwitterName()
    {
        return $this->twitterName;
    }

    /**
     * @return mixed
     */
    public function getFacebookProfileID()
    {
        return $this->facebookProfileID;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setFacebookProfileID($id)
    {
        $this->facebookProfileID = $id;

        return $this;
    }

    /**
     * Sets the media URL.
     *
     * @param \Poundation\PURL $url
     *
     * @return $this
     */
    public function setMediaURL(\Poundation\PURL $url)
    {
        $this->mediaURL = $url;

        return $this;
    }

    /**
     * Removes the media URL.
     *
     * @return $this
     */
    public function removeMediaURL()
    {
        $this->mediaURL = null;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getReasonsToRegisterMobile()
    {
        return $this->reasonsToRegisterMobile;
    }

    /**
     * @param $text
     * @return $this
     */
    public function setReasonsToRegisterMobile($text)
    {
        $this->reasonsToRegisterMobile = (string)$text;

        return $this;
    }

    /**
     * @param $text
     * @return $this
     */
    public function setFacebookName($text)
    {
        $this->facebookName = (string)$text;

        return $this;
    }

    /**
     * @param $text
     * @return $this
     */
    public function setTwitterName($text)
    {
        $this->twitterName = (string)$text;

        return $this;
    }

    public function getPages()
    {

        $pages = array();

        $page = Epr_Cms_Page::getPageWithPath(self::PAGE_PATH);
        if (is_null($page)) {
            $page = new Epr_Cms_Page(self::PAGE_PATH);
            $page->setTitle('about us');
            _dm()->persist($page);
        }

        $pages[] = $page;


        $error404Page = Epr_Cms_Page::getPageWithPath(self::PAGE_ERROR_404_PATH);
        if (is_null($error404Page)) {
            $error404Page = new Epr_Cms_Page(self::PAGE_ERROR_404_PATH);
            $error404Page->setTitle('404 error');
            _dm()->persist($error404Page);
        }

        $error404Content = $error404Page->getElementWithPath(self::ERROR_404_CONTENT);
        if (is_null($error404Content)) {
            $error404Content = new Epr_Cms_Element_HTML(self::ERROR_404_CONTENT);
            $error404Content->setTitle('error 404');
            $error404Content->setContent('You requested a page that <b>could not</b> be delivered. Probably, there is only a little misspelling in the URL you entered.');
            $error404Page->addElement($error404Content);
        }
        $pages[] = $error404Page;

        $imprintPage = Epr_Cms_Page::getPageWithPath(self::PAGE_IMPRINT);
        if (is_null($imprintPage)) {
            $imprintPage = new Epr_Cms_Page(self::PAGE_IMPRINT);
            $imprintPage->setTitle('imprint');
            _dm()->persist($imprintPage);
        }

        $imprintContent = $imprintPage->getElementWithPath(self::IMPRINT_CONTENT);
        if (is_null($imprintContent)) {
            $imprintContent = new Epr_Cms_Element_Imprint(self::IMPRINT_CONTENT);
            $imprintContent->setTitle('imprint');
            $imprintPage->addElement($imprintContent);
        }

        $pages[] = $imprintPage;

        $privacyPolicyPage = Epr_Cms_Page::getPageWithPath(self::PAGE_PRIVACY_POLICY);
        if (is_null($privacyPolicyPage)) {
            $privacyPolicyPage = new Epr_Cms_Page(self::PAGE_PRIVACY_POLICY);
            $privacyPolicyPage->setTitle('privacy policy');
            _dm()->persist($privacyPolicyPage);
        }

        $privacyPolicyContent = $privacyPolicyPage->getElementWithPath(self::PRIVACY_POLICY_CONTENT);
        if (is_null($privacyPolicyContent)) {
            $privacyPolicyContent = new Epr_Cms_Element_PrivacyPolicy(self::PRIVACY_POLICY_CONTENT);
            $privacyPolicyContent->setTitle('privacy policy');
            $privacyPolicyPage->addElement($privacyPolicyContent);
        }

        $pages[] = $privacyPolicyPage;

        $termsOfUsePage = Epr_Cms_Page::getPageWithPath(self::PAGE_TERMS_OF_USE);
        if (is_null($termsOfUsePage)) {
            $termsOfUsePage = new Epr_Cms_Page(self::PAGE_TERMS_OF_USE);
            $termsOfUsePage->setTitle('terms of use');
            _dm()->persist($termsOfUsePage);
        }

        $termsOfUseContent = $termsOfUsePage->getElementWithPath(self::TERMS_OF_USE_CONTENT);
        if (is_null($termsOfUseContent)) {
            $termsOfUseContent = new Epr_Cms_Element_TermsOfUse(self::TERMS_OF_USE_CONTENT);
            $termsOfUseContent->setTitle('terms of use');
            $termsOfUsePage->addElement($termsOfUseContent);
        }

        $pages[] = $termsOfUsePage;

        return $pages;
    }

}