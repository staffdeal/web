<?php
/** @Document */
class Epr_Modules_Events_DetailsElement extends Epr_Cms_Element {

    private $event;

    private $allowBooking = true;

    /**
     * @param Epr_Event $event
     * @return $this
     */
    public function setEvent(Epr_Event $event) {
        $this->event = $event;
        return $this;
    }

    /**
     * @return Epr_Event
     */
    public function getEvent() {
        return $this->event;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setAllowBooking($value) {
        $this->allowBooking = ($value == true);
        return $this;
    }

    public function getAllowBooking() {
        return $this->allowBooking;
    }

    public function renderedContent($titleLevel = 0) {
        $output = '';

        if ($this->getEvent()) {
            $output .= $this->renderedTitle($titleLevel, null, null, false, $this->getEvent()->getTitle() . ' (' . $this->getEvent()->getDate()->getFormatedString(_t('Y/m/d')) . ')');

            $output .= '<div class="content details event">';

            if ($this->getEvent()->getTitleImage()) {

                $image = $this->getEvent()->getTitleImage();

                $width = max(100, min($image->getWidth(), 300));
                $output .= '<img src="' . $image->getPublicIDBasedURL($width) . '" class="pull-left" style="' . $image->getCSSDimensionString() . '" />';
            }

            $output .= '<div class="media">';

            $output .= $this->getEvent()->getText();

            if ($this->getEvent()->getTitleImage()) {
                $output .= '<div class="clearfix"></div>';
            }

            if ($this->allowBooking && _user()) {

                $output .= '<div>';
                $booking = $this->getEvent()->getBookingForUser(_user());
                if (!$booking) {
                    $output .= '<a href="/events/' . $this->getEvent()->getSlug() . '/booking" class="btn btn-lg btn-primary">' . _t('Get in touch for bookings') . '</a>';
                } else {
                    $sentSubelement = $this->getElementWithPath(Epr_Modules_Events::DETAILS_BOOKING_SENT_PATH);
                    $subContent = $sentSubelement->renderedContent($titleLevel + 1);
                    if (strlen($subContent) > 0) {
                        $output .= '<div>' . $subContent . '</div>';
                    }
                }
                $output .= '</div>';
            }

            $output .= '</div>';
            $output .= '</div>';
        }

        return $output;
    }

}
