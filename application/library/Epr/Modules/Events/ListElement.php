<?php

/** @Document */
class Epr_Modules_Events_ListElement extends Epr_Cms_Element_List_Abstract {

    const CACHE_NAME = 'events_list_element';

    private $events = false;

    private $user;

    /** @Field(type="boolean") */
    protected $showImages = true;

    static public function getTypeString()
    {
        return 'Events List';
    }


    /**
     * @return Epr_Event[]
     */
    public function getEvents() {
        if (!$this->events) {
            $this->events = Epr_Events_Collection::getAPIEvents(); // we display the same events as the API does
        }
        return $this->events;
    }

    /**
     * Sets the user. If a valid user is set the deals list is filtered to the deals the user has saved.
     * @param $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        if ($user instanceof Epr_User || is_null($user)) {
            $this->user = $user;
        }

        return $this;
    }

    /**
     * Gets the user. If a valid user is set the deals list is filtered to the deals the user has saved.
     *
     * @return Epr_User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getShowImages() {
        return $this->showImages;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setShowImages($value) {
        $this->showImages = ($value == true);
        return $this;
    }

    public function renderedContent($titleLevel = 0)
    {
        $output = null;

        if ($this->getNumberOfItemsToDisplay() > 0) {
            $output = $this->renderedTitle($titleLevel);

            $events = $this->getEvents();
            $showFirstDifferent = ($this->getWidth() > 5);

            $output .= '<div class="media-list element events ' . (($this->isOnStartPage()) ? 'startPage' : '') . '">';

            for ($i = 0; $i < count($events); $i++) {
                if ($showFirstDifferent && $i == 0) {
                    $output .= $this->getFirstEventContent($events[0]);
                } else {
                    $output .= $this->getEventContent($events[$i], $titleLevel + 1);
                }
            }

            $output .= '</div>';

        }

        return $output;
    }

    private function getFirstEventContent(Epr_Event $event) {
        $output = '';

        $useStartLayout = ($this->isOnStartPage());

        $url = '/events/' . $event->getSlug();
        $output .= '<div class="media first-element link-to" data-link-to="' . $url . '"">';

        $imageTag = null;
        if ($this->getShowImages() && $event->getTitleImage()) {
            $image = $event->getTitleImage();
            if ($image) {

                if ($useStartLayout) {
                    if ($image->getWidth() < 555 || ($image->getWidth() < ($image->getHeight() * 0.8))) {
                        // do not use the startpage layout of the image to small or portrait
                        $useStartLayout = false;
                    }
                }


                $width = ($useStartLayout) ? 555 : (min(floor($this->getPixelWidth() / 2.0), $image->getWidth()));
                if ($this->isOnStartPage() && !$useStartLayout) {
                    $width = 170;
                }

                $height = $image->getHeightByWidth($width);


                if ($useStartLayout) {
                    $height = min(250, $height);
                } else if ($height > 300) {
                    $height = 300;
                    $width = $image->getWidthByHeight($height);
                }

                $isMini = ($width <= 170);
                $output .= '<img src="' . $image->getPublicURL($width, $height) . '" style="' . $image->getCSSDimensionString() . '" class="img-show-left ' . ($isMini ? 'mini' : '') . '"/>';
            }
        }

        $output .= '<h1 class="tint">' . $event->getTitle() . '</h1>';
        $output .= '<div class="info small">';
        $output .= $event->getDate()->getFormatedString(_t('Y/m/d'));
        $output .= '</div>';

        $output .= '<div class="clearfix"></div></div>';
        return $output;
    }

    private function getEventContent(Epr_Event $event, $titleLevel = 2) {
        $output = '';

        $url = '/events/' . $event->getSlug();
        $isSmallList = ($this->getWidth() <= 4);

        if ($isSmallList) {
            $output .= '<div class="media mini not-first-element hide-on-mobile link-to" data-link-to="' . $url . '">';
        } else {
            $output .= '<div class="media not-first-element hide-on-mobile link-to" data-link-to="' . $url . '">';
        }

        if ($this->getShowImages() && $event->getTitleImage()) {

            $imageWidth  = 180;
            $imageHeight = 120;
            $output .= '<img src="' . $event->getTitleImage()->getPublicIDBasedURL($imageWidth, $imageHeight) . '" style="width:' . $imageWidth . 'px; height:' . $imageHeight . 'px;">';
        }

        $output .= '<h' . $titleLevel . '>' . $event->getTitle() . '</h' . $titleLevel . '>';
        $output .= '<div class="info small">';
        $output .= '<span class="tint">' . $event->getDate()->getFormatedString(_t('Y/m/d')) . '</span>';
        $output .= '</div>';
        $output .= '</div>';

        return $output;
    }

}