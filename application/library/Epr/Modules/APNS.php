<?php

/** @Document */
class Epr_Modules_APNS extends Epr_Module_Abstract implements Epr_Module_Notification
{

	const FIELD_ACTIVE        = 'active';
	const FIELD_MODE          = 'mode';
	const FIELD_CERT_DEV      = 'cert_development';
	const FIELD_CERT_PRO      = 'cert_production';
	const FIELD_CERT_PASS_DEV = 'certPass_development';
	const FIELD_CERT_PASS_PRO = 'certPass_production';
	const FIELD_CERT_TEST     = 'testConnection';

	const MODE_DEVELOPMENT = 'development';
	const MODE_PRODUCTION  = 'production';

	const FILENAME_DEVELOPMENT = 'development.pem';
	const FILENAME_PRODUCTION  = 'production.pem';

	/** @Field(type="boolean") */
	private $active;

	/** @Field(type="string") */
	private $mode = self::MODE_DEVELOPMENT;

	/** @Field(type="string") */
	private $certificate;

	/** @Field(type="string") */
	private $certificatePassword;

	/** @Field(type="string") */
	private $developmentCertificate;

	/** @Field(type="string") */
	private $developmentCertificatePassword;

	/** @Field(type="boolean") */
	private $certificatesDidChange = false;

	static $certificatesPath = null;


	static public function title()
	{
		return 'APNS';
	}

	static public function description()
	{
		return 'This plugin handles notifications to Apple Push Notification Service.';
	}

	public static function getCertificatesPath()
	{
		if (is_null(self::$certificatesPath)) {
			$clientConfig = Zend_Registry::get('client');
			if (isset($clientConfig->push) && isset($clientConfig->push->apnsCertificatePath)) {

				$path = __($clientConfig->push->apnsCertificatePath);
				if ($path->length() > 0) {
					self::$certificatesPath = Epr_System::getEffectivePath($path);
				}
			}
		}

		return self::$certificatesPath;
	}

	public function startup()
	{
		if (parent::startup()) {

			$path = self::getCertificatesPath();
			if (!is_null($path)) {

				if (!file_exists($path)) {
					mkdir($path, 0770, true);
				}

				if (file_exists($path)) {

					if ($this->certificatesDidChange) {
						$this->writeCertificateFile($this->getMode());
						$this->certificatesDidChange = false;
					}

					return true;

				} else {
					return false;
				}

			} else {
				return false;
			}
		}
	}


	/**
	 * Return true if the plugin can be used.
	 *
	 * @return true
	 */
	public function isUsable()
	{
		$clientConfig = Zend_Registry::get('client');
		if (isset($clientConfig->push) && isset($clientConfig->push->enabled)) {
			$configEnabled = $clientConfig->push->enabled;
		}

		$usable = $this->isActive() && $clientConfig;

		if ($usable) {

			$ssl = openssl_x509_parse($this->getCertificate($this->getMode()));
			if ($ssl === false) {
				$usable = false;
			}
		}

		return $usable;
	}

	public function notify()
	{
		return false;
	}

	/**
	 * Sets the certificate.
	 *
	 * @param $certificate
	 *
	 * @return $this
	 */
	public function setCertificate($certificate, $mode = self::MODE_DEVELOPMENT)
	{

		if ($mode == self::MODE_DEVELOPMENT) {
			$this->developmentCertificate = $certificate;
			$this->certificatesDidChange = true;
		} else if ($mode == self::MODE_PRODUCTION) {
			$this->certificate = $certificate;
			$this->certificatesDidChange = true;
		}

		return $this;
	}

	/**
	 * Returns the certificate.
	 *
	 * @return string
	 */
	public function getCertificate($mode = self::MODE_DEVELOPMENT)
	{

		$certificate = null;

		if ($mode == self::MODE_DEVELOPMENT) {
			$certificate = $this->developmentCertificate;
		} else if ($mode == self::MODE_PRODUCTION) {
			$certificate = $this->certificate;
		}

		return $certificate;
	}

	public function getCertificateFilename($mode)
	{
		$path = self::getCertificatesPath();
		if (file_exists($path)) {
			$filename = $path . '/';

			if ($mode == self::MODE_DEVELOPMENT) {
				return $filename . self::FILENAME_DEVELOPMENT;
			} else if ($mode == self::MODE_PRODUCTION) {
				return $filename . self::FILENAME_PRODUCTION;
			}
		}

		return null;
	}

	private function writeCertificateFile($mode)
	{

		$certificateFilename = $this->getCertificateFilename($mode);

		if (file_exists(self::getCertificatesPath())) {
			$certificate = $this->getCertificate($mode);

			if (strlen($certificate) > 0) {
				file_put_contents($certificateFilename, $certificate);
			} else {
				if (file_exists($certificateFilename)) {
					unlink($certificateFilename);
				}
			}
		}

	}

	/**
	 * Sets the certificate's password.
	 *
	 * @param $certificatePassword
	 *
	 * @return $this
	 */
	public function setCertificatePassword($certificatePassword, $mode = self::MODE_DEVELOPMENT)
	{
		if ($mode == self::MODE_DEVELOPMENT) {
			$this->developmentCertificatePassword = $certificatePassword;
		} else if ($mode == self::MODE_PRODUCTION) {
			$this->certificatePassword = $certificatePassword;
		}

		return $this;
	}

	/**
	 * Returns the certificate's password.
	 *
	 * @return string
	 */
	public function getCertificatePassword($mode = self::MODE_DEVELOPMENT)
	{
		if ($mode == self::MODE_DEVELOPMENT) {
			return $this->developmentCertificatePassword;
		} else if ($mode == self::MODE_PRODUCTION) {
			return $this->certificatePassword;
		}

		return null;
	}

	/**
	 * Sets the active state.
	 *
	 * @param $active
	 *
	 * @return $this
	 */
	public function setActive($active)
	{
		$this->active = ($active == true);

		return $this;
	}

	/**
	 * Returns the active state.
	 *
	 * @return boolean
	 */
	public function getActive()
	{
		return $this->active;
	}

	/**
	 * Sets the mode.
	 *
	 * @param $mode
	 *
	 * @return $this
	 */
	public function setMode($mode)
	{
		if ($mode == self::MODE_PRODUCTION || $mode == self::MODE_DEVELOPMENT) {
			$this->mode = $mode;
		}

		return $this;
	}

	/**
	 * Returns the mode.
	 *
	 * @return string
	 */
	public function getMode()
	{
		return $this->mode;
	}

	public function getSettingsFormFields()
	{
		$fields = parent::getSettingsFormFields();

		$active = new Zend_Form_Element_Checkbox(self::FIELD_ACTIVE);
		$active->setLabel('Active');
		$active->setChecked($this->getActive());
		$active->setDescription('Set to active when you want to use push notifications to iOS devices.');
		$fields[] = $active;

		$mode = new Zend_Form_Element_Select(self::FIELD_MODE);
		$mode->setLabel('Mode');
		$mode->setMultiOptions(array(
									self::MODE_DEVELOPMENT => 'Development',
									self::MODE_PRODUCTION  => 'Production'
							   ));
		$mode->setValue($this->getMode());
		$mode->setDescription('Choose the mode. Production notification can only be received from production builds.');
		$fields[] = $mode;

		$cert1 = new Zend_Form_Element_Textarea(self::FIELD_CERT_DEV);
		$cert1->setLabel('Development Certificate (Copy & Paste)');
		$cert1->setAttrib('class', 'inp-form middle');

		$descParts = array(
			"Create the PEM file following these steps:<ol>",
			"<li>Generate a certificate according Apple's instructions.</li>",
			"<li>Import the downloaded certificate to your key chain.</li>",
			"<li>Export the certificate and the key (separately) and name the files cert.p12 and key.p12.</li>",
			"<li>Run <i>openssl pkcs12 -clcerts -nokeys -out cert.pem -in cert.p12</i>.</li>",
			"<li>Run <i>openssl pkcs12 -nocerts -out key.pem -in key.p12</i> and remember the password.</li>",
			"<li>Run <i>cat cert.pem key.pem > upload.pem</i></li>",
			"<li>Paste the content of upload.pem into the field.</li>",
			"</ol>"
		);

		$cert1->setDescription(implode('', $descParts));
		$cert1->setAttrib('inlineDescription',true);
		$cert1->setValue($this->getCertificate(self::MODE_DEVELOPMENT));
		$fields[] = $cert1;

		$certPass1 = new Zend_Form_Element_Text(self::FIELD_CERT_PASS_DEV);
		$certPass1->setLabel('Development Certificate Password');
		$certPass1->setAttrib('class', 'inp-form middle');
		$certPass1->setValue($this->getCertificatePassword(self::MODE_DEVELOPMENT));
		$certPass1->setDescription('The password used to protect the certificate.');
		$fields[] = $certPass1;

		$cert2 = new Zend_Form_Element_Textarea(self::FIELD_CERT_PRO);
		$cert2->setLabel('Production Certificate (Copy & Paste)');
		$cert2->setAttrib('class', 'inp-form middle');
		$cert2->setDescription(implode('', $descParts));
		$cert2->setAttrib('inlineDescription',true);
		$cert2->setValue($this->getCertificate(self::MODE_PRODUCTION));
		$fields[] = $cert2;

		$certPass2 = new Zend_Form_Element_Text(self::FIELD_CERT_PASS_PRO);
		$certPass2->setLabel('Production Certificate Password');
		$certPass2->setAttrib('class', 'inp-form middle');
		$certPass2->setValue($this->getCertificatePassword(self::MODE_PRODUCTION));
		$certPass2->setDescription('The password used to protect the certificate.');
		$fields[] = $certPass2;

		return $fields;
	}

	public function saveSettingsField($field)
	{
		$fieldName  = $field->getName();
		$fieldValue = $field->getValue();

		if ($fieldName == self::FIELD_ACTIVE) {
			$this->setActive($fieldValue);

			return true;
		} else if ($fieldName == self::FIELD_MODE) {
			$this->setMode($fieldValue);

			return true;
		} else if ($fieldName == self::FIELD_CERT_DEV) {
			if (strlen($fieldValue) > 0) {
				$ssl = openssl_x509_parse($fieldValue);
				if ($ssl === false) {
					return false;
				} else {
					$this->setCertificate($fieldValue, self::MODE_DEVELOPMENT);

					return true;
				}
			} else {
				$this->setCertificate(null, self::MODE_DEVELOPMENT);

				return true;
			}

		} else if ($fieldName == self::FIELD_CERT_PASS_DEV) {
			$this->setCertificatePassword($fieldValue, self::MODE_DEVELOPMENT);

			return true;
		} else if ($fieldName == self::FIELD_CERT_PRO) {
			if (strlen($fieldValue) > 0) {
				$ssl = openssl_x509_parse($fieldValue);
				if ($ssl === false) {
					return false;
				} else {
					$this->setCertificate($fieldValue, self::MODE_PRODUCTION);

					return true;
				}
			} else {
				$this->setCertificate($fieldValue, self::MODE_PRODUCTION);

				return true;
			}

		} else if ($fieldName == self::FIELD_CERT_PASS_PRO) {
			$this->setCertificatePassword($fieldValue, self::MODE_PRODUCTION);

			return true;
		} else {
			return parent::saveSettingsField($field);
		}
	}

}