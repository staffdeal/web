<?php

/** @Document */
class Epr_Modules_Deals extends Epr_Module_API_Abstract implements Epr_Module_API, Epr_Module_Cms, Epr_Module_Cron
{

    const PAGE_INDEX_PATH         = 'deals';
    const INDEX_SIDEBAR_LEFT_PATH = 'sidebar-left';
    const INDEX_LIST_PATH         = 'list';

    const PAGE_DETAILS_PATH    = 'show-deals';
    const DETAILS_PATH         = 'details';
    const DETAILS_ACTIONS_PATH = 'actions';

    const PAGE_PRINT_PATH = 'print-deal';
    const PRINT_CONTEXT   = 'content';

    public static function title()
    {
        return 'Deals';
    }

    public static function description()
    {
        return 'Manages deals.';
    }

    /* (non-PHPdoc)
     * @see Epr_Module_Builtin_Abstract::apiPath()
    */
    public function getAPIPath()
    {
        return 'deals';
    }

    /**
     * Runs the cron job of the module and returns the output.
     * @param DateInterval $intervalSinceLastRun
     *
     * @return string
     */
    public function runCron(DateInterval $intervalSinceLastRun)
    {
        $output = parent::runCron($intervalSinceLastRun);

        if ($intervalSinceLastRun->m >= 10) {
            // run only once every 10 minutes
            $output = $this->performCodesCheck(true);
        }

        return $output;
    }

    public function performCodesCheck($sendMail = true, $ignoreCurfew = false)
    {
        $allDeals          = Epr_Deal_Collection::getActive();
        $dealsWithWarnings = array();
        foreach ($allDeals as $deal) {
            if ($deal instanceof Epr_Deal) {
                if ($deal->isActive() && (\Poundation\PDate::createDate($deal->getStartDate())
                            ->isPast() && \Poundation\PDate::createDate($deal->getEndDate())->isFuture())
                ) {
                    // deal is active and currently running
                    $onlineModule = $deal->getOnlineCodeModule();
                    if ($onlineModule && $onlineModule instanceof Epr_Module_DealCode) {
                        $hasWarning = $onlineModule->hasWarning();
                        if ($hasWarning) {
                            $dealsWithWarnings[] = $deal;
                        }
                    }
                }
            }
            unset($deal);
        }

        $numberOfDealsWithWarning = count($dealsWithWarnings);
        $output                   = 'Found ' . $numberOfDealsWithWarning . ' deals with a warning.';

        if ($numberOfDealsWithWarning > 0) {
            $mailOutput = [];
            foreach ($dealsWithWarnings as $deal) {
                if ($deal instanceof Epr_Deal) {
                    $useDeal = false;

                    if ($ignoreCurfew) {
                        $useDeal = true;
                    } else {
                        $curfewDate = $deal->getAssociateForKey('codeCheckCurfew');
                        if (is_null($curfewDate)) {
                            $useDeal = true;
                        } else {
                            if (\Poundation\PDate::createDate($curfewDate)->addHours(1)->addSeconds(-5)->isPast()) {
                                $useDeal = true;
                            }
                        }
                    }

                    $dealInfo = '<br>- "' . $deal->getTitle() . '", ending ' . $deal->getEndDate()->format('Y/m/d');
                    $output .= $dealInfo;

                    if ($useDeal) {
                        $mailOutput[] = $dealInfo;
                        $deal->setAssociateForKey(new DateTime('now'), 'codeCheckCurfew');
                    } else {
                        $output .= ' (under curfew)';
                    }
                }
            }

            if ($sendMail && count($mailOutput) > 0) {
                _app()->sendAdminMail('Some of your deals have warnings', 'The check of your deal status found warning. This happens when your deals run low on codes. Please check them.<br><br>' . implode('<br>', $mailOutput));
            }

            return $output;
        }

        return null;
    }


    public function getPages()
    {
        $pages = array();

        $indexPage = Epr_Cms_Page::getPageWithPath(self::PAGE_INDEX_PATH);
        if (is_null($indexPage)) {
            $indexPage = new Epr_Cms_Page(self::PAGE_INDEX_PATH);
            _dm()->persist($indexPage);
        }

        $indexLeft = $indexPage->getElementWithPath(self::INDEX_SIDEBAR_LEFT_PATH);
        if (is_null($indexLeft)) {
            $indexLeft = new Epr_Cms_Element_HTML(self::INDEX_SIDEBAR_LEFT_PATH);
            $indexLeft->setWidth(3);
            $indexLeft->setOrder(0);
            $indexPage->addElement($indexLeft);
        }

        $indexList = $indexPage->getElementWithPath(self::INDEX_LIST_PATH);
        if (is_null($indexList)) {
            $indexList = new Epr_Modules_Deals_ListElement(self::INDEX_LIST_PATH);
            $indexList->setWidth(9);
            $indexList->setOrder(10);
            $indexList->setTitle('current deals');
            $indexList->setNumberOfItemsToDisplay(100);
            $indexPage->addElement($indexList);
        }

        $pages[] = $indexPage;

        $detailsPage = Epr_Cms_Page::getPageWithPath(self::PAGE_DETAILS_PATH);
        if (is_null($detailsPage)) {
            $detailsPage = new Epr_Cms_Page(self::PAGE_DETAILS_PATH);
            _dm()->persist($detailsPage);
        }

        $detailsActionsElement = $detailsPage->getElementWithPath(self::DETAILS_ACTIONS_PATH);
        if (is_null($detailsActionsElement)) {
            $detailsActionsElement = new Epr_Modules_Deals_Details_ActionElement(self::DETAILS_ACTIONS_PATH);
            $detailsActionsElement->setWidth(4);
            $detailsActionsElement->setOrder(0);
            $detailsActionsElement->setTitle('actions');
            $detailsPage->addElement($detailsActionsElement);
        }

        $detailsElement = $detailsPage->getElementWithPath(self::DETAILS_PATH);
        if (is_null($detailsElement)) {
            $detailsElement = new Epr_Modules_Deals_DetailsElement(self::DETAILS_PATH);
            $detailsElement->setWidth(8);
            $detailsElement->setOrder(10);
            $detailsPage->addElement($detailsElement);
        }

        $pages[] = $detailsPage;


        $printPage = Epr_Cms_Page::getPageWithPath(self::PAGE_PRINT_PATH);
        if (is_null($printPage)) {
            $printPage = new Epr_Cms_Page(self::PAGE_PRINT_PATH);
            $printPage->setTitle('Print the deal');
            _dm()->persist($printPage);
        }

        $printContent = $printPage->getElementWithPath(self::PRINT_CONTEXT);
        if (is_null($printContent)) {
            $printContent = new Epr_Cms_Element_HTML(self::PRINT_CONTEXT);
            $printContent->setTitle('Print the voucher');
            $printContent->setContent('Explain the user what to do with this nice little voucher.');
            $printPage->addElement($printContent);
        }
        $pages[] = $printPage;


        return $pages;
    }

    public function getTemplateData()
    {
        return array(
            array(
                'id' => 'dealDescription',
                'platform' => Epr_Module_API::PLATFORM_IOS,
                'title' => 'Deal Description Template iOS',
                'description' => 'Enter the template HTML for displaying the deal description (iOS).'
            ),
            array(
                'id' => 'dealDescription',
                'platform' => Epr_Module_API::PLATFORM_ANDROID,
                'title' => 'Deal Description Template Android',
                'description' => 'Enter the template HTML for displaying the deal description (Android).'
            ),
            array(
                'id' => 'shopDescription',
                'platform' => Epr_Module_API::PLATFORM_IOS,
                'title' => 'Shop Description Template iOS',
                'description' => 'Enter the template HTML for displaying the shop description (iOS).'
            ),
            array(
                'id' => 'shopDescription',
                'platform' => Epr_Module_API::PLATFORM_ANDROID,
                'title' => 'Shop Description Template Android',
                'description' => 'Enter the template HTML for displaying the shop description (Android).'
            )
        );
    }


}