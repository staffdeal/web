<?php
/**
 * @package Backend
 * @author daniel
 */

/** @Document */
class Epr_Modules_Appstores extends Epr_Module_Abstract implements Epr_Module
{

    const APPLE_APPSTORE_ID   = 'appleid';
    const APPLE_APPSTORE_NAME = 'applename';
    const APPLE_APPSTORE_VERSIONNUMBER = 'appleversionnumber';
    const GOOGLE_PLAYSTORE_ID = 'googleid';
    const GOOGLE_VERSIONNUMBER = 'googleversionnumber';
    const UPDATE_TEXT = 'updateText';

    /** @Field(type="string") */
    protected $appleId;

    /** @Field(type="string") */
    protected $appleName;

    /** @Field(type="integer") */
    protected $appleVersionNumber;

    /** @Field(type="string") */
    protected $googleId;

    /** @Field(type="integer") */
    protected $googleVersionNumber;

    /** @Field(type="string") */
    protected $updateText;

    static public function title()
    {
        return 'Appstores';
    }

    static public function description()
    {
        return 'This modules generated appstore links.';
    }

    /**
     * Sets the Apple appstore id.
     * @param $id
     * @return $this
     */
    public function setAppleId($id)
    {
        if (is_null($id) || is_string($id)) {
            $this->appleId = $id;
        }

        return $this;
    }

    /**
     * Returns the Apple appstore id.
     * @return string|null
     */
    public function getAppleId()
    {
        return $this->appleId;
    }

    /**
     * Sets the name of the app on the App Store.
     * @param $name
     * @return $this
     */
    public function setAppleName($name)
    {
        if (is_null($name) || is_string($name)) {
            $this->appleName = $name;
        }

        return $this;
    }

    /**
     * Returns the name of the app on the App Store.
     * @return null|string
     */
    public function getAppleName()
    {
        return $this->appleName;
    }

    /**
     * Sets the version number of the App Store App.
     * @param int $number
     * @return $this
     */
    public function setAppleVersionNumber($number) {

        if (is_integer($number) && $number > 0) {
            $this->appleVersionNumber = $number;
        }

        return $this;
    }

    /**
     * Returns the version number of the App Store App.
     * @return int
     */
    public function getAppleVersionNumber() {
        if (is_null($this->appleVersionNumber)) {
            $this->appleVersionNumber = 0;
        }
        return $this->appleVersionNumber;
    }


    /**
     * Sets the Google Play Store id.
     * @param $id
     * @return $this
     */
    public function setGoogleId($id)
    {
        if (is_null($id) || is_string($id)) {
            $this->googleId = $id;
        }

        return $this;
    }

    /**
     * Sets the Google Play Store App version number.
     * @param int $number
     * @return $this
     */
    public function setGoogleVersionNumber($number) {

        if (is_integer($number) && $number > 0) {
            $this->googleVersionNumber = $number;
        }

        return $this;
    }

    /**
     * Returns the Google Play Store App version number.
     * @return int
     */
    public function getGoogleVersionNumber() {
        if (is_null($this->googleVersionNumber)) {
            $this->googleVersionNumber = 0;
        }
        return $this->googleVersionNumber;
    }

    /**
     * Returns the Google Play Store id.
     * @return string|null
     */
    public function getGoogleId()
    {
        return $this->googleId;
    }

    /**
     * Sets the text which is displayed when the user needs to update the app.
     * @param string $text
     * @return $this
     */
    public function setUpdateText($text) {
        $this->updateText = (string)$text;
        return $this;
    }

    /**
     * Returns the text which is displayed when the user needs to update the app.
     * @return string
     */
    public function getUpdateText() {
        return $this->updateText;
    }

    public function getAppStoreURL()
    {
        $url = null;

        // https://developer.apple.com/library/ios/qa/qa1633/_index.html
        if (strlen($this->getAppleId()) > 1) {
            $name = \Poundation\PString::createFromString($this->getAppleName());
            $name = $name->trim();
            $name = $name->lowercase();

            $charactersToRemove = \Poundation\PCharacterSet::whitespaceCharacterSet()
                ->addCharactersFromString("!¡#$%'()*+,\-./:;<=>¿?@[\]^_`{|}~")
                ->addCharactersFromString("©™®")
                ->addCharacter('"');
            $name               = $name->replaceFromCharacterset($charactersToRemove, '');
            $name               = $name->replace('&', 'and');

            $id  = $this->getAppleId();
            $url = sprintf("https://itunes.apple.com/us/app/%s/id%s?l=de&ls=1&mt=8", $name, $id);
        }

        return $url;
    }

    public function getPlayStoreURL() {
        $url = null;

        if ($this->getGoogleId()) {
            $url = sprintf('http://play.google.com/store/apps/details?id=%s', $this->getGoogleId());
        }

        return $url;
    }

    public function getSettingsFormFields()
    {
        $fields = parent::getSettingsFormFields();

        $apple = new Zend_Form_Element_Text(self::APPLE_APPSTORE_ID);
        $apple->setLabel(_t('Apple Appstore ID'));
        $apple->setValue($this->getAppleId());
        $apple->setDescription(_t('Enter the id of the app in the appstore. This is NOT the app id.'));
        $apple->setAttrib('class', 'inp-form');
        $fields[] = $apple;

        $appleName = new Zend_Form_Element_Text(self::APPLE_APPSTORE_NAME);
        $appleName->setLabel(_t('Apple Appstore Name'));
        $appleName->setValue($this->getAppleName());
        $appleName->setDescription(_t('Enter the name of the app in the appstore. This is NOT the app id.'));
        $appleName->setAttrib('class', 'inp-form');
        $fields[] = $appleName;

        $appleVersion = new Zend_Form_Element_Text(self::APPLE_APPSTORE_VERSIONNUMBER);
        $appleVersion->setLabel(_t('Apple Appstore Version Number'));
        $appleVersion->setValue($this->getAppleVersionNumber());
        $appleVersion->setDescription(_t('Enter the version number of the currently available iOS app.'));
        $appleVersion->setAttrib('class', 'inp-form');
        $appleVersion->addValidator(new Zend_Validate_Between(array('min' => 1, 'max' => 100000)));
        $fields[] = $appleVersion;

        $google = new Zend_Form_Element_Text(self::GOOGLE_PLAYSTORE_ID);
        $google->setLabel(_t('Google Play Store ID'));
        $google->setValue($this->getGoogleId());
        $google->setDescription(_t('Enter the id of the app.'));
        $google->setAttrib('class', 'inp-form');
        $fields[] = $google;

        $googleVersion = new Zend_Form_Element_Text(self::GOOGLE_VERSIONNUMBER);
        $googleVersion->setLabel(_t('Google Play Store Version Number'));
        $googleVersion->setValue($this->getGoogleVersionNumber());
        $googleVersion->setDescription(_t('Enter the version number of the currently available Android app.'));
        $googleVersion->setAttrib('class', 'inp-form');
        $googleVersion->addValidator(new Zend_Validate_Between(array('min' => 1, 'max' => 100000)));
        $fields[] = $googleVersion;

        $updateText = new Zend_Form_Element_Textarea(self::UPDATE_TEXT);
        $updateText->setLabel(_t('Update text'));
        $updateText->setValue($this->getUpdateText());
        $updateText->setDescription(_t('Enter the text the user gets displayed when the app is outdated.'));
        $updateText->setAttrib('class', 'inp-form wide');
        $fields[] = $updateText;

        return $fields;
    }

    public function saveSettingsField($field)
    {

        $name  = $field->getName();
        $value = $field->getValue();

        if ($name == self::APPLE_APPSTORE_ID) {
            $this->setAppleId($value);

            return true;
        } else if ($name == self::APPLE_APPSTORE_NAME) {
            $this->setAppleName($value);

            return true;
        } else if ($name == self::GOOGLE_PLAYSTORE_ID) {
            $this->setGoogleId($value);

            return true;
        } else if ($name == self::APPLE_APPSTORE_VERSIONNUMBER) {
            $this->setAppleVersionNumber((int)$value);

            return true;
        } else if ($name == self::GOOGLE_VERSIONNUMBER) {
            $this->setGoogleVersionNumber((int)$value);

            return true;
        } else if ($name == self::UPDATE_TEXT) {
          $this->setUpdateText($value);

            return true;
        } else {
            return parent::saveSettingsField($field);
        }


    }

}
