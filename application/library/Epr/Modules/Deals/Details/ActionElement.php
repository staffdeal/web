<?php

/** @Document */
class Epr_Modules_Deals_Details_ActionElement extends Epr_Cms_Element
{

    private $saveForm;
    private $deal;

    static public function getTypeString()
    {
        return 'deals details actions';
    }

    public function setDeal(Epr_Dispatcher_Deal $deal)
    {
        $this->deal = $deal;

        return $this;
    }

    /**
     * @return Epr_Dispatcher_Deal
     */
    public function getDeal()
    {
        return $this->deal;
    }

    public function getSaveForm()
    {
        return $this->saveForm;
    }

    public function setSaveForm($form)
    {
        if ($form instanceof Zend_Form || is_null($form)) {
            $this->saveForm = $form;
        }

        return $this;
    }

    public function hasContentToDisplay()
    {
        return true;
    }

    public function renderedTitle($titleLevel = 1, $prepend = null, $append = null, $useBranding = true, $titleOverride = null)
    {
        $this->titleRendered = true;

        $classes = array('panel-title');
        if (outlineTranslation()) {
            $classes[] = 'translate';
        }

        $output = '';
        $output .= '<div class="panel-heading">';
        $output .= '<h' . $titleLevel . ' class="' . implode(' ', $classes) . '">' . $this->getTitle() . '</h' . $titleLevel . '>';
        $output .= '</div>';

        return $output;
    }

    public function renderedContent($titleLevel = 0)
    {
        $output = '';
        $modalBoxes = array();

        $output .= '<div class="panel">';

        $output .= $this->renderedTitle($titleLevel);
        $this->titleRendered = false;

        if ($this->getDeal()) {

            $dealers = Epr_Deal_Collection::getDealersForDeal($this->getDeal()->getReferenceDealId());
            if (count($dealers) > 0) {
                $output .= '<a href="/deals/dealers/' . $this->getDeal()
                        ->getReferenceDealId() . '" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-map-marker"></span>' . _t('find dealer') . '</a>';
            }

            $passes = $this->getDeal()->getPass();

            $buttons = [];
            $saveOnsiteButton = '<a href="#" id="save-deal-onsite" class="save-deal btn btn-lg btn-primary"><span class="glyphicon glyphicon-barcode"></span>' . _t('nail down this deal (on site)') . '</a>';
            $showVoucherButton = '<a href="/deals/print/' . $this->getDeal()
                    ->getReferenceDealId() . '" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-barcode"></span></span>' . _t('print voucher') . '</a>';



            if (_user()) {
                $numberOfSaves = (_user()->hasDealCode($this->getDeal(), Epr_Deal::TYPE_ONSITE)) + (_user()->hasDealCode($this->getDeal(), Epr_Deal::TYPE_ONLINE));

                if (isset($passes->onsite)) {
                    if (_user()->hasDealCode($this->getDeal(), Epr_Deal::TYPE_ONSITE)) {
                        $buttons[] = $showVoucherButton;
                    } else if (isset($passes->onsite)) {
                        $buttons[] = $saveOnsiteButton;
                    }
                }

                if (isset($passes->online)) {
                    if (_user()->hasDealCode($this->getDeal(), Epr_Deal::TYPE_ONLINE)) {

                        if (isset($passes->online->shopURL) && $passes->online->shopURL) {
                            $url = \Poundation\PString::createFromString($passes->online->shopURL);

                            $verifiedURL = \Poundation\PURL::URLWithString($url);
                            if ($verifiedURL->getMailAddress()) {
                                $buttons[] = '<a href="' . $verifiedURL . '?subject=' . rawurlencode($this->getDeal()->getTitle()) . '" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-globe"></span>' . _t('Redeem via mail') . '</a>';
                            } else if ($url->contains('__CODE__')) {
                                // the url has a code placeholder so we redirect the user directly
                                $url = $url->replace('__CODE__', _user()->getDealCode($this->getDeal(), Epr_Deal::TYPE_ONLINE));
                                $buttons[] = '<a href="' . $url . '" target="_blank" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-globe"></span>' . _t('Redeem online') . '</a>';
                            } else if (_user()->getDealCode($this->getDeal(), Epr_Deal::TYPE_ONLINE) == Epr_Deal::ONLINE_NOCODE) {
                                $buttons[] = '<a href="' . $url . '" target="_blank" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-globe"></span>' . _t('Redeem online') . '</a>';
                            } else {
                                // the url has no placeholder so we need to show it
                                $modalBox = new Epr_HTML_ModalBox();
                                $modalBox->setTitle(_t('Redeem online'));
                                $modalBox->setContent('<div class="center">' ._t('Use this code to purchase your deal. ') . '<div class="code">' . _user()->getDealCode($this->getDeal(), Epr_Deal::TYPE_ONLINE) . '</div></div>');
                                $modalBox->setFooter('<a href="' . $url . '" class="btn btn-lg btn-success" target="_blank"><span class="glyphicon glyphicon-shopping-cart"></span>' . _t('Open Shop') . '</a>');

                                $buttonId = 'btn_' . $modalBox->getId();
                                $modalBox->addLinkedElementSelector('#' . $buttonId);
                                $buttons[] = '<a id="' . $buttonId . '" href="#" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-globe"></span>' . _t('Redeem online') . '</a>';
                                $modalBoxes[] = $modalBox;
                            }
                        }
                        if (isset($passes->online->description)) {
                            $buttons[] = '<a href="#" class="deal-online-description btn btn-lg btn-success"><span class="glyphicon glyphicon-info-sign"></span>' . _t('How to redeem this deal online') . '</a>';
                        }


                    } else if (isset($passes->online->available)) {
                        if ($passes->online->available) {
                            $buttons[] = '<a href="#" id="save-deal-online" class="save-deal btn btn-lg btn-primary"><span class="glyphicon glyphicon-globe"></span>' . _t('nail down this deal (online)') . '</a>';
                        } else {
                            $buttons[] = '<div id="save-deal-online" class="save-deal btn btn-lg btn-primary btn-disabled"><span class="glyphicon glyphicon-globe"></span>' . _t('sold out') . '</div>';
                        }
                    }
                }


            }

            $output .= implode('<br />', $buttons);

            if (!_user()) {

                $output .= '<div class="alert alert-danger" style="text-align: center;">';
                $output .= _t('You need to be logged in to get that deal.');
                $output .= '<div style="margin-top: 20px;">';
                $output .= '<a href="/user/login" class="btn btn-lg btn-danger">';
                $output .= _t('Login') . ' / ' . _t('Registration');
                $output .= '</a>';
                $output .= '</div>';
                $output .= '</div>';

            }


        }


        $output .= '</div>';

        foreach($modalBoxes as $box) {
            if ($box instanceof Epr_HTML_ModalBox) {
                $output .= $box->renderedHTML();
            }
        }

        return $output;
    }

}