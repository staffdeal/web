<?php

/** @Document */
class Epr_Modules_Deals_DetailsElement extends Epr_Cms_Element
{

	private $deal;

	private $showEditLink;

    private $showVerbalDate = true;

	static public function getTypeString()
	{
		return "news article details";
	}

	/**
	 * @return Epr_Dispatcher_Deal
	 */
	public function getDeal()
	{
		return $this->deal;
	}

	/**
	 * @param $deal
	 *
	 * @return $this
	 */
	public function setDeal($deal)
	{
		if ($deal instanceof Epr_Dispatcher_Deal || is_null($deal)) {
			$this->deal = $deal;
		}

		return $this;
	}

	/**
	 * @param $showEditLink
	 *
	 * @return $this
	 */
	public function setShowEditLink($showEditLink) {
		$this->showEditLink = ($showEditLink == true);
		return $this;
	}

	/**
	 * @return bool
	 */
	public function getShowEditLink() {
		return $this->showEditLink;
	}

    /**
     * @param $flag
     * @return $this
     */
    public function setShowVerbalDate($flag) {
        $this->showVerbalDate = ($flag == true);
        return $this;
    }

    public function getShowVerbalDate() {
        return $this->showVerbalDate;
    }

	public function hasContentToDisplay()
	{
		return ($this->deal() instanceof Epr_Deal);
	}


	public function renderedContent($titleLevel = 0)
	{
		$output = '';

		if ($this->getDeal() instanceof Epr_Dispatcher_Deal) {

			$output .= $this->renderedTitle($titleLevel, null, null, false, $this->getDeal()->getTitle());

			$output .= '<div class="content details">';

			if ($this->getDeal()->getTitleImage()) {

				$image = $this->getDeal()->getTitleImage();

				$width = max(100, min($image->getWidth(), 300));
				$output .= '<img src="' . $image->getPublicIDBasedURL($width) . '" class="pull-left" style="' . $image->getCSSDimensionString() . '" />';
			}

			$output .= '<div class="media">';

			$output .= '<div class="info small tint">';
            if ($this->getShowVerbalDate()) {
                $range = $this->getDeal()->getEndDate()->getBalancedTimeRange();
                $output .= sprintf(_t('ends in %s'), Epr_System::getVerbalTimeRangeExpression($range, null, true));
            } else {
                $output .= $this->getDeal()->getStartDate()->getFormatedString(_t('Y/m/d')) . ' – ' . $this->getDeal()->getEndDate()->getFormatedString(_t('Y/m/d'));
            }
			$output .= '<span class="tint">&nbsp/&nbsp;</span>' . $this->getDeal()->getProvider();

			$output .= '</div>';

			$output .= $this->getDeal()->getText();

			if ($this->getDeal()->getTitleImage()) {
				$output .= '<div class="clearfix"></div>';
			}

            if ($this->getDeal()->getPrice()) {
                $output .= '<div class="price">';
                if ($this->getDeal()->getOldPrice()) {
                    $output .= '<span class="oldPrice">' . $this->getDeal()->getOldPrice() . '</span>';
                }
                $output .= $this->getDeal()->getPrice();
                $output .= '</div>';
            }

//			if ($this->getDeal()->getPublicUrl()) {
//				$output .= '<a href="' . $this->getNewsItem()->getPublicUrl() . '" class="btn btn-lg btn-primary">' . _t('view source') . '</a>';
//			}

//			if ($this->showEditLink && _user() && _user()->isAdministrator()) {
//				$output .= '<a href="' . $this->getDeal()->getId() . '" class="btn btn-lg btn-primary">' . _t('Edit') . '</a>';
//			}

			$output .= '</div>';
			$output .= '</div>';

		}

		return $output;
	}

}