<?php

/** @Document */
class Epr_Modules_Deals_ListElement extends Epr_Cms_Element_List_Abstract
{

    const CACHE_NAME = 'deals_list_element';

    private $deals = false;

    private $user;

    private $showVerbalDate = true;

    static public function getTypeString()
    {
        return 'Deals List';
    }

    /**
     * @param $flag
     * @return $this
     */
    public function setShowVerbalDate($flag)
    {
        $this->showVerbalDate = ($flag == true);

        return $this;
    }

    public function getShowVerbalDate()
    {
        return $this->showVerbalDate;
    }

    private function isFullWidth()
    {
        $width = $this->getWidth();

        return ($width > 6);
    }

    private function getDeals($limit = -1)
    {

        if ($this->deals === false) {

            if ($this->getUser()) {
                $this->deals = \Poundation\PArray::create($this->getUser()->getDeals());
            } else {
                $this->deals = \Poundation\PArray::create(Epr_Deal_Collection::getProvidedDeals($limit));
            }
        }

        return $this->deals;
    }

    /**
     * Sets the user. If a valid user is set the deals list is filtered to the deals the user has saved.
     * @param $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        if ($user instanceof Epr_User || is_null($user)) {
            $this->user = $user;
        }

        return $this;
    }

    /**
     * Gets the user. If a valid user is set the deals list is filtered to the deals the user has saved.
     *
     * @return Epr_User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    public function renderedContent($titleLevel = 0)
    {
        $output = null;

        if ($this->getNumberOfItemsToDisplay() > 0) {


            $output = $this->renderedTitle($titleLevel);

            $deals = $this->getDeals($this->getNumberOfItemsToDisplay());


            if ($deals->count() > 0) {

                $output .= '<div class="media-list element deals ' . (($this->isOnStartPage()) ? 'startPage' : '') . '">';

                $isFirst = (is_null($this->getUser())); // no special layout if there is a user

                $featuredDeals = [];
                $normalDeals   = [];
                foreach ($deals as $deal) {
                    if ($deal instanceof Epr_Dispatcher_Deal) {
                        if ($deal->getFeatured()) {
                            $featuredDeals[] = $deal;
                        } else {
                            $normalDeals[] = $deal;
                        }
                    }
                }

                $deals = array_merge($featuredDeals, $normalDeals);

                foreach ($deals as $deal) {

                    if ($deal instanceof Epr_Dispatcher_Deal) {

                        if ($isFirst) {
                            $output .= $this->firstDealContent($deal, $titleLevel + 1);
                            $isFirst = false;
                        } else {
                            $output .= $this->dealContent($deal, $titleLevel + 1);
                        }
                    }

                }
                $output .= '</div>';

                if ($this->isOnStartPage()) {
                    $dealsPage = Epr_Cms_Page::getPageWithPath(Epr_Modules_Deals::PAGE_INDEX_PATH);
                    if ($dealsPage instanceof Epr_Cms_Page) {
                        $output .= '<a href="/' . $dealsPage->getPath() . '" class="list-more btn btn-primary btn-lg">'. _t('More deals') . '</a>';
                    }
                }
            }

        }

        return $output;
    }

    private function firstDealContent(Epr_Dispatcher_Deal $deal, $titleLevel = 2)
    {
        $output = '';

        $url = '/deals/show/' . $deal->getReferenceDealId();

        $output .= '<div class="media first-element link-to" data-link-to="' . $url . '"">';

        $useStartLayout = ($this->isOnStartPage());

        $imageTag = null;
        if ($deal->getTitleImageURL()) {
            $image = $deal->getTitleImage();
            if ($image) {

                if ($useStartLayout) {
                    if ($image->getWidth() < 555 || ($image->getWidth() < ($image->getHeight() * 0.8))) {
                        // do not use the startpage layout of the image to small or portrait
                        $useStartLayout = false;
                    }
                }


                $width = ($useStartLayout) ? 555 : (min(floor($this->getPixelWidth() / 2.0), $image->getWidth()));
                if ($this->isOnStartPage() && !$useStartLayout) {
                    $width = 170;
                }

                $height = $image->getHeightByWidth($width);


                if ($useStartLayout) {
                    $height = min(250, $height);
                } else if ($height > 300) {
                    $height = 300;
                    $width = $image->getWidthByHeight($height);
                }

                $isMini = ($width <= 170);
                $imageTag = '<img src="' . $image->getPublicURL($width, $height) . '" style="' . $image->getCSSDimensionString() . '" class="' . ($isMini ? 'mini' : '') . '"/>';
            }
        }


        $savedMarker = '';
        if (_user() && _user()->isDealSaved($deal)) {
            $savedMarker = '<span class="glyphicon glyphicon-floppy-disk"></span>';
        }

        $dateTag = null;
        if ($this->getShowVerbalDate()) {
            $range = $deal->getEndDate()->getBalancedTimeRange();
            $dateTag = sprintf(_t('ends in %s'), Epr_System::getVerbalTimeRangeExpression($range, null, true));
        } else {
            $dateTag = sprintf(
                _t('available from %s to %s'), \Poundation\PDate::createDate($deal->getStartDate())
                    ->getFormatedString(_t('Y/m/d')), \Poundation\PDate::createDate($deal->getEndDate())
                    ->getFormatedString(_t('Y/m/d'))
            );
        }

        $infoTag = '<td class="info small">' . $savedMarker . $dateTag;
        if ($deal->isSoldOut() && _theme()->getSoldOutImage()) {
            $infoTag .= '<img class="soldout" src="' . _theme()->getSoldOutImage()->getPublicURL(100) . '" style="' . _theme()->getSoldOutImage()->getCSSDimensionString() . '" />';
        }
        $infoTag .= '</td>';

        $priceTag = null;
        if ($deal->getPrice()) {
            $priceTag = '';
            if ($deal->getOldPrice()) {
                $priceTag = '<span class="oldPrice">' . $deal->getOldPrice() . '</span>';
            }
            $priceTag .= $deal->getPrice();
        }

        $output .= '<table>';
        if ($useStartLayout) {

            if ($imageTag) {
                $output .= '<tr><td class="teaserImage">' . $imageTag . '</td></tr>';
            }
            $output .= $infoTag;

        } else {

            $output .= '<tr>';
            if ($imageTag) {
                $output .= '<td rowspan="4" class="teaserImage">' . $imageTag . '</td>';
            }
            $output .= $infoTag;

            $output .= '</tr>';
        }


        $output .= '<tr><td class="content"><h' . $titleLevel . ' class="tint">' . $deal->getTitle() . '</h' . $titleLevel . '>';
        $output .= '<div">' . $deal->getTeaser() . '</div>';
        $output .= '</td></tr>';

        if ($priceTag) {
            $output .= '<tr><td class="price">' . $priceTag . '</td></tr>';
        }

        $output .= '</table>';

        $output .= '</div>';
        return $output;
    }

    private function dealContent(Epr_Dispatcher_Deal $deal, $titleLevel = 2)
    {
        $output = '';

        $url = '/deals/show/' . $deal->getReferenceDealId();

        $output .= '<div class="media not-first-element hide-on-mobile link-to" data-link-to="' . $url . '">';

        if ($deal->getTitleImage()) {

            $imageWidth  = 180;
            $imageHeight = 120;
            $output .= '<img src="' . $deal->getTitleImage()
                    ->getPublicIDBasedURL($imageWidth, $imageHeight) . '" style="width:' . $imageWidth . 'px; height:' . $imageHeight . 'px;">';
        }

        $allowDetailsLink = true;

        $featuredMarker = '';
        if ($deal->getFeatured()) {
            $featuredMarker .= '<span class="glyphicon glyphicon-star"></span>';
        }
        $savedMarker = '';
        if (_user() && _user()->isDealSaved($deal)) {
            $savedMarker = '<span class="glyphicon glyphicon-floppy-disk"></span>';
        }

        $output .= '<div class="info small">';
        $output .= $featuredMarker;
        $output .= $savedMarker;
        $output .= '<span class="tint">';
        if ($this->getShowVerbalDate()) {
            $endDate = \Poundation\PDate::createDate($deal->getEndDate());
            $range   = $endDate->getBalancedTimeRange();
            if ($endDate->isFuture()) {
                $output .= sprintf(_t('ends in %s'), Epr_System::getVerbalTimeRangeExpression($range, null, true));
            } else {
                $output .= sprintf(_t('ended %s ago'), Epr_System::getVerbalTimeRangeExpression($range, null, true));
                $allowDetailsLink = false;
            }

        } else {
            $output .= sprintf(
                _t('available from %s to %s'), \Poundation\PDate::createDate($deal->getStartDate())
                    ->getFormatedString(_t('Y/m/d')), \Poundation\PDate::createDate($deal->getEndDate())
                    ->getFormatedString(_t('Y/m/d'))
            );
        }
        if ($deal->isSoldOut() && _theme()->getSoldOutImage()) {
            $output .= '<img class="soldout" src="' . _theme()->getSoldOutImage()->getPublicURL(100) . '" />';
        }
        $output .= '</span></div>';

        $output .= '<h' . $titleLevel . '>' . $deal->getTitle() . '</h' . $titleLevel . '>';

        if ($deal->getPrice()) {
            $output .= '<div class="price">';
            if ($deal->getOldPrice()) {
                $output .= '<span class="oldPrice">' . $deal->getOldPrice() . '</span>';
            }
            $output .= $deal->getPrice();
            $output .= '</div>';
        }


        $output .= '</div>';

        return $output;
    }
}