<?php

interface Epr_Notification {

	const STATUS_EDITING = 0;
	const STATUS_PENDING = 1;
	const STATUS_SENDING = 2;
	const STATUS_SENT = 3;
	const STATUS_FAILED = 4;

	const TYPE_EMAIL = 0;
	const TYPE_IOS_APN = 1;
	const TYPE_ANDROID_GCM = 2;

	const MAX_FAILURE_COUNT = 3;



	/**
	 * Returns the id of the notification.
	 * @return string
	 */
	public function getId();

	/**
	 * Returns true if all values are suitable for sending.
	 * @return boolean
	 */
	public function isValid();

	/**
	 * Returns the type of the notification.
	 * @return integer
	 */
	public function getType();

	/**
	 * Returns the name of the receiver (if any).
	 * @return string
	 */
	public function getReceiverName();

	/**
	 * Returns the
	 * @return string
	 */
	public function getContext();

	/**
	 * Sets the reason string used by clients to open the right screen.
	 * @param $reason
	 *
	 * @return $this
	 */
	public function setReason($reason);

	/**
	 * Gets the reason string used by clients to open the right screen.
	 * @return string
	 */
	public function getReason();

	/**
	 * Sets the name of the receiver.
	 * @param $name
	 *
	 * @return Epr_Notification
	 */
	public function setReceiverName($name);

	/**
	 * Returns the receiver's device id (if any).
	 * @return string
	 */
	public function getDeviceId();

	/**
	 * Returns the sender string.
	 * @return string
	 */
	public function getSender();

	/**
	 * Returns the sent date.
	 * @return Datetime
	 */
	public function getSentDate();

	/**
	 * Returns the creation date.
	 * @return Datetime
	 */
	public function getCreationDate();

	/**
	 * Returns the current state.
	 * @return integer
	 */
	public function getStatus();

	/**
	 * Returns the id which is used for transportation (if the transport system needs this).
	 * @return string
	 */
	public function getTransportId();

	/**
	 * Returns the message content.
	 * @return string
	 */
	public function getMessage();

	/**
	 * Ends editing (aka set the state to Epr_Notification::STATUS_PENDING).
	 * @return boolean
	 */
	public function endEditing();

	/**
	 * Sends the notification.
	 * @return boolean
	 */
	public function send();

	/**
	 * Returns the number of failed delivery attempts.
	 * @return integer
	 */
	public function getFailureCounter();

}