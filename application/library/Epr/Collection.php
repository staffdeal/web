<?php

class Epr_Collection extends \Doctrine\ODM\CouchDB\DocumentRepository {

    static function getSortKeys()
    {
        // override this
        return array();
    }

    /**
     * Returns an array of exclusion fields. Documents having the the given value are not emitted in the view.
     * Use it if you want to exclude all documents which match a specific field/value combination.
     *
     * @return null
     */
    static function getSortExclusions() {
        return null;
    }

    static function getSortDesignDocument($documentName) {
        $keys = static::getSortKeys();
        $exclusions = static::getSortExclusions();
        if (count($keys) > 0) {
            $document = new Epr_DB_SortViewGenerator($documentName, $keys, $exclusions);
            return $document;
        }
        return null;
    }

    public static function registerSortDesignDocument($documentName, $client) {
        if ($client instanceof \Doctrine\CouchDB\CouchDBClient) {
            $document = self::getSortDesignDocument($documentName);
            if ($document) {
                $client->createDesignDocument(static::SORT_DESIGN_DOCUMENT_NAME, $document);
            }
        }
    }

    static function getSearchNames() {
        return array();
    }

    public static function getSearchFieldsForName($name) {
        return array();
    }

    static function getSearchFiltersForName($name) {
        return array();
    }

    public static function registerSearchDesignDocument($documentName, $client) {
        // first, get all the names and fields
        $searchFields = array();

        foreach (array_unique(static::getSearchNames()) as $name) {
            if (strlen($name) > 0) {
                foreach(array_unique(static::getSearchFieldsForName($name)) as $field) {
                    if (strlen($field) > 0) {
                        $searchFields['search'][$name][] = $field;
                    }
                }
                foreach(array_unique(static::getSearchFiltersForName($name)) as $filterKey => $filter) {
                    if (strlen($filter) > 0) {
                        $searchFields['filter'][$name][$filterKey] = $filter;
                    }
                }
            }
        }

        if (count($searchFields) > 0) {
            $document = new Epr_DB_SearchViewGenerator($documentName, $searchFields['search'], $searchFields['filter'], static::getSortKeys());
            if ($document) {
                $client->createDesignDocument(static::SEARCH_DESIGN_DOCUMENT_NAME, $document);
            }
        }

    }

    /**
     * @param $key
     * @param int $direction
     * @param int $count
     * @param int $offset
     * @return Doctrine\CouchDB\View\Result
     */
    static function getSorted($key, $direction = SORT_ASC, $count = 10, $offset = 0)
    {
        $result = null;

        $keys = static::getSortKeys();
        if (array_search($key, $keys) !== false || array_key_exists($key, $keys)) {
            $viewName = getSortViewName($key);
            $query = _dm()->createQuery(static::SORT_DESIGN_DOCUMENT_NAME, $viewName);
            $query->onlyDocs(true);

            if ($direction == SORT_DESC) {
                $query->setDescending(true);
            }

            $query->setLimit((int)$count);
            $query->setSkip($offset);

            $result = $query->execute();

        } else {
            _logger()->error('Sort key "' . $key . '" is not registered as sortKey.');
        }

        return $result;



    }

    static function getMatching($searchName, $query, $sortKey = null, $sortDirection = SORT_ASC, $count = 10, $offset = 0, $analyzer = null) {
        $result = null;

        if (strlen($query) > 0) {

            $dbQuery = _dm()->createLuceneQuery(static::SEARCH_DESIGN_DOCUMENT_NAME, $searchName);
            $dbQuery->setQuery($query);
            $dbQuery->onlyDocs(true);
            $dbQuery->setStale(true);
            $dbQuery->setLimit($count);
            $dbQuery->setSkip($offset);

            if (is_string($sortKey) && strlen($sortKey) > 0) {
                $sortString = ($sortDirection == SORT_ASC) ? '/' : '\\';
                $sortString.= 'sort_' . $sortKey;
                $dbQuery->setSort($sortString);
            }

            $dbAnalyzer = Zend_Registry::get('luceneAnalyzerName');
            if (is_string($analyzer) && strlen($analyzer) > 0) {
                $dbAnalyzer = $analyzer;
            }
            $dbQuery->setAnalyzer($dbAnalyzer);

            try {
                $result = $dbQuery->execute();
            } catch (Exception $e) {
                _logger()->error($e->getMessage());
            }

        }

        return $result;
    }

}