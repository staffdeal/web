<?php

/**
 * meinERP
 * User: Jan Fanslau
 * Date: 07.09.12
 * Time: 21:35
 */
class Epr_Form_Deal extends Epr_Form
{

    static function dealForm($deal = null, $categories = null, $step = null)
    {

        $form = new self();

        $title = new Zend_Form_Element_Text('title');
        $title->setLabel('Title');
        $title->setRequired(true);
        $title->setAttrib('class', 'inp-form wide');
        $title->setDescription('The title of the deal. It is used as a headline.');
        $form->addElement($title);

        $text = new Zend_Form_Element_Textarea('text');
        $text->setRequired(true);
        $text->setAttrib('class', 'wysiwyg wide');
        $text->setLabel('Description');
        $text->setDescription('The description of the deal. It used to explain the details and technical features of the product.');
        $form->addElement($text);

        $teaser = new Zend_Form_Element_Textarea('teaser');
        $teaser->setLabel('Teaser');
        $teaser->setAttrib('class', 'inp-form wide');
        $teaser->setRequired(false);
        $teaser->setDescription('The teaser of the deal. Write two or three sentences about to product. This text is an eyecatcher which is used to attract the user\'s attention.');
        $form->addElement($teaser);

        $dateRange = new Epr_Form_Element_DateRange('period');
        $dateRange->setLabel('Availability period');
        $dateRange->setRequired(true);
        $dateRange->setAttrib('class', 'inp-form');
        $dateRange->setDescription('The start and end date of the deal. The deal is only available to the user within this range of dates.');
        $form->addElement($dateRange);

        $titleImage = new Epr_Form_Element_Image_Selector('titleImage');
        $titleImage->setLabel('Title Image');
        $titleImage->setDescription('The main image of the product. It is used in lists and details views. You may specify more images in the description text.');
        $form->addElement($titleImage);

        $price = new Zend_Form_Element_Text('price');
        $price->setLabel('Price');
        $price->setRequired(false);
        $price->setAttrib('class', 'inp-form narrow');
        $price->setDescription('The price of the deal.');
        $form->addElement($price);

        $oldPrice = new Zend_Form_Element_Text('oldPrice');
        $oldPrice->setLabel('Old Price');
        $oldPrice->setRequired(false);
        $oldPrice->setAttrib('class', 'inp-form narrow');
        $oldPrice->setDescription('The old price of the deal. It used displayed in a striked font.');
        $form->addElement($oldPrice);

        $isFeatured = new Zend_Form_Element_Checkbox('isFeatured');
        $isFeatured->setLabel('Featured');
        $isFeatured->setDescription('Activate this checkbox, if the deal should be displayed in a more prominent way.');
        $form->addElement($isFeatured);

        $reminder = new Zend_Form_Element_Textarea('reminder');
        $reminder->setLabel('Reminder');
        $reminder->setAttrib('class', 'inp-form wide');
        $reminder->setRequired(false);
        $reminder->setDescription('The reminder text of the deal. If the text field is empty, the user will not get a reminder message.');
        $form->addElement($reminder);

        $isActive = new Zend_Form_Element_Checkbox('isActive');
        $isActive->setChecked(true);
        $isActive->setLabel('Active');
        $isActive->setDescription('Only active deals are available.');
        $form->addElement($isActive);

        $isSoldOut = new Zend_Form_Element_Checkbox('isSoldOut');
        $isSoldOut->setChecked(false);
        $isSoldOut->setLabel('Sold out [Override]');
        $isSoldOut->setDescription('Enable if you want to mark this deal as sold out.');
        $form->addElement($isSoldOut);

        /**

        For #368, this is disabled.

        $dealersModule = _app()->getModuleWithIdentifier(Epr_Modules_Dealers::identifier());
        if ($dealersModule instanceof Epr_Modules_Dealers) {

            $i = 0;
            foreach ($dealersModule->getCategories() as $category => $fields) {

                $name = 'addCat' . $i;

                $categorySelect = new Zend_Form_Element_MultiCheckbox($name);
                $categorySelect->setLabel($category);
                $categorySelect->setMultiOptions($fields);
                $categorySelect->setDescription('Choose none, one or more of the dealer specific categories. It is used to deliver the right choice of dealers.');

                if ($deal instanceof Epr_Deal) {
                    $categorySelect->setValue($deal->selectedFieldIndexesInCategory($i));
                }

                $form->addElement($categorySelect);

                $i++;
            }
        }
         * */

        /** @var $deal Epr_Deal */
        if ($deal instanceof Epr_Deal) {
            $title->setValue($deal->getTitle());
            $text->setValue($deal->getText());
            $teaser->setValue($deal->getTeaser());
            $titleImage->setValue($deal->getTitleImage());
            $isFeatured->setValue($deal->isFeatured());
            $isActive->setValue($deal->isActive());
            $isSoldOut->setValue($deal->isManuallySoldOut());
            $price->setValue($deal->getPrice());
            $oldPrice->setValue($deal->getOldPrice());
            $reminder->setValue($deal->getReminderText());

            $period = \Poundation\PDateRange::createRange($deal->getEndDate(), $deal->getStartDate());
            $dateRange->setValue($period);
        } else {
            // if the form has no deal, we could not access the code modules
            $deal = new Epr_Deal();
        }

        $onsiteModule = $deal->getOnsiteCodeModule();
        if ($onsiteModule) {

            $formFields = $onsiteModule->getSettingsFormFields();
            $form->addElements($formFields);

        }

        $onlineModule = $deal->getOnlineCodeModule();
        if ($onlineModule) {

            $formFields = $onlineModule->getSettingsFormFields();
            $form->addElements($formFields);

        }


        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('');
        $submit->setValue('Submit');
        $submit->setAttrib('class', 'form-submit');
        $form->addElement($submit);

        return $form;
    }
}
