<?php

class Epr_Form_Element_Select extends Zend_Form_Element_Select {

	public function __construct($spec, $options = null) {
		parent::__construct($spec, $options);

		$this->setUsesTranslations(false);
	}

	public function setUsesTranslations($value) {
		$this->_translatorDisabled = !($value);
	}

	public function usesTranslations() {
		return !($this->_translatorDisabled);
	}


}