<?php

class Epr_Form_Element_URL extends Zend_Form_Element_Text
{

    private $validator = null;

    public function __construct($spec, $options = null)
    {
        parent::__construct($spec, $options);

        $this->validator = new Epr_Validate_URL();
        $this->addValidator($this->validator);
    }

    public function isValid($value, $context = null)
    {

        $valid = parent::isValid($value, $context);
        if ($valid) {

            $valid = false;

            if (strlen($value) > 0) {
                // we only need to check the validity of the string if there is one
                // parent already did any requirement checks
                $valid = \Poundation\PURL::isValidURLString($value);
                if ($valid) {
                    $this->setValue(\Poundation\PURL::URLWithString($value));
                }
            } else if (!$this->isRequired()) {
                $valid = true;
            }
        }

        return $valid;
    }

    public function getValue()
    {
        $value = parent::getValue();
        if (is_string($value) && strlen($value) == 0) {
            return null;
        }

        return $value;
    }

    public function setAllowEmailURLs($value) {
        $this->validator->setAllowEmailURLs($value);
        return $this;
    }

    public function getAllowEmailURLs() {
        return $this->validator->getAllowEmailURLs();
    }

}
