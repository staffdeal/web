<?php

class Epr_Form_Element_DateRange extends Zend_Form_Element_Xhtml
{

    public $helper = 'dateRangeHelper';

    public $options = array();

    public function isValid($value, $context = null)
    {
        $parsedValue = json_decode($value);
        if (!isset($parsedValue->start) && !isset($parsedValue->end)) {
            $value = null;
        }

        $isValid = parent::isValid($value, $context);

        if ($isValid) {

            $isValid = false;


            if ($parsedValue) {

                $startDate = null;
                $endDate = null;

                if (isset($parsedValue->start)) {
                    $startDate = new \Poundation\PDate($parsedValue->start);
                }
                if (isset($parsedValue->end)) {
                    $endDate = new \Poundation\PDate($parsedValue->end);
                }

                if ($startDate && $endDate) {

                    $timezone = date_default_timezone_get();

                    $startDate->adjustTimezone($timezone);
                    $endDate->adjustTimezone($timezone);

                    if (!$startDate->isBefore($endDate)) {
                        $this->_messages[] = 'Whoop, without a time maschine this cannot end before it has started.';
                    } else {

                        $range = \Poundation\PDateRange::createRange($startDate, $endDate);
                        $this->setValue($range);

                        $isValid = true;
                    }
                } else {
                    if ($startDate && !$endDate) {
                        $this->_messages[] = 'You need to select an end date.';
                    } else if (!$startDate && $endDate) {
                        $this->_messages[] = 'You need to select an start date.';
                    }
                }
            } else {
                if (!$this->isRequired()) {
                    $isValid = true;
                }
            }

        }

        return $isValid;
    }
}
