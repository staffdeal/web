<?php

class Epr_Form_Element_Image_Selector extends Zend_Form_Element_Xhtml {

    const SMALL_IMAGES = 'small';
    const BIG_IMAGES = 'big';

	static $defaultOptions = array();

    public $helper = 'selectorHelper';

    public $options = array();

	static public function addDefaultPreview($caption,$width, $height, $radius = 0) {

		if (is_string($caption) && is_integer($width) && is_integer($height) && is_integer($radius)) {
			if (strlen($caption) > 0 && $width > 0 && $height > 0 && $radius >= 0) {
				self::$defaultOptions['devicePreview'][] = array(
					'width' => $width,
					'height' => $height,
					'radius' => $radius,
					'caption' => $caption
				);
			}
		}
	}

    public function init() {

		$this->options = array_merge($this->options, self::$defaultOptions);

        $this->setEnableUpload(true);
        $this->setSize(200,150);
        $this->setThumbnailSize(self::SMALL_IMAGES);
        $this->setMultipleSelectionEnabled(false);
        $this->setShowThumbnail(true);
		$this->setAutoResizing(false);
        $this->setContext(Epr_Content_Image::ACCESS_GENERAL);
    }

    public function isValid($value, $context = null) {
        $result = parent::isValid($value, $context);
        if ($result) {

            $result = true;

            $valueArray = json_decode($this->_value);

            if ($this->getMultipleSelectionEnabled()) {
                $this->setValue(array());
            } else {
                $this->setValue(null);
            }

            if (is_array($valueArray)) {

                if (count($valueArray) > 0) {
                    $end = ($this->getMultipleSelectionEnabled()) ? count($valueArray) - 1 : 0;
                    for ($i = 0; $i <= $end; $i++) {

                        $imageID = $valueArray[$i];
                        $image = Epr_Content_Image_Collection::imageWithId($imageID);
                        if ($image instanceof Epr_Content_Image) {

                            if ($this->getMultipleSelectionEnabled()) {
                                $this->_value[] = $image;
                            } else {
                                $this->setValue($image);
                            }
                        } else {
                            $result = false;
                        }
                    }
                }

            }
        }
        return $result;
    }

    /**
     * Enables or disabled the image upload.
     * @param $value
     * @return Epr_Form_Element_Image_Selector
     */
    public function setEnableUpload($value) {
        $this->options['enableUploads'] = ($value == true);
        return $this;
    }

    public function getEnableUpload() {
        return $this->options['enableUploads'];
    }

    /**
     * Sets the size of the thumbnails. Valid values are definied as constants.
     * @param $size
     * @return Epr_Form_Element_Image_Selector
     */
    public function setThumbnailSize($size) {
        $this->options['size'] = $size;
        return $this;
    }

    public function getThumbnailSize() {
        return $this->options['size'];
    }

    /**
     * Sets the size of the preview image.
     * @param $width
     * @param $height
     * @return Epr_Form_Element_Image_Selector
     */
    public function setSize($width, $height) {
        $this->options['previewSize'] = array(
            'width' => $width,
            'height'=> $height
        );
        return $this;
    }

    /**
     * Enables or disables multiple selections.
     * @param $value
     * @return Epr_Form_Element_Image_Selector
     */
    public function setMultipleSelectionEnabled($value) {
        $this->options['multiple'] = ($value == true);
        return $this;
    }

    public function getMultipleSelectionEnabled() {
        return $this->options['multiple'];
    }

    /**
     * Enables or disables a thumbnail in the form. This only works when multiple selection is disabled.
     * @param $value
     * @return Epr_Form_Element_Image_Selector
     */
    public function setShowThumbnail($value) {
        $this->options['showThumbnail'] = ($value == true);
        return $this;
    }

    public function getShowThumbnail() {
        return $this->options['showThumbnail'];
    }

    /**
     * Sets the image context.
     * @param $context
     * @return Epr_Form_Element_Image_Selector
     */
    public function setContext($context) {
        $this->options['context'] = $context;
        return $this;
    }

    /**
     * Returns the image context.
     * @return integer
     */
    public function getContext() {
        return $this->options['context'];
    }

	/**
	 * Adds a device preview to the image selector field.
	 * @param $width
	 * @param $height
	 * @param $radius
	 *
	 * @return $this
	 */
	public function addDevicePreview($caption, $width, $height, $radius = 0) {

		if (is_string($caption) && is_integer($width) && is_integer($height) && is_integer($radius)) {
			if (strlen($caption) && $width > 0 && $height > 0 && $radius >= 0) {
				$this->options['devicePreview'][] = array(
					'width' => $width,
					'height' => $height,
					'radius' => $radius,
					'caption' => $caption
				);
			}
		}

		return $this;
	}

	/**
	 * Removes all registered device previews.
	 * @return $this
	 */
	public function clearDevicePreviews() {
		$this->options['devicePreview'] = array();
		return $this;
	}

	/**
	 * Returns true if the auto resizing flag has been set.
	 * @return bool
	 */
	public function getAutoResizing() {
		return (isset($this->options['autoResizing'])) ? $this->options['autoResizing'] : false;
	}

	/**
	 * Sets the auto resizing flag. If set to yes, the element resizes automatically to the size of the selected image.
	 * @param $value
	 *
	 * @return $this
	 */
	public function setAutoResizing($value) {
		$this->options['autoResizing'] = ($value == true);
		return $this;
	}
}