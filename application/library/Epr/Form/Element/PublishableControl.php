<?php

class Epr_Form_Element_PublishableControl extends Zend_Form_Element_Xhtml
{

    const OPTION_LAST_PUBLISH_DATE = 'lastPublishDate';
    const OPTION_LAST_UNPUBLISH_DATE = 'lastUnpublishDate';
    const OPTION_LAST_STATE_CHANGE_REASON = 'lastStateChangeReason';
    const OPTION_ALLOW_PUBLISHDATE_CHANGE = 'allowPublishDateChange';

    public $helper = 'publishableControlHelper';

    public $options = array();


    public function init()
    {
        parent::init();

        $this->setLabel(_t('Saving & Publish state'));
        $this->setDescription(_t('Set the publish state of the document. Clicking on one of the buttons saves this document and returns to the list.'));
        $this->setState(Epr_Document_Publishable::STATE_DRAFT);
        $this->setLastStateChangeReason(Epr_Document_Publishable::STATE_CHANGE_REASON_MANUAL);
    }

    /**
     * @param $state
     * @return $this
     */
    public function setState($state)
    {
        if ($state == Epr_Document_Publishable::STATE_PUBLISHED || $state == Epr_Document_Publishable::STATE_DRAFT) {
            $this->_value = $state;
        }
        return $this;
    }

    public function getState()
    {
        $state = $this->_value;
        return ($state == Epr_Document_Publishable::STATE_PUBLISHED) ? Epr_Document_Publishable::STATE_PUBLISHED : Epr_Document_Publishable::STATE_DRAFT;
    }

    /**
     * @param $date
     * @return $this
     */
    public function setPublishDate($date)
    {
        if ($date instanceof \Poundation\PDate || is_null($date)) {
            $this->options[self::OPTION_LAST_PUBLISH_DATE] = $date;
        }
        return $this;
    }

    /**
     * @return \Poundation\PDate||null
     */
    public function getPublishDate()
    {
        return (isset($this->options[self::OPTION_LAST_PUBLISH_DATE])) ? $this->options[self::OPTION_LAST_PUBLISH_DATE] : null;
    }

    /**
     * @param $date
     * @return $this
     */
    public function setLastUnpublishDate($date)
    {
        if ($date instanceof \Poundation\PDate || is_null($date)) {
            $this->options[self::OPTION_LAST_UNPUBLISH_DATE] = $date;
        }
        return $this;
    }

    /**
     * @return \Poundation\PDate||null
     */
    public function getLastUnpublishDate()
    {
        return (isset($this->options[self::OPTION_LAST_UNPUBLISH_DATE])) ? $this->options[self::OPTION_LAST_UNPUBLISH_DATE] : null;
    }

    /**
     * @param $reason
     * @return $this
     */
    public function setLastStateChangeReason($reason)
    {
        if ($reason == Epr_Document_Publishable::STATE_CHANGE_REASON_MANUAL || $reason == Epr_Document_Publishable::STATE_CHANGE_REASON_AUTOMATIC) {
            $this->options[self::OPTION_LAST_STATE_CHANGE_REASON] = $reason;
        }
        return $this;
    }

    public function setAllowPublishDateChange($value)
    {
        $this->options[self::OPTION_ALLOW_PUBLISHDATE_CHANGE] = ($value == true);
        return $this;
    }

    public function getAllowPublishDateChange()
    {
        return (isset($this->options[self::OPTION_ALLOW_PUBLISHDATE_CHANGE])) ? ($this->options[self::OPTION_ALLOW_PUBLISHDATE_CHANGE] == true) : false;
    }

    public function getLastStateChangeReason()
    {
        return (isset($this->options[self::OPTION_LAST_STATE_CHANGE_REASON])) ? $this->options[self::OPTION_LAST_STATE_CHANGE_REASON] : Epr_Document_Publishable::STATE_CHANGE_REASON_MANUAL;
    }

    public function isDraft()
    {
        return ($this->getState() == Epr_Document_Publishable::STATE_DRAFT);
    }

    public function isPublished()
    {
        return ($this->getState() == Epr_Document_Publishable::STATE_PUBLISHED);
    }

    public function isValid($value, $context = null)
    {

        $isValid = parent::isValid($value, $context);

        if ($isValid) {

            $isValid = false; // we default to NOT valid and explicitely check for the valid cases

            $publishDate = null;
            if ($value == Epr_Document_Publishable::STATE_DRAFT || $value == Epr_Document_Publishable::STATE_PUBLISHED || $value == Epr_View_Helper_PublishableControlHelper::PUBLISH_LATER_VALUE) {
                $isValid = true;
            }

            if ($isValid) {

                $isLater = false;

                if ($value == Epr_Document_Publishable::STATE_PUBLISHED) {
                    if (is_null($this->getPublishDate()) || $this->getPublishDate()->isFuture()) {
                        $publishDate = \Poundation\PDate::now()->addSeconds(-1);
                    }
                } else if ($value == Epr_View_Helper_PublishableControlHelper::PUBLISH_LATER_VALUE) {
                    $isLater = true;
                    $this->_value = Epr_Document_Publishable::STATE_PUBLISHED;
                    $value = $this->_value;
                }


                if (is_null($publishDate)) {
                    $dateName = $this->getName() . '_date';
                    $timeName = $this->getName() . '_time';

                    $dateValue = $context[$dateName];
                    $timeValue = $context[$timeName];

                    if (strlen($dateValue) == 10) {
                        if (strlen($timeValue) != 5) {
                            $timeValue = '00:00';
                        }
                        $publishDate = new \Poundation\PDate($dateValue . ' ' . $timeValue);
                    }
                }

                if (is_null($publishDate) && $isLater) {
                    $publishDate = \Poundation\PDate::now()->addSeconds(-1);
                }


                if ($publishDate) {
                    $this->setPublishDate($publishDate);
                }
            }
        }

        return $isValid;
    }
}