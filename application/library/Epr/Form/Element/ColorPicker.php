<?php

class Epr_Form_Element_ColorPicker extends Zend_Form_Element_Xhtml {

	const PICKER_STYLE_HUE = 'hue';
	const PICKER_STYLE_BRIGHTNESS = 'brightness';
	const PICKER_STYLE_SATURATION = 'saturation';
	const PICKER_STYLE_WHEEL = 'wheel';

	const OPTION_STYLE = 'style';

	public $helper = 'colorPickerHelper';
	public $options = null;

	public function init() {

		if (is_null($this->options)) {
			$this->options = array();
			parent::init();

			$this->setPickerStyle(self::PICKER_STYLE_HUE);

		}
	}

	public function isValid($value, $context = null) {

		$isValid = parent::isValid($value, $context);
		if ($isValid) {

			$isValid = false;

			$parsedColor = \Poundation\PColor::colorFromString($value);
			if ($parsedColor instanceof \Poundation\PColor) {
				$this->_value = $parsedColor;
				$isValid = true;
			}

		}

		return $isValid;
	}

	/**
	 * Sets the style of the picker (possible values are defined as constants, PICKER_STYLE_HUE …).
	 * @param string $style
	 *
	 * @return $this
	 */
	public function setPickerStyle($style) {
		$this->options[self::OPTION_STYLE] = $style;
		return $this;
	}

	/**
	 * Returns the picker style.
	 * @return string
	 */
	public function getPickerStyle() {
		return (isset($this->options[self::OPTION_STYLE])) ? $this->options[self::OPTION_STYLE] : self::PICKER_STYLE_HUE;
	}



}