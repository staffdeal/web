<?php

/**
 * The slug element performs an additional validation where the slug is checked and adjusted if needed.
 * You need to set the document id to perform the validation. When failOnAdjustment is set to true, the element will
 * fail validation if the slug was adjusted.
 */
class Epr_Form_Element_Slug extends Zend_Form_Element_Text
{

	/** @var string */
	private $documentId;

	/** @var string */
	private $slugBaseFieldName;

	/** @var bool */
	private $adjustSlug = true;
	/** @var bool */
	private $failOnAdjustment = true;
	/** @var bool */
	private $slugAdjusted = false;

	/**
	 * Sets the associated document id.
	 *
	 * @param $id
	 *
	 * @return Epr_Form_Element_Slug
	 */
	public function setDocumentId($id)
	{
		if (is_string($id)) {
			$this->documentId = $id;
		}

		return $this;
	}

	/**
	 * Returns the associated document id.
	 *
	 * @return string
	 */
	public function getDocumentId()
	{
		return $this->documentId;
	}

	/**
	 * Sets the name of the form element which is used to create slugs.
	 *
	 * @param $name
	 */
	public function setBaseElementName($name)
	{
		if (is_string($name)) {
			$this->slugBaseFieldName = $name;
		}
	}

	/**
	 * Returns the name of the slug base element.
	 *
	 * @return string
	 */
	public function getBaseElementName()
	{
		return $this->slugBaseFieldName;
	}

	/**
	 * Defines if the slug should be adjusted to a valid (unique) one.
	 *
	 * @param $adjust
	 *
	 * @return Epr_Form_Element_Slug
	 */
	public function setAdjustSlug($adjust)
	{
		$this->adjustSlug = ($adjust == true);

		return $this;
	}

	/**
	 * Returns true if the slug gets adjusted to a valid (unique)  one.
	 *
	 * @return bool
	 */
	public function getAdjustSlug()
	{
		return $this->adjustSlug;
	}

	/**
	 * Defines if the element should fail when the slug was adjusted.
	 *
	 * @param $fail
	 *
	 * @return Epr_Form_Element_Slug
	 */
	public function setFailOnAdjustment($fail)
	{
		$this->failOnAdjustment = ($fail == true);

		return $this;
	}

	/**
	 * Returns true if the element fails validation if the slug was adjusted.
	 *
	 * @return bool
	 */
	public function getFailOnAdjustment()
	{
		return $this->failOnAdjustment;
	}

	/**
	 * Returns true if the slug actually has been adjusted.
	 *
	 * @return bool
	 */
	public function slugAdjusted()
	{
		return $this->slugAdjusted;
	}

	public function isValid($value, $context = null)
	{
		$valid = parent::isValid($value, $context);

		if ($valid) {

			$documentIdToUse = (is_null($this->documentId)) ? 'newDocument' : $this->documentId;

			if (is_string($value)) {

				$loop = true;

				$baseValue = $value;
				if (strlen($baseValue) == 0) {
					$baseElement = $this->getBaseElementName();
					if ($baseElement && isset($context[$baseElement])) {
						$baseValue = $context[$baseElement];
					}
				}

				if (strlen($baseValue) == 0) {
					// we cannot auto-create a slug so we fail and break
					$valid = false;
					$loop  = false;
				}

				// convert umlauts
				$baseValue = (string)__($baseValue)
					->replace('ä', 'ae')
					->replace('ö', 'oe')
					->replace('ü', 'ue')
					->replace('Ä', 'Ae')
					->replace('Ü', 'Ue')
					->replace('Ö', 'Oe');

				// helper variables to create unique slug, used after first check
				$i      = 0;
				$suffix = '';
				while ($loop) {

					$slug = __($baseValue)->appendString($suffix)->slug();

					$query  = _dm()->createQuery('helpers', 'slugs')->setKey((string)$slug);
					$result = $query->execute();

					if (count($result) == 0) {
						// there is no slug so we consider the value to be valid

						$this->setValue((string)$slug);

						// this breaks the loop
						$loop = false;

					} else if (count($result) == 1) {
						// there is already the same slug

						$existingSlug = $result[0];
						if ($existingSlug['id'] == $documentIdToUse) {
							// it exists but it is our document so we are fine
							$this->setValue((string)$slug);
							$loop = false;
						} else {
							// the existing slug belongs to another document so we need to handle the conflict
							if ($this->getAdjustSlug()) {
								// we are supposed to adjust it

								// first, increase the pr
								$suffix = '-' . ++$i;
								// mark it as adjusted
								$this->slugAdjusted = true;

								if ($this->getFailOnAdjustment()) {
									// we fail on adjustment
									$valid = false;
								}

							} else {
								// the slug is taken and we are NOT supposed to adjust it
								// so we invalidate and break here

								$valid = false;
								$loop  = false;

								$this->_messages[] = 'The slug is already in use.';
							}
						}
					} else {
						// there is more than one slug, pretty bad
						throw new Exception('More than one slug "' . $slug . '" found.');
					}
				}
			}
		}

		if (!$valid && $this->getFailOnAdjustment() && $this->slugAdjusted()) {
			$this->_messages[] = 'The slug has been adjusted to be unique. Save again to accept the new slug.';
		}

		return $valid;
	}

}
