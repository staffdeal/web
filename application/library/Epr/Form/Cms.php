<?php

class Epr_Form_Cms extends Epr_Form
{

    const THEME_NAME = 'fr_name';
    const THEME_TITLE = 'fr_title';
    const THEME_TINT_COLOR = 'fr_tintColor';
    const THEME_DARK_AREA_COLOR = 'fr_darkAreaColor';
    const THEME_LIGHT_AREA_COLOR = 'fr_lightAreaColor';
    const THEME_BACKGROUND_COLOR = 'fr_backgroundColor';
    const THEME_LOGO = 'fr_logo';
    const THEME_LOW_CONTRAST_LOGO = 'fr_low_contrast_logo';
    const THEME_FAVICON = 'fr_favicon';
    const THEME_APPLE_TOUCH_ICON = 'fr_apple_touch';
    const THEME_APPLE_TOUCH_ICON_72 = 'fr_apple_touch_72';
    const THEME_APPLE_TOUCH_ICON_114 = 'fr_apple_touch_114';
    const THEME_APPLE_TOUCH_ICON_144 = 'fr_apple_touch_144';
    const THEME_SOULD_OUT_IMAGE = 'fr_soldout';
    const THEME_HTML_HEADER = 'fr_htmlHeader';
    const THEME_CSS = 'fr_css';
    const THEME_FOOTER = 'fr_footer';

    static function themeForm(Epr_Theme $theme)
    {

        $form = new self();

        $name = new Zend_Form_Element_Text(self::THEME_NAME);
        $name->setLabel('Name');
        $name->setRequired(true);
        $name->setAttrib('class', 'inp-form narrow');
        $name->setDescription('The name of the theme. It used to identify it in selections and settings.');
        $name->setValue($theme->getName());
        $form->addElement($name);

        $title = new Zend_Form_Element_Text(self::THEME_TITLE);
        $title->setLabel('Title');
        $title->setRequired(true);
        $title->setAttrib('class', 'inp-form narrow');
        $title->setDescription(
            'The title of the application. Choose a text you want the user to associate with the platform.'
        );
        $title->setValue($theme->getTitle());
        $form->addElement($title);

        $tintColor = new Epr_Form_Element_ColorPicker(self::THEME_TINT_COLOR);
        $tintColor->setLabel('Tint Color');
        $tintColor->setRequired(true);
        $tintColor->setDescription(
            'Choose the tint color. It is used for nearly every colorization like headlines, emphasises and links.'
        );
        $tintColor->setValue($theme->getTintColor());
        $form->addElement($tintColor);

        $darkAreaColor = new Epr_Form_Element_ColorPicker(self::THEME_DARK_AREA_COLOR);
        $darkAreaColor->setLabel('Dark Area Color');
        $darkAreaColor->setRequired(true);
        $darkAreaColor->setDescription('Choose the dark area color. It is used for highlights.');
        $darkAreaColor->setValue($theme->getDarkAreaColor());
        $form->addElement($darkAreaColor);

        $lightAreaColor = new Epr_Form_Element_ColorPicker(self::THEME_LIGHT_AREA_COLOR);
        $lightAreaColor->setLabel('Light Area Color');
        $lightAreaColor->setRequired(true);
        $lightAreaColor->setDescription('Choose the light area color. It is used for highlights.');
        $lightAreaColor->setValue($theme->getLightAreaColor());
        $form->addElement($lightAreaColor);

        $backgroundColor = new Epr_Form_Element_ColorPicker(self::THEME_BACKGROUND_COLOR);
        $backgroundColor->setLabel('Background Color');
        $backgroundColor->setRequired(true);
        $backgroundColor->setDescription('Choose the background color.');
        $backgroundColor->setValue($theme->getBackgroundColor());
        $form->addElement($backgroundColor);

        $logo = new Epr_Form_Element_Image_Selector(self::THEME_LOGO);
        $logo->setLabel('Logo Image');
        $logo->setDescription(
            'Upload and select the main logo image of the platform. It should be in landscape format with at least 30 pixels of height.'
        );
        $logo->setValue($theme->getLogoImage());
        $logo->clearDevicePreviews();
        $logo->setAutoResizing(true);
        $logo->setContext(Epr_Content_Image::ACCESS_THEME);
        $form->addElement($logo);

        $lowContrastLogo = new Epr_Form_Element_Image_Selector(self::THEME_LOW_CONTRAST_LOGO);
        $lowContrastLogo->setLabel('Low Contrast Logo Image');
        $lowContrastLogo->setDescription(
            'Upload and select the low contrast logo image of the platform. It should be a black&white image with low contrast and transparency.'
        );
        $lowContrastLogo->setValue($theme->getLowContrastLogoImage());
        $lowContrastLogo->clearDevicePreviews();
        $lowContrastLogo->setAutoResizing(true);
        $lowContrastLogo->setContext(Epr_Content_Image::ACCESS_THEME);
        $form->addElement($lowContrastLogo);

        $favicon = new Epr_Form_Element_Image_Selector(self::THEME_FAVICON);
        $favicon->setLabel('Favicon Image');
        $favicon->setDescription('Upload and select the favicon image. It should be 16x16 pixels of size.');
        $favicon->setValue($theme->getFaviconImage());
        $favicon->clearDevicePreviews();
        $favicon->setSize(16, 16);
        $favicon->setContext(Epr_Content_Image::ACCESS_THEME);
        $form->addElement($favicon);

        $appleTouch = new Epr_Form_Element_Image_Selector(self::THEME_APPLE_TOUCH_ICON);
        $appleTouch->setLabel('Apple Touch Image (57x57)');
        $appleTouch->setDescription('Upload and select the apple touch image. It should be 57x57 pixels of size.');
        $appleTouch->setValue($theme->getAppleTouchIcon());
        $appleTouch->clearDevicePreviews();
        $appleTouch->setSize(57, 57);
        $appleTouch->setContext(Epr_Content_Image::ACCESS_THEME);
        $form->addElement($appleTouch);

        $appleTouch72 = new Epr_Form_Element_Image_Selector(self::THEME_APPLE_TOUCH_ICON_72);
        $appleTouch72->setLabel('Apple Touch Image (72x72)');
        $appleTouch72->setDescription('Upload and select the apple touch image. It should be 72x72 pixels of size.');
        $appleTouch72->setValue($theme->getAppleTouchIcon72());
        $appleTouch72->clearDevicePreviews();
        $appleTouch72->setSize(72,72);
        $appleTouch72->setContext(Epr_Content_Image::ACCESS_THEME);
        $form->addElement($appleTouch72);

        $appleTouch114 = new Epr_Form_Element_Image_Selector(self::THEME_APPLE_TOUCH_ICON_114);
        $appleTouch114->setLabel('Apple Touch Retina Image (114x114)');
        $appleTouch114->setDescription('Upload and select the apple touch image. It should be 114x114 pixels of size.');
        $appleTouch114->setValue($theme->getAppleTouchIcon114());
        $appleTouch114->clearDevicePreviews();
        $appleTouch114->setSize(57, 57);
        $appleTouch114->setContext(Epr_Content_Image::ACCESS_THEME);
        $form->addElement($appleTouch114);

        $appleTouch144 = new Epr_Form_Element_Image_Selector(self::THEME_APPLE_TOUCH_ICON_144);
        $appleTouch144->setLabel('Apple Touch Retina Image (144x144)');
        $appleTouch144->setDescription('Upload and select the apple touch image. It should be 144x144 pixels of size.');
        $appleTouch144->setValue($theme->getAppleTouchIcon144());
        $appleTouch144->clearDevicePreviews();
        $appleTouch144->setSize(72, 72);
        $appleTouch144->setContext(Epr_Content_Image::ACCESS_THEME);
        $form->addElement($appleTouch144);

        $soldOut = new Epr_Form_Element_Image_Selector(self::THEME_SOULD_OUT_IMAGE);
        $soldOut->setLabel('Sold out image');
        $soldOut->setDescription('Upload and select the sold out image. It is use for deals. It should be extreme landscapeish.');
        $soldOut->setValue($theme->getSoldOutImage());
        $soldOut->clearDevicePreviews();
        $soldOut->setSize(250, 84);
        $soldOut->setContext(Epr_Content_Image::ACCESS_THEME);
        $form->addElement($soldOut);

        $footer = new Zend_Form_Element_Text(self::THEME_FOOTER);
        $footer->setLabel('Footer');
        $footer->setRequired(false);
        $footer->setAttrib('class', 'inp-form wide');
        $footer->setDescription('The text of the footer. Allowed variables: %YEAR%, %COPY%');
        $footer->setValue($theme->getFooterText());
        $form->addElement($footer);

        $htmlHeader = new Zend_Form_Element_Textarea(self::THEME_HTML_HEADER);
        $htmlHeader->setRequired(false);
        $htmlHeader->setLabel('Custom HTML Header');
        $htmlHeader->setDescription(
            'You can add your own HTML header tags e.g. SEO tags or special language information.'
        );
        $htmlHeader->setAttrib('class', 'inp-form wide allowTab');
        $htmlHeader->setValue($theme->getCustomHTMLHeader());
        $form->addElement($htmlHeader);

        $css = new Zend_Form_Element_Textarea(self::THEME_CSS);
        $css->setRequired(false);
        $css->setLabel('Custom CSS');
        $css->setDescription('You can add your own CSS definitions which are applied after the build-ins.');
        $css->setAttrib('class', 'inp-form wide allowTab');
        $css->setValue($theme->getCustomCSS());
        $form->addElement($css);

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('');
        $submit->setValue('Save');
        $submit->setAttrib('class', 'form-submit');
        $form->addElement($submit);

        return $form;

    }

}