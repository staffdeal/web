<?php

class Epr_Form_ContentImage extends Epr_Form
{

    const FIELD_NAME = 'name';

    /** @var Epr_Content_Image */
    private $image = null;

    public function __construct($image)
    {
        if (is_null($image) || $image instanceof Epr_Content_Image) {
            $this->image = $image;
        }

        parent::__construct();
    }

    public function init($options = null)
    {
        parent::init();

        $name = new Zend_Form_Element_Text(self::FIELD_NAME);
        $name->setLabel('Name');
        $name->setRequired(true);
        $name->setAttrib('class', 'wide inp-form');
        $name->setDescription('The filename of the asset.');
        if ($this->image) {
            $name->setValue($this->image->getName());
        }
        $this->addElement($name);

        if ($this->image) {
            $url = new Zend_Form_Element_Text('url'); // using magic name since we never read it
            $url->setLabel('Public URL');
            $url->setAttrib('class', 'wide inp-form');
            $url->setAttrib('disabled', 'disabled');
            $url->setDescription('The public URL of the asset.');
            $url->setValue($this->image->getPublicURL());
            $this->addElement($url);

            $width = new Zend_Form_Element_Text('width');
            $width->setLabel('Width');
            $width->setAttrib('class', 'narrow inp-form');
            $width->setAttrib('disabled', 'disabled');
            $width->setDescription('The width of the image.');
            $width->setValue($this->image->getWidth());
            $this->addElement($width);

            $height = new Zend_Form_Element_Text('height');
            $height->setLabel('Height');
            $height->setAttrib('class', 'narrow inp-form');
            $height->setAttrib('disabled', 'disabled');
            $height->setDescription('The height of the image.');
            $height->setValue($this->image->getHeight());
            $this->addElement($height);

        }

        $focalPointX = new Zend_Form_Element_Hidden('focalX');
        if ($this->image) {
            $focalPointX->setValue($this->image->getFocalXPercentage());
        }
        $this->addElement($focalPointX);

        $focalPointY = new Zend_Form_Element_Hidden('focalY');
        if ($this->image) {
            $focalPointY->setValue($this->image->getFocalYPercentage());
        }
        $this->addElement($focalPointY);


        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('');
        $submit->setValue('Submit');
        $submit->setAttrib('class', 'form-submit');
        $this->addElement($submit);

    }

    public function isValid($data)
    {

        $data['url'] = $this->getElement('url')->getValue();
        $data['width'] = $this->getElement('width')->getValue();
        $data['height'] = $this->getElement('height')->getValue();

        $isValid = parent::isValid($data);
        if ($isValid && $this->image) {

            $filename = $this->getFilename();
            $mime = \Poundation\PMIME::getTypeForFilename($filename);
            if ($mime != $this->image->getMIME()->getMIMEType()) {

                $isValid = false;
                if (strpos($filename, '.') !== false) {
                    $fileInfo = pathinfo($filename);
                    $filename = basename($filename, '.' . $fileInfo[PATHINFO_EXTENSION]);
                }
                $filename .= '.' . $this->image->getMIME()->getExtension();
                $this->setFilename($filename);
                $this->getElement(self::FIELD_NAME)->addError('The filename has been adjusted to match the type of the asset. Check and save again.');

            }
        }

        return $isValid;
    }


    public function setFilename($value) {
        $this->getElement(self::FIELD_NAME)->setValue($value);
    }

    public function getFilename() {
        return $this->getValue(self::FIELD_NAME);
    }


}