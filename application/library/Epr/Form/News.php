<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 07.09.12
 * Time: 21:35
 */
class Epr_Form_News extends Epr_Form
{
	const NEWS_TITLE = 'title';
	const NEWS_SLUG = 'slug';
	const NEWS_TEXT = 'text';
	const NEWS_CATEGORY = 'category';
	const NEWS_TITLE_IMAGE = 'titleImage';
	const NEWS_PUBLIC_URL = 'publicURL';
	const NEWS_SOURCE = 'source';
	const NEWS_PUBLISH_DATE = 'publishDate';
	const NEWS_PUBLISH_TIME = 'publishTime';
	const NEWS_ACTIVE = 'active';
	const NEWS_NOTIFY = 'notify';

	const FEED_TITLE = 'title';
	const FEED_URL = 'url';
	const FEED_ACTIVE = 'active';
	const FEED_CATEGORY = 'category';

    public function getCategoriesSelectArray($categories = null)
    {

        if (is_null($categories)) {
            throw new Exception('No Categories set');
        }

        $selectArray = array();
        $selectArray['0'] = 'No category';
        foreach ($categories as $category) {
            $selectArray[$category->getId()] = $category->getTitle();
        }

        return $selectArray;
    }

    static function newsForm($news = null, $categories = null){

        $form = new self($news);
        $form->setEnctype('multipart/form-data');

        $title = new Zend_Form_Element_Text(self::NEWS_TITLE);
        $title->setLabel('Title');
        $title->setRequired(true);
        $title->setAttrib('class','inp-form wide');
		$title->setDescription('The title will be truncated on mobile devices when the it contains more than 60 characters.');
        $form->addElement($title);

        $slug = new Epr_Form_Element_Slug(self::NEWS_SLUG);
        $slug->setLabel('Slug');
        $slug->setAttrib('class','inp-form narrow');
        $slug->setBaseElementName('title');
		$slug->setDescription('The slug of the news. It is used to create descriptive URLs on the web page. When no value is given, it is computed from the title.');
        $form->addElement($slug);

        $text = new Zend_Form_Element_Textarea(self::NEWS_TEXT);
        $text->setAttrib('class', 'wysiwyg wide');
        $text->setLabel('Text');
		$text->setDescription('The content of the news. You can write as much text as you want and include links, images and any other HTML element.');
        $form->addElement($text);

        $category = new Zend_Form_Element_Select(self::NEWS_CATEGORY);
        $category->setMultiOptions($form->getCategoriesSelectArray($categories));
        $category->setAttrib('class','styledselect_form_1');
        $category->setLabel('Category');
		$category->setDescription('The news category. The news inherits the color of the category.');
        $form->addElement($category);

        $titleImage = new Epr_Form_Element_Image_Selector(self::NEWS_TITLE_IMAGE);
        $titleImage->setLabel('Title Image');
		$titleImage->setDescription('The main image of the news. You can use more images within the text.');
        $form->addElement($titleImage);

        $publicUrl = new Epr_Form_Element_URL(self::NEWS_PUBLIC_URL);
        $publicUrl->setLabel('Public URL');
        $publicUrl->setRequired(false);
        $publicUrl->setAttrib('class','inp-form wide');
		$publicUrl->setDescription('The public URL of the source. If it is set, the user can share the link via social networks.');
        $form->addElement($publicUrl);

		$source = new Zend_Form_Element_Text(self::NEWS_SOURCE);
		$source->setLabel('Source');
		$source->setRequired(false);
		$source->setAttrib('class', 'inp-form wide');
		$source->setDescription('The name of the source. You should always specify it when the news is not directly from you.');
		$form->addElement($source);

        /** @var $news Epr_News */
        if($news instanceof Epr_News && $news->getId()){
            $title->setValue($news->getTitle());

            $slug->setValue($news->getSlug());
            $slug->setDocumentId($news->getId());

            $publicUrl->setValue($news->getPublicUrl());
            $category->setValue($news->getCategory()->getId());
            $text->setValue($news->getText());
            $titleImage->setValue($news->getTitleImage());
			$source->setValue($news->getSource());
        }

        return $form;
    }


    static function getCategoryForm($category = null, $categories = null) {

        $form = new self();

        $name = new Zend_Form_Element_Text('name');
        $name->setLabel('Name');
        $name->setRequired(true);
        $name->setAttrib('class','inp-form');
		$name->setDescription('The name of the news category.');
        $form->addElement($name);

        $slug = new Epr_Form_Element_Slug('slug');
        $slug->setLabel('Slug');
        $slug->setAttrib('class','inp-form');
        $slug->setBaseElementName('name');
		$slug->setDescription('The slug of the news category. It is used to create descriptive URLs on the web page. When no value is given, it is computed from the title.');
        $form->addElement($slug);

        $color = new Epr_Form_Element_ColorPicker('color');
        $color->setLabel('Color');
        $color->setRequired(true);
		$color->setDescription('The color of the category. Every news in this category uses the color for distinction.');
        $form->addElement($color);

        $parentCategory = new Zend_Form_Element_Select('parentCategory');
        $parentCategory->setMultiOptions($form->getCategoriesSelectArray($categories));
        $parentCategory->setLabel('Parent Category');
        $parentCategory->setAttrib('class','styledselect_form_1');
		$parentCategory->setDescription('Specify a parent category if you want this category to be a sub-category of another one.');
        $form->addElement($parentCategory);

        $isActive = new Zend_Form_Element_Checkbox('isActive');
        $isActive->setLabel('Is Active');
        $isActive->setChecked(true);
		$isActive->setDescription('Only news in active categories are displayed to the user.');
        $form->addElement($isActive);

		$isSticky = new Zend_Form_Element_Checkbox('isSticky');
		$isSticky->setLabel('Is Sticky');
		$isSticky->setChecked(false);
		$isSticky->setDescription('Sticky categories can not be unchecked/unselected.');
		$form->addElement($isSticky);

        /** @var $category Epr_News_Category */
        if($category instanceof Epr_News_Category){
            $name->setValue($category->getTitle());
            $color->setValue($category->getColor());

            $slug->setValue($category->getSlug());
            $slug->setDocumentId($category->getId());
			$isSticky->setValue($category->isSticky());
            $isActive->setValue($category->isActive());
            if ($category->getCategory() instanceof Epr_News_Category) {
                $parentCategory->setValue($category->getCategory()->getId());
            }
        }

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('');
        $submit->setValue('Save');
        $submit->setAttrib('class','form-submit');
        $form->addElement($submit);

        return $form;
    }

	static public function feedForm($feed = null, $categories = null) {

		$form = new self();

		$url = new Epr_Form_Element_URL(self::FEED_URL);
		$url->setLabel('URL');
		$url->setAttrib('class','inp-form middle');
		$url->setRequired(true);
		$url->setDescription('The feed URL which must deliver a valid feed XML document. Validity is checked on saving.');
		$form->addElement($url);

		$title = new Zend_Form_Element_Text(self::FEED_TITLE);
		$title->setLabel('Titel');
		$title->setAttrib('class', 'inp-form middle');
		$title->setDescription('This text is used to identify the feed. It is displayed as \'source\' in all clients. If you do not provide a title the title is read from the feed.');
		$form->addElement($title);

		$category = new Zend_Form_Element_Select(self::FEED_CATEGORY);
		$category->setMultiOptions($form->getCategoriesSelectArray($categories));
		$category->setAttrib('class','styledselect_form_1');
		$category->setLabel('Category');
		$category->setDescription('Choose the category which news items from this feed get assigned to.');
		$form->addElement($category);

		$active = new Zend_Form_Element_Checkbox(self::FEED_ACTIVE);
		$active->setLabel('Active');
		$active->setChecked(true);
		$active->setDescription('Disabled feeds are not automatically imported.');
		$form->addElement($active);

		if ($feed instanceof Epr_Feed) {
			$title->setValue($feed->getTitle());
			$url->setValue($feed->getUrl());
			$active->setChecked($feed->isActive());
			if ($feed->getCategory()) {
				$category->setValue($feed->getCategory()->getId());
			}
		}

		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('');
		$submit->setValue('Save');
		$submit->setAttrib('class','form-submit');
		$form->addElement($submit);

		return $form;

	}

}
