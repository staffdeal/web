<?php

class Epr_Form_Frontend_Registration extends Twitter_Bootstrap_Form_Vertical
{

	const FIELD_EMAIL = 'reg_email';
	const FIELD_PASSWORD = 'reg_password';
    const FIELD_FIRSTNAME = 'reg_firstname';
    CONST FIELD_LASTNAME = 'req_lastname';
	const FIELD_PASSWORD_REPEAT = 'reg_password2';
	const FIELD_DEALER = 'reg_dealer';
	const FIELD_TERMS_OF_USE = 'reg_tou';
	const FIELD_PRIVACY_POLICY = 'reg_pp';
	const FIELD_SUBMIT = 'req_submit';

	private $dealers = array();
	private $module = null;

	public function __construct($options)
	{

		if (isset($options['dealers']) && is_array($options['dealers'])) {
			$this->dealers = new \Poundation\PArray($options['dealers']);
			$this->dealers->sortByPropertyName('title');
			unset($options['dealers']);
		}

		if (isset($options['module']) && $options['module'] instanceof Epr_Module_UserRegistration_Abstract) {
			$this->module = $options['module'];
			unset($options['module']);
		}

		parent::__construct($options);

		$this->addElementPrefixPath('Epr_Form_Element_Decorator', 'Epr/Form/Element/Decorator', 'decorator');
	}

	public function init()
	{

		$sections = array();
		foreach ($this->dealers as $dealer) {
			if ($dealer instanceof Epr_Dealer) {
				if ($dealer->getAddress() instanceof \Poundation\PAddress) {

					$city = $dealer->getAddress()->getCity();
					if (strlen($city) > 0) {
						if (!isset($sections[$city])) {
							$sections[$city] = array();
						}

						$sections[$city][$dealer->getId()] = $dealer->getTitle();
					}
				}
			}
		}

		ksort($sections);

		$sections = array_merge(array(0 => _t("select dealer")), $sections);

		$this->addElement('text', self::FIELD_EMAIL, array(
			'required' => true,
			'label' => _t('email address'),
			'attribs' => array('class' => 'form-control')
		));
		$this->getElement(self::FIELD_EMAIL)->getDecorator('Label')->setOption('escape', false);
		$this->getElement(self::FIELD_EMAIL)->getDecorator('ElementErrors')->setOption('escape', false);
		$this->getElement(self::FIELD_EMAIL)->setDisableTranslator(true);
		$this->getElement(self::FIELD_EMAIL)->addValidator(new Epr_Validate_UniqueUser());

        $this->addElement('text', self::FIELD_FIRSTNAME, array(
                'required' => true,
                'label' => _t('firstname'),
                'attribs' => array('class' => 'form-control')
            ));
        $this->getElement(self::FIELD_FIRSTNAME)->getDecorator('Label')->setOption('escape', false);
        $this->getElement(self::FIELD_FIRSTNAME)->getDecorator('ElementErrors')->setOption('escape', false);
        $this->getElement(self::FIELD_FIRSTNAME)->setDisableTranslator(true);

        $this->addElement('text', self::FIELD_LASTNAME, array(
                'required' => true,
                'label' => _t('lastname'),
                'attribs' => array('class' => 'form-control')
            ));
        $this->getElement(self::FIELD_LASTNAME)->getDecorator('Label')->setOption('escape', false);
        $this->getElement(self::FIELD_LASTNAME)->getDecorator('ElementErrors')->setOption('escape', false);
        $this->getElement(self::FIELD_LASTNAME)->setDisableTranslator(true);

		$this->addElement('password', self::FIELD_PASSWORD, array(
			'required' => true,
			'label' => _t('password'),
			'attribs' => array('class' => 'form-control')
		));
		$this->getElement(self::FIELD_PASSWORD)->getDecorator('Label')->setOption('escape', false);
		$this->getElement(self::FIELD_PASSWORD)->getDecorator('ElementErrors')->setOption('escape', false);
		$this->getElement(self::FIELD_PASSWORD)->setDisableTranslator(true);




		if (!is_null($this->module)) {
			/** @var $module Epr_Module_UserRegistration_Abstract */
            $validator = new Epr_Validate_SecurePassword(array(
                'passwordMinimumLength' => $this->module->getPasswordMinimumLength(),
                'passwordContainsNumber' => $this->module->getPasswordContainsNumber(),
                'passwordContainsLetter' => $this->module->getPasswordContainsLetter(),
                'passwordContainsCapital' => $this->module->getPasswordContainsCapital(),
                'passwordContainsSymbols' => $this->module->getPasswordContainsSymbol(),
            ));
			$this->getElement(self::FIELD_PASSWORD)->addValidator($validator);
            $this->getElement(self::FIELD_PASSWORD)->setDescription($validator->getHintString());
		}



		$this->addElement('password', self::FIELD_PASSWORD_REPEAT, array(
			'required' => true,
			'label' => _t('repeat password'),
			'attribs' => array('class' => 'form-control')
		));
		$this->getElement(self::FIELD_PASSWORD_REPEAT)->getDecorator('Label')->setOption('escape', false);
		$this->getElement(self::FIELD_PASSWORD_REPEAT)->getDecorator('ElementErrors')->setOption('escape', false);
		$this->getElement(self::FIELD_PASSWORD_REPEAT)->setDisableTranslator(true);

		$dealer = new Epr_Form_Element_Select(self::FIELD_DEALER);
		$dealer->setLabel(_t('Dealer'));
		$dealer->setRequired(true);
		$dealer->setAttrib('class', 'form-control');
		$dealer->setMultiOptions($sections);
		$dealer->getDecorator('Label')->setOption('escape', false);
		$dealer->setDisableTranslator(true);
		$this->addElement($dealer);

		$this->addElement('checkbox', self::FIELD_TERMS_OF_USE, array(
			'required' => true,
			'attribs' => array('class' => 'form-control checkbox terms-of-use'),
			'label' => sprintf(_t('I have read and understood the <a href="%s" class="terms-of-use">Terms of Use</a>.'), '#')
		));
		$this->getElement(self::FIELD_TERMS_OF_USE)->getDecorator('Label')->setOption('escape', false)->setOption('placement', 'APPEND');
		$this->getElement(self::FIELD_TERMS_OF_USE)->getDecorator('ElementErrors')->setOption('escape', false);
		$this->getElement(self::FIELD_TERMS_OF_USE)->setDisableTranslator(true);


		$this->addElement('checkbox', self::FIELD_PRIVACY_POLICY, array(
			'required' => true,
			'attribs' => array('class' => 'form-control checkbox privacy-policy'),
			'label' => sprintf(_t('I have read and understood the <a href="%s" class="privacy-policy">Privacy Policy</a>.'), '#')
		));
		$this->getElement(self::FIELD_PRIVACY_POLICY)->getDecorator('Label')->setOption('escape', false)->setOption('placement', 'APPEND');
		$this->getElement(self::FIELD_PRIVACY_POLICY)->getDecorator('ElementErrors')->setOption('escape', false);
		$this->getElement(self::FIELD_PRIVACY_POLICY)->setDisableTranslator(true);

		$this->addElement('button', self::FIELD_SUBMIT, array(
			'label' => _t('register me'),
			'type' => 'submit',
			'buttonType' => 'success',
			'icon' => 'ok',
			'escape' => false
		));
		$this->getElement(self::FIELD_SUBMIT)->setDisableTranslator(true);

		$this->addDisplayGroup(array(
			self::FIELD_EMAIL,
			self::FIELD_PASSWORD,
			self::FIELD_PASSWORD_REPEAT,
			self::FIELD_DEALER,
			self::FIELD_PRIVACY_POLICY,
			self::FIELD_TERMS_OF_USE,
			self::FIELD_SUBMIT
		), 'registration', array());

	}

	public function isValid($data)
	{

		$valid = parent::isValid($data);

		$passwordField = $this->getElement(self::FIELD_PASSWORD);
		$passwordRepeatField = $this->getElement(self::FIELD_PASSWORD_REPEAT);
		$dealerField = $this->getElement(self::FIELD_DEALER);

		if ($passwordField && $passwordRepeatField) {

			if ($passwordField->getValue() === $passwordRepeatField->getValue()) {
				$valid = $valid && true;
			} else {

				$valid = $valid && false;
				$passwordRepeatField->addError(_t('The passwords do not match.'));

			}

		}

		$dealerId = $dealerField->getValue();
		if ($dealerId == '0') {
			$valid = $valid && false;
			$dealerField->addError(_t('Please select your employing dealer.'));
		} else {

			$selectedDealer = _dm()->find(Epr_Dealer::DOCUMENTNAME, $dealerId);
			$valid = $valid && ($selectedDealer instanceof Epr_Dealer);
		}

		return $valid;

	}

}