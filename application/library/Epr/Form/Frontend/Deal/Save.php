<?php

class Epr_Form_Frontend_Deal_Save extends Twitter_Bootstrap_Form_Vertical
{

    const FIELDSET           = 'save_deal';
    const FIELD_TERMS_OF_USE = 'save_termsOfUse';
    const FIELD_SUBMIT       = 'save_submit';
    const FIELD_TYPE         = 'save_type';

    private $deal;

    public function __construct(Epr_Dispatcher_Deal $deal)
    {
        parent::__construct();
        $this->deal = $deal;
        $this->setAttrib('id', 'deal_save');
    }

    public function init()
    {

        $this->addElement(
            'checkbox', self::FIELD_TERMS_OF_USE, array(
                'required' => true,
                'label'    => sprintf(_t('I have read and understood the <a href="%s" target="_blank">Terms of Use</a>.'), '/pages/terms-of-use'),
                'attribs'  => array('class' => 'form-control checkbox terms-of-use')
            )
        );
        $this->getElement(self::FIELD_TERMS_OF_USE)->getDecorator('Label')->setOption('escape', false);
        $this->getElement(self::FIELD_TERMS_OF_USE)->getDecorator('ElementErrors')->setOption('escape', false);
        $this->getElement(self::FIELD_TERMS_OF_USE)->setDisableTranslator(true);
        $this->getElement(self::FIELD_TERMS_OF_USE)->setValue(0);

        $this->addElement(
            'button', self::FIELD_SUBMIT, array(
                'label'      => _t('Save deal'),
                'type'       => 'submit',
                'buttonType' => 'primary',
                'icon'       => 'ok',
                'escape'     => false,
                'attribs' => array('class' => 'pull-right')
            )
        );
        $this->getElement(self::FIELD_SUBMIT)->setDisableTranslator(true);

        $this->addDisplayGroup(
            array(
                self::FIELD_TERMS_OF_USE,
                self::FIELD_SUBMIT
            ), self::FIELDSET, array()
        );

        $type = new Zend_Form_Element_Hidden(self::FIELD_TYPE);
        $this->addElement($type);
    }

}