<?php

class Epr_Form_Frontend_Login extends Twitter_Bootstrap_Form_Vertical
{

	const FIELD_EMAIL    = 'login_email';
	const FIELD_PASSWORD = 'login_password';
	const FIELD_SUBMIT   = 'login_submit';

	public function init()
	{
		$usersModule = _app()->getModuleWithIdentifier(Epr_Modules_Users::identifier());

		$this->addElement('text', self::FIELD_EMAIL, array(
														  'label'    => _t('email address'),
														  'required' => true,
														  'attribs'  => array('class' => 'form-control')
													 ));
		$this->getElement(self::FIELD_EMAIL)->getDecorator('Label')->setOption('escape',false);
		$this->getElement(self::FIELD_EMAIL)->getDecorator('ElementErrors')->setOption('escape', false);
		$this->getElement(self::FIELD_EMAIL)->setDisableTranslator(true);

		$passwordOptions = array(
			'label'    => _t('password'),
			'required' => true,
			'attribs'  => array('class' => 'form-control')
		);

		if ($usersModule instanceof Epr_Modules_Users) {
			if ($usersModule->canResetUser()) {
				$passwordOptions['append'] = '<a href="/user/reset-password"><span class="glyphicon glyphicon-share-alt"></span>' . _t('forgot password') . '</a>';
			}
		}

		$this->addElement('password', self::FIELD_PASSWORD, $passwordOptions);
		$this->getElement(self::FIELD_PASSWORD)->getDecorator('Label')->setOption('escape',false);
		$this->getElement(self::FIELD_PASSWORD)->getDecorator('ElementErrors')->setOption('escape', false);
		$this->getElement(self::FIELD_PASSWORD)->setDisableTranslator(true);

		$this->addElement('button', self::FIELD_SUBMIT, array(
															 'label'      => _t('Login'),
															 'type'       => 'submit',
															 'buttonType' => 'primary',
															 'escape'     => false
														));
		$this->getElement(self::FIELD_SUBMIT)->setDisableTranslator(true);

		$this->addDisplayGroup(array(
									self::FIELD_EMAIL,
									self::FIELD_PASSWORD,
									self::FIELD_SUBMIT,
							   ), 'login', array());

	}

}