<?php

class Epr_Form_Frontend_Navbar_Login extends Twitter_Bootstrap_Form_Inline {

	public function init() {

		$this->setAttrib('class', 'nav navbar-form login');

		$email = new Zend_Form_Element_Text(Epr_Form_Frontend_Login::FIELD_EMAIL);
		$email->setRequired('true');
		$email->setOptions(array('placeholder' => _t('email address'),));
		$email->setAttrib('class', 'form-control email');
		$email->setLabel(_t('Your email address'));
		$email->getDecorator('Label')->setOption('escape',false);
		$email->setDisableTranslator(true);
		$this->addElement($email);

		$password = new Zend_Form_Element_Password(Epr_Form_Frontend_Login::FIELD_PASSWORD);
		$password->setRequired(true);
		$password->setOptions(array('placeholder' => _t('password')));
		$password->setAttrib('class', 'form-control');
		$password->setLabel(_t('Your password'));
		$password->getDecorator('Label')->setOption('escape',false);
		$password->setDisableTranslator(true);
		$this->addElement($password);

		$submit = new Twitter_Bootstrap_Form_Element_Submit(Epr_Form_Frontend_Login::FIELD_SUBMIT, array('buttonType' => Twitter_Bootstrap_Form_Element_Submit::BUTTON_PRIMARY));
		$submit->setLabel(_t('Login'));
		$this->addElement($submit);

	}

}