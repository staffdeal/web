<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 01.04.13
 * Time: 16:58
 */

class Epr_Form_Frontend_Idea extends Twitter_Bootstrap_Form_Vertical
{

	const FIELD_TEXT   = 'sub_text';
	const FIELD_URL    = 'sub_url';
	const FIELD_FILES  = 'sub_files';
	const FIELD_SUBMIT = 'sub_submit';

	public function init()
	{

		$this->addElement('textarea', self::FIELD_TEXT, array(
															 'required' => true,
															 'label'    => _t('describe your idea'),
															 'attribs'  => array('class' => 'form-control wide'),
															 'rows'     => 12
														));
		$this->getElement(self::FIELD_TEXT)->getDecorator('Label')->setOption('escape',false);
		$this->getElement(self::FIELD_TEXT)->getDecorator('ElementErrors')->setOption('escape', false);
		$this->getElement(self::FIELD_TEXT)->setDisableTranslator(true);

		$this->addElement('text', self::FIELD_URL, array(
														'label'   => _t('URL'),
														'attribs' => array('class' => 'form-control wide')
												   ));
		$this->getElement(self::FIELD_URL)->getDecorator('Label')->setOption('escape',false);
		$this->getElement(self::FIELD_URL)->getDecorator('ElementErrors')->setOption('escape', false);
		$this->getElement(self::FIELD_URL)->setDisableTranslator(true);
		$this->getElement(self::FIELD_URL)->addValidator(new Epr_Validate_URL());

		$this->addElement('file', self::FIELD_FILES, array('label'  => _t('attach an image'),
														  'attribs' => array('class' => 'form-control wide')
													 ));
		$this->getElement(self::FIELD_FILES)->getDecorator('Label')->setOption('escape',false);
		$this->getElement(self::FIELD_FILES)->getDecorator('ElementErrors')->setOption('escape', false);
		$this->getElement(self::FIELD_FILES)->setDisableTranslator(true);

		$this->addElement('button', self::FIELD_SUBMIT, array(
															 'label'      => _t('save'),
															 'type'       => 'submit',
															 'buttonType' => 'primary',
															 'escape'     => false,
															 'attribs'		=> array('class' => 'btn-lg')
														));
		$this->getElement(self::FIELD_SUBMIT)->setDisableTranslator(true);

		$this->addDisplayGroup(array(
									self::FIELD_TEXT,
									self::FIELD_URL,
									self::FIELD_FILES,
									self::FIELD_SUBMIT
							   ), 'submit_idea', array());

	}
}