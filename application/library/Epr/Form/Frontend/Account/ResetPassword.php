<?php

class Epr_Form_Frontend_Account_ResetPassword extends Twitter_Bootstrap_Form_Vertical
{

	const FIELD_EMAIL = 'rp_email';
	const FIELD_SUBMIT = 'rp_submit';

	public function init()
	{

		$this->addElement('text', self::FIELD_EMAIL, array(
														  'label'    => _t('email address'),
														  'required' => true,
														  'attribs'  => array('class' => 'form-control max big autofocus')
													 ));
		$this->getElement(self::FIELD_EMAIL)->getDecorator('Label')->setOption('escape',false);
		$this->getElement(self::FIELD_EMAIL)->getDecorator('ElementErrors')->setOption('escape', false);
		//$this->getElement(self::FIELD_EMAIL)->addValidator(new Zend_Validate_EmailAddress());
		$this->getElement(self::FIELD_EMAIL)->setDisableTranslator(true);

		$this->addElement('button', self::FIELD_SUBMIT, array(
															 'label'      => _t('reset password'),
															 'type'       => 'submit',
															 'buttonType' => 'primary',
															 'escape'     => false
														));
		$this->getElement(self::FIELD_SUBMIT)->setDisableTranslator(true);

		$this->addDisplayGroup(array(
									self::FIELD_EMAIL,
									self::FIELD_SUBMIT,
							   ), 'login', array());

	}

}