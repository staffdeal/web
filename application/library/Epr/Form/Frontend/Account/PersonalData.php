<?php

class Epr_Form_Frontend_Account_PersonalData extends Twitter_Bootstrap_Form_Vertical
{

	const FIELD_FIRSTNAME = 'data_firstname';
	const FIELD_LASTNAME  = 'data_lastname';
	const FIELD_EMAIL     = 'data_email';
	const FIELD_SUBMIT    = 'data_submit';

	private $user;

	public function __construct(Epr_User $user)
	{
		$this->user = $user;
		parent::__construct();
	}


	public function init()
	{

		$this->setDisableTranslator(true);


		if ($this->user instanceof Epr_User) {
			$this->addElement('text', self::FIELD_EMAIL, array(
															  'label'    => _t('email address'),
															  'disabled' => true,
															  'value'    => $this->user->getEmail(),
															  'attribs'  => array('class' => 'form-control')
														 ));
			$this->getElement(self::FIELD_EMAIL)->getDecorator('Label')->setOption('escape',false);
			$this->getElement(self::FIELD_EMAIL)->getDecorator('ElementErrors')->setOption('escape', false);
			$this->getElement(self::FIELD_EMAIL)->setDisableTranslator(true);

			$this->addElement('text', self::FIELD_FIRSTNAME, array(
															  'label'    => _t('firstname'),
															  'required' => false,
															  'value'    => $this->user->getFirstname(),
															  'attribs'  => array('class' => 'form-control')
														 ));
			$this->getElement(self::FIELD_FIRSTNAME)->getDecorator('Label')->setOption('escape',false);
			$this->getElement(self::FIELD_FIRSTNAME)->getDecorator('ElementErrors')->setOption('escape', false);
			$this->getElement(self::FIELD_FIRSTNAME)->setDisableTranslator(true);

			$this->addElement('text', self::FIELD_LASTNAME, array(
															  'label'    => _t('lastname'),
															  'required' => false,
															  'value'    => $this->user->getLastname(),
															  'attribs'  => array('class' => 'form-control')
														 ));
			$this->getElement(self::FIELD_LASTNAME)->getDecorator('Label')->setOption('escape',false);
			$this->getElement(self::FIELD_LASTNAME)->getDecorator('ElementErrors')->setOption('escape', false);
			$this->getElement(self::FIELD_LASTNAME)->setDisableTranslator(true);

			$this->addElement('button', self::FIELD_SUBMIT, array(
																 'label'      => _t('save'),
																 'type'       => 'submit',
																 'buttonType' => 'primary',
																 'escape'     => false
															));
			$this->getElement(self::FIELD_SUBMIT)->setDisableTranslator(true);

			$this->addDisplayGroup(array(
										self::FIELD_EMAIL,
										self::FIELD_FIRSTNAME,
										self::FIELD_LASTNAME,
										self::FIELD_SUBMIT
								   ), 'login', array());

		}

	}

}