<?php

class Epr_Form_Frontend_Account_Password extends Twitter_Bootstrap_Form_Vertical
{

	const FIELD_PASSWORD_OLD    = 'pass_old';
	const FIELD_PASSWORD_NEW    = 'pass_new';
	const FIELD_PASSWORD_REPEAT = 'pass_repeat';
	const FIELD_SUBMIT          = 'pass_submit';

	private $user;
	private $module;

	public function __construct(Epr_User $user, Epr_Module_UserRegistration_Abstract $module)
	{

		$this->user   = $user;
		$this->module = $module;
		parent::__construct();
	}

	public function init()
	{
		$this->setDisableTranslator(true);

		$this->addElement('password', self::FIELD_PASSWORD_OLD, array(
																	 'label'    => _t('old password'),
																	 'required' => true,
																	 'attribs'  => array('class' => 'form-control')
																));
		$this->getElement(self::FIELD_PASSWORD_OLD)->getDecorator('Label')->setOption('escape',false);
		$this->getElement(self::FIELD_PASSWORD_OLD)->getDecorator('ElementErrors')->setOption('escape', false);
		$this->getElement(self::FIELD_PASSWORD_OLD)->setDisableTranslator(true);

		$this->addElement('password', self::FIELD_PASSWORD_NEW, array(
																	 'label'    => _t('new password'),
																	 'required' => true,
																	 'attribs'  => array('class' => 'form-control')
																));
		$this->getElement(self::FIELD_PASSWORD_NEW)->getDecorator('Label')->setOption('escape',false);
		$this->getElement(self::FIELD_PASSWORD_NEW)->getDecorator('ElementErrors')->setOption('escape', false);
		$this->getElement(self::FIELD_PASSWORD_NEW)->setDisableTranslator(true);

		if (!is_null($this->module)) {
			/** @var $module Epr_Module_UserRegistration_Abstract */
			$this->getElement(self::FIELD_PASSWORD_NEW)->addValidator(new Epr_Validate_SecurePassword(array(
																										   'passwordMinimumLength'   => $this->module->getPasswordMinimumLength(),
																										   'passwordContainsNumber'  => $this->module->getPasswordContainsNumber(),
																										   'passwordContainsLetter'  => $this->module->getPasswordContainsLetter(),
																										   'passwordContainsCapital' => $this->module->getPasswordContainsCapital(),
																										   'passwordContainsSymbols' => $this->module->getPasswordContainsSymbol(),
																									  )));
		}

		$this->addElement('password', self::FIELD_PASSWORD_REPEAT, array(
																		'label'    => _t('repeat password'),
																		'required' => true,
																		'attribs'  => array('class' => 'form-control')
																   ));
		$this->getElement(self::FIELD_PASSWORD_REPEAT)->getDecorator('Label')->setOption('escape',false);
		$this->getElement(self::FIELD_PASSWORD_REPEAT)->getDecorator('ElementErrors')->setOption('escape', false);
		$this->getElement(self::FIELD_PASSWORD_REPEAT)->setDisableTranslator(true);

		$this->addElement('button', self::FIELD_SUBMIT, array(
															 'label'      => _t('save'),
															 'type'       => 'submit',
															 'buttonType' => 'primary',
															 'escape'     => false
														));
		$this->getElement(self::FIELD_SUBMIT)->setDisableTranslator(true);

		$this->addDisplayGroup(array(
									self::FIELD_PASSWORD_OLD,
									self::FIELD_PASSWORD_NEW,
									self::FIELD_PASSWORD_REPEAT,
									self::FIELD_SUBMIT
							   ), 'login', array());

	}




	public function isValid($data)
	{

		$valid = parent::isValid($data);

		$oldPasswordField    = $this->getElement(self::FIELD_PASSWORD_OLD);
		$passwordField       = $this->getElement(self::FIELD_PASSWORD_NEW);
		$passwordRepeatField = $this->getElement(self::FIELD_PASSWORD_REPEAT);

		if ($oldPasswordField->getValue() === $passwordField->getValue()) {
			$passwordField->addError(_t('You need to choose a new password.'));
		}

		if ($this->user->checkPassword($oldPasswordField->getValue())) {
			$valid = $valid && true;
		} else {
			$valid = $valid && false;
			$oldPasswordField->addError(_t('Your old password is incorrect.'));
		}

		if ($passwordField && $passwordRepeatField) {

			if ($passwordField->getValue() === $passwordRepeatField->getValue()) {
				$valid = $valid && true;
			} else {

				$valid = $valid && false;
				$passwordRepeatField->addError(_t('The passwords do not match.'));
			}

		}

		return $valid;
	}

}