<?php

class Epr_Form_CodeGenerator extends Epr_Form {

    const FIELD_START_NUMBER = 'start';
    const FIELD_END_NUMBER = 'end';
    const FIELD_PREFIX = 'prefix';
    const FIELD_APPENDIX = 'appendix';

    static function form () {

        $form = new self();

        $prefix = new Zend_Form_Element_Text(self::FIELD_PREFIX);
        $prefix->setLabel('Prefix');
        $prefix->setAttrib('class', 'inp-form');
        $prefix->setAttrib('color', 'green');
        $prefix->setDescription('The prefix is the fix part at the beginning of all codes.');
        $form->addElement($prefix);

        $start = new Zend_Form_Element_Text(self::FIELD_START_NUMBER);
        $start->setLabel('Start number');
        $start->setAttrib('class','inp-form narrow');
        $start->setAttrib('color', 'blue');
        $start->addValidator(new Zend_Validate_Digits());
        $start->addValidator(new Zend_Validate_Between(array('min' => 1, 'max' => 1000000000)));
        $start->setRequired(true);
        $start->setDescription('Enter the first number used to generate codes.');
        $form->addElement($start);

        $end = new Zend_Form_Element_Text(self::FIELD_END_NUMBER);
        $end->setLabel('End number');
        $end->setAttrib('class','inp-form');
        $end->setAttrib('color', 'blue');
        $end->addValidator(new Zend_Validate_Digits());
        $end->setRequired(true);
        $end->setDescription('Enter the last number used to generate codes.');
        $form->addElement($end);

        $appendix = new Zend_Form_Element_Text(self::FIELD_APPENDIX);
        $appendix->setLabel('Appendix');
        $appendix->setAttrib('class', 'inp-form ');
        $appendix->setAttrib('color', 'green');
        $appendix->setDescription('The appendix is the fix part at the end of all codes.');
        $form->addElement($appendix);

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('');
        $submit->setValue('Generate');
        $submit->setAttrib('class', 'form-submit');
        $form->addElement($submit);

        return $form;
    }

}