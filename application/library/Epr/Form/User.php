<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 07.09.12
 * Time: 21:35
 */
class Epr_Form_User extends Epr_Form
{


	static function userForm($user = null, $step = null)
	{

		$form = new self();

		$email = new Zend_Form_Element_Text('email');
		$email->setLabel('E-Mail');
		$email->setRequired(true);
		$email->setAttrib('class', 'inp-form narrow');
		$email->setValidators(array('EmailAddress'));
		$email->setDescription('The email address of the user. It is checked for syntactical correctness but not validated though.');
		$form->addElement($email);

		$firstname = new Zend_Form_Element_Text('firstname');
		$firstname->setLabel('Firstname');
		$firstname->setRequired(false);
		$firstname->setAttrib('class', 'inp-form narrow');
		$firstname->setDescription('The firstname of the user.');
		$form->addElement($firstname);

		$lastname = new Zend_Form_Element_Text('lastname');
		$lastname->setLabel('Lastname');
		$lastname->setRequired(false);
		$lastname->setAttrib('class', 'inp-form narrow');
		$lastname->setDescription('The lastname of the user.');
		$form->addElement($lastname);

		$password = new Zend_Form_Element_Password('password');
		$password->setLabel('Password');
		$password->setAttrib('class', 'inp-form');
		$password->setDescription('The new password of the user. NO complexity rules are applied.');
		$form->addElement($password);

		$passwordRepeat = new Zend_Form_Element_Password('password_repeat');
		$passwordRepeat->setLabel('Password Repeat');
		$passwordRepeat->setAttrib('class', 'inp-form');
		$passwordRepeat->setDescription('Repeat the password to avoid misspellings.');
		$form->addElement($passwordRepeat);

		$roles = Epr_Roles::allRoles();
		$roles->removeValueForKey(Epr_Roles::ROLE_GUEST);
		$currentUser = _user();
		if ($currentUser->getRole() != Epr_Roles::ROLE_ROOT) {
			$roles->removeValueForKey(Epr_Roles::ROLE_ROOT);
		}

		$role = new Zend_Form_Element_Select('role');
		$role->setMultiOptions($roles->nativeArray());
		$role->setAttrib('class', 'styledselect_form_1');
		$role->setLabel('Role');
		$role->setDescription('The role of the user. Do not ever change it to anything else than \'user\' unless you are absolutely sure.');
		$form->addElement($role);

		$isActive = new Zend_Form_Element_Checkbox('isActive');
		$isActive->setLabel('Is Active');
		$isActive->setChecked(true);
		$isActive->setDescription('Only active users can login.');
		$form->addElement($isActive);

		/** @var $user Epr_User */
		if ($user instanceof Epr_User) {
			$firstname->setValue($user->getFirstname());
			$lastname->setValue($user->getLastname());
			$email->setValue($user->getEmail());
			$role->setValue($user->getRole());
			$isActive->setValue($user->isActive());

            $registrationText = new Zend_Form_Element_Textarea('registrationText');
            $registrationText->setRequired(false);
            $registrationText->setAttrib('class', 'wide');
            $registrationText->setAttrib('disabled', 'disabled');
            $registrationText->setLabel('Registration Information');
            $registrationText->setValue($user->getRegistrationInformation());
            $form->addElement($registrationText);

        }

		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('');
		$submit->setValue('Submit');
		$submit->setAttrib('class', 'form-submit');
		$form->addElement($submit);

		return $form;
	}


	static function getLoginForm()
	{
		$form = new self();
		$form->setPreserveDecorators(true);

		$email = new Zend_Form_Element_Text('email');

		$email->setRequired(true);
		$email->setAttrib('class', 'login-inp');
		$email->setLabel('');
		$email->setValidators(array('EmailAddress'));
		$email->removeDecorator('label')->removeDecorator('HtmlTag');

		$password = new Zend_Form_Element_Password('password');
		$password->setRequired(true);
		$password->setLabel('');
		$password->setAttrib('class', 'login-inp');
		$password->removeDecorator('label')->removeDecorator('HtmlTag');

		$form->addElements(array(
								$email,
								$password
						   ));

		return $form;
	}

	static function getForgetPasswordForm()
	{
		$form = new self();
		$form->setPreserveDecorators(true);

		$email = new Zend_Form_Element_Text('email');
		$email->setLabel('E-Mail');
		$email->setRequired(true);
		$email->setAttrib('class', 'inp-form');
		$email->setValidators(array('EmailAddress'));
		$email->removeDecorator('label')->removeDecorator('HtmlTag');

		$form->addElements(array($email));

		return $form;
	}

	static function writeMessageForm()
	{



	}
}
