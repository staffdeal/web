<?php

class Epr_Form_Dealer extends Epr_Form {

    const DEALER_FORM_NAME = 'name';
    const DEALER_FORM_STREET = 'street';
    const DEALER_FORM_ZIP = 'zip';
    const DEALER_FORM_CITY = 'city';
    const DEALER_FORM_COUNTRY = 'country';
    const DEALER_FORM_LAT = 'latitude';
    const DEALER_FORM_LON = 'longitude';
    const DEALER_FORM_LOGO = 'logo';
    const DEALER_FORM_TEL = 'telefon';
    const DEALER_FORM_WEB = 'web';
    const DEALER_FORM_MAIL = 'email';
    const DEALER_FORM_OPENINGHOURS = 'openingHours';
    const DEALER_FORM_DESCRIPTION = 'text';
    const DEALER_FORM_ACTIVE = 'active';
    const DEALER_FORM_CATEGORIES_PREFIX = 'category';

	const IMPORT_FORM_VALUE = 'value';
	const IMPORT_FORM_START_LINE = 'startLine';
	const IMPORT_FORM_DELIMITER = 'delimiter';

    static function dealerForm($additionalCategories, $dealer = null) {

        $form = new self();

        $name = new Zend_Form_Element_Text(self::DEALER_FORM_NAME);
        $name->setAttrib('class', 'inp-form middle');
        $name->setLabel('Name');
        $name->setRequired(true);
		$name->setDescription('The name of the dealer.');
        $form->addElement($name);

        $street = new Zend_Form_Element_Text(self::DEALER_FORM_STREET);
        $street->setAttrib('class', 'inp-form');
        $street->setLabel('Street & House Number');
		$street->setDescription('The street name and house number of the shop\'s address. The user might use it to get directions.');
        $street->setRequired(true);
        $form->addElement($street);

        $zip = new Zend_Form_Element_Text(self::DEALER_FORM_ZIP);
        $zip->setAttrib('class','inp-form');
        $zip->setLabel('Zip Code');
        $zip->setRequired(true);
		$zip->setDescription('The postal code (aka zip code) of the shop\'s address.');
        $form->addElement($zip);

        $city = new Zend_Form_Element_Text(self::DEALER_FORM_CITY);
        $city->setAttrib('class', 'inp-form middle');
        $city->setLabel('City');
		$city->setDescription('The city of the shop\'s address.');
        $city->setRequired(true);
        $form->addElement($city);

        $country = new Zend_Form_Element_Text(self::DEALER_FORM_COUNTRY);
        $country->setAttrib('class', 'inp-form');
        $country->setLabel('Country');
		$country->setDescription('The country of the shop\'s address.');
        $country->setRequired(true);
        $country->setValue('Germany');
        $form->addElement($country);

        $lat = new Zend_Form_Element_Text(self::DEALER_FORM_LAT);
        $lat->setAttrib('class', 'inp-form');
        $lat->setLabel('Latitude');
		$lat->setDescription('The latitude of the shop\'s coordinate. Must be beetween -90° and 90°.');
        $lat->addValidator(new Zend_Validate_Between(array(
            'min' => \Poundation\PCoordinate::LATITUDE_MINIMUM,
            'max' => \Poundation\PCoordinate::LATITUDE_MAXIMUM)));
        $form->addElement($lat);

        $lon = new Zend_Form_Element_Text(self::DEALER_FORM_LON);
        $lon->setAttrib('class', 'inp-form');
        $lon->setLabel('Longitude');
		$lon->setDescription('The longitude of the shop\'s coordinate. Must be beetween -180° and 180°.');
        $lon->addValidator(new Zend_Validate_Between(array(
            'min' => \Poundation\PCoordinate::LONGITUDE_MINIMUM,
            'max' => \Poundation\PCoordinate::LONGITUDE_MAXIMUM
        )));
        $form->addElement($lon);

        $logo = new Epr_Form_Element_Image_Selector(self::DEALER_FORM_LOGO);
        $logo->setLabel('Logo');
        $logo->setSize(100,100);
		$logo->setDescription('The logo (or any other image) of the dealer.');
        $logo->setContext(Epr_Content_Image::ACCESS_DEALER_LOGO);
        $form->addElement($logo);

        $tel = new Zend_Form_Element_Text(self::DEALER_FORM_TEL);
        $tel->setAttrib('class', 'inp-form');
		$tel->setDescription('The telephone number of the shop. The user might use it to contact the dealer.');
        $tel->setLabel('Telephone');
        $form->addElement($tel);

        $web = new Zend_Form_Element_Text(self::DEALER_FORM_WEB);
        $web->setAttrib('class', 'inp-form');
        $web->setLabel('Internet Address');
		$web->setDescription('The public URL of the shop. The user might use it to look for further information.');
        $web->addValidator(new Epr_Validate_URL());
        $form->addElement($web);

        $mail = new Zend_Form_Element_Text(self::DEALER_FORM_MAIL);
        $mail->setAttrib('class', 'inp-form');
        $mail->setLabel('Email');
		$mail->setDescription('The public email address of the shop. The user might use it to contact the dealer.');
        $mail->addValidator(new Zend_Validate_EmailAddress());
        $form->addElement($mail);

        $oh = new Zend_Form_Element_Textarea(self::DEALER_FORM_OPENINGHOURS);
        $oh->setAttrib('class', 'inp-form');
        $oh->setLabel('Opening Hours');
		$oh->setDescription('The opening hours of the shop.');
        $form->addElement($oh);

        $text = new Zend_Form_Element_Textarea(self::DEALER_FORM_DESCRIPTION);
        $text->setAttrib('class', 'wysiwyg wide');
        $text->setLabel('Description');
		$text->setDescription('A descriptive text about the dealer. Might be use in later versions.');
        $form->addElement($text);

        $active = new Zend_Form_Element_Checkbox(self::DEALER_FORM_ACTIVE);
        $active->setLabel('Is Active');
		$active->setDescription('Only active dealers are used.');
        $active->setChecked(true);
        $form->addElement($active);

        $i = 0;
        foreach($additionalCategories as $categoryName => $categoryFields) {

            $categoryElement = new Zend_Form_Element_Select(self::DEALER_FORM_CATEGORIES_PREFIX . $i);
            $categoryElement->setLabel($categoryName);
            $categoryElement->setMultiOptions($categoryFields);
            $categoryElement->setAttrib('class','styledselect_form_1');
			$categoryElement->setDescription('Specify the additional category.');
            if ($dealer instanceof Epr_Dealer) {
                $categoryElement->setValue($dealer->getAdditionCategoryValue($i));
            }
            $form->addElement($categoryElement);
            $i++;
        }

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('');
        $submit->setValue('Save');
        $submit->setAttrib('class','form-submit');
        $form->addElement($submit);


        if ($dealer instanceof Epr_Dealer) {
            $name->setValue($dealer->getTitle());
            if ($dealer->getAddress()) {
                $street->setValue($dealer->getAddress()->getStreet());
                $zip->setValue($dealer->getAddress()->getZip());
                $city->setValue($dealer->getAddress()->getCity());
                $country->setValue($dealer->getAddress()->getCountry());
                $logo->setValue($dealer->getTitleImage());
                $tel->setValue($dealer->getTelephone());
                $web->setValue((string)$dealer->getWeb());
                $mail->setValue((string)$dealer->getEmail());
                $oh->setValue($dealer->getOpeningHours());
                $text->setValue($dealer->getText());
                $active->setValue($dealer->isActive());

                if ($dealer->getAddress()->getCoordinate()) {
                    $lat->setValue($dealer->getAddress()->getCoordinate()->getLatitude());
                    $lon->setValue($dealer->getAddress()->getCoordinate()->getLongitude());
                }

            }
        }

        return $form;

    }

	static public function importForm($value = null, $start = 1, $delimiter = ';') {

		$form = new self();

		$value = new Zend_Form_Element_Textarea(self::IMPORT_FORM_VALUE);
		$value->setAttrib('class', 'wide');
		$value->setLabel('CSV');
		$value->setRequired(true);
		$form->addElement($value);

		$startLine = new Zend_Form_Element_Text(self::IMPORT_FORM_START_LINE);
		$startLine->setAttrib('class', 'inp-form');
		$startLine->setLabel('Start Line');
		$startLine->setValue($start);
		$startLine->setRequired(true);
		$numberValidator = new Zend_Validate_Between(array('min' => 0, 'max' => 1000000));
		$startLine->addValidator($numberValidator, true);
		$form->addElement($startLine);

		$delimiterSign = new Zend_Form_Element_Text(self::IMPORT_FORM_DELIMITER);
		$delimiterSign->setAttrib('class', 'inp-form');
		$delimiterSign->setAttrib('maxLength','1');
		$delimiterSign->setLabel('Delimiter');
		$delimiterSign->setValue($delimiter);
		$signValidator = new Zend_Validate_StringLength(array('min' => 1, 'max' => 1));
		$delimiterSign->addValidator($signValidator);
		$delimiterSign->setRequired(true);
		$form->addElement($delimiterSign);

		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('');
		$submit->setValue('Continue');
		$submit->setAttrib('class','form-submit');
		$form->addElement($submit);

		return $form;

	}

}