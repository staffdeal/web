<?php

class Epr_Form_Event extends Epr_Form {

    const FIELD_STATE = 'state';
    const FIELD_TITLE = 'title';
    const FIELD_SLUG = 'slug';
    const FIELD_DATE = 'date';
    const FIELD_IMAGE = 'image';
    const FIELD_TEXT = 'text';

    /**
     * @return Epr_Event
     */
    public function getEvent() {
        return $this->getEntity();
    }

    public function init($options = NULL) {

        parent::init($options);

        $event = $this->getEvent();

        $title = new Zend_Form_Element_Text(self::FIELD_TITLE);
        $title->setLabel(_t('Title'));
        $title->setDescription(_t('Enter the title of the event. Do not include the date.'));
        $title->setAttrib('class', 'inp-form wide');
        $title->setRequired(true);
        if ($event) {
            $title->setValue($event->getTitle());
        }
        $this->addElement($title);

        $slug = new Epr_Form_Element_Slug(self::FIELD_SLUG);
        $slug->setLabel('Slug');
        $slug->setAttrib('class','inp-form narrow');
        $slug->setBaseElementName(self::FIELD_TITLE);
        $slug->setDescription('The slug of the event. It is used to create descriptive URLs on the web page. When no value is given, it is computed from the title.');
        $this->addElement($slug);
        if ($event->getId()) {
            $slug->setValue($event->getSlug());
            $slug->setDocumentId($event->getId());
        }

        $date = new Zend_Form_Element_Text(self::FIELD_DATE);
        $date->setLabel(_t('Date'));
        $date->setDescription(_t('Enter the date of the event.'));
        $date->setRequired(true);
        $date->setAttrib('class','inp-form date-picker narrow');
        $date->setAttrib('readonly','readonly');
        if ($event && $event->getDate()) {
            $date->setValue($event->getDate()->getFormatedString('Y-m-d'));
        } else {
            $date->setValue(\Poundation\PDate::today()->addDays(14)->getFormatedString('Y-m-d'));
        }
        $this->addElement($date);

        $titleImage = new Epr_Form_Element_Image_Selector(self::FIELD_IMAGE);
        $titleImage->setLabel('Title Image');
        $titleImage->setDescription('The title image of the event. You can use more images within the text.');
        if ($event) {
            $titleImage->setValue($event->getTitleImage());
        }
        $this->addElement($titleImage);

        $text = new Zend_Form_Element_Textarea(self::FIELD_TEXT);
        $text->setAttrib('class', 'wysiwyg wide');
        $text->setLabel('Description');
        $text->setDescription('The details description of the event. You can write as much text as you want and include links, images and any other HTML element.');
        if ($event) {
            $text->setValue($event->getText());
        }
        $this->addElement($text);
    }
}