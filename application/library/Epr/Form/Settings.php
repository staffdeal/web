<?php

use Poundation\PArray;

class Epr_Form_Settings extends Epr_Form
{

	/**
	 *
	 * @param Epr_Module_Abstract $module
	 *
	 * @return Epr_Form_Settings
	 */
	static function moduleForm($module)
	{
		$form = new self();
		$form->setEnctype('multipart/form-data');

		return $form;
	}
}