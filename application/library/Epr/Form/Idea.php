<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 07.09.12
 * Time: 21:35
 */
class Epr_Form_Idea extends Epr_Form
{

    public function getCategoriesSelectArray($categories = null)
    {

        if (is_null($categories)) {
            throw new Exception('No Categories set');
        }

        $selectArray = array();
        $selectArray['0'] = 'No category';
        foreach ($categories as $category) {
            $selectArray[$category->getId()] = $category->getTitle();
        }

        return $selectArray;
    }

    public function getCompetitionSelectArray($competitions = null)
    {
        if (is_null($competitions)) {
            throw new Exception('No Categories set');
        }

        $selectArray = array();
        $selectArray['0'] = 'No Competition';
        foreach ($competitions as $competition) {
            $selectArray[$competition->getId()] = $competition->getTitle();
        }

        return $selectArray;
    }

    static function competitionForm($competition = null, $categories = null, $step = null){

        $form = new self();

        $title = new Zend_Form_Element_Text('title');
        $title->setLabel('Title');
        $title->setRequired(true);
        $title->setAttrib('class','inp-form');
		$title->setDescription('The title of the competition.');
        $form->addElement($title);

        $slug = new Epr_Form_Element_Slug('slug');
        $slug->setLabel('Slug');
        $slug->setAttrib('class','inp-form');
        $slug->setBaseElementName('title');
		$slug->setDescription('The slug of the competition. It is used to create descriptive URLs on the web page. When no value is given, it is computed from the title.');
        $form->addElement($slug);

        $company = new Zend_Form_Element_Text('company');
        $company->setLabel('Company');
        $company->setRequired(true);
        $company->setAttrib('class','inp-form');
		$company->setDescription('The sponsorship company of the competition.');
        $company->setValue(_app()->getName());
        $form->addElement($company);

        $category = new Zend_Form_Element_Select('category');
        $category->setLabel('Catgegory');
        $category->setMultiOptions($form->getCategoriesSelectArray($categories));
        $category->setRequired(true);
		$category->setDescription('The competition\'s category.');
        $category->setAttrib('class','styledselect_form_1');
        $form->addElement($category);

        $image = new Epr_Form_Element_Image_Selector('image');
        $image->setLabel('Image');
		$image->setDescription('The main image of the competition. It is used to attract the user\'s attention.');
		$image->clearDevicePreviews();
		$image->setAutoResizing(true);
        $form->addElement($image);

        $teaser = new Zend_Form_Element_Textarea('teaser');
        $teaser->setLabel('Teaser Text');
        $teaser->setDescription('A teaser text.');
        $teaser->setAttrib('class', 'inp-form wide');
        $form->addElement($teaser);

        $description = new Zend_Form_Element_Textarea('text');
        $description->setLabel('Description');
        $description->setRequired(true);
		$description->setDescription('A descriptive text which explains what the competition is about.');
        $description->setAttrib('class','wysiwyg wide');
        $form->addElement($description);

        $rules = new Zend_Form_Element_Textarea('rules');
        $rules->setLabel('Rules & Prices');
        $rules->setRequired(true);
		$rules->setDescription('A text to explain the rules and prices.');
        $rules->setAttrib('class','wysiwyg wide');
        $form->addElement($rules);

        $period = new Epr_Form_Element_DateRange('period');
        $period->setLabel('Active period');
        $period->setRequired(true);
		$period->setDescription('The time period of the competition. Users can only submit ideas during this time.');
        $period->setAttrib('class', 'inp-form');
        $form->addElement($period);

        $endedtext = new Zend_Form_Element_Textarea('endText');
        $endedtext->setLabel('Ended Competition Text');
        $endedtext->setDescription('A descriptive text displayed after competition has ended.');
        $endedtext->setAttrib('class','wysiwyg wide');
        $form->addElement($endedtext);

        $isActive = new Zend_Form_Element_Checkbox('isActive');
        $isActive->setChecked(true);
		$isActive->setDescription('Only active competitions are visible.');
        $isActive->setLabel('Is Active');
        $form->addElement($isActive);

        /** @var $competition Epr_Idea_Competition */
        if($competition instanceof Epr_Idea_Competition){

            $title->setValue($competition->getTitle());

            $slug->setValue($competition->getSlug());
            $slug->setDocumentId($competition->getId());

            $company->setValue($competition->getCompany());
            $teaser->setValue($competition->getTeaser());
            $description->setValue($competition->getText());
			$endedtext->setValue($competition->getEndText());
            $rules->setValue($competition->getRules());

            $dateRange = \Poundation\PDateRange::createRange($competition->getStartDate(), $competition->getEndDate());
            $period->setValue($dateRange);

            $category->setValue($competition->getCategory()->getId());
            $isActive->setValue($competition->isActive());
            $image->setValue($competition->getTitleImage());
        }


        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('');
        $submit->setValue('Save');
        $submit->setAttrib('class','form-submit');
        $form->addElement($submit);

        return $form;
    }


    static function getCategoryForm($category = null, $categories = null) {

        $form = new self();

        $name = new Zend_Form_Element_Text('name');
        $name->setLabel('Name');
        $name->setRequired(true);
        $name->setAttrib('class','inp-form');
		$name->setDescription('The name of the category.');
        $form->addElement($name);

        $slug = new Epr_Form_Element_Slug('slug');
        $slug->setLabel('Slug');
        $slug->setAttrib('class','inp-form');
		$slug->setDescription('The slug of the category. It is used to create descriptive URLs on the web page. When no value is given, it is computed from the name.');
        $slug->setBaseElementName('name');

        $form->addElement($slug);

        $image = new Epr_Form_Element_Image_Selector('image');
        $image->setLabel('Icon');
        $image->setContext(Epr_Content_Image::ACCESS_IDEA_CATEGORY | Epr_Content_Image::ACCESS_IDEA_IMAGE);
		$image->setDescription('The icon image of the category.');
        $form->addElement($image);

        $isActive = new Zend_Form_Element_Checkbox('isActive');
        $isActive->setChecked(true);
        $isActive->setLabel('Active');
		$isActive->setDescription('Only competitions in active categories are available.');
        $form->addElement($isActive);


        /** @var $category Epr_Idea_Category */
        if($category instanceof Epr_Idea_Category){
            $name->setValue($category->getTitle());

            $slug->setValue($category->getSlug());
            $slug->setDocumentId($category->getId());

            $isActive->setValue($category->isActive());
            $image->setValue($category->getTitleImage());
        }

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('');
        $submit->setValue('Save');
        $submit->setAttrib('class','form-submit');
        $form->addElement($submit);

        return $form;
    }


    static function getIdeaForm($competitions)
    {
        $form = new self();

        $text = new Zend_Form_Element_Textarea('text');
        $text->setLabel('Text')
            ->setRequired(true)
            ->setDecorators(array('Composite'));

        $url = new Zend_Form_Element_Text('url');
        $url->setLabel('URL')
            ->setRequired(false)
            ->setAttrib('class','inp-form')
            ->setDecorators(array('Composite'));

        $files = new Zend_Form_Element_File('files');
        $files->setLabel('Files (3 Files allowed)')
            ->setMultiFile(3)
            ->setDecorators(array(
            'File',
            'Errors',
            array(array('data' => 'HtmlTag'), array('tag' => 'td')),
            array('Label', array('tag' => 'th')),
            array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
        ));

        $competition = new Zend_Form_Element_Select('competition');
        $competition->setLabel('Competition')
            ->setMultiOptions($form->getCompetitionSelectArray($competitions))
            ->setRequired(true)
            ->setAttrib('class','styledselect_form_1')
            ->setDecorators(array('Composite'));

        $user = new Zend_Form_Element_Hidden('userId');
        $user->setValue(_user()->getId());

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('');
        $submit->setValue('Submit');
        $submit->setAttrib('class','form-submit');
        $submit->setDecorators(array('Composite'));

        $reset = new Zend_Form_Element_Submit('reset');
        $reset->setValue('Reset');
        $reset->setLabel('');
        $reset->setAttrib('class','form-reset');
        $reset->setDecorators(array('Composite'));

        $form->addElements(array($text, $url, $files, $competition, $user, $submit, $reset));

        return $form;
    }

    static function getIdeaFrontendForm($competition)
    {
        $form = new self();

        $text = new Zend_Form_Element_Textarea('text');
        $text->setLabel('Text')
            ->setRequired(true)
        ;

        $form->addElement($text);

        $url = new Zend_Form_Element_Text('url');
        $url->setLabel('URL')
            ->setRequired(false);
        $form->addElement($url);

        $files = new Zend_Form_Element_File('files');
        $files->setLabel('Files (3 Files allowed)')
            ->setMultiFile(3);
        $form->addElement($files);

        $form->getElement('files')->setDecorators(
            array(
                'File',
                'Errors',
                array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                array('Label', array('tag' => 'th')),
                array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
            )
        );




        $user = new Zend_Form_Element_Hidden('userId');
        $user->setValue(_user()->getId());
        $form->addElement($user);

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('');
        $submit->setValue('Submit');
        $submit->setAttrib('class','btn');
        $form->addElement($submit);


        return $form;
    }


}
