<?php

/** @Document (repositoryClass="Epr_Theme_Collection") */
class Epr_Theme extends Epr_Document
{

    const DOCUMENTNAME = 'Epr_Theme';

    /** @Id */
    public $id;

    /** @Field(type="string") */
    private $name;

    /** @Field(type="string") */
    private $title;

    /** @Field(type="color") */
    private $tintColor;

    /** @Field(type="color") */
    private $backgroundColor;

    /** @Field(type="color") */
    private $darkAreaColor;

    /** @Field(type="color") */
    private $lightAreaColor;

    /** @ReferenceOne(targetDocument="Epr_Content_Image") */
    private $logoImage;

    /** @ReferenceOne(targetDocument="Epr_Content_Image") */
    private $lowContrastLogoImage;

    /** @ReferenceOne(targetDocument="Epr_Content_Image") */
    private $faviconImage;

    /** @ReferenceOne(targetDocument="Epr_Content_Image") */
    private $appleTouchIcon;

    /** @ReferenceOne(targetDocument="Epr_Content_Image") */
    private $appleTouchIcon_72;

    /** @ReferenceOne(targetDocument="Epr_Content_Image") */
    private $appleTouchIcon_114;

    /** @ReferenceOne(targetDocument="Epr_Content_Image") */
    private $appleTouchIcon_144;

    /** @ReferenceOne(targetDocument="Epr_Content_Image") */
    private $soldOutImage;

    /** @Field(type="string") */
    private $customHTMLHeader;

    /** @Field(type="string") */
    private $customCSS;

    /** @Field(type="string") */
    private $footerText;

    private $cssObject = null;

    /**
     * Returns the name of the theme.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name of the theme.
     *
     * @param $name string
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = (string)$name;

        return $this;
    }

    /**
     * Returns the title which is uses as HTML title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title which is used as HTML title.
     *
     * @param $title string
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = (string)$title;

        return $this;
    }

    /**
     * Returns the tint color which is used for nearly every coloration.
     *
     * @return \Poundation\PColor|null
     */
    public function getTintColor()
    {
        if (is_null($this->tintColor)) {
            $this->tintColor = \Poundation\PColor::colorFromString('#b73c3c');
        }

        return $this->tintColor;
    }

    /**
     * Sets the tint color which is used for nearly every coloration.
     *
     * @param \Poundation\PColor $color
     *
     * @return $this
     */
    public function setTintColor($color)
    {

        if (is_null($color) || $color instanceof \Poundation\PColor) {
            $this->tintColor = $color;
        }

        return $this;
    }

    /**
     * Sets the background color.
     *
     * @param \Poundation\PColor $color
     *
     * @return $this
     */
    public function setBackgroundColor($color)
    {

        if (is_null($color) || $color instanceof \Poundation\PColor) {
            $this->backgroundColor = $color;
        }

        return $this;
    }

    /**
     * Returns the background color.
     *
     * @return \Poundation\PColor|null
     */
    public function getBackgroundColor()
    {
        if (is_null($this->backgroundColor)) {
            $this->backgroundColor = \Poundation\PColor::colorFromString('#FFFFFF');
        }

        return $this->backgroundColor;
    }

    /**
     * Returns the dark area color which is used for highlighted areas.
     *
     * @return \Poundation\PColor|null
     */
    public function getDarkAreaColor()
    {
        if (is_null($this->darkAreaColor)) {
            $this->darkAreaColor = \Poundation\PColor::colorFromString('#A0A0A0');
        }

        return $this->darkAreaColor;
    }

    /**
     * Sets the dark area color which is used for highlighted areas.
     *
     * @param \Poundation\PColor $color
     *
     * @return $this
     */
    public function setDarkAreaColor($color)
    {

        if (is_null($color) || $color instanceof \Poundation\PColor) {
            $this->darkAreaColor = $color;
        }

        return $this;
    }

    /**
     * Returns the light area color which is used for highlighted areas.
     *
     * @return \Poundation\PColor|null
     */
    public function getLightAreaColor()
    {
        if (is_null($this->lightAreaColor)) {
            $this->lightAreaColor = \Poundation\PColor::colorFromString('#F0F0F0');
        }

        return $this->lightAreaColor;
    }

    /**
     * Sets the light area color which is used for highlighted areas.
     *
     * @param \Poundation\PColor $color
     *
     * @return $this
     */
    public function setLightAreaColor($color)
    {

        if (is_null($color) || $color instanceof \Poundation\PColor) {
            $this->lightAreaColor = $color;
        }

        return $this;
    }

    /**
     * Returns the logo image.
     *
     * @return Epr_Content_Image|null
     */
    public function getLogoImage()
    {
        return $this->logoImage;
    }

    /**
     * Sets the logo image.
     *
     * @param $image
     *
     * @return $this
     */
    public function setLogoImage($image)
    {
        if ($image instanceof Epr_Content_Image || is_null($image)) {
            $this->logoImage = $image;
        }

        return $this;
    }

    /**
     * Sets the logo image.
     *
     * @param $image
     *
     * @return $this
     */
    public function setLowContrastLogoImage($image)
    {
        if ($image instanceof Epr_Content_Image || is_null($image)) {
            $this->lowContrastLogoImage = $image;
        }

        return $this;
    }

    /**
     * Returns the logo image.
     *
     * @return Epr_Content_Image|null
     */
    public function getLowContrastLogoImage()
    {
        return $this->lowContrastLogoImage;
    }


    /**
     * Returns the theme's favicon image.
     * @return Epr_Content_Image|null
     */
    public function getFaviconImage()
    {
        return $this->faviconImage;
    }

    /**
     * Sets the theme's favicon image.
     * @param $image
     *
     * @return $this
     */
    public function setFaviconImage($image)
    {
        if ($image instanceof Epr_Content_Image || is_null($image)) {
            $this->faviconImage = $image;
        }

        return $this;
    }

    /**
     * Returns the apple touch icon with 57x57.
     * @return Epr_Content_Image|null
     */
    public function getAppleTouchIcon()
    {
        return $this->appleTouchIcon;
    }

    /**
     * Sets the 57x57 apple touch icon.
     * @param Epr_Content_Image|null $image
     * @return $this
     */
    public function setAppleTouchIcon($image)
    {
        if ($image instanceof Epr_Content_Image || is_null($image)) {
            $this->appleTouchIcon = $image;
        }

        return $this;
    }

    /**
     * Returns the apple touch icon with 72x72.
     * @return Epr_Content_Image|null
     */
    public function getAppleTouchIcon72()
    {
        return $this->appleTouchIcon_72;
    }

    /**
     * Sets the 72x72 apple touch icon.
     * @param Epr_Content_Image|null $image
     * @return $this
     */
    public function setAppleTouchIcon72($image)
    {
        if ($image instanceof Epr_Content_Image || is_null($image)) {
            $this->appleTouchIcon_72 = $image;
        }

        return $this;
    }


    /**
     * Returns the apple touch icon with 114x114.
     * @return Epr_Content_Image|null
     */
    public function getAppleTouchIcon114()
    {
        return $this->appleTouchIcon_114;
    }

    /**
     * Sets the 114x114 apple touch icon.
     * @param Epr_Content_Image|null $image
     * @return $this
     */
    public function setAppleTouchIcon114($image)
    {
        if ($image instanceof Epr_Content_Image || is_null($image)) {
            $this->appleTouchIcon_114 = $image;
        }

        return $this;
    }

    /**
     * Returns the apple touch icon with 144x144.
     * @return Epr_Content_Image|null
     */
    public function getAppleTouchIcon144()
    {
        return $this->appleTouchIcon_144;
    }

    /**
     * Sets the 144x144 apple touch icon.
     * @param Epr_Content_Image|null $image
     * @return $this
     */
    public function setAppleTouchIcon144($image)
    {
        if ($image instanceof Epr_Content_Image || is_null($image)) {
            $this->appleTouchIcon_144 = $image;
        }

        return $this;
    }

    /**
     * @return Epr_Content_Image|null
     */
    public function getSoldOutImage() {
        return $this->soldOutImage;
    }

    /**
     * @param $image
     * @return $this
     */
    public function setSoldOutImage($image) {
        if ($image instanceof Epr_Content_Image || is_null($image)) {
            $this->soldOutImage = $image;
        }
        return $this;
    }


    /**
     * Returns the theme's custom HTML header text.
     *
     * @return string
     */
    public function getCustomHTMLHeader()
    {
        return $this->customHTMLHeader;
    }

    /**
     * Sets theme's custom HTML header text.
     *
     * @param $text string
     *
     * @return $this
     */
    public function setCustomHTMLHeader($text)
    {
        $this->customHTMLHeader = $text;

        return $this;
    }

    /**
     * Returns the theme's custom CSS text.
     *
     * @return string
     */
    public function getCustomCSS()
    {
        return $this->customCSS;
    }

    /**
     * Sets the theme's custom CSS text.
     *
     * @param $text string
     *
     * @return $this
     */
    public function setCustomCSS($text)
    {
        $this->customCSS = $text;

        return $this;
    }

    public function getFooterText()
    {
        return $this->footerText;
    }

    /**
     * Sets the footer text.
     * @param $text
     *
     * @return $this
     */
    public function setFooterText($text)
    {
        $this->footerText = (string)$text;
        return $this;
    }

    /**
     * Returns the effective footer text where variables have been substituted.
     * @return string
     */
    public function getEffectiveFooterText()
    {
        return (string)__($this->getFooterText())->replace('%COPY%', '&copy;')->replace('%YEAR%', date('Y'));
    }

    /**
     * @return null|\Poundation\PCSS
     */
    public function getCSS()
    {
        if (is_null($this->cssObject)) {

            $invertTintColor = ($this->getTintColor()->isDarkColor()) ? \Poundation\PColor::colorFromString(
                '#FFFFFF'
            ) : \Poundation\PColor::colorFromString('#000000');
            $lightFontColor = ($this->getLightAreaColor()->isDarkColor()) ? \Poundation\PColor::colorFromString(
                '#F0F0F0'
            ) : \Poundation\PColor::colorFromString('#606060');

            $this->cssObject = new \Poundation\PCSS();

            $body = $this->cssObject->addNewSelector('body');
            $body->addPropertyAndValue('background-color', $this->getBackgroundColor());

            $tint = $this->cssObject->addNewSelector('.tint');
            $tint->addPropertyAndValue('color', $this->getTintColor());

            $headlines = $this->cssObject->addNewSelector('h1, h2, h3, h4, h5, h1 a, h2 a, h3 a, h4 a, h5 a');
            $headlines->addPropertyAndValue('color', '#333333');

            $h2 = $this->cssObject->addNewSelector('h2');
            $h2->addPropertyAndValue('background-color', $this->getLightAreaColor());
            $h2->addPropertyAndValue('color', $lightFontColor);

            if ($this->getLowContrastLogoImage()) {
                $hBrand = $this->cssObject->addNewSelector(
                    'h1.branding, h2.branding, h3.branding, h4.branding, h5.branding'
                );
                $hBrand->addPropertyAndValue(
                    'background-image',
                    'url(' . $this->getLowContrastLogoImage()->getPublicIDBasedURL() . ')'
                );
            }

            $btnHoover = $this->cssObject->addNewSelector('.btn:hover');
            $btnHoover->addProperty(\Poundation\PCSS_Property::importantPropertyWithNameAndValue('background-color', $this->getDarkAreaColor()));

            $this->cssObject->addNewSelector('.btn-primary.btn-disabled:hover')->addPropertyAndValue('background-color', $this->getTintColor());

            $links = $this->cssObject->addNewSelector('a, .category-menu a:hover');
            $links->addPropertyAndValue('color', $this->getTintColor());
            $linksHover = $this->cssObject->addNewSelector('a:hover');
            $linksHover->addPropertyAndValue('color', $this->getDarkAreaColor());


            $tintBackground = $this->cssObject->addNewSelector('.tintBackground');
            $tintBackground->addPropertyAndValue('background-color', $this->getTintColor());

            $navbar = $this->cssObject->addNewSelector('.navbar.main-menu, .navbar-footer');
            $navbar->addPropertyAndValue('background-color', $this->getLightAreaColor());

            $navbarColor = ($this->getLightAreaColor()->isDarkColor()) ? \Poundation\PColor::colorFromString(
                '#FFFFFF'
            ) : \Poundation\PColor::colorFromString('#000000');

            $navbar->addPropertyAndValue('color', $navbarColor);

            $brand = $this->cssObject->addNewSelector(
                '.navbar .brand, .brand, .navbar .brand, .navbar .nav > li > a, .navbar-link'
            );
            $brand->addPropertyAndValue('color', $navbarColor);

            $this->cssObject->addNewSelector('.navbar-brand .navbar-toggle')->setBackgroundColor(
                $this->getDarkAreaColor()
            );
            $this->cssObject->addNewSelector('.navbar-brand .icon-bar')->setBackgroundColor($this->getLightAreaColor());

            $menuActive = $this->cssObject->addNewSelector('.navbar .nav .active > a, .navbar .nav .active > a:focus');
            $menuActive->addPropertyAndValue('color', $navbarColor);


            $menuHoover = $this->cssObject->addNewSelector('.navbar .nav .inactive > a:hover, .navbar-link:hover');

            $dropDown = $this->cssObject->addNewSelector('.dropdown-menu > li > a:hover');
            $dropDown->addPropertyAndValue('background-color', $this->getTintColor());

            if ($this->getTintColor()->isDarkColor()) {
                $brand->addPropertyAndValue('text-shadow', '0 -1px ' . $navbarColor->darkerColor(0.7));
            } else {
                $brand->addPropertyAndValue('text-shadow', '0 -1px ' . $navbarColor->lighterColor(0.7));
            }

            $caret = $this->cssObject->addNewSelector('.nav .caret');
            $caret->addPropertyAndValue('border-top-color', $navbarColor);
            $caret->addPropertyAndValue('border-bottom-color', $navbarColor);

            $heroUnit = $this->cssObject->addNewSelector('.hero-unit, .jumbotron');
            $heroUnit->addPropertyAndValue('background-color', $this->getTintColor());
            $heroUnit->addPropertyAndValue('color', $invertTintColor);

            $thumbnail = $this->cssObject->addNewSelector('a.thumbnail, a.thumbnail');
            $thumbnail->addPropertyAndValue('border-color', $this->getLightAreaColor());
            $thumbnailHover = $this->cssObject->addNewSelector(
                'a.thumbnail:hover, a.thumbnail:focus, .thumbnail img:hover'
            );
            $thumbnailHover->addPropertyAndValue('border-color', $this->getTintColor());

            $footerLinks = $this->cssObject->addNewSelector('footer a');
            $footerLinks->addPropertyAndValue('color', $navbarColor);

            $panelsHeadings = $this->cssObject->addNewSelector(
                '.panel-heading, a.list-group-item.active, .list-group-item.active:hover'
            );
            $panelsHeadings->setBackgroundColor($this->getTintColor(), true);
            $panelsHeadings->setBorderColor($this->getLightAreaColor(), true);

            $panelsTitles = $this->cssObject->addNewSelector('.panel-title');
            $panelsTitles->addPropertyAndValue('color', $invertTintColor);

            $listGroupItem = $this->cssObject->addNewSelector('.list-group-item, a.list-group-item');
            $listGroupItem->addPropertyAndValue('color', '#333333');

            $listGroupHover = $this->cssObject->addNewSelector('a.list-group-item:hover, a.list-group-item:focus');
            $listGroupHover->addPropertyAndValue('background-color', $this->getLightAreaColor());


            $primaryButton = $this->cssObject->addNewSelector('.btn-primary');
            $primaryButton->addPropertyAndValue('background-color', $this->getTintColor());
            $primaryButton->addPropertyAndValue('border-color', $this->getTintColor());

            $mediaListFirstElement = $this->cssObject->addNewSelector('.media.first-element');
            $mediaListFirstElement->addPropertyAndValue('background-color', $this->getLightAreaColor());
            $mediaListFirstElement->addPropertyAndValue('color', $lightFontColor);

            $mediaListNotFirstElement = $this->cssObject->addNewSelector('.media.not-first-element');
            $mediaListNotFirstElement->addPropertyAndValue('border-bottom', '1px solid ' . $this->getLightAreaColor());

            $mediaListInformation = $this->cssObject->addNewSelector('.media .info');
            $mediaListInformation->addPropertyAndValue('color', $lightFontColor);
            $mediaListInformationSmall = $this->cssObject->addNewSelector('.media .info.small');
            $mediaListInformationSmall->addPropertyAndValue('color', $lightFontColor->lighterColor(0.2));

            $priceElement = $this->cssObject->addNewSelector('.price');
            $priceElement->addPropertyAndValue('color', $this->getTintColor());



        }

        return $this->cssObject;
    }

}