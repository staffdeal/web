<?php

use Doctrine\ODM\CouchDB\DocumentRepository;

class Epr_Token_Collection extends Epr_Collection implements Epr_Paginatable {

    const SORT_DESIGN_DOCUMENT_NAME = 'sortTokens';

    static function getSortKeys()
    {
        return array(
            'creationDate',
            'lastUsageDate',
            'tokenType'
        );
    }

	/**
	 * @return \Doctrine\ODM\CouchDB\DocumentRepository
	 */
	static function getRepository() {
		return _dm()->getRepository(Epr_Token::DOCUMENTNAME);
	}

	static function getTokensForUser(Epr_User $user) {

		$query = _dm()->createQuery('auth', 'tokensByUserId')->onlyDocs(true);
		$query->setKey($user->getId());

		$result = $query->execute();
		return $result;
	}

	static function getAllTokens() {
		$result = self::getRepository()->findAll();
		return $result;
	}

	static public function getTokenWithDeviceToken($deviceToken) {

		$query = _dm()->createQuery('notifications', 'tokensByDeviceToken')->onlyDocs(true);
		$query->setKey((string)$deviceToken);

		$result = $query->execute();
		return (count($result) > 0) ? $result[0] : null;
	}

	/**
	 * @param $id
	 *
	 * @return Epr_Token|null
	 */
	static public function getTokenWithId($id) {

		$result = _dm()->find(Epr_Token::DOCUMENTNAME, $id);
		return $result;

	}

	static public function getTokenWithType($type) {

		$query = _dm()->createQuery('auth', 'tokensByType')->onlyDocs(true);
		$query->setKey((string)$type);

		$result = $query->execute();
		return $result;

	}
	
}

?>