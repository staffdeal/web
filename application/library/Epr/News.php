<?php
/**
 * meinERP
 * User: Jan Fanslau
 * Date: 14.09.12
 * Time: 12:26
 */
/** @Document (repositoryClass="Epr_News_Collection") */
class Epr_News extends Epr_Document_WebPage implements Epr_Categorizable, Epr_Document_Publishable
{
	use Epr_Document_PublishableTrait;

	const DOCUMENTNAME = 'Epr_News';

	/** @Id */
	protected $id;

	/** @Field(type="datetime") */
	protected $publishDate;

	/** @ReferenceOne(targetDocument="Epr_News_Category") */
	protected $category;

	/** @Field(type="url") */
	protected $publicUrl;

	/** @Field(type="string") */
	protected $source;

    /** @Field(type="integer") */
    protected $hitsCounter;

	/** @Field(type="integer") */
	protected $viewCounter;


	/**
	 * Loads the news with the id.
	 * @param $id
	 *
	 * @return Epr_News
	 */
	static public function getNewsWithId($id) {
		return _dm()->find(self::DOCUMENTNAME, $id);
	}

	/**
	 * Sets the category of the news.
	 *
	 * @param Epr_News_Category $category
	 *
	 * @return Epr_News
	 */
	public function setCategory($category)
	{
		if ($category instanceof Epr_News_Category || is_null($category)) {
			$this->category = $category;
		}

		return $this;
	}

	/**
	 * Removes the category.
	 *
	 * @return Epr_News
	 */
	public function removeCategory()
	{
		$this->category = null;

		return $this;
	}

	/**
	 * Returns the category of the news item.
	 *
	 * @return Epr_News_Category
	 */
	public function getCategory()
	{
		if (!$this->category instanceof Epr_News_Category) {
			$category = new Epr_News_Category();
			$category->setTitle('Uncategorized');
			$category->activate();
			return $category;
		}

		return $this->category;
	}

	public function getCategoryPathArray() {

		$path = array();

		if ($this->category instanceof Epr_News_Category) {

			$parent = $this->category;
			while($parent instanceof Epr_News_Category) {
				$path[] = $parent;
				$parent = $parent->getCategory();
			}
		}

		return array_reverse($path);
	}

	/**
	 * Sets the public URL.
	 *
	 * @param Poundation\PURL $publicUrl
	 *
	 * @return Epr_News
	 */
	public function setPublicUrl($publicUrl)
	{
		if ($publicUrl  instanceof \Poundation\PURL || is_null($publicUrl)) {
			$this->publicUrl = $publicUrl;
		}

		return $this;
	}

	/**
	 * Removes the public url.
	 *
	 * @return Epr_News
	 */
	public function removePublicUrl()
	{
		$this->publicUrl = null;

		return $this;
	}

	/**
	 * Returns the URL.
	 *
	 * @return Poundation\PURL
	 */
	public function getPublicUrl()
	{
		return $this->publicUrl;
	}


	/**
	 * @param DateTime $publishDate
	 * @deprecated
	 */
	public function setPublishDate(DateTime $publishDate)
	{
		$this->publishDate = $publishDate;
	}

	/**
	 * Returns the publish date
	 *
	 * @deprecated
	 * @return DateTime
	 */
	public function getPublishDate()
	{
		return $this->publishDate;
	}

	/**
	 * Returns the source.
	 *
	 * @return string
	 */
	public function getSource()
	{
		return $this->source;
	}

	/**
	 * Sets the source.
	 *
	 * @param $source  string
	 *
	 * @return $this
	 */
	public function setSource($source)
	{
		$this->source = $source;

		return $this;
	}

    /**
     * Returns the count of hits.
     * @return integer
     */
    public function getHitsCounter() {
        return $this->hitsCounter;
    }

	/**
	 * Returns the count of views.
	 * @return integer
	 */
	public function getViewCounter() {
		return $this->viewCounter;
	}

    /**
     * Registers a hit.
     * @return $this
     */
    public function registerHit() {
        $this->hitsCounter++;
        return $this;
    }

	/**
	 * Registers a view.
	 * @return $this
	 */
	public function registerView($count = 1) {
        $this->viewCounter = $this->viewCounter + max(1,((int)$count));

		return $this;
	}

	public function allowAutomaticPublishing()
	{
		return true;
	}

	public function sendNotifications() {

		$count = 0;

		$notificationsModule = _app()->getModuleWithIdentifier(Epr_Modules_Notifications::identifier());
		if ($notificationsModule instanceof Epr_Modules_Notifications) {

			$notifications = $notificationsModule->createNotificationsForContext('news:' . $this->getId(), $this->getTitle());

			if (count($notifications) > 0) {

				$queue = Epr_Notification_Queue::getQueue();

				foreach ($notifications as $notification) {
					if ($notification instanceof Epr_Notification) {
						$notification->setReason('news:' . $this->getId());
					}
					$queue->addNotification($notification);
				}
			}

			$count = count($notifications);

		}

		return $count;
	}


}

