<?php

/** @Document (repositoryClass="Epr_User_Collection") */
abstract class Epr_Notification_Abstract extends Epr_Document implements Epr_Notification
{

    /** @Id */
    protected $id;

    /** @Field(type="datetime") */
    protected $creationDate;

    /** @Field(type="string") */
    private $parent_type = 'Epr_Notification';

    /** @Field(type="string") */
    private $context;

    /** @Field(type="integer") */
    protected $failureCount = 0;

    /**
     * @Index
     * @Field(type="integer")
     */
    private $status;

    /** @Field(type="string") */
    protected $deviceId;

    /** @Field(type="string") */
    protected $receiverName;

    /** @Field(type="string") */
    protected $sender;

    /** @Field(type="datetime") */
    private $sentDate;

    /** @Field(type="string") */
    private $message;

    /** @Field(type="string") */
    private $transportId;

    /** @Field(type="string") */
    private $transportMessage;

    /** @Field(type="string") */
    private $reason;

    public function __construct($deviceId, $message, $sender, $context = null, $reason = null)
    {
        parent::__construct();

        $this->deviceId = $deviceId;
        $this->sender   = $sender;
        $this->message  = $message;
        $this->context  = $context;
        $this->reason   = $reason;
        $this->setTransportId(time());
        $this->status = self::STATUS_EDITING;
    }

    public function isValid()
    {
        return false;
    }

    public function getDeviceId()
    {
        return $this->deviceId;
    }

    public function getType()
    {
        if ($this instanceof Epr_Notification_APN) {
            return Epr_Notification::TYPE_IOS_APN;
        } else if ($this instanceof Epr_Notification_GCM) {
            return Epr_Notification::TYPE_ANDROID_GCM;
        }
    }

    public function getContext()
    {
        return $this->context;
    }


    public function getReceiverName()
    {
        return $this->receiverName;
    }

    public function setReceiverName($name)
    {
        $this->receiverName = (string)$name;

        return $this;
    }

    public function getSender()
    {
        return $this->sender;
    }

    public function getSentDate()
    {
        return $this->sentDate;
    }

    public function getStatus()
    {
        return $this->status;
    }

    protected function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getTransportId()
    {
        return $this->transportId;
    }

    protected function setTransportId($id)
    {
        $this->transportId = $id;

        return $this;
    }

    public function getTransportMessage() {
        return $this->transportMessage;
    }

    protected function setTransportMessage($message) {
        $this->transportMessage = (string)$message;
        return $this;
    }

    protected function addTransportMessage($message) {
        if (!is_string($this->transportMessage)) {
            $this->transportMessage = '';
        }

        $this->transportMessage.= $message;
        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function endEditing()
    {
        $this->setStatus(Epr_Notification::STATUS_PENDING);

        return true;

    }

    protected function sendSucceeded()
    {
        $this->sentDate = new DateTime();
        $this->setStatus(Epr_Notification::STATUS_SENT);

        return true;
    }

    protected function sendFailed($message = null)
    {

        $this->sentDate = null;

        if ($this->failureCount++ >= Epr_Notification::MAX_FAILURE_COUNT) {
            $this->setStatus(Epr_Notification::STATUS_FAILED);
        } else {
            $this->setStatus(Epr_Notification::STATUS_PENDING);
        }

        $errorMessage = (string)$this->failureCount;
        if ($message) {
            $errorMessage.= ' ' . $message;
        } else {
            $errorMessage.= ' -';
        }

        $this->addTransportMessage($errorMessage . "\n");

        return false;
    }

    public function cancel() {
        $this->setStatus(Epr_Notification::STATUS_FAILED);
        return $this;
    }

    public function getFailureCounter()
    {
        return $this->failureCount;
    }

    public function getReason()
    {
        return $this->reason;
    }

    public function setReason($reason)
    {
        $this->reason = (string)$reason;

        return $this;
    }


    public function send()
    {

    }

    public function queue()
    {

    }

}