<?php

use Doctrine\ODM\CouchDB\DocumentRepository;
use Doctrine\CouchDB\View\FolderDesignDocument;

class Epr_Notification_Collection extends \Doctrine\ODM\CouchDB\DocumentRepository
{

	static function getAllNotifications() {
		return self::getNotificationsByStatuses(array(
													 Epr_Notification::STATUS_EDITING,
													 Epr_Notification::STATUS_PENDING,
													 Epr_Notification::STATUS_SENDING,
													 Epr_Notification::STATUS_SENT,
													 Epr_Notification::STATUS_FAILED
												));
	}

	static function getNotificationsByStatuses($values) {

		$query = _dm()->createQuery('notifications', 'notificationsByStatus')->onlyDocs(true)->setDescending(true);
		$query->setKeys(array_values($values));

		$result = $query->execute();

		return $result;

	}

	static function getNotificationsByStatus($status)
	{

		$query = _dm()->createQuery('notifications', 'notificationsByStatus')->onlyDocs(true);
		$query->setKey($status);

		$result = $query->execute();

		return $result;
	}

	static function getNotificationsWithContext($context) {

		$query = _dm()->createQuery('notifications', 'notificationsByContext')->onlyDocs(true);
		$query->setKey($context);

		$result = $query->execute();

		return $result;

	}

    static function getNotificationWithId($id) {
        $query = _dm()->createQuery('notifications','notificationById')->onlyDocs(true);
        $query->setKey($id);

        $result = $query->execute();

        return (count($result) > 0) ? $result[0] : null;
    }
}
