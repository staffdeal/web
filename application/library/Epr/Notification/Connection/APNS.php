<?php

class Epr_Notification_Connection_APNS extends Zend_Mobile_Push_Apns {

	public function __construct($certificateString = null, $certificatePassphrase = null) {

		if (is_string($certificateString)) {
			$this->setCertificateString($certificateString);
		}

		if (is_string($certificatePassphrase)) {
			$this->setCertificatePassphrase($certificatePassphrase);
		}

	}

	public function setCertificateString($cert) {
		$this->_certificate = (string)$cert;
	}

}