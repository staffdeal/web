<?php

/** @Document */
class Epr_Notification_GCM extends Epr_Notification_Abstract
{

    static $GCM = null;

    /**
     * @param Epr_Token $receiver
     * @param $message
     * @param null $sender
     * @param null $context
     * @return Epr_Notification_GCM|null
     */
    public static function createMessage(Epr_Token $receiver, $message, $sender = null, $context = null)
    {

        $notification = null;

        if (is_null($sender)) {
            $sender = _app()->getName();
        }
        $notification = new self($receiver->getDeviceToken(), $message, $sender, $context);

        return $notification;
    }

    public function isValid()
    {
        return (is_string($this->deviceId) && strlen($this->deviceId) > 10);
    }

    public function send()
    {
        if ($this->isValid()) {

            $message = new Zend_Mobile_Push_Message_Gcm();

            $payload = array('message' => $this->getMessage());
            if ($this->getReason()) {
                $payload['reason'] = $this->getReason();
            }

            $message->setData($payload);
            $message->setToken($this->getDeviceId());
            $message->setId($this->getTransportId());

            if ($this->GCM() instanceof Zend_Mobile_Push_Gcm) {
                if (!$this->GCM()->isConnected()) {
                    _logger()->critical('GCM Service not connected.');
                } else {
                    try {
                        $this->setStatus(Epr_Notification::STATUS_SENDING);
                        if ($this->GCM()->send($message)) {
                            return $this->sendSucceeded();
                        } else {
                            return $this->sendFailed();
                        }

                    } catch (Exception $e) {
                        _logger()->critical('GCM: ' . $e->getMessage());
                        $this->setStatus(self::STATUS_FAILED);
                    }
                }
            }
        }

        return false;
    }

    /**
     * @return null|Zend_Mobile_Push_Gcm
     */
    public function GCM()
    {
        if (is_null(self::$GCM)) {

            $module = _app()->getModuleWithIdentifier(Epr_Modules_GCM::identifier());
            if ($module instanceof Epr_Modules_GCM) {

                if ($module->isUsable()) {
                    self::$GCM = new Zend_Mobile_Push_Gcm();
                    self::$GCM->setApiKey($module->getApiKey());

                    try {
                        self::$GCM->connect();
                    } catch (Exception $e) {
                        _logger()->critical('GCM: ' . $e->getMessage());
                    }
                }
            }
        }

        return self::$GCM;
    }


}