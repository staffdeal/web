<?php

class Epr_Notification_Queue extends \Poundation\PObject
{

	private static $instance;

	private $loaded = array();
	private $notifications = array();

	/**
	 * Returns the static notification queue.
	 *
	 * @return Epr_Notification_Queue
	 */
	static public function getQueue()
	{

		if (!self::$instance instanceof Epr_Notification_Queue) {

			self::$instance = new Epr_Notification_Queue();

		}

		return self::$instance;
	}

	public function __construct()
	{

		$this->notifications[Epr_Notification::STATUS_EDITING] = array();
		$this->loaded[Epr_Notification::STATUS_EDITING]        = false;

		$this->notifications[Epr_Notification::STATUS_PENDING] = array();
		$this->loaded[Epr_Notification::STATUS_PENDING]        = false;

		$this->notifications[Epr_Notification::STATUS_SENDING] = array();
		$this->loaded[Epr_Notification::STATUS_SENDING]        = false;

		$this->notifications[Epr_Notification::STATUS_SENT] = array();
		$this->loaded[Epr_Notification::STATUS_SENT]        = false;

		$this->notifications[Epr_Notification::STATUS_FAILED] = array();
		$this->loaded[Epr_Notification::STATUS_FAILED]        = false;

	}

	/**
	 * Returns an array of notifications with the given status.
	 * @param $status
	 *
	 * @return array
	 */
	public function getNotifications($status = Epr_Notification::STATUS_EDITING)
	{

		if ($this->loaded[$status] == false) {

			$this->loaded[$status] = true;

			$loadedNotifications = Epr_Notification_Collection::getNotificationsByStatus($status);
			foreach ($loadedNotifications as $notification) {
				if ($notification instanceof Epr_Notification) {
					$this->notifications[$status][$notification->getId()] = $notification;
				}
			}
		}

		return $this->notifications[$status];
	}

	/**
	 * @param Epr_Notification $notification
	 *
	 * @return bool
	 */
	public function addNotification(Epr_Notification $notification)
	{

		if ($notification->getStatus() != Epr_Notification::STATUS_EDITING) {
			throw new Exception('You cannot queue a notification which is not in editing state.');
		}

		if ($notification->endEditing()) {
			_dm()->persist($notification);
			$this->notifications[Epr_Notification::STATUS_PENDING][] = $notification;

			return true;
		} else {
			return false;
		}

	}

}