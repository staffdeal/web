<?php

/** @Document */
class Epr_Notification_APN extends Epr_Notification_Abstract
{

    static $APNS = null;

    /**
     * @param Epr_Token $receiver
     * @param $message
     * @param null $sender
     * @param null $context
     * @return Epr_Notification_APN|null
     */
    public static function createMessage(Epr_Token $receiver, $message, $sender = null, $context = null)
    {

        $notification = null;

        if (is_null($sender)) {
            $sender = _app()->getName();
        }
        $notification = new Epr_Notification_APN($receiver->getDeviceToken(), $message, $sender, $context);

        return $notification;
    }

    public function isValid()
    {
        return (is_string($this->deviceId) && strlen($this->deviceId) > 10);
    }

    public function send()
    {
        if ($this->isValid()) {

            $this->setStatus(Epr_Notification::STATUS_SENDING);

            $message = new Zend_Mobile_Push_Message_Apns();
            $message->setAlert($this->getMessage());
            $message->setToken($this->getDeviceId());
            $message->setId($this->getTransportId());
            $message->setCustomData(
                array(
                    'reason' => $this->getReason()
                )
            );

            if ($this->APNS() instanceof Zend_Mobile_Push_Apns) {

                if (!$this->APNS()->isConnected()) {
                    _logger()->critical('APN Service not connected.');
                } else {
                    try {
                        if ($this->APNS()->send($message)) {
                            return $this->sendSucceeded();
                        } else {
                            return $this->sendFailed();
                        }

                    } catch (Exception $e) {


                    }
                }
            } else {
                $this->setStatus(Epr_Notification::STATUS_FAILED);
            }

        } else {
            $this->setStatus(Epr_Notification::STATUS_FAILED);
        }

        return false;
    }

    public function APNS()
    {
        if (is_null(self::$APNS)) {

            $module = _app()->getModuleWithIdentifier(Epr_Modules_APNS::identifier());
            if ($module instanceof Epr_Modules_APNS) {

                if ($module->isUsable()) {

                    self::$APNS = new Zend_Mobile_Push_Apns();
                    self::$APNS->setCertificate($module->getCertificateFilename($module->getMode()));
                    self::$APNS->setCertificatePassphrase($module->getCertificatePassword($module->getMode()));

                    $mode = ($module->getMode() == Epr_Modules_APNS::MODE_PRODUCTION) ? Epr_Notification_Connection_APNS::SERVER_PRODUCTION_URI : Epr_Notification_Connection_APNS::SERVER_SANDBOX_URI;
                    try {
                        self::$APNS->connect($mode);
                    } catch (Exception $e) {
                        _logger()->critical('APNS: ' . $e->getMessage());
                    }
                }
            }
        }

        return self::$APNS;
    }

}