<?php

interface Epr_Categorizable {

	public function getCategory();
	public function setCategory($category);

}